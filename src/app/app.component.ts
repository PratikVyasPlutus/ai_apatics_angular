import { SessionService } from './services/session.service';
import { Component } from '@angular/core';
import '../app/rxjs.operators';
import { Router, NavigationEnd } from "@angular/router";
import { BroadcastService } from "./services/broadcast.service";
import { Global } from "app/constants/global";
import { GlobalEvents } from 'app/constants/global.events';

@Component({
    selector: 'body',
    //template: '<router-outlet></router-outlet>'
    templateUrl: './app.component.html',
    //styleUrls: ['./app.component.css'],
    providers: [SessionService]
})
export class AppComponent {
    staticRoot = Global.staticRoot;
    loggedIn: boolean = false;

    constructor(private _sessionService: SessionService,
        private router: Router,
        private _broadcastService: BroadcastService) {

        this.listenToBroadcast();
        this.checkUserSession();
    }

    checkUserSession() {
        this._sessionService.getUserSession().subscribe(
            res => {
                this.loggedIn = res ? true : false;
            },
            err => {
                console.log('Error while retrieving session data.')
            }
        )
    }

    goToHome() {
        this._sessionService.getUserSession().subscribe(
            res => {
                if (res) {
                    this.router.navigate(['/home']);
                } else {
                    this.router.navigate(['/login']);
                }
            },
            err => {
                console.log('Error while retrieving session data.')
            }
        )
    }

    logOut() {
        this._sessionService.clearUserSession().subscribe(
            res => {
                this.loggedIn = false;
                this.router.navigate(['/login']);
            },
            err => {
                console.log('Error while clearing session data.')
            }
        )
    }

    logIn() {
        this.router.navigate(['/login']);
    }

    listenToBroadcast() {
        this._broadcastService.on(GlobalEvents.LOGIN_UPDATE).subscribe(
            (evData: any) => {
                if (evData.loggedIn) {
                    this.loggedIn = evData.loggedIn;
                }
            }
        );
    }
}
