import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule, LocationStrategy, HashLocationStrategy, DatePipe } from '@angular/common';
import { HttpModule, JsonpModule } from '@angular/http';

import { GlobalClass } from "./services/global"
import { HttpClient } from "./services/http.client"
import { FDAService } from "./services/fda.service"
import { HttpSOLRClient } from "./services/http-solr.client"
import { SOLRService } from "./services/solr.service"
import { ScoutService } from './services/scout.service';
import { HttpScoutClient } from "./services/http-scout.client"
import { TwitterService } from "./services/twitter.service"

import { AppComponent } from './app.component';
import { Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';
import { SearchFacets } from './pipes/facets-search.pipe';
import { SelectedFacets } from './pipes/facets-selected.pipe';
import { HighlightPipe } from './pipes/highlight.pipe';
import { DateUTCPipe } from './pipes/dateUTC.pipe';

import { Ng2PaginationModule } from 'ng2-pagination';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';
import { NouisliderModule } from 'ng2-nouislider';
import { PdfViewerComponent } from 'ng2-pdf-viewer';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { ModalModule } from "ngx-modal";
import { ModalModule as TempModule } from 'ng2-bootstrap/modal';
import { SelectModule } from 'ng2-select';
// Routing Module
import { AppRoutingModule } from './app.routing';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import { HeaderLayoutComponent } from './layouts/header-layout.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { FDAComponent } from './pages/fda/fda.component';
import { DCAComponent } from './pages/dca/dca.component';
import { ESIComponent } from './pages/esi/esi.component';
import { ICD9Component } from './pages/icd9/icd9.component';
import { RevenueCodeComponent } from './pages/revenuecode/revenuecode.component';
import { SecretaryOfStateComponent } from './pages/secretaryofstate/secretaryofstate.component';
import { NICBComponent } from './pages/nicb/nicb.component';
import { MBCOIGComponent } from './pages/mbcoig/mbcoig.component';
import { ChiropractorComponent } from './pages/chiropractor/chiropractor.component';
import { DNBComponent } from './pages/dnb/dnb.component';
import { GDComponent } from './pages/gd/gd.component';
import { ProviderDashboardComponent } from './pages/providerdashboard/providerdashboard.component';
import { ProviderOutcomeComponent } from './pages/providerOutcome/providerOutcome.component';
import { ProviderReportsComponent } from './pages/providerReports/providerReports.component';
import { ProviderMapComponent } from './pages/providerMap/providerMap.component';
import { OpioidComponent } from './pages/Opioid/Opioid.component';
import { ClaimSegmentationComponent } from './pages/ClaimSegmentation/ClaimSegmentation.component';
import { SocialNetworksClaimantComponent } from './pages/socialNetworksClaimant/socialNetworksClaimant.component'
import { SocialNetworksProviderComponent } from './pages/socialNetworksProvider/socialNetworksProvider.component';
import { DuplicateBillingComponent } from './pages/duplicatebilling/duplicatebilling.component';


import { DropdownModule } from 'ng2-bootstrap/dropdown';
import { TabsModule } from 'ng2-bootstrap/tabs';
import { AuthGuard } from './guards/index';
//import { AuthGuard } from './guards/index';
import { LoginComponent } from './pages/login/login.component';
// import {LoginComponent} from './components/login/login.component';
import { Commonutil } from "app/utils/common-util";
import { BroadcastService } from "app/services/broadcast.service";

import { HeaderComponent } from './layouts/header.component';
import { HeaderDefaultComponent } from "./layouts/header-default.component";
import { NvD3Module } from 'ng2-nvd3';

// import { Tabset } from "./shared/tabs/tabset.component";
// import { Tab } from "./shared/tabs/tab.directive";
// import {NgTransclude} from './shared/tabs/utils';
// import {TabHeading} from './shared/tabs/tab-heading.directive'

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        Ng2BootstrapModule,
        Ng2PaginationModule,
        DropdownModule.forRoot(),
        TabsModule.forRoot(),
        ChartsModule,
        NvD3Module,
        NouisliderModule,
        HttpModule, JsonpModule,
        ModalModule,
        SelectModule,
        TempModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAB_dbNjPKgAtscBsOOCwuB8FTFO5szjV0'
        })
    ],
    declarations: [
        AppComponent,
        FullLayoutComponent, SimpleLayoutComponent, HeaderLayoutComponent,
        ProviderDashboardComponent, DuplicateBillingComponent,
        ProviderOutcomeComponent, ProviderReportsComponent, ProviderMapComponent, OpioidComponent, ClaimSegmentationComponent,
        SocialNetworksClaimantComponent, SocialNetworksProviderComponent,
        DashboardComponent, FDAComponent, SearchFacets, SelectedFacets, HighlightPipe,
        DCAComponent, SecretaryOfStateComponent,
        ESIComponent,
        ICD9Component, RevenueCodeComponent,
        NICBComponent, MBCOIGComponent, ChiropractorComponent,
        DNBComponent, PdfViewerComponent,
        GDComponent,
        NAV_DROPDOWN_DIRECTIVES,
        BreadcrumbsComponent,
        SIDEBAR_TOGGLE_DIRECTIVES,
        AsideToggleDirective,
        DateUTCPipe,
        LoginComponent,
        HeaderComponent,
        HeaderDefaultComponent,
        // Tabset,
        // Tab,
        // NgTransclude,
        // TabHeading
    ],
    providers: [{
        provide: LocationStrategy,
        useClass: HashLocationStrategy
    }, GlobalClass, HttpClient, HttpSOLRClient, HttpScoutClient, DatePipe, AuthGuard, Commonutil, BroadcastService],
    bootstrap: [AppComponent]
})
export class AppModule { }
