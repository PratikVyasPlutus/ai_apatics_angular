import { Injectable } from "@angular/core";

@Injectable()
export class Commonutil {

    getLocalStorageItem(key, json: boolean = false): any {
        let val = localStorage.getItem(key);
        return json ? JSON.parse(val) : val;
    }

    setLocalStorageItem(key, value, json: boolean = false): void {
        let toBeSetval = json ? JSON.stringify(value) : value;
        localStorage.setItem(key, toBeSetval);
    }

    removeLocalStorageItem(key): void {
        localStorage.removeItem(key);
    }

    getSessionStorageItem(key, json: boolean = false): any {
        let val = sessionStorage.getItem(key);
        return json ? JSON.parse(val) : val;
    }

    setSessionStorageItem(key, value, json: boolean = false): void {
        let toBeSetval = json ? JSON.stringify(value) : value;
        sessionStorage.setItem(key, toBeSetval);
    }

    removeSessionStorageItem(key): void {
        sessionStorage.removeItem(key);
    }

    cloneArray(old) {
        if (!old) {
            return old;
        }
        return old.slice(0).reverse();
    }

}