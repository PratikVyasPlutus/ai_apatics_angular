import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'selectedFacets',
    pure: false
})
export class SelectedFacets implements PipeTransform {
    transform(allFacets: any[]) {
        if (allFacets) {
            return allFacets.filter(objFacet => objFacet.selected);
        }
    }
}