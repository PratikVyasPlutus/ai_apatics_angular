import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({ name: 'searchFacets' })
export class SearchFacets implements PipeTransform {
    transform(allFacets: any[], searchText: string, type: string) {
        //console.log('allFacets', allFacets);
        //objFacet.name.toLowerCase.indexOf(searchText,0) == 0
        if (allFacets) {
            if (searchText && searchText.length > 0) {
                return allFacets.filter(objFacet => objFacet.name.toLowerCase().indexOf(searchText) == 0).slice(0, 10);
            }
            else {
                return allFacets.slice(0, 10);
                //return allFacets;
            }

        }
    }
}