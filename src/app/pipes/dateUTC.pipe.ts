import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'dateutc'
})

export class DateUTCPipe implements PipeTransform {
    transform(value: any, format: string = ""): string {
        // Try and parse the passed value.
        // var formattedDate = new Date(value.replace("UTC", "").trim());
        var formattedDate = new Date(value.replace("UTC", "").trim().replace(" ", "T"));
        // If moment didn't understand the value, return it unformatted.
        if (!formattedDate) return value;

        // Otherwise, return the date formatted as requested.

        return (new Date(Date.UTC(formattedDate.getFullYear(), formattedDate.getMonth(), formattedDate.getDate(), formattedDate.getHours(), formattedDate.getMinutes()))).toISOString();
    }
}
