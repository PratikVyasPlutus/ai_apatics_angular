import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions, Response, URLSearchParams, Jsonp } from "@angular/http";
import { DatePipe } from '@angular/common';
import { GlobalClass } from './global';
import { HttpClient } from "./http.client"
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class TwitterService {
    constructor(private httpClient: HttpClient, private _jsonp: Jsonp, private datePipe: DatePipe, private globals: GlobalClass) {
    }

    getAccessToken(): Observable<any> {
        let headers = new Headers();
        headers.append("Content-Type", "application/x-www-form-urlencoded");
        headers.append("Authorization", 'Basic dUROSHFJWExaTWtsQWhQclplNW5VaVd5MTo2VUpaOTA0N2VrWkc4SmRjZGlkQ1luVWlLOTVNRGhQYlJEQ2VzenRRVGM1RWF5WnhaYQ==');

        let body = new URLSearchParams();
        body.set('grant_type', 'client_credentials');
        body.set('Username', 'uDNHqIXLZMklAhPrZe5nUiWy1');
        body.set('Password', '6UJZ9047ekZG8JdcdidCYnUiK95MDhPbRDCesztQTc5EayZxZa');
        // body.set('client_secret', '6UJZ9047ekZG8JdcdidCYnUiK95MDhPbRDCesztQTc5EayZxZa');
        // body.set('client_id', 'uDNHqIXLZMklAhPrZe5nUiWy1');

        var url = "https://api.twitter.com/oauth2/token";
        return this.httpClient.postWithHeaders(url, body, headers).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    getTwitterData(): Observable<any> {
        let headers = new Headers();
        headers.append("Authorization", '{"Authorization":"Bearer AAAAAAAAAAAAAAAAAAAAAMxc0QAAAAAApSjDkbhDP15mbeF%2Fh5aHE%2FUc8Mc%3DAHJKTHej8vzN2kbxglvK1lmsQEv32nSWXlBi24Ll42jVBZh1RK"}');

        var url = "https://api.twitter.com/1.1/search/tweets.json?q=%23healthcarefraud&result_type=recent";
        return this.httpClient.getWithHeaders(url, headers).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    getDialogFeedData(): Observable<any> {
        var url = "https://app.dialogfeed.com/en/snippet/social-wall-8835.json?api_key=14c61d37d4205650e762c2b219e33c23";
        return this.httpClient.get(url).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }
}