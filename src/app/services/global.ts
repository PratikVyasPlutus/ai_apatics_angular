import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

//export var serviceBaseUrl='http://localhost/TaskLiveRestAPI/v1/';
@Injectable()
export class GlobalClass {
    public BaseUrl = 'http://localhost/TasksLiveApp/';

    // BaseApiUrl(){
    //     return this.BaseAPIUrl;
    // }
    public data: any;

    public roleEnum = {
        "SuperAdmin": 1,
        "Admin": 2,
        "MyClient": 3,
        "Employee": 4
    };

    constructor() {
        //console.log('Global class constructor called');
    }

    extractSolrData(res: any) {
        console.log('resextractSolrData', res)
        let body = res.json();
        return body || {};
    }

    extractData(res: any) {
        let body = res.json();
        return body || {};
    }

    extractError(error: any) {
        // In a real world app, we might use a remote logging infrastructure

        let errMsg: string;
        if (error) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    convertToMSJSON(objDate: Date) {
        var date = '/Date(' + objDate.getTime() + ')/'; //CHANGED LINE
        return date;
    }
}