import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Commonutil } from "../utils/common-util";
import * as _ from 'underscore';
import { UUID } from "angular2-uuid";
import { LoginRequest } from "app/models/request/login-request";
import { UserSession } from "app/models/dto/user.session";

@Injectable()
export class SessionService {
    constructor(private commonutil: Commonutil) { }

    createUserSession(req: LoginRequest): Observable<UserSession> {
        return new Observable<UserSession>(
            observer => {
                let session: UserSession = {
                    sessionId: UUID.UUID(),
                    name: req.username
                };
                this.commonutil.setSessionStorageItem('userSession', session, true);
                observer.next(session);
                observer.complete();
            });
    }

    getUserSession(): Observable<UserSession> {
        return new Observable<UserSession>(
            observer => {
                let session: UserSession = this.commonutil.getSessionStorageItem('userSession', true);
                observer.next(session);
                observer.complete();
            });
    }

    clearUserSession(): Observable<boolean> {
        return new Observable<boolean>(
            observer => {
                this.commonutil.removeSessionStorageItem('userSession');
                observer.next(true);
                observer.complete();
            });
    }

}