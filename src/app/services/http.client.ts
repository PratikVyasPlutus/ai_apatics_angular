import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";

@Injectable()
export class HttpClient {
    private http;
    // private ApiBaseUrl = 'http://localhost/SolrPOCAPI/v1/';
    private ApiBaseUrl = 'http://ai.apatics.net/api/SolrPOCService.svc/';//Production
    private headers = new Headers();
   
    constructor(http: Http) {
        this.http = http;
        //this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
        // this.headers.append('access-control-allow-origin','*');
        // this.headers.append('access-control-max-age','1728000');
        // this.headers.append('access-control-allow-methods','GET, HEAD, POST, PUT, DELETE, OPTIONS');
        // this.headers.append('access-control-allow-headers','Content-Type');
        //  this.headers.append('access-control-expose-headers','WWW-Authenticate, Server-Authorization');
      
    }

    get(url) {
        let fullUrl: string = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.get(fullUrl, { headers: this.headers });
    }

    post(url, data) {
        let fullUrl: string = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.post(fullUrl, data, { headers: this.headers });
    }

    getWithHeaders(url, headers) {
        let fullUrl: string = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.get(fullUrl, { headers: headers });
    }

    postWithHeaders(url, data, headers) {
        let fullUrl: string = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.post(fullUrl, data, { headers: headers });
    }
}