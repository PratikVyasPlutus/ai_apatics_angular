import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";

@Injectable()
export class HttpScoutClient {
    private http;
    //private ApiBaseUrl = 'http://localhost:8983/solr/';
    //private ApiBaseUrl = 'http://dg.scoutcms.com/';
    private ApiBaseUrl = 'http://demo.scoutcms.com/Controllers/DataService.asmx?op=';
    //private ApiBaseUrl = '/api/v1/';//Production
    private headers = new Headers();

    constructor(http: Http) {
        this.http = http;
        this.headers.append('Content-Type', 'text/xml');
        this.headers.append('Accept', 'application/xml');
    }

    get(url) {
        let fullUrl: string = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.get(fullUrl, { headers: this.headers });
    }

    post(url, data) {
        let fullUrl: string = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.post(fullUrl, data, { headers: this.headers });
    }

    postWithHeaders(url, data, headers) {
        let fullUrl: string = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.post(fullUrl, data, { headers: this.headers });
    }
}