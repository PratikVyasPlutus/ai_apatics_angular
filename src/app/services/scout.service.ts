import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions, Response, URLSearchParams } from "@angular/http";
import { DatePipe } from '@angular/common';
import { Jsonp } from '@angular/http';
import { GlobalClass } from './global';
import { HttpScoutClient } from "./http-scout.client"
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()
export class ScoutService {
    constructor(private httpClient: HttpScoutClient, private _jsonp: Jsonp, private datePipe: DatePipe, private globals: GlobalClass) {
    }

    changeCaseStatus(scoutID, status): Observable<any> {

        // var url = 'provider_summary_core/select?q=' + encodeURIComponent(searchText) + '&rows=0&wt=json&indent=true&facet=true&facet.sort=index' +
        //     '&facet.field=PARTY_FIRST_NAME&facet.field=PROVIDER_SPECIALTY&facet.field=ZIP_CODE&facet.field=TAX_ID_NUMBER';

        var data = `<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                            <soap:Body>
                                <ChangeStatus xmlns="http://dg.scoutcms.com">
                                <ClientToken>ve{+5^y!}_Tr1>XBIFo2+u79</ClientToken>
                                <ScoutID>`+ scoutID + `</ScoutID>
                                <Status>`+ status + `</Status>
                                <EmailAddress>ankur.akvaliya@plutustec.com</EmailAddress>
                                </ChangeStatus>
                            </soap:Body>
                            </soap:Envelope>`;
        return this.httpClient.post('ChangeStatus', data).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    importCase(objData): Observable<any> {
        var data = `<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
            <ImportCase xmlns="http://dg.scoutcms.com">
            <ClientToken>ve{+5^y!}_Tr1>XBIFo2+u79</ClientToken>
            <ReferralDate></ReferralDate>
            <CaseType>Ineligibility</CaseType>
            <ReferralMethod>Portal</ReferralMethod>
            <Department>Healthcare</Department>
            <Claims>
                <Claim>
                    <ClaimNumber></ClaimNumber>
                    <AdjusterName></AdjusterName>
                    <AdjusterPhoneNumber></AdjusterPhoneNumber>
                    <PolicyNumber></PolicyNumber>
                    <PolicyCountryCode></PolicyCountryCode>
                    <PolicyState></PolicyState>
                    <LineOfBusiness></LineOfBusiness>
                    <DateOfLoss></DateOfLoss>
                    <DateOfNotice></DateOfNotice>
                    <CauseOfLoss></CauseOfLoss>
                    <LossDescription></LossDescription>
                    <LossLocation>
                        <Name></Name>
                        <Line1></Line1>
                        <Line2></Line2>
                        <Line3></Line3>
                        <City></City>
                        <County></County>
                        <State></State>
                        <Country></Country>
                        <Zip></Zip>
                    </LossLocation>
                    <ReferralReasons></ReferralReasons>
                    <ReferralDate></ReferralDate>
                    <Comments></Comments>
                </Claim>                
            </Claims>
            <Parties>
                <Party>
                    <FirstName>`+ objData.providerBasic.PARTY_FIRST_NAME + `</FirstName>
                    <MiddleName></MiddleName>
                    <LastName>`+ objData.providerBasic.PARTY_LAST_NAME + `</LastName>
                    <Aliases></Aliases>
                    <DateOfBirth></DateOfBirth>
                    <Gender></Gender>
                    <SSN></SSN>
                    <Emails></Emails>
                    <Notes></Notes>
                    <PhoneNumbers>
                    </PhoneNumbers>
                    <Addresses>
                        <Address>`+ objData.providerBasic.STREET_ADDRESS + `</Address>
                        <Address>`+ objData.providerBasic.CITY + `</Address>
                        <Address>`+ objData.providerBasic.STATE + `</Address>
                        <Address>`+ objData.providerBasic.ZIP_CODE + `</Address>
                    </Addresses>
                    <Roles>
                    </Roles>
                </Party>               
            </Parties>
            <Autos>            
            </Autos>
            <Exposures>
            </Exposures>
            <Assignments>
                <Assignment>
                    <AssignedToEmail></AssignedToEmail>
                    <Role></Role>
                    <DateCreated></DateCreated>
                    <IsPrimary>false</IsPrimary>
                </Assignment>              
            </Assignments>
            </ImportCase>
            </soap:Body>
        </soap:Envelope>`;
        return this.httpClient.post('ImportCase', data).map(res => res.text()/*JSON.parse(xml2json(res.text(),'  '))*/)
            .catch(this.globals.extractError);
    }

}