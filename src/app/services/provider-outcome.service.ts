import { Injectable } from '@angular/core';
import { HttpClient } from "./http.client"
import { GlobalClass } from './global';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable()
export class ProviderOutcomeService {

  //private ApiBaseUrl = 'http://localhost/pocwebexcel/';
  private ApiBaseUrl = 'http://ai.apatics.net/apiv2/';

  constructor(private httpClient: HttpClient, private globals: GlobalClass) { }

  getCityList() {
    return this.httpClient.get(this.ApiBaseUrl + 'filter.php?api_code=1')
      .map(this.globals.extractData)
      .catch(this.globals.extractError);
  }

  getZipCodeList(city) {
    return this.httpClient.get(this.ApiBaseUrl + 'filter.php?api_code=2&VENDOR_CITY=' + city)
      .map(this.globals.extractData)
      .catch(this.globals.extractError);
  }

  getICDCodeList(zipcode, cityName) {
    return this.httpClient.get(this.ApiBaseUrl + 'filter.php?api_code=3&VENDOR_ZIP=' + zipcode + '&VENDOR_CITY=' + cityName)
      .map(this.globals.extractData)
      .catch(this.globals.extractError);
  }

  sendSMS(sendList, ProviderList) {
    return this.httpClient.post(this.ApiBaseUrl + 'sendSMS.php', { sendList: sendList, ProviderList: ProviderList })
      .map(this.globals.extractData)
      .catch(this.globals.extractError);
  }

  getSideFilter(filter) {
    return this.httpClient.post(this.ApiBaseUrl + 'filter_fields_api.php', { filter: filter })
      .map(this.globals.extractData)
      .catch(this.globals.extractError);
  }

  getAllSideBarFilterForClaimant(filter) {
    return this.httpClient.post(this.ApiBaseUrl + 'claimant_filter.php', { filter: filter })
      .map(this.globals.extractData)
      .catch(this.globals.extractError);
  }

  getAllSideBarFilterForProviderNetwork(filter) {
    return this.httpClient.post(this.ApiBaseUrl + 'network_provrder_filter.php', { filter: filter })
      .map(this.globals.extractData)
      .catch(this.globals.extractError);
  }

  getSideBarFilterMap(filter) {
    return this.httpClient.post(this.ApiBaseUrl + 'map_filter.php', { filter: filter })
      .map(this.globals.extractData)
      .catch(this.globals.extractError);
  }

  getAllProviderDataWithFilter(filter) {
    //let Finalfilter = JSON.stringify(filter);
    return this.httpClient.post(this.ApiBaseUrl + 'api.php', { filter: filter })
      .map(this.globals.extractData)
      .catch(this.globals.extractError);
  }

  getAllClaimantData(filter) {
    return this.httpClient.post(this.ApiBaseUrl + 'claimant.php', { filter: filter })
      .map(this.globals.extractData)
      .catch(this.globals.extractError);
  }

  getAllProviderNetworkData(filter) {
    return this.httpClient.post(this.ApiBaseUrl + 'nework_provider.php', { filter: filter })
      .map(this.globals.extractData)
      .catch(this.globals.extractError);
  }

  getAllMapData(filter) {
    return this.httpClient.post(this.ApiBaseUrl + 'map.php', { filter: filter })
      .map(this.globals.extractData)
      .catch(this.globals.extractError);
  }
}
