import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions, Response, URLSearchParams } from "@angular/http";
import { DatePipe } from '@angular/common';
import { Jsonp } from '@angular/http';
import { GlobalClass } from './global';
import { HttpSOLRClient } from "./http-solr.client"
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class SOLRService {
    constructor(private httpClient: HttpSOLRClient, private _jsonp: Jsonp, private datePipe: DatePipe, private globals: GlobalClass) {
    }

    searchProviderSummaryFacets(searchText: string): Observable<any> {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";

        var url = 'provider_summary_core/select?q=' + encodeURIComponent(searchText) + '&rows=0&wt=json&indent=true&facet=true&facet.sort=index' +
            '&facet.field=PARTY_FIRST_NAME&facet.field=PROVIDER_SPECIALTY&facet.field=ZIP_CODE&facet.field=TAX_ID_NUMBER';

        return this.httpClient.get(url).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchProviderSummaryRecordsWithFilter(searchText: string, bar: string, dr: string, par: string, pn: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";
        let facetSearchString = "";
        if (bar)
            facetSearchString += "&fq=PARTY_FIRST_NAME:\"" + bar + "\"";
        if (dr)
            facetSearchString += "&fq=PROVIDER_SPECIALTY:\"" + dr + "\"";
        if (par)
            facetSearchString += "&fq=ZIP_CODE:\"" + par + "\"";
        if (pn)
            facetSearchString += "&fq=TAX_ID_NUMBER:\"" + pn + "\"";

        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;

        var startRecordNumber = (pageNumber * recordsToFetch) - recordsToFetch;

        var url = 'provider_summary_core/select?indent=on&q=' + encodeURIComponent(searchText) + facetSearchString + '&rows=' + recordsToFetch + '&start=' + startRecordNumber + '&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    getProviderSummaryBillStatistics(taxIDNumber): Observable<any> {
        if (!taxIDNumber)
            taxIDNumber = "*";

        var url = 'ps_bill_statistics/select?indent=on&q=' + encodeURIComponent("TAX_ID_NUMBER:" + taxIDNumber) + '&rows=100&start=0&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    getProviderSummaryRedFlagAlerts(taxIDNumber): Observable<any> {
        if (!taxIDNumber)
            taxIDNumber = "*";

        var url = 'ps_red_flag_alerts/select?indent=on&q=' + encodeURIComponent("TAX_ID_NUMBER:" + taxIDNumber) + '&rows=100&start=0&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    getProviderSummaryChiropracticAlerts(licenseNumber): Observable<any> {
        if (!licenseNumber)
            licenseNumber = "*";
        else
            licenseNumber = "DC" + licenseNumber;

        var url = 'chiropractic_alerts/select?indent=on&q=' + encodeURIComponent("licenseNo:" + licenseNumber) + '&rows=100&start=0&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchDuplicateBillingFacets(taxIDNumber, searchText: string): Observable<any> {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";

        var url = 'duplicate_billing_core/select?q=' + encodeURIComponent(searchText) + '&rows=0&wt=json&indent=true&facet=true&facet.sort=index' +
            '&fq=taxIDNumber1:' + taxIDNumber + ' OR taxIDNumber2:' + taxIDNumber +
            '&facet.field=dateOfService&facet.field=vendorName1&facet.field=billedAmount1&facet.field=paidAmount1';

        return this.httpClient.get(url).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchDuplicateBillingRecordsWithFilter(taxIDNumber, searchText: string, vn1: string, dos: string, ba1: string, pa1: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";
        let facetSearchString = '&fq=taxIDNumber1:' + taxIDNumber + ' OR taxIDNumber2:' + taxIDNumber;
        if (vn1)
            facetSearchString += "&fq=vendorName1:\"" + vn1 + "\"";
        if (dos)
            facetSearchString += "&fq=dateOfService:\"" + dos + "\"";
        if (ba1)
            facetSearchString += "&fq=billedAmount1:" + ba1;
        if (pa1)
            facetSearchString += "&fq=paidAmount1:" + pa1;

        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;

        var startRecordNumber = (pageNumber * recordsToFetch) - recordsToFetch;

        var url = 'duplicate_billing_core/select?indent=on&q=' + encodeURIComponent(searchText) + facetSearchString + '&rows=' + recordsToFetch + '&start=' + startRecordNumber + '&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    getAuthToken() {
        let headers = new Headers();
        headers.append('authorization', 'Basic QXBhdGljc0FQSWFwcDpteS1zZWNyZXQtdG9rZW4tdG8tY2hhbmdlLWluLXByb2R1Y3Rpb24=');
        let body = new URLSearchParams();
        body.set('username', 'admin');
        body.set('password', 'admin');
        body.set('grant_type', 'password');
        body.set('scope', 'read write');
        body.set('client_secret', 'my-secret-token-to-change-in-production');
        body.set('client_id', 'ApaticsAPIapp');

        return this.httpClient.postWithHeaders('http://54.191.44.17/oauth/token', body, headers)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    getProviderFacets(searchText: string) {
        let headers = new Headers();
        headers.append('authorization', 'Bearer ' + window.localStorage.getItem('apaticsAuthToken'));
        let objData: any = {};
        objData.q = searchText ? (searchText + "*") : "*";
        objData.fields = ["partyFirstName", "providerSpeciality", "zipCode", "taxIdNumber"];

        //let body = {"q": "*", "fields": ["partyFirstName", "providerSpeciality", "zipCode", "taxIdNumber"]};
        return this.httpClient.postWithHeaders('http://54.191.44.17/api/provider/get_facets', objData, headers)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    getProviderRecordsWithFilter(searchText: string, scrollId: string, limit: number) {
        let headers = new Headers();
        headers.append('authorization', 'Bearer ' + window.localStorage.getItem('apaticsAuthToken'));
        let objData: any = {};
        objData.q = searchText ? searchText : "*";
        objData.limit = limit;
        // if (scrollId.length > 0)
        //     objData.scrollId = scrollId;

        return this.httpClient.postWithHeaders('http://54.191.44.17/api/provider/search_providers', objData, headers)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    }


}