
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { SessionService } from "./session.service";
import * as _ from 'underscore';
import { Global } from "app/constants/global";
import { UserSession } from "app/models/dto/user.session";
import { LoginRequest } from "app/models/request/login-request";

@Injectable()
export class LoginService {
    constructor(private _sessionService: SessionService) { }

    login(req: LoginRequest): Observable<UserSession> {
        return new Observable<UserSession>(
            observer => {
                if (this._userExists(req)) {
                    this._sessionService.createUserSession(req).subscribe(
                        res => {
                            observer.next(res);
                        },
                        err => {
                            observer.next(null);
                        },
                        () => {
                            observer.complete();
                        }
                    );
                } else {
                    observer.next(null);
                    observer.complete();
                }
            });
    }

    _userExists(req: LoginRequest): boolean {
        return _.some(Global.userList, (user) => user.username == req.username && user.password == req.password);
    }

    logout(): Observable<boolean> {
        return new Observable<boolean>(
            observer => {
                this._sessionService.clearUserSession().subscribe(
                    res => {
                        observer.next(true);
                    },
                    err => {
                        observer.next(false);
                    },
                    () => {
                        observer.complete();
                    }
                );
            });
    }

}