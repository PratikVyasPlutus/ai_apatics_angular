import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions, Response, URLSearchParams, Jsonp } from "@angular/http";
import { DatePipe } from '@angular/common';
import { GlobalClass } from './global';
import { HttpClient } from "./http.client"
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class FDAService {
    constructor(private httpClient: HttpClient, private _jsonp: Jsonp, private datePipe: DatePipe, private globals: GlobalClass) {
    }

    getSelectedFDA(productId: string): Observable<any> {
        if (!productId || productId.length == 0)
            productId = "0002-1975_6bf7a335-bf6b-4f7f-b8cf-c7b6ee1ba089";

        return this.httpClient.get('FDA/select/?q=' + productId).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchFDAFacets(searchText: string): Observable<any> {
        if (!searchText)
            searchText = "";

        return this.httpClient.get('FDA/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchFDARecords(searchText: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "";
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('FDA/search/?q=' + searchText + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchFDARecordsWithFilter(searchText: string, pndc: string, ptn: string, pn: string, npn: string, deas: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "";
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('FDA/searchwithfilter/?q=' + searchText + '&f1=' + pndc + '&f2=' + ptn + '&f3=' + pn + '&f4=' + npn + '&f5=' + deas + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchDCAFacets(searchText: string): Observable<any> {
        if (!searchText)
            searchText = "";

        return this.httpClient.get('DCA/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchDCARecordsWithFilter(searchText: string, an: string, lt: string, sc: string, ls: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "";
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('DCA/searchwithfilter/?q=' + searchText + '&f1=' + an + '&f2=' + lt + '&f3=' + sc + '&f4=' + ls + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchESIFacets(searchText: string): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = searchText.replace(/&/g, '`').replace(/#/g, '^');

        return this.httpClient.get('ESI/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchESIRecordsWithFilter(searchText: string, phn: string, plc: string, pzc: string, pln: string, cc: string, dt: string, bar: string, car: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = searchText.replace(/&/g, '`').replace(/#/g, '^');

        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('ESI/searchwithfilter/?q=' + searchText + '&f1=' + phn + '&f2=' + plc + '&f3=' + pzc + '&f4=' + pln + '&f5=' + cc + '&f6=' + dt + '&f7=' + bar + '&f8=' + car + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchICD9Facets(searchText: string): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = searchText.replace(/&/g, '`').replace(/#/g, '^');

        return this.httpClient.get('ICD9/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchICD9RecordsWithFilter(searchText: string, cde: string, cdedsc: string, cdetyp: string, cdetypdsc: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = searchText.replace(/&/g, '`').replace(/#/g, '^');
        if (cde)
            cde = cde.replace(/&/g, '`').replace(/#/g, '^');
        if (cdedsc)
            cdedsc = cdedsc.replace(/&/g, '`').replace(/#/g, '^');
        if (cdetyp)
            cdetyp = cdetyp.replace(/&/g, '`').replace(/#/g, '^');
        if (cdetypdsc)
            cdetypdsc = cdetypdsc.replace(/&/g, '`').replace(/#/g, '^');
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('ICD9/searchwithfilter/?q=' + searchText + '&f1=' + cde + '&f2=' + cdedsc + '&f3=' + cdetyp + '&f4=' + cdetypdsc + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchRevenueCodeFacets(searchText: string): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = searchText.replace(/&/g, '`').replace(/#/g, '^');

        return this.httpClient.get('RevenueCode/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchRevenueCodeRecordsWithFilter(searchText: string, cde: string, cdedsc: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = searchText.replace(/&/g, '`').replace(/#/g, '^');
        if (cde)
            cde = cde.replace(/&/g, '`').replace(/#/g, '^');
        if (cdedsc)
            cdedsc = cdedsc.replace(/&/g, '`').replace(/#/g, '^');
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('RevenueCode/searchwithfilter/?q=' + searchText + '&f1=' + cde + '&f2=' + cdedsc + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchSecretaryOfStateFacets(searchText: string): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);

        return this.httpClient.get('SecretaryOfState/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchSecretaryOfStateRecordsWithFilter(searchText: string, cn: string, col: string, ct: string, cc: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        if (cn)
            cn = encodeURIComponent(cn);
        if (col)
            col = encodeURIComponent(col);
        if (ct)
            ct = encodeURIComponent(ct);
        if (cc)
            cc = encodeURIComponent(cc);
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('SecretaryOfState/searchwithfilter/?q=' + searchText + '&f1=' + cn + '&f2=' + col + '&f3=' + ct + '&f4=' + cc + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchNICBFacets(searchText: string): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);

        return this.httpClient.get('NICB/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchNICBRecordsWithFilter(searchText: string, ps: string, plt: string, pla: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        if (ps)
            ps = encodeURIComponent(ps);
        if (plt)
            plt = encodeURIComponent(plt);
        if (pla)
            pla = encodeURIComponent(pla);
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('NICB/searchwithfilter/?q=' + searchText + '&f1=' + ps + '&f2=' + plt + '&f3=' + pla + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchMBCOIGFacets(searchText: string): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);

        return this.httpClient.get('MBCOIG/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }


    searchMBCOIGRecordsWithFilter(searchText: string, doay: string, at: string, sp: string, bt: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        if (doay)
            doay = encodeURIComponent(doay);
        if (at)
            at = encodeURIComponent(at);
        if (sp)
            sp = encodeURIComponent(sp);
        if (bt)
            bt = encodeURIComponent(bt);
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('MBCOIG/searchwithfilter/?q=' + searchText + '&f1=' + doay + '&f2=' + at + '&f3=' + sp + '&f4=' + bt + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchChiropractorFacets(searchText: string): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);

        return this.httpClient.get('CED/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchChiropractorRecordsWithFilter(searchText: string, dt: string, ln: string, dod: string, state: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        if (dt)
            dt = encodeURIComponent(dt);
        if (ln)
            ln = encodeURIComponent(ln);
        if (dod)
            dod = encodeURIComponent(dod);
        if (state)
            state = encodeURIComponent(state);
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('CED/searchwithfilter/?q=' + searchText + '&f1=' + dt + '&f2=' + ln + '&f3=' + dod + '&f4=' + state + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchGDFacets(searchText: string): Observable<any> {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);

        return this.httpClient.get('GeographicDiscrepancy/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchGDRecordsWithFilter(searchText: string, bar: string, dr: string, par: string, pn: string, tin: string, tbar: string, tpar: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";
        if (bar)
            bar = encodeURIComponent(bar);
        if (dr)
            dr = encodeURIComponent(dr);
        if (par)
            par = encodeURIComponent(par);
        if (pn)
            pn = encodeURIComponent(pn);
        if (tin)
            tin = encodeURIComponent(tin);
        if (tbar)
            tbar = encodeURIComponent(tbar);
        if (tpar)
            tpar = encodeURIComponent(tpar);

        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;

        var params = new URLSearchParams();
        params.set('indent', 'on');
        params.set('q', encodeURIComponent(searchText)); // the user's search value
        params.set('wt', 'json');

        var startRecordNumber = (pageNumber * recordsToFetch) - recordsToFetch;

        var url = 'http://35.160.49.120:8983/solr/claimant_provider_distance_core/select?indent=on&q=' + encodeURIComponent(searchText) + '&rows=' + recordsToFetch + '&start=' + startRecordNumber + '&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
        // return this.httpClient.get('GeographicDiscrepancy/searchwithfilter/?q=' + searchText + '&f1=' + bar + '&f2=' + dr + '&f3=' + par + '&f4=' + pn + '&f5=' + tin + '&f6=' + tbar + '&f7=' + tpar + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
        //     .catch(this.globals.extractError);
        //indent=on&q=' + encodeURIComponent(searchText) + '&rows=20&start=0&wt=json&format=jsonP&callback=__ng_jsonp__.__req0.finished';
        // return this._jsonp.get(url, { search: params })
        //     .map(data => data.json());
    }

    searchProviderSummaryFacets(searchText: string): Observable<any> {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";

        var url = 'http://data.apatics.net:8983/solr/provider_summary_core/select?q=' + encodeURIComponent(searchText) + '&rows=0&wt=json&indent=true&facet=true&facet.sort=index' +
            '&facet.field=PARTY_FIRST_NAME&facet.field=PROVIDER_SPECIALTY&facet.field=ZIP_CODE&facet.field=TAX_ID_NUMBER';

        return this.httpClient.get(url).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    searchProviderSummaryRecordsWithFilter(searchText: string, bar: string, dr: string, par: string, pn: string, pageNumber: number, recordsToFetch: number): Observable<any> {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";
        if (bar)
            bar = encodeURIComponent(bar);
        if (dr)
            dr = encodeURIComponent(dr);
        if (par)
            par = encodeURIComponent(par);
        if (pn)
            pn = encodeURIComponent(pn);

        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;

        var startRecordNumber = (pageNumber * recordsToFetch) - recordsToFetch;

        var url = 'http://data.apatics.net:8983/solr/provider_summary_core/select?indent=on&q=' + encodeURIComponent(searchText) + '&rows=' + recordsToFetch + '&start=' + startRecordNumber + '&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    getAuthToken() {
        let headers = new Headers();
        headers.append('authorization', 'Basic QXBhdGljc0FQSWFwcDpteS1zZWNyZXQtdG9rZW4tdG8tY2hhbmdlLWluLXByb2R1Y3Rpb24=');
        let body = new URLSearchParams();
        body.set('username', 'admin');
        body.set('password', 'admin');
        body.set('grant_type', 'password');
        body.set('scope', 'read write');
        body.set('client_secret', 'my-secret-token-to-change-in-production');
        body.set('client_id', 'ApaticsAPIapp');
        // let urlSearchParams = new URLSearchParams();
        // for (let key in obj) {
        //     urlSearchParams.append(key, obj[key]);
        // }

        return this.httpClient.postWithHeaders('http://54.191.44.17/oauth/token', body, headers)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    getProviderFacets(searchText: string) {
        let headers = new Headers();
        headers.append('authorization', 'Bearer ' + window.localStorage.getItem('apaticsAuthToken'));
        let objData: any = {};
        objData.q = searchText ? (searchText + "*") : "*";
        objData.fields = ["partyFirstName", "providerSpeciality", "zipCode", "taxIdNumber"];

        //let body = {"q": "*", "fields": ["partyFirstName", "providerSpeciality", "zipCode", "taxIdNumber"]};
        return this.httpClient.postWithHeaders('http://54.191.44.17/api/provider/get_facets', objData, headers)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    saveCaseStatus(objData: any): Observable<any> {
        return this.httpClient.post('CaseStatus', { data: objData }).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    getCaseStatus(taxIdNumber: string): Observable<any> {
        if (!taxIdNumber)
            taxIdNumber = "";

        // return this.httpClient.get('CaseStatus/' + taxIdNumber).map(this.globals.extractData)
        //     .catch(this.globals.extractError);
        //var url = "http://localhost/ApaticsTaskLiveRestAPI/v1/";
        var url = "http://casemgmt.apatics.net/api/v1/";
        return this.httpClient.get(url + 'CaseStatus/' + taxIdNumber).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    saveCase(objData: any): Observable<any> {
        //var url = "http://localhost/ApaticsTaskLiveRestAPI/v1/";
        var url = "http://casemgmt.apatics.net/api/v1/";
        return this.httpClient.post(url + 'CaseSave', { data: objData }).map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

    getTwitterHashtagData(): Observable<any> {
        return this.httpClient.get('Twitter/SearchHashtag').map(this.globals.extractData)
            .catch(this.globals.extractError);
    }

}