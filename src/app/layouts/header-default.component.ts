import { Component, OnInit } from '@angular/core';
import { SessionService } from '../services/session.service';
import { Router, NavigationEnd } from "@angular/router";
@Component({
    selector: 'app-header-default',
    templateUrl: './header-default.component.html',
    providers: [SessionService]
})
export class HeaderDefaultComponent implements OnInit {
    loggedIn: boolean = false;
    constructor(private _sessionService: SessionService, private router: Router, ) {


    }
    logOut() {
        this._sessionService.clearUserSession().subscribe(
            res => {
                this.loggedIn = false;
                this.router.navigate(['/login']);
            },
            err => {
                console.log('Error while clearing session data.')
            }
        )
    }
    ngOnInit(): void { }
}
