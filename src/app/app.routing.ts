import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import { HeaderLayoutComponent } from './layouts/header-layout.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { FDAComponent } from './pages/fda/fda.component';
import { DCAComponent } from './pages/dca/dca.component';
import { ESIComponent } from './pages/esi/esi.component';
import { ICD9Component } from './pages/icd9/icd9.component';
import { RevenueCodeComponent } from './pages/revenuecode/revenuecode.component';
import { SecretaryOfStateComponent } from './pages/secretaryofstate/secretaryofstate.component';
import { NICBComponent } from './pages/nicb/nicb.component';
import { MBCOIGComponent } from './pages/mbcoig/mbcoig.component';
import { ChiropractorComponent } from './pages/chiropractor/chiropractor.component';
import { DNBComponent } from './pages/dnb/dnb.component';
import { GDComponent } from './pages/gd/gd.component';
import { ProviderDashboardComponent } from './pages/providerdashboard/providerdashboard.component';
import { ProviderOutcomeComponent } from './pages/providerOutcome/providerOutcome.component';
import { ProviderReportsComponent } from './pages/providerReports/providerReports.component';
import { ProviderMapComponent } from './pages/providerMap/providerMap.component';
import { SocialNetworksClaimantComponent } from './pages/socialNetworksClaimant/socialNetworksClaimant.component'
import { SocialNetworksProviderComponent } from './pages/socialNetworksProvider/socialNetworksProvider.component';
import { OpioidComponent } from './pages/Opioid/Opioid.component';
import { ClaimSegmentationComponent } from './pages/ClaimSegmentation/ClaimSegmentation.component';
import { DuplicateBillingComponent } from './pages/duplicatebilling/duplicatebilling.component';
import { LoginComponent } from './pages/login/login.component';
//guards
import { AuthGuard } from './guards/index';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        canActivate: [AuthGuard],
        pathMatch: 'full',
    },
    {
        path: 'login',
        component: LoginComponent,
        data: {
            title: 'Login'
        }
    },
    {
        path: 'providerdashboard',
        component: ProviderDashboardComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Provider Dashboard'
        }
    },
    {
        path: 'provider-outcome',
        component: ProviderOutcomeComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Provider Outcome'
        }
    },
    {
        path: 'predictive-analytics/provider-reports',
        component: ProviderReportsComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Provider Reports'
        }
    },
    {
        path: 'predictive-analytics/provider-map',
        component: ProviderMapComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Provider Map'
        }
    },
    {
        path: 'social-networks/claimant',
        component: SocialNetworksClaimantComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Provider Map'
        }
    },
    {
        path: 'social-networks/provider',
        component: SocialNetworksProviderComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Provider Map'
        }
    },
    {
        path: 'opioid',
        component: OpioidComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Provider Map'
        }
    },
    {
        path: 'claim-segmentation',
        component: ClaimSegmentationComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Claim Segmentation'
        }
    },
    {
        path: 'doublebilling',
        component: DuplicateBillingComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Double Billing'
        }
    },
    {
        path: 'fda',
        component: FDAComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'FDA'
        }
    },
    {
        path: 'home',
        component: DashboardComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Home'
        }/*,
        children: [
            {
                path: 'dashboard',
                loadChildren: 'app/dashboard/dashboard.module#DashboardModule'
            }
        ]*/
    },
    {
        path: 'dca',
        component: DCAComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'DCA'
        }
    },
    {
        path: 'esi',
        component: ESIComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'ESI'
        }
    },
    {
        path: 'icd9',
        component: ICD9Component,
        canActivate: [AuthGuard],
        data: {
            title: 'ICD9'
        }
    },
    {
        path: 'revenuecode',
        component: RevenueCodeComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Revenue Code'
        }
    },
    {
        path: 'secretaryofstate',
        component: SecretaryOfStateComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Secretary of State'
        }
    },
    {
        path: 'nicb',
        component: NICBComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'NICB'
        }
    },
    {
        path: 'mbcoig',
        component: MBCOIGComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'MBC & OIG'
        }
    },
    {
        path: 'chiropractor',
        component: ChiropractorComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Chiropractor alert'
        }
    },
    {
        path: 'dnb',
        component: DNBComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'DNB'
        }
    },
    {
        path: 'gd',
        component: GDComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Gegraphic discrepancy'
        }
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
