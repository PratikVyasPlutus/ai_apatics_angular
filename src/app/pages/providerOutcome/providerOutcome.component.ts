import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Http, Response } from "@angular/http";
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { NouisliderModule } from 'ng2-nouislider';
//
import { ProviderOutcomeService } from '../../services/provider-outcome.service';

//declare function xml2json(xml, tab): any;
import { ModalDirective } from 'ng2-bootstrap/modal/modal.component';
import { SelectComponent } from 'ng2-select'
import { NvD3Module } from 'ng2-nvd3';

import 'd3';
import 'nvd3'
import { setTimeout } from 'timers';

declare var jQuery: any;
declare var d3: any;
@Component({
    templateUrl: 'providerOutcome.component.html',
    providers: [ProviderOutcomeService, NouisliderModule, NvD3Module],
    styleUrls: [
        '../../../../node_modules/nvd3/build/nv.d3.css'
    ],
    encapsulation: ViewEncapsulation.None
})

export class ProviderOutcomeComponent implements OnInit {
    @ViewChild('selCity') selCity: SelectComponent;
    @ViewChild('selZipCode') selZipCode: SelectComponent;
    @ViewChild('selICDCode') selICDCode: SelectComponent;
    @ViewChild('selSpeciality') selSpeciality: SelectComponent;
    @ViewChild('addressInfoModel') addressInfoModel;
    @ViewChild('sendSmsInfo') sendSmsInfo;
    activeFilters: any = {};
    addressSelectObj: any = {};
    sendSmsSelectObj: any = {};
    searchProviderSummaryText: string = "";
    cityList: any = [];
    zipCodeList: any = [];
    icdCodeList: any = [];
    specialityList: any = [];
    providerOutcomeData: any = [];
    someRange = [2, 10];
    paidChartOptions;
    paidChartData;
    isFirstAddressSelection: boolean = true;
    isShowLoad: boolean = false;

    public sendersList: any = [];

    constructor(http: Http, public providerOutcomeService: ProviderOutcomeService, private _location: Location) {
        this.initFilter();
    }

    ngOnInit() {
        //this.getAllProviderDataWithFilter();
        //this.getSideFilter();
    }

    ngAfterViewInit() {
        //console.log('selCity', this.selCity);
        //console.log('addressInfoModel', this.addressInfoModel);
        // this.selCity.opened.subscribe(res => {
        //     console.log('selCity open fire', res);
        // });
        // this.selCity.selected.subscribe(res => {
        //     console.log('selCity selected fire', res);
        // });
        this.getCityList();
    }

    initFilter() {
        this.activeFilters['mnp'] = [];
        this.activeFilters['work_day'] = [];
        this.activeFilters['modified_day'] = [];
        this.activeFilters['default'] = { city: "", zip: "", icd: "", speciality: "" };
    }

    selectAllProvider(ev) {
        //ev.target.checked
        // for (var i = 0; i < this.providerOutcomeData.length; i++) {
        //     this.providerOutcomeData[i].selected = true;
        // }
        this.providerOutcomeData.forEach(x => { x.selected = ev.target.checked })
    }

    changeMnpSelection(mnp) {
        let changedSelection = !mnp.selected;
        mnp.selected = changedSelection;
        //console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }
    changeWorkDaySelection(wd) {
        let changedSelection = !wd.selected;
        wd.selected = changedSelection;
        //console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    changeModifiedDaySelection(mjd) {
        let changedSelection = !mjd.selected;
        mjd.selected = changedSelection;
        //console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    onAgeChange(age, flag) {
        //console.log('call onAgeChange...');
    }

    setUpDayFilter(maxDay) {
        console.log('maxDay ', maxDay);
        var i = 0;
        var DayArrar = [];
        while (i < parseInt(maxDay)) {
            DayArrar.push({ start: i, end: (i + 10) });
            i += 10;
            console.log(i);
        }
        console.log('DayArrar', DayArrar);
        return DayArrar;
    }
    //------------------- address info model selection function --------------------
    getCityList() {
        this.providerOutcomeService.getCityList().subscribe(res => {
            //console.log('getCityList res....', res);
            this.cityList = res.data;
            //this.cityList = [{ id: 6, text: 'Birmingham' }, { id: 7, text: 'Bradford' }, { id: 26, text: 'Leeds' }, { id: 30, text: 'London' }, { id: 34, text: 'Manchester' }, { id: 47, text: 'Sheffield' }];
            this.addressInfoModel.show();
        }, err => {
            //console.error('getCityList error', err);
        });
    }

    getZipCodeListByCity(city) {
        if (city) {
            this.clearDataByEle(this.selZipCode);
            this.clearDataByEle(this.selICDCode);
            this.providerOutcomeService.getZipCodeList(city).subscribe(res => {
                //console.log('getZipCodeList res....', res);
                this.zipCodeList = res.data;
            }, err => {
                //console.error('getZipCodeList error', err);
            });
        }

    }

    getICDCodeListByzipCode(zipCode, cityName) {
        if (zipCode) {
            this.clearDataByEle(this.selICDCode);
            this.clearDataByEle(this.selSpeciality);
            this.providerOutcomeService.getICDCodeList(zipCode, cityName).subscribe(res => {
                //console.log('getZipCodeList res....', res);
                this.icdCodeList = res.data.icdList;
                this.specialityList = res.data.speciality;
            }, err => {
                //console.error('getZipCodeList error', err);
            });
        }

    }

    selectedCity(value) {
        //console.log('value', value.text);
        this.getZipCodeListByCity(value.text);
    }

    selectedZip(value: any) {
        //this.addressSelectObj['zipCode'] = value.text;
        //console.log('addressSelectObj', this.addressSelectObj);
        let cityName = (this.addressSelectObj.city && this.addressSelectObj.city.length > 0 && this.addressSelectObj.city[0].hasOwnProperty('id')) ? this.addressSelectObj.city[0].id : "";
        this.getICDCodeListByzipCode(value.text, cityName);
    }

    selectedICD(value) {
        this.clearDataByEle(this.selSpeciality);
    }

    selectedSpeciality(value) {
        this.clearDataByEle(this.selICDCode);
    }

    // clearData() {
    //     console.log('clearData this.select1', this.select1);
    //     var activeItems = this.select1.active;
    //     this.select1.active = [];
    //     this.select1.doEvent('removed', activeItems);
    // }

    clearDataByEle(ele) {
        let activeItems = ele.active;
        ele.clickedOutside();
        ele.active = [];
        //ele.itemObjects = [];
        ele.doEvent('removed', activeItems);
    }

    submitAddressModelData() {
        console.log('this.addressSelectObj', this.addressSelectObj);
        this.activeFilters['default'].city = (this.addressSelectObj.city && this.addressSelectObj.city.length > 0 && this.addressSelectObj.city[0].hasOwnProperty('id')) ? this.addressSelectObj.city[0].id : "";
        this.activeFilters['default'].zip = (this.addressSelectObj.zip && this.addressSelectObj.zip.length > 0 && this.addressSelectObj.zip[0].hasOwnProperty('id')) ? this.addressSelectObj.zip[0].id : "";
        this.activeFilters['default'].icd = (this.addressSelectObj.icd && this.addressSelectObj.icd.length > 0 && this.addressSelectObj.icd[0].hasOwnProperty('id')) ? this.addressSelectObj.icd[0].id : "";
        this.activeFilters['default'].speciality = (this.addressSelectObj.speciality && this.addressSelectObj.speciality.length > 0 && this.addressSelectObj.speciality[0].hasOwnProperty('id')) ? this.addressSelectObj.speciality[0].id : "";

        if (this.activeFilters['default'].city && this.activeFilters['default'].zip && (this.activeFilters['default'].icd || this.activeFilters['default'].speciality)) {
            this.addressInfoModel.hide();
            this.getSideFilter();
            this.getAllProviderDataWithFilter();
            this.isFirstAddressSelection = false;
        }
    }

    closeAddressInfoModel() {
        this.addressInfoModel.hide();
        if (this.isFirstAddressSelection) {
            this._location.back();
        }
    }

    changeAddressInfoModel() {
        this.addressInfoModel.show();
    }

    closeSMSInfoModel() {
        this.sendSmsInfo.hide();
    }

    openSendSMSModel() {
        this.sendSmsSelectObj = [];
        this.sendSmsInfo.show();
    }

    ShowMsg() {
        jQuery('#ProvideMsgScccess').show().delay(3500).fadeOut();
    }

    sendSMStoSenders() {
        console.log('this.sendSmsSelectObj', this.sendSmsSelectObj);
        let selectProviderList = this.providerOutcomeData.filter(x => { return x.selected; });
        console.log('selectProviderList', selectProviderList);
        if (this.sendSmsSelectObj.sender && this.sendSmsSelectObj.sender.length > 0 && selectProviderList.length > 0) {
            this.isShowLoad = true;
            this.providerOutcomeService.sendSMS(this.sendSmsSelectObj.sender, selectProviderList).subscribe(res => {
                //console.log('sendSMStoSenders res....', res);
                this.isShowLoad = false;
                this.providerOutcomeData.forEach(x => { x.selected = false });
                this.sendSmsSelectObj = [];
                this.ShowMsg();
                setTimeout(() => {
                    this.sendSmsInfo.hide();
                }, 5000);
            }, err => {
                //console.error('sendSMStoSenders error', err);
                this.isShowLoad = false;
                this.sendSmsInfo.hide();
            });
        }

    }
    ///----------------End---------------/////

    //---------Pagination Data-----------------////

    getAllProviderDataWithFilter() {
        //console.log('this.activeFilters', this.activeFilters);
        this.isShowLoad = true;
        this.providerOutcomeService.getAllProviderDataWithFilter(this.activeFilters).subscribe(res => {
            //console.log('getZipCodeList res....', res);
            this.providerOutcomeData = res.data;
            this.loadChart();
            this.isShowLoad = false;
        }, err => {
            //console.error('getZipCodeList error', err);
        });
    }

    getSideFilter() {
        this.providerOutcomeService.getSideFilter(this.activeFilters).subscribe(res => {
            this.activeFilters['mnp'] = res.side_bar_filter.mnp;
            this.activeFilters['work_day'] = res.side_bar_filter.work_day;
            this.activeFilters['modified_day'] = res.side_bar_filter.modified_day;
            this.sendersList = res.side_bar_filter.claimant;
            console.log('this.activeFilters', this.activeFilters);
        }, err => {
            //console.error('getZipCodeList error', err);
        });
    }

    loadChart() {
        var dollarFormat = function (d) { return '$' + d3.format(',f')(d) };

        this.paidChartOptions = {
            chart: {
                type: 'multiBarHorizontalChart',
                height: 200,
                showLegend: false, // to hide legend
                showControls: false, // to hide controls
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 50,
                    left: 100
                },
                x: function (d) { return d.label; },
                y: function (d) { return d.value; },
                showValues: true,
                valueFormat: function (d) {
                    var format = d3.format(",.2f");
                    return "$" + format(d);
                },
                multiTooltipTemplate: function (label) {
                    return label.datasetLabel + ': ' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                },
                //axis.tickFormat;
                axis: function (d) { return "$" + d; },
                duration: 1000,
                yAxis: {
                    axisLabel: 'TOTAL AMOUNT PAID TO PROVIDER',
                    showMaxMin: false,
                    tickFormat: dollarFormat,
                    // axisLabelDistance: 10,
                    //staggerLabels: true,
                },
                yDomain: [0, 2000]
            }
        }

        let paidData = {
            key: "TOTAL AMOUNT PAID TO PROVIDER",
            values: []
        }

        this.providerOutcomeData.forEach(element => {
            //console.log('element', element);
            paidData.values.push({
                "label": element.PROVIDER_NAME,
                "value": element.TOT_PAID_AMT
            });
        });

        this.paidChartData = [
            paidData
        ];


    }
}
