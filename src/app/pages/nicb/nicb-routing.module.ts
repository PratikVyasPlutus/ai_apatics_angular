import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NICBComponent } from './nicb.component';

const routes: Routes = [
    {
        path: '',
        component: NICBComponent,
        data: {
            title: 'NICB'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NICBRoutingModule { }
