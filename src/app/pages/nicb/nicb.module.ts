import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule } from '@angular/forms';

import { NICBComponent } from './nicb.component';
import { NICBRoutingModule } from './nicb-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [
        FormsModule,
        NICBRoutingModule,
        ChartsModule
    ],
    declarations: []
})
export class NICBModule { }
