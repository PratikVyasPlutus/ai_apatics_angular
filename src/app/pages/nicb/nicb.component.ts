import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FDAService } from '../../services/fda.service';
import { Observable } from 'rxjs';

interface IServerResponse {
    items: string[];
    total: number;
}

declare var jQuery: any;
@Component({
    templateUrl: 'nicb.component.html',
    providers: [FDAService]
})
export class NICBComponent implements OnInit {
    selectedProductId: string = "";
    selectedNICB: any = {};
    pageNumber: number = 1;
    recordsToFetch: number = 10;
    totalRecords: number = 0;
    resFacets: any = {};
    asyncNICB: Observable<any[]>;
    loading: boolean;
    searchNICBText: string = "";
    selectedPS: string[] = [];
    selectedPLT: string[] = [];
    selectedPLA: string[] = [];
    rersNICB: any[] = [];
    foundRecord: boolean = true;
    constructor(private zone: NgZone, private fdaService: FDAService, private _elmRef: ElementRef) {

    }

    searchNICBFacets() {
        this.fdaService.searchNICBFacets(this.searchNICBText)
            .subscribe(res => {
                if (res.success) {
                    this.zone.run(() => {
                        this.resFacets = res.response;
                    });
                }
            }
            , error => {
            });
    }

    changeSelectFacet(objFacet, index) {
        this.zone.run(() => {
            let changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    this.selectedPS.push(objFacet.name);
                else if (index == 2)
                    this.selectedPLT.push(objFacet.name);
                else if (index == 3)
                    this.selectedPLA.push(objFacet.name);
            }
            else {
                let arrayIndex: number = -1;
                if (index == 1) {
                    arrayIndex = this.selectedPS.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPS.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = this.selectedPLT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPLT.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = this.selectedPLA.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPLA.splice(arrayIndex, 1);
                }
            }
        });
        this.searchNICBRecordsWithFilters(1);
    }

    removeSelectedFacet(objFacet, index) {
        this.zone.run(() => {
            objFacet.selected = false;
            let arrayIndex: number = -1;
            if (index == 1) {
                arrayIndex = this.selectedPS.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPS.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = this.selectedPLT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPLT.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = this.selectedPLA.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPLA.splice(arrayIndex, 1);
            }
        });
        this.searchNICBRecordsWithFilters(1);
    }

    clearSelectedFacets() {
        this.selectedPS = [];
        this.selectedPLT = [];
        this.selectedPLA = [];
    }

    searchNICB() {
        this.clearSelectedFacets();
        this.searchNICBRecordsWithFilters(1);
        this.searchNICBFacets();
    }

    searchNICBRecordsWithFilters(pageNum: number) {
        this.pageNumber = pageNum;
        let selectedPSString: string = "";
        let selectedPLTString: string = "";
        let selectedPLAString: string = "";
        if (this.selectedPS && this.selectedPS.length > 0)
            selectedPSString = this.selectedPS.join();
        if (this.selectedPLT && this.selectedPLT.length > 0)
            selectedPLTString = this.selectedPLT.join();
        if (this.selectedPLA && this.selectedPLA.length > 0)
            selectedPLAString = this.selectedPLA.join();

        this.asyncNICB = this.fdaService.searchNICBRecordsWithFilter(this.searchNICBText, selectedPSString, selectedPLTString,
             selectedPLAString, this.pageNumber, this.recordsToFetch)
            .do((res: any) => {
                if (res.success) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.totalRecords;
                        this.rersNICB = res.response.results || [];
                        this.foundRecord = res.response.totalRecords > 0;
                    });
                }
                else {
                    this.zone.run(() => {
                        this.totalRecords = 0;
                        this.rersNICB = [];
                    });
                }
            })
            .map(res => res.success ? res.response.results : []);
    }

    ngOnInit(): void {
        this.searchNICBRecordsWithFilters(1);
        this.searchNICBFacets();
    }
}

/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals: string[], page: number): Observable<IServerResponse> {
    const perPage = 10;
    const start = (page - 1) * perPage;
    const end = start + perPage;

    return Observable
        .of({
            items: meals.slice(start, end),
            total: 100
        }).delay(1000);
}
