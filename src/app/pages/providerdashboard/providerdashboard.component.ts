import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef, ViewContainerRef, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { Http, Response } from "@angular/http";
import { SOLRService } from '../../services/solr.service';
import { ScoutService } from '../../services/scout.service';
import { FDAService } from '../../services/fda.service';
import { TwitterService } from '../../services/twitter.service';
import { Observable } from 'rxjs';
//import { Overlay, overlayConfigFactory } from 'angular2-modal';
//import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';


//import {CORE_DIRECTIVES} from '@angular/common';
import { Tab } from '../../shared/tabs/tab.directive';
import { Tabset } from '../../shared/tabs/tabset.component';
import { TabHeading } from '../../shared/tabs/tab-heading.directive';
import { DateUTCPipe } from '../../pipes/dateUTC.pipe';

import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { DomSanitizer } from '@angular/platform-browser';
import 'rxjs/Rx'

//import * as xml2js from 'xml2js';

//declare function xml2json(xml, tab): any;

interface IServerResponse {
    items: string[];
    total: number;
}

declare var jQuery: any;
@Component({

    templateUrl: 'providerdashboard.component.html',
    providers: [SOLRService, ScoutService, FDAService, TwitterService, DateUTCPipe]
})
export class ProviderDashboardComponent implements OnInit {
    private httpP;
    @ViewChild('moreInfoModal') moreInfoModal;
    @ViewChild('BusinessAtSubjectAddressSectionModal') BusinessAtSubjectAddressSectionModal;
    @ViewChild('LicenseSectionModal') LicenseSectionModal;
    @ViewChild('SSNAddressFraudSectionModal') SSNAddressFraudSectionModal;
    @ViewChild('QuickAnalysisFlagSectionModal') QuickAnalysisFlagSectionModal;
    @ViewChild('CriminalSectionModal') CriminalSectionModal;

    pageNumber: number = 1;
    recordsToFetch: number = 1;
    fetchLimit: number = 1;
    totalRecords: number = 6;
    resFacets: any = {};
    providerSummary: any[] = [];
    asyncProviderSummary: Observable<any[]>;
    loading: boolean;
    searchProviderSummaryText: string = "";
    selectedPN: string[] = [];
    selectedPS: string[] = [];
    selectedZC: string[] = [];
    selectedTIN: string[] = [];
    rersProviderSummary: any[] = [];
    resBillStatistics: any[] = [];
    resRedFlagAlerts: any[] = [];
    resChiropracticAlerts: any[] = [];
    foundRecord: boolean = true;
    scrollId: string = "";
    downloadJsonHref: any = "";
    resJsonResponse: any = {};
    caseOpened: boolean = false;
    caseStatus: string = "";
    constructor(private zone: NgZone, private solrService: SOLRService, private _elmRef: ElementRef,
        private router: Router, private route: ActivatedRoute, private sanitizer: DomSanitizer, private scoutService: ScoutService, http: Http,
        private fdaService: FDAService, private twitterService: TwitterService) {
        this.httpP = http;

    }

    exportCSV() {
        var options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            fields: ["TOTAL_PAID_AMOUNT", "PROFESSIONAL_LICENSE_ACTION", "ZIP_CODE", "STATE", "PROVIDER_LICENSE_STATUS", "PROVIDER_INDIVIDUAL_NPI",
                "NICB_ALERTS", "TAX_ID_NUMBER", "PARTY_LAST_NAME", "BUSINESS_NAME", "PROFESSIONAL_LICENSE_NUMBER", "PROVIDER_SPECIALTY",
                "AlertsDisciplinaryAction", "SUSPICIOUS_CONNECTIONS", "TOTAL_BILLED_AMOUNT", "CITY", "PARTY_FIRST_NAME", "STREET_ADDRESS",
                "AlertType", "AlertValue", "TOTAL_MEDICAL_BILLS", "BILL_YEAR"],
            fieldNames: ["Total Paid Amount", "Proffessional License Action", "Zip Code", "State", "Provider License Status", "Provider Individual NPI",
                "NICB Alerts", "Tax ID Number", "Party Last Name", "Business Name", "Professional License Number", "Provider Speciality",
                "Alerts Disciplinary Action", "Suspicious Connections", "Total Billed Amount", "City", "Party First Name", "Street Address",
                "Alert Name", "Alert Count", "Total Medical Bills", "Bill Year"]
        };
        let date = new Date();

        var objCSV: any = [];
        objCSV.push('Provider Summary');
        objCSV.push(JSON.parse(JSON.stringify(this.rersProviderSummary)));
        objCSV.push('Red Flag Alerts');
        objCSV.push(this.resRedFlagAlerts);
        objCSV.push('Bill Statistics');
        objCSV.push(this.resBillStatistics);

        delete objCSV[1][0]['BILL_SATISTICS'];
        delete objCSV[1][0]['RED_FLAG_ALERTS'];
        objCSV.forEach(element => {
            if (Object.prototype.toString.call(element) === '[object Array]') {
                element.forEach(element1 => {
                    for (var field in element1) {
                        if ('_version_,id,DNB_REPORT_LINK,LEXIS_NEXIS_REPORT,PROFILE_PICTURE'.indexOf(field) > -1) {
                            delete element1[field];
                        }
                    }
                });
            }
        });
        let res = new Angular2Csv(objCSV, 'Povider Summary - ' + date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate(), options);
        //console.info('res', res);
    }

    //dynamic Tabs
    public angular2TabsExample: Array<any> = [
        { title: 'Angular Tab 1', content: 'Angular 2 Tabs are navigable windows, each window (called tab) contains content', disabled: false, removable: true },
        { title: 'Angular Tab 2', content: 'generally we categorize contents depending on the theme', disabled: false, removable: true },
        { title: 'Angular Tab 3', content: 'Angular 2 Tabs Content', disabled: false, removable: true }
    ];
    //on select a tab do something
    public doOnTabSelect(currentTab: any) {
        console.log("doOnTabSelect" + currentTab);
    };
    //on remove Tab do something
    public removeThisTabHandler(tab: any) {
        console.log('Remove Tab handler' + tab);
    };

    searchProviderSummaryFacets() {
        this.solrService.searchProviderSummaryFacets(this.searchProviderSummaryText)
            .subscribe(res => {
                if (res.facet_counts) {
                    this.zone.run(() => {
                        let facetArray = [];
                        this.resFacets = {};
                        if (res.facet_counts.facet_fields) {
                            if (res.facet_counts.facet_fields["PARTY_FIRST_NAME"] && res.facet_counts.facet_fields["PARTY_FIRST_NAME"].length > 0) {
                                for (var index = 0; index < res.facet_counts.facet_fields["PARTY_FIRST_NAME"].length; index++) {
                                    if (res.facet_counts.facet_fields["PARTY_FIRST_NAME"][index + 1] > 0)
                                        facetArray.push({ name: res.facet_counts.facet_fields["PARTY_FIRST_NAME"][index], count: res.facet_counts.facet_fields["PARTY_FIRST_NAME"][index + 1] });
                                    index++;
                                }
                                this.resFacets.PARTY_FIRST_NAME = facetArray;
                            }

                            if (res.facet_counts.facet_fields["PROVIDER_SPECIALTY"] && res.facet_counts.facet_fields["PROVIDER_SPECIALTY"].length > 0) {
                                facetArray = [];
                                for (var index = 0; index < res.facet_counts.facet_fields["PROVIDER_SPECIALTY"].length; index++) {
                                    if (res.facet_counts.facet_fields["PROVIDER_SPECIALTY"][index + 1] > 0)
                                        facetArray.push({ name: res.facet_counts.facet_fields["PROVIDER_SPECIALTY"][index], count: res.facet_counts.facet_fields["PROVIDER_SPECIALTY"][index + 1] });
                                    index++;
                                }
                                this.resFacets.PROVIDER_SPECIALTY = facetArray;
                            }


                            if (res.facet_counts.facet_fields["ZIP_CODE"] && res.facet_counts.facet_fields["ZIP_CODE"].length > 0) {
                                facetArray = [];
                                for (var index = 0; index < res.facet_counts.facet_fields["ZIP_CODE"].length; index++) {
                                    if (res.facet_counts.facet_fields["ZIP_CODE"][index + 1] > 0)
                                        facetArray.push({ name: res.facet_counts.facet_fields["ZIP_CODE"][index], count: res.facet_counts.facet_fields["ZIP_CODE"][index + 1] });
                                    index++;
                                }
                                this.resFacets.ZIP_CODE = facetArray;
                            }


                            if (res.facet_counts.facet_fields["TAX_ID_NUMBER"] && res.facet_counts.facet_fields["TAX_ID_NUMBER"].length > 0) {
                                facetArray = [];
                                for (var index = 0; index < res.facet_counts.facet_fields["TAX_ID_NUMBER"].length; index++) {
                                    if (res.facet_counts.facet_fields["TAX_ID_NUMBER"][index + 1] > 0)
                                        facetArray.push({ name: res.facet_counts.facet_fields["TAX_ID_NUMBER"][index], count: res.facet_counts.facet_fields["TAX_ID_NUMBER"][index + 1] });
                                    index++;
                                }
                                this.resFacets.TAX_ID_NUMBER = facetArray;
                            }

                            this.route
                                .queryParams
                                .subscribe(params => {
                                    if (params.provider_firstname) {
                                        var provider_firstname = params.provider_firstname;
                                        var provider_lastname = params.provider_lastname;
                                        //console.log('provider_firstname', provider_firstname);
                                        //console.log('provider_lastname', provider_lastname);
                                        for (var key in this.resFacets.PARTY_FIRST_NAME) {
                                            //console.log('key', this.resFacets.PARTY_FIRST_NAME[key].name);
                                            if (this.resFacets.PARTY_FIRST_NAME[key].name == provider_firstname) {
                                                //console.log('INNN', this.resFacets.PARTY_FIRST_NAME[key]);
                                                this.changeSelectFacet(this.resFacets.PARTY_FIRST_NAME[key], 1);
                                            }
                                        }
                                    }
                                });
                        }
                    });
                }
            }
            , error => {
            });
    }

    getProviderFacets() {
        this.solrService.getProviderFacets(this.searchProviderSummaryText)
            .subscribe(res => {
                if (res.fields) {
                    this.zone.run(() => {
                        this.resFacets = {};
                        this.resFacets.PARTY_FIRST_NAME = res.fields.partyFirstName;
                        this.resFacets.PROVIDER_SPECIALTY = res.fields.providerSpeciality;
                        this.resFacets.ZIP_CODE = res.fields.zipCode;
                        this.resFacets.TAX_ID_NUMBER = res.fields.taxIdNumber;
                    });
                }
            }
            , error => {
                if (error.toString().indexOf('401') >= 0) {
                    this.getProviderFacetsWithNewToken();
                }
            });
    }

    getProviderFacetsWithNewToken() {
        this.solrService.getAuthToken()
            .subscribe(res => {
                if (res.access_token) {
                    window.localStorage.setItem('apaticsAuthToken', res.access_token);
                    this.solrService.getProviderFacets(this.searchProviderSummaryText)
                        .subscribe(res => {
                            if (res.fields) {
                                this.zone.run(() => {
                                    this.resFacets = {};
                                    this.resFacets.PARTY_FIRST_NAME = res.fields.partyFirstName;
                                    this.resFacets.PROVIDER_SPECIALTY = res.fields.providerSpeciality;
                                    this.resFacets.ZIP_CODE = res.fields.zipCode;
                                    this.resFacets.TAX_ID_NUMBER = res.fields.taxIdNumber;
                                });
                            }
                        }
                        , error => {
                        });
                }
            }
            , error => {
            });
    }

    changeSelectFacet(objFacet, index) {
        this.zone.run(() => {
            let changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    this.selectedPN.push(objFacet.name);
                else if (index == 2)
                    this.selectedPS.push(objFacet.name);
                else if (index == 3)
                    this.selectedZC.push(objFacet.name);
                else if (index == 4)
                    this.selectedTIN.push(objFacet.name);
            }
            else {
                let arrayIndex: number = -1;
                if (index == 1) {
                    arrayIndex = this.selectedPN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPN.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = this.selectedPS.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPS.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = this.selectedZC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedZC.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = this.selectedTIN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedTIN.splice(arrayIndex, 1);
                }
            }
        });
        this.searchProviderSummaryRecordsWithFilters(1);
        //this.getProviderRecordsWithFilters();
    }

    removeSelectedFacet(objFacet, index) {
        this.zone.run(() => {
            objFacet.selected = false;
            let arrayIndex: number = -1;
            if (index == 1) {
                arrayIndex = this.selectedPN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPN.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = this.selectedPS.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPS.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = this.selectedZC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedZC.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = this.selectedTIN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedTIN.splice(arrayIndex, 1);
            }
        });
        this.searchProviderSummaryRecordsWithFilters(1);
        //this.getProviderRecordsWithFilters();
    }

    clearSelectedFacets() {
        this.selectedPN = [];
        this.selectedPS = [];
        this.selectedZC = [];
        this.selectedTIN = [];
    }

    searchProviderSummary() {
        this.clearSelectedFacets();
        this.searchProviderSummaryRecordsWithFilters(1);
        this.searchProviderSummaryFacets();
        //this.getProviderRecordsWithFilters();//new aptics api
        //this.getProviderFacets();//new aptics api
    }

    getProviderRecordsWithFilters() {
        let fLimit: number = 0;
        let selectedPNString: string = JSON.parse(JSON.stringify(this.searchProviderSummaryText));
        //alert('j' + selectedPNString + 'j');
        if (this.selectedPN && this.selectedPN.length > 0)
            selectedPNString += (selectedPNString.trim().length > 0 ? ' AND ' : '') + 'partyFirstName: ' + this.selectedPN.join(' OR ');
        if (this.selectedPS && this.selectedPS.length > 0)
            selectedPNString += (selectedPNString.trim().length > 0 ? ' AND ' : '') + 'providerSpeciality: ' + this.selectedPS.join(' OR ');
        if (this.selectedZC && this.selectedZC.length > 0)
            selectedPNString += (selectedPNString.trim().length > 0 ? ' AND ' : '') + 'zipCode: ' + this.selectedZC.join(' OR ');
        if (this.selectedTIN && this.selectedTIN.length > 0)
            selectedPNString += (selectedPNString.trim().length > 0 ? ' AND ' : '') + 'taxIdNumber: ' + this.selectedTIN.join(' OR ');

        //alert(selectedPNString);
        //this.fetchLimit = selectedPNString.trim().length > 0 ? 6 : this.fetchLimit;
        this.asyncProviderSummary = this.solrService.getProviderRecordsWithFilter(selectedPNString, this.scrollId, selectedPNString.trim().length > 0 ? 6 : this.fetchLimit)
            .do(res => {
                this.scrollId = '';
                if (res.scrollId) {
                    this.scrollId = res.scrollId;
                }
                this.totalRecords = selectedPNString.trim().length > 0 ? res.providers.length : 6;
                fLimit = res.providers.length - 1;
                if (res.providers && res.providers.length > 0) {
                    this.bindBillStatistics(res.providers[fLimit].taxIdNumber);
                    this.bindRedFlagAlerts(res.providers[fLimit].taxIdNumber);
                    this.bindChiropracticAlerts(res.providers[fLimit].professionalLicenseNumber);
                }
                // console.info('this.scrollId', this.scrollId);
                // console.info('res', res);
            }).map(res => {
                fLimit = res.providers.length - 1;
                if (res.providers && res.providers.length > 0) {
                    let providerSummary: any = {};
                    providerSummary.PARTY_FIRST_NAME = res.providers[fLimit].partyFirstName;
                    providerSummary.PARTY_LAST_NAME = res.providers[fLimit].partyLastName;
                    providerSummary.NICB_ALERTS = res.providers[fLimit].nicbAlerts;
                    providerSummary.DNB_REPORT_LINK = res.providers[fLimit].dnbReportLink.replace('prototype', 'assets');
                    providerSummary.LEXIS_NEXIS_REPORT = res.providers[fLimit].lexisNexisReport.replace('prototype', 'assets');
                    providerSummary.STREET_ADDRESS = res.providers[fLimit].streetAddress || '';
                    providerSummary.STATE = res.providers[fLimit].state;
                    providerSummary.ZIP_CODE = res.providers[fLimit].zipCode || '';
                    providerSummary.TAX_ID_NUMBER = res.providers[fLimit].taxIdNumber || '';
                    providerSummary.PROFESSIONAL_LICENSE_NUMBER = res.providers[fLimit].professionalLicenseNumber || '';
                    providerSummary.PROVIDER_LICENSE_STATUS = res.providers[fLimit].providerLicenseStatus || '';
                    providerSummary.PROVIDER_INDIVIDUAL_NPI = res.providers[fLimit].providerIndividualNPI;
                    providerSummary.STATE = res.providers[fLimit].state;
                    providerSummary.STATE = res.providers[fLimit].state;
                    providerSummary.STATE = res.providers[fLimit].state;
                    providerSummary.PROFILE_PICTURE = res.providers[fLimit].profilePicture.replace('prototype', 'assets');
                    return [providerSummary];
                } else {
                    return [];
                }
            });
    }

    searchProviderSummaryRecordsWithFilters(pageNum: number) {
        this.pageNumber = pageNum;
        let selectedPNString: string = "";
        let selectedPSString: string = "";
        let selectedZCString: string = "";
        let selectedTINString: string = "";
        if (this.selectedPN && this.selectedPN.length > 0)
            selectedPNString = this.selectedPN.join('"OR"');
        if (this.selectedPS && this.selectedPS.length > 0)
            selectedPSString = this.selectedPS.join('"OR"');
        if (this.selectedZC && this.selectedZC.length > 0)
            selectedZCString = this.selectedZC.join('"OR"');
        if (this.selectedTIN && this.selectedTIN.length > 0)
            selectedTINString = this.selectedTIN.join('"OR"');


        this.asyncProviderSummary = this.solrService.searchProviderSummaryRecordsWithFilter(this.searchProviderSummaryText, selectedPNString, selectedPSString,
            selectedZCString, selectedTINString, this.pageNumber, this.recordsToFetch)
            .do((res: any) => {
                //console.info('res.response', res.response);
                if (res.response) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.numFound;
                        this.rersProviderSummary = res.response.docs || [];
                        this.foundRecord = res.response.numFound > 0;
                    });
                    if (this.rersProviderSummary && this.rersProviderSummary.length > 0) {
                        //generate res for download json
                        this.generatePersonalInfoResponse();
                        if (this.rersProviderSummary[0].PARTY_FIRST_NAME && this.rersProviderSummary[0].PARTY_FIRST_NAME.toLowerCase().indexOf('medical') >= 0) {
                            this.rersProviderSummary[0].PARTY_FIRST_NAME = 'Rebecca';
                            this.rersProviderSummary[0].PARTY_LAST_NAME = 'Hall';
                        }

                        this.bindBillStatistics(this.rersProviderSummary[0].TAX_ID_NUMBER);
                        this.bindRedFlagAlerts(this.rersProviderSummary[0].TAX_ID_NUMBER);
                        this.bindChiropracticAlerts(this.rersProviderSummary[0].PROFESSIONAL_LICENSE_NUMBER);

                        //this.checkImportedCase(this.rersProviderSummary[0].TAX_ID_NUMBER);
                    }

                }
                else {
                    this.zone.run(() => {
                        this.totalRecords = 0;
                        this.rersProviderSummary = [];
                        this.resBillStatistics = [];
                        this.resRedFlagAlerts = [];
                        this.resChiropracticAlerts = [];
                    });
                }
                this
            })
            .map(res => res.response ? res.response.docs : []);
    }

    bindBillStatistics(taxIDNumber: number) {
        this.resBillStatistics = [];
        this.solrService.getProviderSummaryBillStatistics(taxIDNumber)
            .subscribe(res => {
                if (res.response && res.response.docs) {
                    this.resBillStatistics = res.response.docs;

                    //generate res for download json
                    this.generateBillStatisticsResponse();
                }
            }
            , error => {
            });
    }

    bindRedFlagAlerts(taxIDNumber: number) {
        this.resRedFlagAlerts = [];
        this.solrService.getProviderSummaryRedFlagAlerts(taxIDNumber)
            .subscribe(res => {
                if (res.response && res.response.docs) {
                    this.resRedFlagAlerts = res.response.docs;

                    //generate res for download json
                    this.generateRedflagAlertsResponse();
                }
            }
            , error => {
            });
    }

    bindChiropracticAlerts(licenseNumber: number) {
        this.resChiropracticAlerts = [];
        //licenseNumber = 25373;
        this.solrService.getProviderSummaryChiropracticAlerts(licenseNumber)
            .subscribe(res => {
                if (res.response && res.response.docs) {
                    this.resChiropracticAlerts = res.response.docs;
                }
            }
            , error => {
            });
    }

    viewRedFlagAlertDetails(taxIDNumber: number, alertType: string) {
        switch (alertType.replace(/ /g, "").replace(/_/g, "").toLowerCase()) {
            case "upcoding":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 1);
                break;
            case "excessivetesting":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 2);
                break;
            case "deniedclaims":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 3);
                break;
            case "duplicatebilling":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 4);
                break;
            case "excessivedmeprescription":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 5);
                break;
            case "excessimagingservices":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 6);
                break;
            case "clusterbilling":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 7);
                break;
            case "painpumps":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 8);
                break;
            case "autologoustransfusion":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 9);
                break;
            default:
                break;
        }
    }

    navigateRedFlagAlertDetails(pagename, taxIDNumber, pageIndex) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "taxIdNumber": taxIDNumber,
                "p": pageIndex
            }
        };
        this.router.navigate(['../' + pagename], navigationExtras);
    }

    generatePersonalInfoResponse() {
        this.resJsonResponse = {};
        if (this.rersProviderSummary && this.rersProviderSummary.length > 0) {
            var objRes = this.rersProviderSummary[0];
            var objPersonalInfo = {};
            objPersonalInfo["Name"] = objRes.PARTY_FIRST_NAME + ' ' + objRes.PARTY_LAST_NAME;
            objPersonalInfo["Speciality"] = "Chiropractor"//objRes.PARTY_FIRST_NAME;
            objPersonalInfo["NICB Alerts"] = objRes.NICB_ALERTS;
            objPersonalInfo["Address"] = objRes.STREET_ADDRESS + ' ' + objRes.CITY + ' ' + objRes.STATE + ' ' + objRes.ZIP_CODE;
            objPersonalInfo["TIN"] = objRes.TAX_ID_NUMBER;
            objPersonalInfo["Licence No"] = objRes.PROFESSIONAL_LICENSE_NUMBER;
            objPersonalInfo["Licence Status"] = objRes.PROVIDER_LICENSE_STATUS;
            objPersonalInfo["NPI"] = objRes.PROVIDER_INDIVIDUAL_NPI;

            this.resJsonResponse["Personal Info"] = objPersonalInfo;
        }
        this.generateDownloadJsonUri();
    }

    generateBillStatisticsResponse() {
        if (this.resBillStatistics && this.resBillStatistics.length > 0) {
            var objBillStatistics = [];
            this.resBillStatistics.forEach(objBS => {
                objBillStatistics.push({
                    "Year": objBS.BILL_YEAR, "Bill Count": objBS.TOTAL_MEDICAL_BILLS,
                    "Billed Amount": objBS.TOTAL_BILLED_AMOUNT, "Paid Amount": objBS.TOTAL_PAID_AMOUNT
                });
            });

            this.resJsonResponse["Bill Statistics"] = objBillStatistics;
        }
        this.generateDownloadJsonUri();
    }

    generateRedflagAlertsResponse() {
        if (this.resRedFlagAlerts && this.resRedFlagAlerts.length > 0) {
            var objRedFlagAlerts = {};
            this.resRedFlagAlerts.forEach(objRFA => {
                objRedFlagAlerts[objRFA.AlertType] = objRFA.AlertValue;
            });

            this.resJsonResponse["Red Flag Alerts"] = objRedFlagAlerts;
        }
        this.generateDownloadJsonUri();
    }

    generateDownloadJsonUri() {
        var theJSON = JSON.stringify(this.resJsonResponse);
        var uri = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
        this.downloadJsonHref = uri;
    }

    caseTaxIDNumber: number;
    caseTaskId: string = '';
    caseFirmId: string = '';
    checkImportedCase(taxIDNumber) {
        this.caseTaxIDNumber = taxIDNumber;
        this.caseOpened = false;
        this.caseStatus = '';
        this.caseTaskId = '';
        this.startCaseStatusInterval(taxIDNumber);

        this.fdaService.getCaseStatus(taxIDNumber)
            .subscribe(res => {
                if (res.success && res.response) {
                    if (res.response.status) {
                        this.caseOpened = true;
                        this.caseStatus = res.response.status;
                        this.caseTaskId = res.response.taskId;
                        this.caseFirmId = res.response.firmId;
                    }
                }
            }
            , error => {
            });
    }

    startCaseStatusInterval(taxIDNumber) {
        this.caseOpened = false;
        this.caseStatus = '';
        //this.setCaseStatusInterval(taxIDNumber);
        // this.caseStatusTimer
        //     .subscribe(res => {
        //         if (res.success && res.response) {
        //             if (res.response.status) {
        //                 this.caseOpened = true;
        //                 this.caseStatus = res.response.status;
        //             }
        //         }
        //     }
        //     , error => { 
        //     });
    }

    caseStatusTimer: any;
    setCaseStatusInterval(taxIDNumber) {
        if (this.caseStatusTimer)
            //this.caseStatusTimer.unsubscribe();

            this.caseStatusTimer = Observable.interval(5000)
                .flatMap(() => {
                    return this.fdaService.getCaseStatus(taxIDNumber);
                })
                .subscribe((res: any) => {
                    if (res.success && res.response) {
                        if (res.response.status) {
                            this.caseOpened = true;
                            this.caseStatus = res.response.status;
                        }
                        else {
                            this.caseOpened = false;
                            this.caseStatus = '';
                        }
                    }
                }
                , error => {
                });
    }

    importCaseInScout(taxIdNumber) {
        let objData: any = {};
        objData.providerBasic = this.rersProviderSummary[0];
        objData.billStatistics = this.resBillStatistics;
        objData.redFlagAlerts = this.resRedFlagAlerts;
        objData.chiropractorAlerts = this.resChiropracticAlerts;
        this.scoutService.importCase(objData)
            .subscribe(res => {
                console.log('resSoap', res);
                let resXMLString: string = res;
                let resSubString: string = "";
                resSubString = this.parseXMLTag(resXMLString, "Success");;
                let resSuccess: boolean = resSubString == "true";

                console.log("resSuccess", resSuccess);
                if (resSuccess) {
                    resSubString = this.parseXMLTag(resXMLString, "ScoutID");
                    var resScoutId = parseInt(resSubString);
                    console.log("resScoutId", resScoutId);
                    var objJSONData = { 'taxIdNumber': taxIdNumber, "scoutId": resScoutId, "status": "Open" };

                    this.caseOpened = true;
                    this.caseStatus = "Open";
                    this.saveCase(objJSONData);
                    //this.changeCaseStatus(resScoutId, 'Pending');
                }
                else {
                    resSubString = this.parseXMLTag(resXMLString, "Message");
                    var resErrorMessage = resSubString;
                    console.log("resErrorMessage", resErrorMessage);
                    alert(resErrorMessage);

                    // var objJSONData = { 'taxIdNumber': taxIdNumber, "scoutId": 123, "status": "pending" };
                    // this.saveCase(objJSONData);
                }
            }
            , error => {
                console.log('error', error);
            });

        // xml2js.parseString('<foo>bar</foo>', function (err, result) {
        //     console.log(result);
        // });
    }

    generateCase(objProvider) {
        let objData: any = {};
        objData.taxIdNumber = objProvider.TAX_ID_NUMBER;
        objData.partyFirstName = objProvider.PARTY_FIRST_NAME;
        objData.partyLastName = objProvider.PARTY_LAST_NAME;
        objData.profilePicture = objProvider.PROFILE_PICTURE;
        objData.providerSpecialty = objProvider.PROVIDER_SPECIALTY;
        objData.professionalLicenseNumber = objProvider.PROFESSIONAL_LICENSE_NUMBER;
        objData.professionalLicenseAction = objProvider.PROFESSIONAL_LICENSE_ACTION;
        objData.providerLicenseStatus = objProvider.PROVIDER_LICENSE_STATUS;
        objData.providerIndividualNPI = objProvider.PROVIDER_INDIVIDUAL_NPI;
        objData.businessName = objProvider.BUSINESS_NAME;
        objData.nICBAlerts = objProvider.NICB_ALERTS;
        objData.suspiciousConnections = objProvider.SUSPICIOUS_CONNECTIONS;
        objData.alertsDisciplinaryAction = objProvider.AlertsDisciplinaryAction;
        objData.totalPaidAmount = objProvider.TOTAL_PAID_AMOUNT;
        objData.totalBilledAmount = objProvider.TOTAL_BILLED_AMOUNT;
        objData.streetAddress = objProvider.STREET_ADDRESS;
        objData.city = objProvider.CITY;
        objData.state = objProvider.STATE;
        objData.zipCode = objProvider.ZIP_CODE;
        objData.dNBReportLink = objProvider.DNB_REPORT_LINK;
        objData.lexisNexisReportLink = objProvider.LEXIS_NEXIS_REPORT;

        let jsonStringifyObject: string = "";
        if (this.resRedFlagAlerts && this.resRedFlagAlerts.length > 0)
            jsonStringifyObject = JSON.stringify(this.resRedFlagAlerts);
        objData.redFlagAlerts = jsonStringifyObject;
        jsonStringifyObject = "";
        if (this.resBillStatistics && this.resBillStatistics.length > 0)
            jsonStringifyObject = JSON.stringify(this.resBillStatistics);
        objData.billStatistics = jsonStringifyObject;

        this.fdaService.saveCase(objData)
            .subscribe(res => {
                if (res.success) {
                    this.caseOpened = true;
                    this.caseStatus = res.response.status;
                    this.caseTaskId = res.response.taskId;
                    this.caseFirmId = res.response.firmId;
                    this.openCaseDetailInCaseManagement();
                }
            }
            , error => {
            });
    }

    parseXMLTag(resXMLString, tagName) {
        let resTagValue: string = "";
        let startIndex: number = -1;
        let endIndex: number = -1;
        startIndex = resXMLString.indexOf('<' + tagName + '>');
        endIndex = resXMLString.indexOf('</' + tagName + '>');

        if (startIndex >= 0 && endIndex >= 0)
            resTagValue = resXMLString.substring(startIndex, endIndex).replace("<" + tagName + ">", "");

        return resTagValue;
    }

    saveCase(objJSONData) {
        this.fdaService.saveCaseStatus(objJSONData)
            .subscribe(res => {
                console.log('resSaveCase', res);
            }
            , error => {
            });
    }

    changeCaseStatus(scoutID, status) {
        this.caseOpened = false;
        this.caseStatus = '';
        this.scoutService.changeCaseStatus(scoutID, status)
            .subscribe(res => {
                if (res.success && res.response) {
                    if (res.response.id > 0) {
                        this.caseOpened = true;
                        this.caseStatus = res.response.status;
                    }
                }
            }
            , error => {
            });
    }

    openCaseDetailInCaseManagement() {
        //var pageUrl = 'http://localhost/ApaticsTasksLive/portal/tasks.aspx';
        var pageUrl = 'http://casemgmt.apatics.net/portal/tasks.aspx'

        window.open(pageUrl + '?tid=' + this.caseTaskId + '&fid=' + this.caseFirmId, '_blank');
    }

    twitterAuccessToken: string = '';
    gerAccessToken() {
        this.twitterService.getAccessToken()
            .subscribe(res => {
                console.log('resAccessToken', res);
            }
            , error => {
            });
    }

    resSocial: any = {};
    getTwitterData() {
        this.fdaService.getTwitterHashtagData()
            .subscribe(res => {
                if (res.success && res.response) {
                    if (res.response.healthCareFrauds) {
                        this.resSocial.twitterHealthCareFraud = res.response.healthCareFrauds;
                        if (this.resSocial.twitterHealthCareFraud && this.resSocial.twitterHealthCareFraud.length > 0 && this.resSocial.twitterHealthCareFraud.length < 6) {
                            var twitterHealthCareArrayLength = this.resSocial.twitterHealthCareFraud.length;
                            for (var index = 0; index < 12 - twitterHealthCareArrayLength; index++) {
                                this.resSocial.twitterHealthCareFraud.push(this.resSocial.twitterHealthCareFraud[index]);
                            }
                        }
                    }
                    if (res.response.medicalBillings) {
                        this.resSocial.twitterMedicalBillings = res.response.medicalBillings;
                        if (this.resSocial.twitterMedicalBillings && this.resSocial.twitterMedicalBillings.length > 0 && this.resSocial.twitterMedicalBillings.length < 12) {
                            var medicalBillingsArrayLength = this.resSocial.twitterMedicalBillings.length;
                            for (var index = 0; index < 12 - medicalBillingsArrayLength; index++) {
                                this.resSocial.twitterMedicalBillings.push(this.resSocial.twitterMedicalBillings[index]);
                            }
                        }
                    }

                    console.log('this.resSocial', this.resSocial);
                }
            }
            , error => {
            });
    }

    getDialogFeedSocialWall() {
        this.twitterService.getDialogFeedData()
            .subscribe(res => {
                console.log('resSocialWall', res);
                if (res.news_feed && res.news_feed.posts && res.news_feed.posts.post && res.news_feed.posts.post.length > 0) {
                    var resPosts = res.news_feed.posts.post;
                    var resTwitter = [];
                    var resInstagram = [];
                    var resRssFeed = [];
                    res.news_feed.posts.post.forEach(objPost => {
                        if (objPost.source && objPost.source.name == 'twitter') {
                            resTwitter.push(objPost);
                        }
                        else if (objPost.source && objPost.source.name == 'instagram') {
                            resInstagram.push(objPost);
                        }
                        else if (objPost.source && objPost.source.name == 'rss') {
                            resRssFeed.push(objPost);
                        }
                    });

                    if (resTwitter.length > 0)
                        this.resSocial.twitter = resTwitter;
                    if (resInstagram.length > 0)
                        this.resSocial.instagram = resInstagram;
                    if (resRssFeed.length > 0)
                        this.resSocial.rssFeed = resRssFeed;

                    if (this.resSocial.twitter && this.resSocial.twitter.length > 0 && this.resSocial.twitter.length < 18) {
                        var twitterArrayLength = this.resSocial.twitter.length;
                        for (var index = 0; index < 18 - this.resSocial.twitter.length; index++) {
                            this.resSocial.twitter.push(this.resSocial.twitter[index]);
                        }
                    }
                    if (this.resSocial.instagram && this.resSocial.instagram.length > 0 && this.resSocial.instagram.length < 12) {
                        var instagramArrayLength = this.resSocial.instagram.length;
                        for (var index = 0; index < 12 - instagramArrayLength; index++) {
                            this.resSocial.instagram.push(this.resSocial.instagram[index]);
                        }
                    }
                    if (this.resSocial.rssFeed && this.resSocial.rssFeed.length > 0 && this.resSocial.rssFeed.length < 24) {
                        var rssFeedArrayLength = this.resSocial.rssFeed.length;
                        for (var index = 0; index < 24 - rssFeedArrayLength; index++) {
                            this.resSocial.rssFeed.push(this.resSocial.rssFeed[index]);
                        }
                    }
                    console.log('this.resSocial', this.resSocial);
                }
            }
            , error => {
            });
    }

    ngOnInit(): void {
        this.searchProviderSummaryRecordsWithFilters(1);
        //this.getProviderRecordsWithFilters();//new aptics api
        this.searchProviderSummaryFacets();
        //this.getProviderFacets();//new aptics api
        //this.testSoutService();
        //console.log(xml2json("<foo>bar</foo>",""));
        this.getTwitterData();
        //this.getDialogFeedSocialWall();
        this.ongetmoreinfo('subjectSection');
        this.ongetmoreinfo('SSNAddressFraudSection');
        this.ongetmoreinfo('BusinessAtSubjectAddressSection');
        this.ongetmoreinfo('QuickAnalysisFlagSection');
        this.ongetmoreinfo('LicenseSection');
    }

    ngOnDestroy() {
        //this.caseStatusTimer.unsubscribe();
    }
    matchdata: any;
    responcesubjectInfo: any;
    responceSubjectAddressInfo: any;
    responceLicenseSectionInfo: any;
    responceSSNAddressFraudSectionInfo: any;
    responceQuickAnalysisFlagSectionInfo: any = [];
    tmpresponceQuickAnalysisFlagSectionInfo: any;
    responceCriminalSectionInfo: any;
    ongetmoreinfo(sectionName: string): void {

        return this.httpP.get("assets/json/bryan.json")
            .subscribe(matchdata => {
                var resJson = matchdata.json();
                //console.log("hello");
                if (sectionName === 'subjectSection') {
                    this.responcesubjectInfo = resJson["pr:PersonReportDetails"]['ClearSectionResults'][0]['SectionDetails']["ns2:SubjectSection"]["SubjectRecord"]["PersonAKAInfo"];
                    //this.moreInfoModal.open();
                }
                if (sectionName === 'BusinessAtSubjectAddressSection') {
                    this.responceSubjectAddressInfo = resJson["pr:PersonReportDetails"]['ClearSectionResults'][6]['SectionDetails']["ns2:BusinessAtSubjectAddressSection"]["BusinessAtSubjectAddressRecord"];
                    //this.BusinessAtSubjectAddressSectionModal.open();
                }
                if (sectionName === 'LicenseSection') {
                    this.responceLicenseSectionInfo = resJson["pr:PersonReportDetails"]['ClearSectionResults'][4]['SectionDetails']["ns2:LicenseSection"]["ProfLicenseRecord"];
                    //this.LicenseSectionModal.open();
                }
                if (sectionName === 'SSNAddressFraudSection') {
                    this.responceSSNAddressFraudSectionInfo = resJson["pr:PersonReportDetails"]['ClearSectionResults'][1]['SectionDetails']["ns2:SSNAddressFraudSection"]["SSNAddressFraudRecord"];
                    //this.SSNAddressFraudSectionModal.open();
                }
                if (sectionName === 'QuickAnalysisFlagSection') {
                    this.tmpresponceQuickAnalysisFlagSectionInfo = resJson["pr:PersonReportDetails"]['ClearSectionResults'][3]['SectionDetails']["ns2:QuickAnalysisFlagSection"]["QuickAnalysisFlagRecord"];
                    this.tmpresponceQuickAnalysisFlagSectionInfo.forEach(element1 => {
                        for (var field in element1["RiskFlags"]) {
                            //console.log(field); console.log(element1);
                            if (element1["RiskFlags"][field] === 'Yes' || element1["RiskFlags"][field] === 'Yes') {
                                this.responceQuickAnalysisFlagSectionInfo.push({ 'riskname': field, 'riskvalue': element1["RiskFlags"][field] });
                            }

                            //this.responceQuickAnalysisFlagSectionInfo["riskvalue"] = element1;
                        }
                    });
                    // this.QuickAnalysisFlagSectionModal.open();
                }
                if (sectionName === 'CriminalSection') {
                    this.responceCriminalSectionInfo = resJson["pr:PersonReportDetails"]['ClearSectionResults'][5]['SectionDetails']["ns2:CriminalSection"]["CriminalExpansionRecord"];
                    this.CriminalSectionModal.open();
                }
                console.log(this.responceQuickAnalysisFlagSectionInfo);

            });
    }

}

/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals: string[], page: number): Observable<IServerResponse> {
    const perPage = 10;
    const start = (page - 1) * perPage;
    const end = start + perPage;

    return Observable
        .of({
            items: meals.slice(start, end),
            total: 100
        }).delay(1000);
}
