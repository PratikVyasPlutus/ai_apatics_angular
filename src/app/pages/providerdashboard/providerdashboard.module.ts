import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule } from '@angular/forms';

import { ProviderDashboardComponent } from './providerdashboard.component';
import { ProviderDashboardRoutingModule } from './providerdashboard-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [ProviderDashboardRoutingModule],
    declarations: []
})
export class ProviderDashboardModule { }
