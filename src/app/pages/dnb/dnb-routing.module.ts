import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DNBComponent } from './dnb.component';

const routes: Routes = [
    {
        path: '',
        component: DNBComponent,
        data: {
            title: 'DNB'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DNBRoutingModule { }
