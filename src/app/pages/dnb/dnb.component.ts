import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser'

declare var jQuery: any;
@Component({
    templateUrl: 'dnb.component.html'
})
export class DNBComponent {

    pdfSrc: string = '/assets/pdf/DNB_364667645.pdf';
    pageurl: SafeResourceUrl;

    // or pass options as object
    // pdfSrc: any = {
    //   url: './pdf-test.pdf',
    //   withCredentials: true,
    //// httpHeaders: { // cross domain
    ////   'Access-Control-Allow-Credentials': true
    //// }
    // };

    page: number = 1;
    rotation: number = 0;
    zoom: number = 1.0;
    originalSize: boolean = false;
    showAll: boolean = true;
    pdf: any;
    renderText: boolean = true;

    constructor(private domSanitizer: DomSanitizer) {
        this.afterLoadComplete = this.afterLoadComplete.bind(this);
    }

    incrementPage(amount: number) {
        this.page += amount;
    }

    incrementZoom(amount: number) {
        this.zoom += amount;
    }

    rotate(angle: number) {
        this.rotation += angle;
    }

    /**
     * Render PDF preview on selecting file
     */
    onFileSelected() {
        var $img: any = document.querySelector('#file');

        if (typeof (FileReader) !== 'undefined') {
            var reader = new FileReader();

            reader.onload = (e: any) => {
                this.pdfSrc = e.target.result;
            };

            reader.readAsArrayBuffer($img.files[0]);
        }
    }

    /**
     * Get pdf information after it's loaded
     * @param pdf
     */
    afterLoadComplete(pdf: any) {
        this.pdf = pdf;
    }

    ngOnInit() {
        this.pageurl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.pdfSrc);
    }
}
