import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { DNBComponent } from './dnb.component';
import { DNBRoutingModule } from './dnb-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [
        FormsModule,
        DNBRoutingModule
    ],
    declarations: []
})
export class DNBModule { }
