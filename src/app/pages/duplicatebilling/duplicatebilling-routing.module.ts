import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DuplicateBillingComponent } from './duplicatebilling.component';

const routes: Routes = [
    {
        path: '/:taxIdNumber',
        component: DuplicateBillingComponent,
        data: {
            title: 'Duplicate Billing'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DuplicateBillingRoutingModule { }
