import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef, ViewContainerRef, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SOLRService } from '../../services/solr.service';
import { Observable } from 'rxjs';
//import {CORE_DIRECTIVES} from '@angular/common';
import { Tab } from '../../shared/tabs/tab.directive';
import { Tabset } from '../../shared/tabs/tabset.component';
import { TabHeading } from '../../shared/tabs/tab-heading.directive';
import { DatePipe } from '@angular/common';

import { Angular2Csv } from 'angular2-csv/Angular2-csv';

interface IServerResponse {
    items: string[];
    total: number;
}

declare var jQuery: any;
@Component({
    templateUrl: 'duplicatebilling.component.html',
    providers: [SOLRService]
})
export class DuplicateBillingComponent implements OnInit {
    pageNumber: number = 1;
    recordsToFetch: number = 10;
    totalRecords: number = 0;
    resFacets: any = {};
    asyncDuplicateBilling: Observable<any[]>;
    loading: boolean;
    searchDuplicateBillingText: string = "";
    selectedVN: string[] = [];
    selectedDOS: string[] = [];
    rersDuplicateBilling: any[] = [];
    foundRecord: boolean = true;
    gPanelShow: number = -1;
    pageName: string = "Double Billing";
    constructor(private zone: NgZone, private solrervice: SOLRService, private _elmRef: ElementRef,
        private route: ActivatedRoute, private datePipe: DatePipe) {
    }
    // //dynamic Tabs
    // public angular2TabsExample: Array<any> = [
    //     { title: 'Angular Tab 1', content: 'Angular 2 Tabs are navigable windows, each window (called tab) contains content', disabled: false, removable: true },
    //     { title: 'Angular Tab 2', content: 'generally we categorize contents depending on the theme', disabled: false, removable: true },
    //     { title: 'Angular Tab 3', content: 'Angular 2 Tabs Content', disabled: false, removable: true }
    // ];
    // //on select a tab do something
    // public doOnTabSelect(currentTab: any) {
    //     console.log("doOnTabSelect" + currentTab);
    // };
    // //on remove Tab do something
    // public removeThisTabHandler(tab: any) {
    //     console.log('Remove Tab handler' + tab);
    // };

    billedAmountRange: number[] = [0, 150000];
    public billedAmountConfig: any = {
        connect: true,
        start: [0, 150000],
        //step: 15000,
        pageSteps: 10,  // number of page steps, defaults to 10
        range: {
            min: 0,
            max: 150000
        },
        format: {
            to(value: any): any {
                return parseFloat(value.toFixed());
            },
            from(value: any): any {
                return parseFloat(value).toFixed(2);
            }
        }
    };

    exportCSV() {
        var options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            fields: ["vendorName1", "vendorName2", "billReceivedDate2", "vendorName1", "billedCPTDescription", "billReceivedDate1", "paidCPTCode2",
                "billedAmount1", "typeOfBillCode", "paidCPTCode1", "billedAmount2", "taxIDNumber2", "dateOfServiceYear", "taxIDNumber1", "paidAmount2",
                "typeOfBillDescription", "paidAmount1", "billedCPTCode", "dateOfService", "paidCPTDescription1", "paidCPTDescription2"
            ],
            fieldNames: ["Vendor Name 1", "Vendor Name 2", "Bill Received Date 2", "Vendor Name 1", "Billed CPT Description", "Bill Received Date 1", "Paid CPT Code 2",
                "Billed Amount 1", "Type Of Bill Code", "Paid CPT Code 1", "Billed Amount 2", "Tax ID Number 2", "Date of Service Year", "Tax ID Number 1", "Paid Amount 2",
                "Type of Bill Description", "Paid Amount 1", "Billed CPT Code", "Date of Service", "Paid CPT Description 1", "Paid CPT Description 2"
            ]
        };
        let date = new Date();

        var objCSV: any = [];
        // objCSV.push('Duplicate Billing');
        objCSV.push(JSON.parse(JSON.stringify(this.rersDuplicateBilling)));

        objCSV.forEach(element => {
            if (Object.prototype.toString.call(element) === '[object Array]') {
                element.forEach(element1 => {
                    for (var field in element1) {
                        if ('_version_,id,DNB_REPORT_LINK,LEXIS_NEXIS_REPORT,PROFILE_PICTURE'.indexOf(field) > -1) {
                            delete element1[field];
                        }
                    }
                });
            }
        });

        let res = new Angular2Csv(objCSV, 'Duplicate Billing - ' + date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate(), options);
        //console.info('res', res);
    }

    onChangeBilledAmount(event) {
        this.searchDuplicateBillingRecordsWithFilters(1);
    }

    paidAmountRange: number[] = [0, 150000];
    public paidAmountConfig: any = {
        connect: true,
        start: [0, 150000],
        //step: 15000,
        pageSteps: 10,  // number of page steps, defaults to 10
        range: {
            min: 0,
            max: 150000
        },
        format: {
            to(value: any): any {
                return parseFloat(value.toFixed());
            },
            from(value: any): any {
                return parseFloat(value).toFixed(2);
            }
        }
    };

    onChangePaidAmount(event) {
        this.searchDuplicateBillingRecordsWithFilters(1);
    }

    searchDuplicateBillingFacets() {
        this.solrervice.searchDuplicateBillingFacets(this.taxIdNumber, this.searchDuplicateBillingText)
            .subscribe(res => {
                if (res.facet_counts) {
                    this.zone.run(() => {
                        let facetArray = [];
                        this.resFacets = {};
                        if (res.facet_counts.facet_fields) {
                            if (res.facet_counts.facet_fields["dateOfService"] && res.facet_counts.facet_fields["dateOfService"].length > 0) {
                                for (var index = 0; index < res.facet_counts.facet_fields["dateOfService"].length; index++) {
                                    if (res.facet_counts.facet_fields["dateOfService"][index + 1] > 0)
                                        facetArray.push({ name: this.datePipe.transform((res.facet_counts.facet_fields["dateOfService"][index]), "dd-MMM-yyyy"), recordCount: res.facet_counts.facet_fields["dateOfService"][++index] });
                                }
                                this.resFacets.dateOfService = facetArray;
                            }

                            if (res.facet_counts.facet_fields["vendorName1"] && res.facet_counts.facet_fields["vendorName1"].length > 0) {
                                facetArray = [];
                                for (var index = 0; index < res.facet_counts.facet_fields["vendorName1"].length; index++) {
                                    if (res.facet_counts.facet_fields["vendorName1"][index + 1] > 0)
                                        facetArray.push({ name: res.facet_counts.facet_fields["vendorName1"][index], recordCount: res.facet_counts.facet_fields["vendorName1"][++index] });
                                }
                                this.resFacets.vendorName1 = facetArray;
                            }


                            if (res.facet_counts.facet_fields["billedAmount1"] && res.facet_counts.facet_fields["billedAmount1"].length > 0) {
                                let maxAmount = 0;
                                for (var index = 0; index < res.facet_counts.facet_fields["billedAmount1"].length; index++) {
                                    if (parseFloat(res.facet_counts.facet_fields["billedAmount1"][index]) > maxAmount)
                                        maxAmount = parseFloat(res.facet_counts.facet_fields["billedAmount1"][index]);
                                    index++;
                                }
                                this.resFacets.maxBilledAmount = maxAmount;
                            }


                            if (res.facet_counts.facet_fields["paidAmount1"] && res.facet_counts.facet_fields["paidAmount1"].length > 0) {
                                let maxAmount = 0;
                                for (var index = 0; index < res.facet_counts.facet_fields["paidAmount1"].length; index++) {
                                    if (parseFloat(res.facet_counts.facet_fields["paidAmount1"][index]) > maxAmount)
                                        maxAmount = parseFloat(res.facet_counts.facet_fields["billedAmount1"][index]);
                                    index++;
                                }
                                this.resFacets.maxPaidAmount = maxAmount;
                            }

                            if (this.resFacets.maxBilledAmount) {
                                this.billedAmountConfig.range = { min: 0, max: this.resFacets.maxBilledAmount };
                                this.billedAmountRange = [0, this.resFacets.maxBilledAmount];
                            }
                            if (this.resFacets.maxPaidAmount) {
                                this.paidAmountConfig.range.max = this.resFacets.maxPaidAmount;
                                this.paidAmountRange = [0, this.resFacets.maxPaidAmount];
                            }

                            //console.log('this.resFacets', this.resFacets);
                        }
                    });
                }
            }
            , error => {
            });
    }

    changeSelectFacet(objFacet, index) {
        this.zone.run(() => {
            let changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    this.selectedVN.push(objFacet.name);
                else if (index == 2)
                    this.selectedDOS.push(objFacet.name);
            }
            else {
                let arrayIndex: number = -1;
                if (index == 1) {
                    arrayIndex = this.selectedVN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedVN.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = this.selectedDOS.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedDOS.splice(arrayIndex, 1);
                }

            }
        });
        this.searchDuplicateBillingRecordsWithFilters(1);
    }

    removeSelectedFacet(objFacet, index) {
        this.zone.run(() => {
            objFacet.selected = false;
            let arrayIndex: number = -1;
            if (index == 1) {
                arrayIndex = this.selectedVN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedVN.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = this.selectedDOS.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedDOS.splice(arrayIndex, 1);
            }
        });
        this.searchDuplicateBillingRecordsWithFilters(1);
    }

    clearSelectedFacets() {
        this.selectedVN = [];
        this.selectedDOS = [];
        this.billedAmountRange = [0, 0];
        this.paidAmountRange = [0, 0];
    }

    searchDuplicateBilling() {
        this.clearSelectedFacets();
        this.searchDuplicateBillingRecordsWithFilters(1);
        this.searchDuplicateBillingFacets();
    }

    searchDuplicateBillingRecordsWithFilters(pageNum: number) {
        this.pageNumber = pageNum;
        let selectedVNString: string = "";
        let selectedDOSString: string = "";
        let selectedBARtring: string = "";
        let selectedPARString: string = "";
        if (this.selectedVN && this.selectedVN.length > 0)
            selectedVNString = this.selectedVN.join('"OR"');
        if (this.selectedDOS && this.selectedDOS.length > 0) {
            var dosArray = [];
            for (var index = 0; index < this.selectedDOS.length; index++) {
                dosArray.push(this.datePipe.transform(this.selectedDOS[index], "yyyy-MM-dd") + 'T00:00:00.000Z');
            }
            //console.log('this.selectedDOS', dosArray);
            selectedDOSString = dosArray.join('"OR"');
        }
        if (this.resFacets.maxBilledAmount && this.resFacets.maxBilledAmount > 0 && this.billedAmountRange && this.billedAmountRange.length > 1
            && this.billedAmountRange[0] > 0 || this.billedAmountRange[1] < this.resFacets.maxBilledAmount)
            selectedBARtring = '[' + this.billedAmountRange.join(' TO ') + ']';
        if (this.resFacets.maxPaidAmount && this.resFacets.maxPaidAmount > 0 && this.paidAmountRange && this.paidAmountRange.length > 1
            && this.paidAmountRange[0] > 0 || this.paidAmountRange[1] < this.resFacets.maxPaidAmount)
            selectedPARString = '[' + this.paidAmountRange.join(' TO ') + ']';

        this.asyncDuplicateBilling = this.solrervice.searchDuplicateBillingRecordsWithFilter(this.taxIdNumber, this.searchDuplicateBillingText,
            selectedVNString, selectedDOSString, selectedBARtring, selectedPARString, this.pageNumber, this.recordsToFetch)
            .do((res: any) => {
                if (res.response) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.numFound;
                        this.rersDuplicateBilling = res.response.docs || [];
                        this.foundRecord = res.response.numFound > 0;
                        //console.info('rersDuplicateBilling', this.rersDuplicateBilling)
                        //console.info('this.asyncDuplicateBilling', this.asyncDuplicateBilling);
                    });
                }
                else {
                    this.zone.run(() => {
                        this.totalRecords = 0;
                        this.rersDuplicateBilling = [];
                    });
                }
                this
            })
            .map(res => res.response ? res.response.docs : []);
        //console.info('this.asyncDuplicateBilling', this.asyncDuplicateBilling);

    }

    taxIdNumber: number = -1;
    ngOnInit(): void {
        this.route.queryParams.subscribe(params => {
            console.log('queryParams', params);
            if (params["taxIdNumber"]) {
                this.taxIdNumber = params["taxIdNumber"];
                //setting static number
                this.taxIdNumber = 954885742;
                this.searchDuplicateBillingFacets();
                this.searchDuplicateBillingRecordsWithFilters(1);
            }
            if (params['p']) {
                switch (params['p'].toString()) {
                    case "1":
                        this.pageName = "Upcoding";
                        break;
                    case "2":
                        this.pageName = "Excessive Testing";
                        break;
                    case "3":
                        this.pageName = "Denied Claims";
                        break;
                    case "4":
                        this.pageName = "Duplicate Billing";
                        break;
                    case "5":
                        this.pageName = "Excessive DME Prescription";
                        break;
                    case "6":
                        this.pageName = "Excess Imaging Services";
                        break;
                    case "7":
                        this.pageName = "Cluster Billing";
                        break;
                    case "8":
                        this.pageName = "Pain Pumps";
                        break;
                    case "9":
                        this.pageName = "Autologous Transfusion";
                        break;
                    default:
                        break;


                }
            }
        });
    }
}

/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals: string[], page: number): Observable<IServerResponse> {
    const perPage = 10;
    const start = (page - 1) * perPage;
    const end = start + perPage;

    return Observable
        .of({
            items: meals.slice(start, end),
            total: 100
        }).delay(1000);
}
