import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule } from '@angular/forms';

import { DuplicateBillingComponent } from './duplicatebilling.component';
import { DuplicateBillingRoutingModule } from './duplicatebilling-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [DuplicateBillingRoutingModule],
    declarations: []
})
export class DuplicateBillingModule { }
