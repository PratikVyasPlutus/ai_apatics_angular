import { Component, OnInit, ElementRef } from '@angular/core';
//
import { ProviderOutcomeService } from '../../services/provider-outcome.service';


declare var jQuery: any;
declare var vis: any;
@Component({
    templateUrl: 'socialNetworksClaimant.component.html',
    providers: [ProviderOutcomeService],
})

export class SocialNetworksClaimantComponent implements OnInit {

    activeFilters: any = {};
    tableOneData: any = [];
    tableTwoData: any = [];
    tableThreeData: any = [];
    tableFourData: any = [];
    searchQuery: any = "";
    isShowLoad: boolean = false;

    constructor(public providerOutcomeService: ProviderOutcomeService, private element: ElementRef) {
        this.initFilter();
    }


    ngOnInit() {
        this.providerOutcomeService.getAllSideBarFilterForClaimant('').subscribe(res => {
            console.log('getAllSideBarFilterForClaimant res', res);
            this.activeFilters['providers'] = res.providers;
            this.activeFilters['attorneys'] = res.attorneys;
            this.activeFilters['claimant'] = res.claimant;
            this.activeFilters['claimnumber'] = res.claimnumber;
            this.getAllProviderDataWithFilter();
        }, err => {

        });

    }

    loadGraph(nodes, edges) {
        var container = document.querySelector('#network-graph');
        var data = {
            nodes: nodes,
            edges: edges
        };
        var options = {
            nodes: {
                shape: 'dot',
                font: { size: 10 }
            },
            interaction: {
                zoomView: true,
                hover: true,
                navigationButtons: true,
            }
        }
        let network = new vis.Network(container, data, options);
    }

    ngAfterViewInit() {

    }

    initFilter() {
        this.activeFilters['providers'] = [];
        this.activeFilters['attorneys'] = [];
        this.activeFilters['claimant'] = [];
        this.activeFilters['claimnumber'] = [];
        this.activeFilters['searchQuery'] = "";
    }

    changeProvSelection(prov) {
        let changedSelection = !prov.selected;
        prov.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    changeAttorneySelection(atto) {
        let changedSelection = !atto.selected;
        atto.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    changeClaimantSelection(claim) {
        let changedSelection = !claim.selected;
        claim.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    changeClaimNumberSelection(cno) {
        let changedSelection = !cno.selected;
        cno.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    searchProviderSummary() {
        this.activeFilters['searchQuery'] = this.searchQuery;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    removeSearchQuery() {
        this.activeFilters['searchQuery'] = "";
        this.searchQuery = "";
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    getAllProviderDataWithFilter() {
        this.isShowLoad = true;
        this.providerOutcomeService.getAllClaimantData(this.activeFilters).subscribe(res => {
            console.log('getAllClaimantData res', res);

            var nodes = [];
            res.nodes.forEach(element => {
                nodes.push({
                    id: element.id,
                    label: element.label,
                    title: element.title,
                    level: element.level,
                    color: {
                        background: element.background,
                        highlight: element.highlight,
                        hover: element.hover,
                    }
                });
            });

            var edges = [];
            res.edges.forEach(element => {
                edges.push(element);
            });

            this.loadGraph(nodes, edges);
            this.tableOneData = res.table_one;
            this.tableTwoData = res.table_two;
            this.tableThreeData = res.table_three;
            this.tableFourData = res.table_four;
            this.isShowLoad = false;
        }, err => {
            console.log('getAllProviderDataWithFilter err', err);
            this.isShowLoad = false;
        });
    }
}
