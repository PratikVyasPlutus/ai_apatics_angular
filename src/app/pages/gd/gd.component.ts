import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FDAService } from '../../services/fda.service';
import { Observable } from 'rxjs';
import { MapsAPILoader } from 'angular2-google-maps/core';
declare var google: any;

interface IServerResponse {
    items: string[];
    total: number;
}

declare var jQuery: any;
@Component({
    templateUrl: 'gd.component.html',
    providers: [FDAService]
})
export class GDComponent implements OnInit {
    selectedProductId: string = "";
    selectedGD: any = {};
    pageNumber: number = 1;
    recordsToFetch: number = 10;
    totalRecords: number = 0;
    resFacets: any = {};
    asyncGD: Observable<any[]>;
    loading: boolean;
    searchGDText: string = "";
    selectedPN: string[] = [];
    selectedTIN: string[] = [];
    rersGD: any[] = [];
    foundRecord: boolean = true;
    showMap: boolean = false;
    distanceString: string = "";
    constructor(private zone: NgZone, private fdaService: FDAService, private _elmRef: ElementRef, private _loader: MapsAPILoader) {

    }

    billedAmountRange: number[] = [0, 150000];
    public billedAmountConfig: any = {
        connect: true,
        start: [0, 150000],
        //step: 15000,
        pageSteps: 10,  // number of page steps, defaults to 10
        range: {
            min: 0,
            max: 150000
        },
        format: {
            to(value: any): any {
                return parseFloat(value.toFixed());
            },
            from(value: any): any {
                return parseFloat(value).toFixed(2);
            }
        }
    };

    onChangeBilledAmount(event) {
        this.searchGDRecordsWithFilters(1);
    }

    distanceRange: number[] = [0, 5000];
    public distanceConfig: any = {
        connect: true,
        start: [0, 5000],
        //step: 500,
        pageSteps: 10,  // number of page steps, defaults to 10
        range: {
            min: 0,
            max: 5000
        },
        format: {
            to(value: any): any {
                return parseFloat(value.toFixed());
            },
            from(value: any): any {
                return parseFloat(value).toFixed(2);
            }
        }
    };

    onChangeDistance(event) {
        this.searchGDRecordsWithFilters(1);
    }

    paidAmountRange: number[] = [0, 150000];
    public paidAmountConfig: any = {
        connect: true,
        start: [0, 150000],
        //step: 15000,
        pageSteps: 10,  // number of page steps, defaults to 10
        range: {
            min: 0,
            max: 150000
        },
        format: {
            to(value: any): any {
                return parseFloat(value.toFixed());
            },
            from(value: any): any {
                return parseFloat(value).toFixed(2);
            }
        }
    };

    onChangePaidAmount(event) {
        this.searchGDRecordsWithFilters(1);
    }

    totalBilledAmountRange: number[] = [0, 150000];
    public totalBilledAmountConfig: any = {
        connect: true,
        start: [0, 150000],
        step: 15000,
        pageSteps: 0,  // number of page steps, defaults to 10
        range: {
            min: 0,
            max: 150000
        },
        format: {
            to(value: any): any {
                return parseFloat(value.toFixed());
            },
            from(value: any): any {
                return parseFloat(value).toFixed(2);
            }
        }
    };

    onChangeTotalBilledAmount(event) {
        this.searchGDRecordsWithFilters(1);
    }

    totalPaidAmountRange: number[] = [0, 150000];
    public totalPaidAmountConfig: any = {
        connect: true,
        start: [0, 150000],
        step: 15000,
        pageSteps: 0,  // number of page steps, defaults to 10
        range: {
            min: 0,
            max: 150000
        },
        format: {
            to(value: any): any {
                return parseFloat(value.toFixed());
            },
            from(value: any): any {
                return parseFloat(value).toFixed(2);
            }
        }
    };

    onChangeTotalPaidAmount(event) {
        this.searchGDRecordsWithFilters(1);
    }

    searchGDFacets() {
        this.fdaService.searchGDFacets(this.searchGDText)
            .subscribe(res => {
                if (res.success) {
                    this.zone.run(() => {
                        this.resFacets = res.response;
                        if (this.resFacets.maxBilledAmount) {
                            this.billedAmountConfig.range = { min: 0, max: this.resFacets.maxBilledAmount };
                            this.billedAmountRange = [0, this.resFacets.maxBilledAmount];
                        }
                        if (this.resFacets.maxDistance) {
                            this.distanceConfig.range.max = this.resFacets.maxDistance;
                            this.distanceRange = [0, this.resFacets.maxDistance];
                        }
                        if (this.resFacets.maxPaidAmount) {
                            this.paidAmountConfig.range.max = this.resFacets.maxPaidAmount;
                            this.paidAmountRange = [0, this.resFacets.maxPaidAmount];
                        }
                        if (this.resFacets.maxTotalBilledAmount) {
                            this.totalBilledAmountConfig.range.max = this.resFacets.maxTotalBilledAmount;
                            this.totalBilledAmountRange = [0, this.resFacets.maxTotalBilledAmount];
                        }
                        if (this.resFacets.maxTotalPaidAmount) {
                            this.totalPaidAmountConfig.range.max = this.resFacets.maxTotalPaidAmount;
                            this.totalPaidAmountRange = [0, this.resFacets.maxTotalPaidAmount];
                        }
                    });
                }
            }
            , error => {
            });
    }

    changeSelectFacet(objFacet, index) {
        this.zone.run(() => {
            let changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    this.selectedPN.push(objFacet.name);
                else if (index == 2)
                    this.selectedTIN.push(objFacet.name);
            }
            else {
                let arrayIndex: number = -1;
                if (index == 1) {
                    arrayIndex = this.selectedPN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPN.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = this.selectedTIN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedTIN.splice(arrayIndex, 1);
                }
            }
        });
        this.searchGDRecordsWithFilters(1);
    }

    removeSelectedFacet(objFacet, index) {
        this.zone.run(() => {
            objFacet.selected = false;
            let arrayIndex: number = -1;
            if (index == 1) {
                arrayIndex = this.selectedPN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPN.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = this.selectedTIN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedTIN.splice(arrayIndex, 1);
            }
        });
        this.searchGDRecordsWithFilters(1);
    }

    clearSelectedFacets() {
        this.selectedPN = [];
        this.selectedTIN = [];
        this.billedAmountRange = [0, 0];
        this.distanceRange = [0, 0];
        this.paidAmountRange = [0, 0];
        this.totalBilledAmountRange = [0, 0];
        this.totalPaidAmountRange = [0, 0];
        this.resFacets = {};
    }

    searchGD() {
        this.clearSelectedFacets();
        this.searchGDRecordsWithFilters(1);
        this.searchGDFacets();
    }

    searchGDRecordsWithFilters(pageNum: number) {
        this.showMap = false;
        this.selectedGD = {};
        this.pageNumber = pageNum;
        let selectedPNString: string = "";
        let selectedTINString: string = "";
        let selectedBARtring: string = "";
        let selectedDRString: string = "";
        let selectedPARString: string = "";
        let selectedTBARString: string = "";
        let selectedTPARString: string = "";
        if (this.billedAmountRange && this.billedAmountRange.length > 0)
            selectedBARtring = this.billedAmountRange.join();
        if (this.distanceRange && this.distanceRange.length > 0)
            selectedDRString = this.distanceRange.join();
        if (this.paidAmountRange && this.paidAmountRange.length > 0)
            selectedPARString = this.paidAmountRange.join();
        if (this.selectedPN && this.selectedPN.length > 0)
            selectedPNString = this.selectedPN.join();
        if (this.selectedTIN && this.selectedTIN.length > 0)
            selectedTINString = this.selectedTIN.join();
        if (this.totalBilledAmountRange && this.totalBilledAmountRange.length > 0)
            selectedTBARString = this.totalBilledAmountRange.join();
        if (this.totalPaidAmountRange && this.totalPaidAmountRange.length > 0)
            selectedTPARString = this.totalPaidAmountRange.join();

        this.asyncGD = this.fdaService.searchGDRecordsWithFilter(this.searchGDText, selectedBARtring, selectedDRString,
            selectedPARString, selectedPNString, selectedTINString, selectedTBARString, selectedTPARString, this.pageNumber, this.recordsToFetch)
            //  .subscribe(data => {
            //     console.info('data', data);
            // });
            .do((res: any) => {
                if (res.response) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.numFound;
                        this.rersGD = res.response.docs || [];
                        this.foundRecord = res.response.numFound > 0;

                        console.log('resGD', res);
                    });
                }
                else {
                    this.zone.run(() => {
                        this.totalRecords = 0;
                        this.rersGD = [];
                    });
                }
            })
            .map(res => res.response ? res.response.docs : []);
    }

    /*map section*/
    mapCenterLocation: any = {
        lat: 51.678418,
        lng: 7.809007
    }

    markers: marker[] = [
        {
            lat: 51.673858,
            lng: 7.815982,
            label: 'C',
            draggable: false,
            icon: '',
            info: 'Info'
        },
        {
            lat: 51.373858,
            lng: 7.815982,
            label: 'P',
            draggable: false,
            icon: '',
            info: 'Info'
        }];

    openCloseMap(objGD) {
        if (this.showMap) {
            if (objGD == this.selectedGD)
                this.showMap = false;
            else {
                this.selectedGD = objGD;
                this.setLocationFromAddressInMarker(0, objGD.claimantAddress);
                this.setLocationFromAddressInMarker(1, objGD.providerAddress);
                this.calculateDistance(objGD.claimantAddress, objGD.providerAddress);
                //this.resizeMap()
            }
        }
        else {
            this.selectedGD = objGD;
            this.setLocationFromAddressInMarker(0, objGD.claimantAddress);
            this.setLocationFromAddressInMarker(1, objGD.providerAddress);
            this.calculateDistance(objGD.claimantAddress, objGD.providerAddress);
            //this.resizeMap();
            this.showMap = true;
        }
    }

    closeMap() {
        this.selectedGD = {};
        this.showMap = false;
    }

    geocoder: any;
    serviceDistanceMatrix: any;
    mapBounds: any;
    mapOverlayView: any;

    initMap() {
        this._loader.load().then(() => {
            this.geocoder = new google.maps.Geocoder();
            this.serviceDistanceMatrix = new google.maps.DistanceMatrixService();
            this.mapBounds = new google.maps.LatLngBounds();
            this.mapOverlayView = new google.maps.OverlayView();
        });

    }

    resizeMap() {
        this.mapBounds = new google.maps.LatLngBounds();
        this.mapBounds.extend(new google.maps.LatLng(this.markers[0].lat, this.markers[0].lng));
        this.mapBounds.extend(new google.maps.LatLng(this.markers[1].lat, this.markers[1].lng));
        this.mapCenterLocation.lat = this.mapBounds.getCenter().lat();
        this.mapCenterLocation.lng = this.mapBounds.getCenter().lng();

        // let gmap = this.mapOverlayView.getMap();
        // console.log('zoom', gmap.getZoom());

    }

    mapZoomLevel: number = 11;
    calculateDistance(sourceAddress, destinationAddress) {
        this.serviceDistanceMatrix.getDistanceMatrix(
            {
                origins: [sourceAddress],
                destinations: [destinationAddress],
                travelMode: 'DRIVING',
                // transitOptions: TransitOptions,
                // drivingOptions: DrivingOptions,
                // unitSystem: UnitSystem,
                avoidHighways: false,
                avoidTolls: false,
            }, (response, status) => {
                if (response.rows[0] && response.rows[0].elements[0] && response.rows[0].elements[0].duration && response.rows[0].elements[0].distance) {
                    this.zone.run(() => {
                        let distanceInMiles: number = Number((response.rows[0].elements[0].distance.value / 1609.344).toFixed(2));
                        this.distanceString = distanceInMiles + ' Miles ' + '(' + response.rows[0].elements[0].duration.text + ')';
                        /* if (distanceInMiles <= 25)
                             this.mapZoomLevel = 11;
                         else if (distanceInMiles <= 50)
                             this.mapZoomLevel = 10;
                         else if (distanceInMiles <= 100)
                             this.mapZoomLevel = 10
                         else if (distanceInMiles <= 300)
                             this.mapZoomLevel = 9
                         else if (distanceInMiles <= 600)
                             this.mapZoomLevel = 8
                         else if (distanceInMiles <= 800)
                             this.mapZoomLevel = 7
                         else if (distanceInMiles <= 1200)
                             this.mapZoomLevel = 6
                         else if (distanceInMiles <= 2000)
                             this.mapZoomLevel = 5
                         else if (distanceInMiles <= 3000)
                             this.mapZoomLevel = 4
                         else if (distanceInMiles <= 4000)
                             this.mapZoomLevel = 2
                         else
                             this.mapZoomLevel = 1*/

                        //console.log('this.mapZoomLevel', this.mapZoomLevel);
                    });
                }
            });
    }

    setLocationFromAddressInMarker(markerIndex, address: string) {
        this.geocoder.geocode({ 'address': address }, (results, status) => {
            this.markers[markerIndex].lat = results[0].geometry.location.lat();
            this.markers[markerIndex].lng = results[0].geometry.location.lng();
            this.markers[markerIndex].info = address;
            if (markerIndex > 0) {
                this.resizeMap();
            }
        });
    }

    ngOnInit(): void {
        this.searchGDRecordsWithFilters(1);
        this.searchGDFacets();
        this.initMap();
        //this.calculateDistance();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    }
}

// just an interface for type safety.
interface marker {
    lat: number;
    lng: number;
    label?: string;
    draggable: boolean;
    icon: string;
    info: string
}

/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals: string[], page: number): Observable<IServerResponse> {
    const perPage = 10;
    const start = (page - 1) * perPage;
    const end = start + perPage;

    return Observable
        .of({
            items: meals.slice(start, end),
            total: 100
        }).delay(1000);
}
