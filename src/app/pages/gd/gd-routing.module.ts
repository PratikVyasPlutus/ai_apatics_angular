import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GDComponent } from './gd.component';

const routes: Routes = [
    {
        path: '',
        component: GDComponent,
        data: {
            title: 'GD'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GDRoutingModule { }
