import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { GDComponent } from './gd.component';
import { GDRoutingModule } from './gd-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [
        FormsModule,
        GDRoutingModule
    ],
    declarations: []
})
export class GDModule { }
