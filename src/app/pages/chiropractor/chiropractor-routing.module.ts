import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChiropractorComponent } from './chiropractor.component';

const routes: Routes = [
    {
        path: '',
        component: ChiropractorComponent,
        data: {
            title: 'Chiropractor'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ChiropractorRoutingModule { }
