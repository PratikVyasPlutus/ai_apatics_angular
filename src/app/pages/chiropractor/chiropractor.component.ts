import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FDAService } from '../../services/fda.service';
import { Observable } from 'rxjs';

interface IServerResponse {
    items: string[];
    total: number;
}

declare var jQuery: any;
@Component({
    templateUrl: 'chiropractor.component.html',
    providers: [FDAService]
})
export class ChiropractorComponent implements OnInit {
    selectedProductId: string = "";
    selectedChiropractor: any = {};
    pageNumber: number = 1;
    recordsToFetch: number = 10;
    totalRecords: number = 0;
    resFacets: any = {};
    asyncChiropractor: Observable<any[]>;
    loading: boolean;
    searchChiropractorText: string = "";
    selectedDT: string[] = [];
    selectedLN: string[] = [];
    selectedDOD: string[] = [];
    selectedSTATE: string[] = [];
    rersChiropractor: any[] = [];
    foundRecord: boolean = true;
    constructor(private zone: NgZone, private fdaService: FDAService, private _elmRef: ElementRef) {

    }

    searchChiropractorFacets() {
        this.fdaService.searchChiropractorFacets(this.searchChiropractorText)
            .subscribe(res => {
                if (res.success) {
                    this.zone.run(() => {
                        this.resFacets = res.response;
                    });
                }
            }
            , error => {
            });
    }

    changeSelectFacet(objFacet, index) {
        this.zone.run(() => {
            let changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    this.selectedDT.push(objFacet.name);
                else if (index == 2)
                    this.selectedLN.push(objFacet.name);
                else if (index == 3)
                    this.selectedDOD.push(objFacet.name);
                else if (index == 4)
                    this.selectedSTATE.push(objFacet.name);
            }
            else {
                let arrayIndex: number = -1;
                if (index == 1) {
                    arrayIndex = this.selectedDT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedDT.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = this.selectedLN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedLN.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = this.selectedDOD.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedDOD.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = this.selectedSTATE.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedSTATE.splice(arrayIndex, 1);
                }
            }
        });
        this.searchChiropractorRecordsWithFilters(1);
    }

    removeSelectedFacet(objFacet, index) {
        this.zone.run(() => {
            objFacet.selected = false;
            let arrayIndex: number = -1;
            if (index == 1) {
                arrayIndex = this.selectedDT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedDT.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = this.selectedLN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedLN.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = this.selectedDOD.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedDOD.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = this.selectedSTATE.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedSTATE.splice(arrayIndex, 1);
            }
        });
        this.searchChiropractorRecordsWithFilters(1);
    }

    clearSelectedFacets() {
        this.selectedDT = [];
        this.selectedLN = [];
        this.selectedDOD = [];
        this.selectedSTATE = [];
    }

    searchChiropractor() {
        this.clearSelectedFacets();
        this.searchChiropractorRecordsWithFilters(1);
        this.searchChiropractorFacets();
    }

    searchChiropractorRecordsWithFilters(pageNum: number) {
        this.pageNumber = pageNum;
        let selectedDTString: string = "";
        let selectedLNString: string = "";
        let selectedDODString: string = "";
        let selectedSTATEString: string = "";
        if (this.selectedDT && this.selectedDT.length > 0)
            selectedDTString = this.selectedDT.join();
        if (this.selectedLN && this.selectedLN.length > 0)
            selectedLNString = this.selectedLN.join();
        if (this.selectedDOD && this.selectedDOD.length > 0)
            selectedDODString = this.selectedDOD.join();
        if (this.selectedSTATE && this.selectedSTATE.length > 0)
            selectedSTATEString = this.selectedSTATE.join();

        this.asyncChiropractor = this.fdaService.searchChiropractorRecordsWithFilter(this.searchChiropractorText, selectedDTString, selectedLNString,
            selectedDODString, selectedSTATEString, this.pageNumber, this.recordsToFetch)
            .do((res: any) => {
                if (res.success) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.totalRecords;
                        this.rersChiropractor = res.response.results || [];
                        this.foundRecord = res.response.totalRecords > 0;
                    });
                }
                else {
                    this.zone.run(() => {
                        this.totalRecords = 0;
                        this.rersChiropractor = [];
                    });
                }
            })
            .map(res => res.success ? res.response.results : []);
    }

    ngOnInit(): void {
        this.searchChiropractorRecordsWithFilters(1);
        this.searchChiropractorFacets();
    }
}

/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals: string[], page: number): Observable<IServerResponse> {
    const perPage = 10;
    const start = (page - 1) * perPage;
    const end = start + perPage;

    return Observable
        .of({
            items: meals.slice(start, end),
            total: 100
        }).delay(1000);
}
