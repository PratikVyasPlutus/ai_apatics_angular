import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule } from '@angular/forms';

import { ChiropractorComponent } from './chiropractor.component';
import { ChiropractorRoutingModule } from './chiropractor-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [
        FormsModule,
        ChiropractorRoutingModule,
        ChartsModule
    ],
    declarations: []
})
export class ChiropractorModule { }
