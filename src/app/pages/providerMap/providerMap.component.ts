import { Component, OnInit } from '@angular/core';
//
import { ProviderOutcomeService } from '../../services/provider-outcome.service';



declare var jQuery: any;
declare var google: any;
declare var google: any;

@Component({
    templateUrl: 'providerMap.component.html',
    providers: [ProviderOutcomeService],
})

export class ProviderMapComponent implements OnInit {

    activeFilters: any = {};
    mapData: any = [];

    constructor(public providerOutcomeService: ProviderOutcomeService) {
        this.initFilter();
    }

    ngOnInit() {
        this.SetUpSideBarFilter();
    }

    ngAfterViewInit() {
        //this.initMap();
        //this.loadMarkers();
        this.getAllProviderDataWithFilter();
    }

    initFilter() {
        this.activeFilters['citys'] = [];
        this.activeFilters['zips'] = [];
        this.activeFilters['providers'] = [];
        this.activeFilters['tin_nums'] = [];
        this.activeFilters['providers_values'] = [];
        this.activeFilters['searchQuery'] = "";
    }

    changeCitySelection(city) {
        let changedSelection = !city.selected;
        city.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }
    changeZipSelection(zip) {
        let changedSelection = !zip.selected;
        zip.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }
    changeProvSelection(prov) {
        let changedSelection = !prov.selected;
        prov.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }
    changeTinSelection(tin) {
        let changedSelection = !tin.selected;
        tin.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }
    changePvSelection(pv) {
        let changedSelection = !pv.selected;
        pv.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    initMap() {
        let ca = { lat: 36.778261, lng: -119.417932 };

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            center: ca,
            styles: [{ elementType: 'geometry', stylers: [{ color: '#023e58' }] },

            { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
            { elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },

            { featureType: 'administrative.locality', elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },

            { featureType: 'poi', elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },
            { featureType: 'poi.park', elementType: 'geometry', stylers: [{ color: '#263c3f' }] },
            { featureType: 'poi.park', elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },

            { featureType: 'road', elementType: 'geometry', stylers: [{ color: '#38414e' }] },
            { featureType: 'road', elementType: 'geometry.stroke', stylers: [{ color: '#212a37' }] },
            { featureType: 'road', elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },
            { featureType: 'road.highway', elementType: 'geometry', stylers: [{ color: '#746855' }] },
            { featureType: 'road.highway', elementType: 'geometry.stroke', stylers: [{ color: '#1f2835' }] },
            { featureType: 'road.highway', elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },

            { featureType: 'transit', elementType: 'geometry', stylers: [{ color: '#2f3948' }] },
            { featureType: 'transit.station', elementType: 'labels.text.fill', stylers: [{ color: '#d59563' }] },

            { featureType: 'water', elementType: 'geometry', stylers: [{ color: '#17263c' }] },
            { featureType: 'water', elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },
            { featureType: 'water', elementType: 'labels.text.stroke', stylers: [{ color: '#17263c' }] }
            ]
        });
        this.loadMarkers(map);
    }

    loadMarkers(map) {
        this.mapData.forEach(ele => {
            //console.log('element', ele);
            //parseFloat()
            if (ele.PROVIDER_VALUE == 'Most Valued') {
                var icon = "assets/img/gmap-icons/turquoise-blue.png";
            } else if (ele.PROVIDER_VALUE == 'Medium Valued') {
                var icon = "assets/img/gmap-icons/yellow.png";
            } else {
                var icon = "assets/img/gmap-icons/red-dark.png";
            }
            var marker = new google.maps.Marker({
                position: { lat: parseFloat(ele.LAT), lng: parseFloat(ele.LNG) },
                icon: icon,
                map: map
            });
        });
    }

    SetUpSideBarFilter() {
        this.providerOutcomeService.getSideBarFilterMap('').subscribe(res => {
            console.log('getSideBarFilterMap res', res);
            this.activeFilters['citys'] = res.citys;
            this.activeFilters['zips'] = res.zips;
            this.activeFilters['providers'] = res.providers;
            this.activeFilters['tin_nums'] = res.tin_nums;
            this.activeFilters['providers_values'] = res.provider_values;
        }, err => {
            console.error('getCityList error', err);
        });
    }

    getAllProviderDataWithFilter() {
        this.providerOutcomeService.getAllMapData(this.activeFilters).subscribe(res => {
            console.log('getSideBarFilterMap res', res);
            this.mapData = res.data;
            this.initMap();
        }, err => {
            console.error('getCityList error', err);
        });
    }
}
