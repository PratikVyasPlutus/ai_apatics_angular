import { NgModule, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Component, OnInit, Inject } from '@angular/core';
import { LoginService } from "../../services/login.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { SessionService } from "../../services/session.service";
import { BroadcastService } from "../../services/broadcast.service";
import { AlertModule } from 'ng2-bootstrap';
import { GlobalEvents } from 'app/constants/global.events';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService, SessionService]
})
export class LoginComponent implements OnInit {

  loginError: string;
  returnUrl: string;
  sessionExists: boolean;
  loginstatus: boolean;

  constructor(private _loginService: LoginService,
    private router: Router,
    private _sessionService: SessionService,
    private _broadcastService: BroadcastService,
    public route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.checkUserSession();
  }

  checkUserSession(): void {
    this._sessionService.getUserSession().subscribe(
      res => {
        if (res) {
          console.log('check user session login !!');
          this.router.navigate([this.returnUrl]);
          this.sessionExists = true;
        } else {
          this.sessionExists = false;
        }
      },
      err => {
        console.log('Error while retrieving session data.');
        this.sessionExists = false;
      }
    )
  }

  onClose(): void {
    this.loginError = null;
  }

  broadcastLoginUpdate(): void {
    this._broadcastService.broadcast(GlobalEvents.LOGIN_UPDATE, { loggedIn: true });
  }

  login(username, password): void {

    this.loginError = null;
    this._loginService.login({ username, password }).subscribe(
      res => {
        if (res) {
          this.router.navigate([this.returnUrl || '/home']);
          this.broadcastLoginUpdate();
          this.loginstatus = true;
        } else {
          this.loginError = 'Invalid credentials. Please try again...';
          this.loginstatus = false;
        }
      },
      err => {
        this.loginError = 'Error while processing log in. Please try again...';
        this.loginstatus = false;
      },
      () => {
      }
    );
  }

}
