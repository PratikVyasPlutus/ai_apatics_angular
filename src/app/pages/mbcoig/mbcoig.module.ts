import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule } from '@angular/forms';

import { MBCOIGComponent } from './mbcoig.component';
import { MBCOIGRoutingModule } from './mbcoig-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [
        FormsModule,
        MBCOIGRoutingModule,
        ChartsModule
    ],
    declarations: []
})
export class MBCOIGModule { }
