import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MBCOIGComponent } from './mbcoig.component';

const routes: Routes = [
    {
        path: '',
        component: MBCOIGComponent,
        data: {
            title: 'MBC & OIG'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MBCOIGRoutingModule { }
