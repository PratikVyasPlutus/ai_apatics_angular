import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FDAService } from '../../services/fda.service';
import { Observable } from 'rxjs';

interface IServerResponse {
    items: string[];
    total: number;
}

declare var jQuery: any;
@Component({
    templateUrl: 'mbcoig.component.html',
    providers: [FDAService]
})
export class MBCOIGComponent implements OnInit {
    selectedProductId: string = "";
    selectedMBCOIG: any = {};
    pageNumber: number = 1;
    recordsToFetch: number = 10;
    totalRecords: number = 0;
    resFacets: any = {};
    asyncMBCOIG: Observable<any[]>;
    loading: boolean;
    searchMBCOIGText: string = "";
    selectedDOAY: string[] = [];
    selectedAT: string[] = [];
    selectedSP: string[] = [];
    selectedBT: string[] = [];
    rersMBCOIG: any[] = [];
    foundRecord: boolean = true;
    constructor(private zone: NgZone, private fdaService: FDAService, private _elmRef: ElementRef) {

    }

    searchMBCOIGFacets() {
        this.fdaService.searchMBCOIGFacets(this.searchMBCOIGText)
            .subscribe(res => {
                if (res.success) {
                    this.zone.run(() => {
                        this.resFacets = res.response;
                    });
                }
            }
            , error => {
            });
    }

    changeSelectFacet(objFacet, index) {
        this.zone.run(() => {
            let changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    this.selectedDOAY.push(objFacet.name);
                else if (index == 2)
                    this.selectedAT.push(objFacet.name);
                else if (index == 3)
                    this.selectedSP.push(objFacet.name);
                else if (index == 4)
                    this.selectedBT.push(objFacet.name);
            }
            else {
                let arrayIndex: number = -1;
                if (index == 1) {
                    arrayIndex = this.selectedDOAY.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedDOAY.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = this.selectedAT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedAT.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = this.selectedSP.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedSP.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = this.selectedBT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedBT.splice(arrayIndex, 1);
                }
            }
        });
        this.searchMBCOIGRecordsWithFilters(1);
    }

    removeSelectedFacet(objFacet, index) {
        this.zone.run(() => {
            objFacet.selected = false;
            let arrayIndex: number = -1;
            if (index == 1) {
                arrayIndex = this.selectedDOAY.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedDOAY.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = this.selectedAT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedAT.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = this.selectedSP.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedSP.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = this.selectedBT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedBT.splice(arrayIndex, 1);
            }
        });
        this.searchMBCOIGRecordsWithFilters(1);
    }

    clearSelectedFacets() {
        this.selectedDOAY = [];
        this.selectedAT = [];
        this.selectedSP = [];
        this.selectedBT = [];
    }

    searchMBCOIG() {
        this.clearSelectedFacets();
        this.searchMBCOIGRecordsWithFilters(1);
        this.searchMBCOIGFacets();
    }

    searchMBCOIGRecordsWithFilters(pageNum: number) {
        this.pageNumber = pageNum;
        let selectedDOAYString: string = "";
        let selectedATString: string = "";
        let selectedSPString: string = "";
        let selectedBTString: string = "";
        if (this.selectedDOAY && this.selectedDOAY.length > 0)
            selectedDOAYString = this.selectedDOAY.join();
        if (this.selectedAT && this.selectedAT.length > 0)
            selectedATString = this.selectedAT.join();
        if (this.selectedSP && this.selectedSP.length > 0)
            selectedSPString = this.selectedSP.join();
        if (this.selectedBT && this.selectedBT.length > 0)
            selectedBTString = this.selectedBT.join();

        this.asyncMBCOIG = this.fdaService.searchMBCOIGRecordsWithFilter(this.searchMBCOIGText, selectedDOAYString, selectedATString,
            selectedSPString, selectedBTString, this.pageNumber, this.recordsToFetch)
            .do((res: any) => {
                if (res.success) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.totalRecords;
                        this.rersMBCOIG = res.response.results || [];
                        this.foundRecord = res.response.totalRecords > 0;
                    });
                }
                else {
                    this.zone.run(() => {
                        this.totalRecords = 0;
                        this.rersMBCOIG = [];
                    });
                }
            })
            .map(res => res.success ? res.response.results : []);
    }

    ngOnInit(): void {
        this.searchMBCOIGRecordsWithFilters(1);
        this.searchMBCOIGFacets();
    }
}

/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals: string[], page: number): Observable<IServerResponse> {
    const perPage = 10;
    const start = (page - 1) * perPage;
    const end = start + perPage;

    return Observable
        .of({
            items: meals.slice(start, end),
            total: 100
        }).delay(1000);
}
