import { Component, OnInit, ViewEncapsulation } from '@angular/core';
//
import { ProviderOutcomeService } from '../../services/provider-outcome.service';

import { NvD3Module } from 'ng2-nvd3';

import 'd3';
import 'nvd3'

declare var jQuery: any;
declare var d3: any;
@Component({
    templateUrl: 'providerReports.component.html',
    providers: [ProviderOutcomeService, NvD3Module],
    styleUrls: [
        '../../../../node_modules/nvd3/build/nv.d3.css'
    ],
    encapsulation: ViewEncapsulation.None
})

export class ProviderReportsComponent implements OnInit {

    boxPlotOptions;
    boxPlotData;
    scatterOptions;
    scatterData;

    constructor(public providerOutcomeService: ProviderOutcomeService) {

    }

    ngOnInit() {
        //this.loadBoxPlotChart();
        //this.loadScatterChart();
    }

    loadBoxPlotChart() {
        this.boxPlotOptions = {
            chart: {
                type: 'boxPlotChart',
                height: 400,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 30,
                    left: 50
                },
                color: ['darkblue', 'darkorange', 'green', 'darkred', 'darkviolet'],
                x: function (d) { return d.label; },
                maxBoxWidth: 55,
                yDomain: [0, 500]
            }
        }
        this.boxPlotData = [
            {
                label: "Sample A",
                values: {
                    Q1: 180,
                    Q2: 200,
                    Q3: 250,
                    whisker_low: 0,
                    whisker_high: 400
                }
            },
            {
                label: "Sample B",
                values: {
                    Q1: 300,
                    Q2: 350,
                    Q3: 400,
                    whisker_low: 0,
                    whisker_high: 425
                }
            },
            {
                label: "Sample C",
                values: {
                    Q1: 100,
                    Q2: 200,
                    Q3: 300,
                    whisker_low: 0,
                    whisker_high: 400
                }
            }
        ];
    }

    loadScatterChart() {
        this.scatterOptions = {
            chart: {
                type: 'scatterChart',
                height: 400,
                color: d3.scale.category10().range(),
                scatter: {
                    onlyCircles: false
                },
                showDistX: true,
                showDistY: true,
                duration: 350,
                useInteractiveGuideline: false,
                tooltip: {
                    enabled: false,
                    useInteractiveGuideline: false
                },
                useVoronoi: false,
                xAxis: {
                    axisLabel: 'Combination of Predictors(TIN, Amount Billed/Paid/Reduced, PD CPT DIFF FLAG...)',
                    tickFormat: function (d) {
                        return null
                    },
                    ticks: 8,
                    axisLabelDistance: 30,
                    tickLength: 0
                },
                yAxis: {
                    axisLabel: 'Combination of Predictors(Holiday/WeekDay, Bills accepted/rejected...)',
                    tickFormat: function (d) {
                        return null
                    },
                    ticks: 8,
                    axisLabelDistance: 30,
                    tickLength: 0
                },
                //xDomain: [0, 1],
                //yDomain: [0, 1],
            }
        }
        let scatterData = [
            {
                color: "#00c5ad",
                key: "Most Valued Provider",
                value: 1,
                values: [
                    { x: 1, y: 1, size: 1, shape: "circle" }
                ]
            },
            {
                color: "#fdd105",
                key: "Medium Valued Provider",
                value: 2,
                values: [
                    { x: 2, y: 2, size: 2, shape: "circle" }
                ]
            },
            {
                color: "#f72b00",
                key: "Least Valued Provider",
                value: 3,
                values: [
                    { x: 2.5, y: 2.5, size: 3, shape: "circle" }
                ]
            }
        ];
        this.scatterData = dummyDataScatter(scatterData);
        console.log('this.scatterData', this.scatterData);
    }
}

function generateDataScatter(groups, points) {
    var data = [],
        shapes = ['circle'],
        random = d3.random.normal();

    for (var i = 0; i < groups; i++) {
        data.push({
            key: 'Group ' + i,
            values: []
        });

        for (var j = 0; j < points; j++) {
            data[i].values.push({
                x: random()
                , y: random()
                , size: Math.random()
                , shape: shapes[j % 6]
            });
        }
    }
    return data;
}

function dummyDataScatter(dummyData) {
    var data = [],
        shapes = ['circle'],
        random = d3.random.normal();
    console.log('dummyData', dummyData);
    for (var i = 0; i < dummyData.length; i++) {
        console.log('dummyData[i]', dummyData[i]);
        for (var j = 0; j < 20; j++) {
            dummyData[i].values.push({
                x: random()
                , y: random()
                , size: 10
                , shape: shapes[j % 6]
            });
        }
    }
    return dummyData;
}