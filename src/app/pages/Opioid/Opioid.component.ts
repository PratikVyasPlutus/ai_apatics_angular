import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
//
import { ProviderOutcomeService } from '../../services/provider-outcome.service';
import { setTimeout } from 'timers';
import { SelectComponent } from 'ng2-select'
declare var jQuery: any;

@Component({
    templateUrl: 'Opioid.component.html',
    providers: [ProviderOutcomeService],
})

export class OpioidComponent implements OnInit {
    @ViewChild('InformProviderModel') InformProviderModel;
    @ViewChild('AssignManagerModel') AssignManagerModel;

    public provideList: any = [];
    public selectedProvider: any = [];
    public selectedManager: any = [];
    public managerList: any = [];
    public tooltipData = [
        {
            "color": "#f7766d",
            "number": 1,
            "content": "#tooltip_1",
            "contentId": "tooltip_1",
            "tableData": [
                {
                    "DERIVED_SCHEDULE": "GENERAL : 0",
                    "AGE_GROUP_1": "BELOW 50 : 0",
                    "GENDER": "FEMALE : 0",
                },
                {
                    "DERIVED_SCHEDULE": "OPIOID : 1930",
                    "AGE_GROUP_1": "OVER 50 :1930",
                    "GENDER": "MALE : 1930",
                }
            ]
        },
        {
            "color": "#cc9600",
            "number": 2,
            "content": "#tooltip_2",
            "contentId": "tooltip_2",
            "tableData": [
                {
                    "DERIVED_SCHEDULE": "GENERAL : 2046",
                    "AGE_GROUP_1": "BELOW 50 : 0",
                    "GENDER": "FEMALE : 0",
                },
                {
                    "DERIVED_SCHEDULE": "OPIOID : 0",
                    "AGE_GROUP_1": "OVER 50 : 2046",
                    "GENDER": "MALE  : 2046",
                }
            ]
        },
        {
            "color": "#7cad00",
            "number": 3,
            "content": "#tooltip_3",
            "contentId": "tooltip_3",
            "tableData": [
                {
                    "DERIVED_SCHEDULE": "GENERAL : 0",
                    "AGE_GROUP_1": "BELOW 50 : 878",
                    "GENDER": "FEMALE : 0",
                },
                {
                    "DERIVED_SCHEDULE": "OPIOID : 878",
                    "AGE_GROUP_1": "OVER 50 : 0",
                    "GENDER": "MALE  : 878",
                }
            ]
        },
        {
            "color": "#009752",
            "number": 4,
            "content": "#tooltip_4",
            "contentId": "tooltip_4",
            "tableData": [
                {
                    "DERIVED_SCHEDULE": "GENERAL : 1090",
                    "AGE_GROUP_1": "BELOW 50 : 1090",
                    "GENDER": "FEMALE : 0",
                },
                {
                    "DERIVED_SCHEDULE": "OPIOID : 0",
                    "AGE_GROUP_1": "OVER 50 : 0",
                    "GENDER": "MALE  : 1090",
                }
            ]
        },
        {
            "color": "#00bec3",
            "number": 5,
            "content": "#tooltip_5",
            "contentId": "tooltip_5",
            "tableData": [
                {
                    "DERIVED_SCHEDULE": "GENERAL : 1903",
                    "AGE_GROUP_1": "BELOW 50 : 0",
                    "GENDER": "FEMALE : 1903",
                },
                {
                    "DERIVED_SCHEDULE": "OPIOID : 0",
                    "AGE_GROUP_1": "OVER 50 : 1903",
                    "GENDER": "MALE  : 0",
                }
            ]
        },
        {
            "color": "#00a8fe",
            "number": 6,
            "content": "#tooltip_6",
            "contentId": "tooltip_6",
            "tableData": [
                {
                    "DERIVED_SCHEDULE": "GENERAL : 0",
                    "AGE_GROUP_1": "BELOW 50 : 0",
                    "GENDER": "FEMALE : 1524",
                },
                {
                    "DERIVED_SCHEDULE": "OPIOID : 1524",
                    "AGE_GROUP_1": "OVER 50 : 1524",
                    "GENDER": "MALE  : 0",
                }
            ]
        },
        {
            "color": "#c67cfe",
            "number": 7,
            "content": "#tooltip_7",
            "contentId": "tooltip_7",
            "tableData": [
                {
                    "DERIVED_SCHEDULE": "GENERAL : 0",
                    "AGE_GROUP_1": "BELOW 50 : 342",
                    "GENDER": "FEMALE : 342",
                },
                {
                    "DERIVED_SCHEDULE": "OPIOID : 342",
                    "AGE_GROUP_1": "OVER 50 : 0",
                    "GENDER": "MALE  : 0",
                }
            ]
        },
        {
            "color": "#fe61cb",
            "number": 8,
            "content": "#tooltip_8",
            "contentId": "tooltip_8",
            "tableData": [
                {
                    "DERIVED_SCHEDULE": "GENERAL : 536",
                    "AGE_GROUP_1": "BELOW 50 : 536",
                    "GENDER": "FEMALE : 536",
                },
                {
                    "DERIVED_SCHEDULE": "OPIOID : 0",
                    "AGE_GROUP_1": "OVER 50 : 0",
                    "GENDER": "MALE  : 0",
                }
            ]
        }
    ];

    public riskData = {
        'AtRisk': {
            'NewClaimants': [
                { 'name': 'Alvis Klimpt', 'id': '1985459', 'comments': 'Prior Hepatitis C, Age, Demographic' },
                { 'name': 'Charisse Swetmore', 'id': '1940266', 'comments': 'Prior AA, criminal history, age' },
                { 'name': 'Kevin Bestiman', 'id': '3628763', 'comments': 'Prior ICD, unit exceeded' }
            ],
            'AssignedClaimants': [
                { 'name': 'Antonius Arnout', 'id': '3736645', 'caseManager': 'Delia', 'note': 'Patient is getting better. Provider prescribed Tylenol.' },
                { 'name': 'Fraser Dytham', 'id': '3391799', 'caseManager': 'Will', 'note': 'Patient willing to go for rehab after the injury treatment.' },
                { 'name': 'Mureil Ivison', 'id': '2810867', 'caseManager': 'Frank', 'note': 'Patient admitted to the hospital for overdose.' }
            ]
        },
        'MediumRisk': {
            'NewClaimants': [
                { 'name': 'Colas MacCook', 'id': '2093649', 'comments': 'Age, Demographic' },
                { 'name': 'Glen Haggath', 'id': '1573234', 'comments': 'age, prolonged injury' },
                { 'name': 'Livvy Dedrick', 'id': '1746951', 'comments': 'Third claim in 1 year' }
            ],
            'AssignedClaimants': [
                { 'name': 'Trueman Tabrett', 'id': '3736645', 'caseManager': 'Delia', 'note': 'Patient is getting better. Provider prescribed Tylenol.' },
                { 'name': 'Wenonah Pace', 'id': '3391799', 'caseManager': 'Will', 'note': 'Patient willing to go for rehab after the injury treatment.' },
                { 'name': 'Ida Ho', 'id': '2814741', 'caseManager': 'Frank', 'note': 'Patient admitted to the hospital for overdose.' }
            ]

        }
    };

    public isShowAtRisk = true;
    public isShowMediumRisk = false;
    public isShowLoad = false;

    constructor(public providerOutcomeService: ProviderOutcomeService) {

    }

    ngOnInit() {
        console.log('this.tooltipData', this.tooltipData);
        console.log('this.riskData', this.riskData);
        this.providerOutcomeService.getAllSideBarFilterForClaimant('').subscribe(res => {
            //console.log('getAllSideBarFilterForClaimant res', res);
            var tempArr1 = []
            for (var i = 0; i < res.providers.length; i++) {
                tempArr1.push({ id: res.providers[i]['id'], text: res.providers[i]['name'] });
            }
            this.provideList = tempArr1;

            var tempArr2 = []
            for (var i = 0; i < res.attorneys.length; i++) {
                tempArr2.push({ id: res.attorneys[i]['id'], text: res.attorneys[i]['name'] });
            }
            this.managerList = tempArr2;

            console.log('this.provideList', this.provideList);
            console.log('this.managerList', this.managerList);
        }, err => {

        });
    }

    ngAfterViewInit() {
        jQuery(document).ready(function () {
            jQuery('.opioid-tooltip').tooltipster();
        });
    }

    activeSideNavLink(index) {
        if (index == 1) {
            this.isShowAtRisk = true;
            this.isShowMediumRisk = false;
        } else {
            this.isShowAtRisk = false;
            this.isShowMediumRisk = true;
        }
    }

    openInformProviderModel() {
        this.InformProviderModel.show();
    }

    closeInformProviderModel() {
        this.InformProviderModel.hide();
    }

    sendMailToProvider() {
        this.isShowLoad = true;
        setTimeout(() => {
            this.selectedProvider = [];
            this.isShowLoad = false;
            jQuery('#ProvideMsgScccess1').show().delay(3000).fadeOut();
            setTimeout(() => {
                this.InformProviderModel.hide();
            }, 5000);
        }, 3500);
    }

    openAssignManagerModel() {
        this.AssignManagerModel.show();
    }

    closeAssignManagerModel() {
        this.AssignManagerModel.hide();
    }

    AssignManager() {
        this.isShowLoad = true;
        setTimeout(() => {
            this.selectedManager = [];
            this.isShowLoad = false;
            jQuery('#ProvideMsgScccess2').show().delay(3000).fadeOut();
            setTimeout(() => {
                this.AssignManagerModel.hide();
            }, 5000);
        }, 3500);
    }
}
