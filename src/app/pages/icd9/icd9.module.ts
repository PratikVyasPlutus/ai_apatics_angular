import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule } from '@angular/forms';

import { ICD9Component } from './icd9.component';
import { ICD9RoutingModule } from './icd9-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [
        FormsModule,
        ICD9RoutingModule,
        ChartsModule
    ],
    declarations: []
})
export class ICD9Module { }
