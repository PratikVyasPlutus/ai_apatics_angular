import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FDAService } from '../../services/fda.service';
import { Observable } from 'rxjs';

interface IServerResponse {
    items: string[];
    total: number;
}

declare var jQuery: any;
@Component({
    templateUrl: 'icd9.component.html',
    providers: [FDAService]
})
export class ICD9Component implements OnInit {
    selectedICD9: any = {};
    pageNumber: number = 1;
    recordsToFetch: number = 10;
    totalRecords: number = 0;
    resFacets: any = {};
    asyncICD9: Observable<any[]>;
    loading: boolean;
    searchICD9Text: string = "";
    selectedCDE: string[] = [];
    selectedCDE_DSC: string[] = [];
    selectedCDE_TYP: string[] = [];
    selectedCDE_TYP_DSC: string[] = [];
    rersICD9: any[] = [];
    foundRecord: boolean = true;
    constructor(private zone: NgZone, private fdaService: FDAService, private _elmRef: ElementRef) {

    }

    searchICD9Facets() {
        this.fdaService.searchICD9Facets(this.searchICD9Text)
            .subscribe(res => {
                if (res.success) {
                    this.zone.run(() => {
                        this.resFacets = res.response;
                    });
                }
            }
            , error => {
            });
    }

    changeSelectFacet(objFacet, index) {
        this.zone.run(() => {
            let changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    this.selectedCDE.push(objFacet.name);
                else if (index == 2)
                    this.selectedCDE_DSC.push(objFacet.name);
                else if (index == 3)
                    this.selectedCDE_TYP.push(objFacet.name);
                else if (index == 4)
                    this.selectedCDE_TYP_DSC.push(objFacet.name);
            }
            else {
                let arrayIndex: number = -1;
                if (index == 1) {
                    arrayIndex = this.selectedCDE.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedCDE.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = this.selectedCDE_DSC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedCDE_DSC.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = this.selectedCDE_TYP.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedCDE_TYP.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = this.selectedCDE_TYP_DSC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedCDE_TYP_DSC.splice(arrayIndex, 1);
                }
            }
        });
        this.searchICD9RecordsWithFilters(1);
    }

    removeSelectedFacet(objFacet, index) {
        this.zone.run(() => {
            objFacet.selected = false;
            let arrayIndex: number = -1;
            if (index == 1) {
                arrayIndex = this.selectedCDE.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedCDE.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = this.selectedCDE_DSC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedCDE_DSC.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = this.selectedCDE_TYP.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedCDE_TYP.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = this.selectedCDE_TYP_DSC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedCDE_TYP_DSC.splice(arrayIndex, 1);
            }
        });
        this.searchICD9RecordsWithFilters(1);
    }

    clearSelectedFacets() {
        this.selectedCDE = [];
        this.selectedCDE_DSC = [];
        this.selectedCDE_TYP = [];
        this.selectedCDE_TYP_DSC = [];
    }

    searchICD9() {
        this.clearSelectedFacets();
        this.searchICD9RecordsWithFilters(1);
        this.searchICD9Facets();
    }

    searchICD9RecordsWithFilters(pageNum: number) {
        this.pageNumber = pageNum;
        let selectedCDEString: string = "";
        let selectedCDE_DSCString: string = "";
        let selectedCDE_TYPString: string = "";
        let selectedCDE_TYP_DSCString: string = "";
        if (this.selectedCDE && this.selectedCDE.length > 0)
            selectedCDEString = this.selectedCDE.join();
        if (this.selectedCDE_DSC && this.selectedCDE_DSC.length > 0)
            selectedCDE_DSCString = this.selectedCDE_DSC.join();
        if (this.selectedCDE_TYP && this.selectedCDE_TYP.length > 0)
            selectedCDE_TYPString = this.selectedCDE_TYP.join();
        if (this.selectedCDE_TYP_DSC && this.selectedCDE_TYP_DSC.length > 0)
            selectedCDE_TYP_DSCString = this.selectedCDE_TYP_DSC.join();

        this.asyncICD9 = this.fdaService.searchICD9RecordsWithFilter(this.searchICD9Text, selectedCDEString, selectedCDE_DSCString,
            selectedCDE_TYPString, selectedCDE_TYP_DSCString, this.pageNumber, this.recordsToFetch)
            .do((res: any) => {
                if (res.success) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.totalRecords;
                        this.rersICD9 = res.response.results || [];
                        this.foundRecord = res.response.totalRecords > 0;
                    });
                }
                else {
                    this.zone.run(() => {
                        this.totalRecords = 0;
                        this.rersICD9 = [];
                    });
                }
            })
            .map(res => res.success ? res.response.results : []);
    }

    ngOnInit(): void {
        this.searchICD9RecordsWithFilters(1);
        this.searchICD9Facets();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    }
}

/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals: string[], page: number): Observable<IServerResponse> {
    const perPage = 10;
    const start = (page - 1) * perPage;
    const end = start + perPage;

    return Observable
        .of({
            items: meals.slice(start, end),
            total: 100
        }).delay(1000);
}
