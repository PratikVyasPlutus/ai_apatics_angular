import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ICD9Component } from './icd9.component';

const routes: Routes = [
    {
        path: '',
        component: ICD9Component,
        data: {
            title: 'ICD9'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ICD9RoutingModule { }
