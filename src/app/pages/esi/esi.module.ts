import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ESIComponent } from './esi.component';
import { ESIRoutingModule } from './esi-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [
        FormsModule,
        ESIRoutingModule
    ],
    declarations: []
})
export class ESIModule { }
