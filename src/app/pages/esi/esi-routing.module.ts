import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ESIComponent } from './esi.component';

const routes: Routes = [
    {
        path: '',
        component: ESIComponent,
        data: {
            title: 'ESI'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ESIRoutingModule { }
