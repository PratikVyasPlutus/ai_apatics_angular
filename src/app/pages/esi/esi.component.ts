import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FDAService } from '../../services/fda.service';
import { Observable } from 'rxjs';

interface IServerResponse {
    items: string[];
    total: number;
}

declare var jQuery: any;
@Component({
    templateUrl: 'esi.component.html',
    providers: [FDAService]
})
export class ESIComponent implements OnInit {
    selectedProductId: string = "";
    selectedESI: any = {};
    pageNumber: number = 1;
    recordsToFetch: number = 10;
    totalRecords: number = 0;
    resFacets: any = {};
    asyncESI: Observable<any[]>;
    loading: boolean;
    searchESIText: string = "";
    selectedPHN: string[] = [];
    selectedPLC: string[] = [];
    selectedPZC: string[] = [];
    selectedPLN: string[] = [];
    selectedCC: string[] = [];
    selectedDT: string[] = [];
    rersESI: any[] = [];
    foundRecord: boolean = true;
    constructor(private zone: NgZone, private fdaService: FDAService, private _elmRef: ElementRef) {

    }

    billedAmountRange: number[] = [0, 5000];
    public billedAmountConfig: any = {
        connect: true,
        start: [0, 5000],
        step: 100,
        pageSteps: 10,  // number of page steps, defaults to 10
        range: {
            min: 0,
            max: 5000
        },
        format: {
            to(value: any): any {
                return parseFloat(value.toFixed());
            },
            from(value: any): any {
                return parseFloat(value).toFixed(2);
            }
        }
    };

    onChangeBilledAmount(event) {
        this.searchESIRecordsWithFilters(1);
    }

    claimAmountRange: number[] = [0, 90000];
    public claimAmountConfig: any = {
        connect: true,
        start: [0, 90000],
        step: 5000,
        pageSteps: 0,  // number of page steps, defaults to 10
        range: {
            min: 0,
            max: 90000
        },
        format: {
            to(value: any): any {
                return parseFloat(value.toFixed());
            },
            from(value: any): any {
                return parseFloat(value).toFixed(2);
            }
        }
    };

    onChangeClaimAmount(event) {
       this.searchESIRecordsWithFilters(1);
    }

    searchESIFacets() {
        this.fdaService.searchESIFacets(this.searchESIText)
            .subscribe(res => {
                if (res.success) {
                    this.zone.run(() => {
                        this.resFacets = res.response;
                    });
                }
            }
            , error => {
            });
    }

    changeSelectFacet(objFacet, index) {
        this.zone.run(() => {
            let changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    this.selectedPHN.push(objFacet.name);
                else if (index == 2)
                    this.selectedPLC.push(objFacet.name);
                else if (index == 3)
                    this.selectedPZC.push(objFacet.name);
                else if (index == 4)
                    this.selectedPLN.push(objFacet.name);
                else if (index == 5)
                    this.selectedCC.push(objFacet.name);
                else if (index == 6)
                    this.selectedDT.push(objFacet.name);
            }
            else {
                let arrayIndex: number = -1;
                if (index == 1) {
                    arrayIndex = this.selectedPHN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPHN.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = this.selectedPLC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPLC.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = this.selectedPZC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPZC.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = this.selectedPLN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPLN.splice(arrayIndex, 1);
                }
                else if (index == 5) {
                    arrayIndex = this.selectedCC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedCC.splice(arrayIndex, 1);
                }
                else if (index == 6) {
                    arrayIndex = this.selectedDT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedDT.splice(arrayIndex, 1);
                }
            }
        });
        this.searchESIRecordsWithFilters(1);
    }

    removeSelectedFacet(objFacet, index) {
        this.zone.run(() => {
            objFacet.selected = false;
            let arrayIndex: number = -1;
            if (index == 1) {
                arrayIndex = this.selectedPHN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPHN.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = this.selectedPLC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPLC.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = this.selectedPZC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPZC.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = this.selectedPLN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPLN.splice(arrayIndex, 1);
            }
            else if (index == 5) {
                arrayIndex = this.selectedCC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedCC.splice(arrayIndex, 1);
            }
            else if (index == 6) {
                arrayIndex = this.selectedDT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedDT.splice(arrayIndex, 1);
            }
        });
        this.searchESIRecordsWithFilters(1);
    }

    clearSelectedFacets() {
        this.selectedPHN = [];
        this.selectedPLC = [];
        this.selectedPZC = [];
        this.selectedPLN = [];
        this.selectedCC = [];
        this.selectedDT = [];
        this.billedAmountRange = [0, 5000];
        this.claimAmountRange = [0, 90000];
        // this.resFacets.pharmacyNames.forEach(objFacet => {
        //     objFacet.selected = false;
        // });
        // this.resFacets.pharmacyLocationCities.forEach(objFacet => {
        //     objFacet.selected = false;
        // });
        // this.resFacets.pharmacyZipCodes.forEach(objFacet => {
        //     objFacet.selected = false;
        // });
        // this.resFacets.prescriberLastNames.forEach(objFacet => {
        //     objFacet.selected = false;
        // });
        // this.resFacets.claimCounts.forEach(objFacet => {
        //     objFacet.selected = false;
        // });
        // this.resFacets.deaTypes.forEach(objFacet => {
        //     objFacet.selected = false;
        // });
    }

    searchESI() {
        this.clearSelectedFacets();
        this.searchESIRecordsWithFilters(1);
        this.searchESIFacets();
    }

    searchESIRecordsWithFilters(pageNum: number) {
        this.pageNumber = pageNum;
        let selectedPHNString: string = "";
        let selectedPLCString: string = "";
        let selectedPZCString: string = "";
        let selectedPLNString: string = "";
        let selectedCCString: string = "";
        let selectedDTString: string = "";
        let selectedBARtring: string = "";
        let selectedCARtring: string = "";
        if (this.selectedPHN && this.selectedPHN.length > 0)
            selectedPHNString = this.selectedPHN.join().replace(/&/g, '`').replace(/#/g, '^');
        if (this.selectedPLC && this.selectedPLC.length > 0)
            selectedPLCString = this.selectedPLC.join().replace(/&/g, '`').replace(/#/g, '^');
        if (this.selectedPZC && this.selectedPZC.length > 0)
            selectedPZCString = this.selectedPZC.join().replace(/&/g, '`').replace(/#/g, '^');
        if (this.selectedPLN && this.selectedPLN.length > 0)
            selectedPLNString = this.selectedPLN.join().replace(/&/g, '`').replace(/#/g, '^');
        if (this.selectedCC && this.selectedCC.length > 0)
            selectedCCString = this.selectedCC.join().replace(/&/g, '`').replace(/#/g, '^');
        if (this.selectedDT && this.selectedDT.length > 0)
            selectedDTString = this.selectedDT.join().replace(/&/g, '`').replace(/#/g, '^');
        if (this.billedAmountRange && this.billedAmountRange.length > 0)
            selectedBARtring = this.billedAmountRange.join();
        if (this.claimAmountRange && this.claimAmountRange.length > 0)
            selectedCARtring = this.claimAmountRange.join();

        this.asyncESI = this.fdaService.searchESIRecordsWithFilter(this.searchESIText, selectedPHNString, selectedPLCString,
            selectedPZCString, selectedPLNString, selectedCCString, selectedDTString, selectedBARtring, selectedCARtring, this.pageNumber, this.recordsToFetch)
            .do((res: any) => {
                if (res.success) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.totalRecords;
                        this.rersESI = res.response.results || [];
                        this.foundRecord = res.response.totalRecords > 0;
                    });
                }
                else {
                    this.zone.run(() => {
                        this.totalRecords = 0;
                        this.rersESI = [];
                    });
                }
            })
            .map(res => res.success ? res.response.results : []);
    }

    ngOnInit(): void {
        this.searchESIRecordsWithFilters(1);
        this.searchESIFacets();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    }
}

/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals: string[], page: number): Observable<IServerResponse> {
    const perPage = 10;
    const start = (page - 1) * perPage;
    const end = start + perPage;

    return Observable
        .of({
            items: meals.slice(start, end),
            total: 100
        }).delay(1000);
}
