import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RevenueCodeComponent } from './revenuecode.component';

const routes: Routes = [
    {
        path: '',
        component: RevenueCodeComponent,
        data: {
            title: 'Revenue Code'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RevenueCodeRoutingModule { }
