import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule } from '@angular/forms';

import { RevenueCodeComponent } from './revenuecode.component';
import { RevenueCodeRoutingModule } from './revenuecode-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [
        FormsModule,
        RevenueCodeRoutingModule,
        ChartsModule
    ],
    declarations: []
})
export class RevenueCodeModule { }
