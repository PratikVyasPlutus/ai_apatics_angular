import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FDAService } from '../../services/fda.service';
import { Observable } from 'rxjs';

interface IServerResponse {
    items: string[];
    total: number;
}

declare var jQuery: any;
@Component({
    templateUrl: 'revenuecode.component.html',
    providers: [FDAService]
})
export class RevenueCodeComponent implements OnInit {
    selectedProductId: string = "";
    selectedRevenueCode: any = {};
    pageNumber: number = 1;
    recordsToFetch: number = 10;
    totalRecords: number = 0;
    resFacets: any = {};
    asyncRevenueCode: Observable<any[]>;
    loading: boolean;
    searchRevenueCodeText: string = "";
    selectedREV_CDE: string[] = [];
    selectedREV_DSC: string[] = [];
    rersRevenueCode: any[] = [];
    foundRecord: boolean = true;
    constructor(private zone: NgZone, private fdaService: FDAService, private _elmRef: ElementRef) {

    }

    searchRevenueCodeFacets() {
        this.fdaService.searchRevenueCodeFacets(this.searchRevenueCodeText)
            .subscribe(res => {
                if (res.success) {
                    this.zone.run(() => {
                        this.resFacets = res.response;
                    });
                }
            }
            , error => {
            });
    }

    changeSelectFacet(objFacet, index) {
        this.zone.run(() => {
            let changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    this.selectedREV_CDE.push(objFacet.name);
                else if (index == 2)
                    this.selectedREV_DSC.push(objFacet.name);
            }
            else {
                let arrayIndex: number = -1;
                if (index == 1) {
                    arrayIndex = this.selectedREV_CDE.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedREV_CDE.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = this.selectedREV_DSC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedREV_DSC.splice(arrayIndex, 1);
                }
            }
        });
        this.searchRevenueCodeRecordsWithFilters(1);
    }

    removeSelectedFacet(objFacet, index) {
        this.zone.run(() => {
            objFacet.selected = false;
            let arrayIndex: number = -1;
            if (index == 1) {
                arrayIndex = this.selectedREV_CDE.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedREV_CDE.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = this.selectedREV_DSC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedREV_DSC.splice(arrayIndex, 1);
            }
        });
        this.searchRevenueCodeRecordsWithFilters(1);
    }

    clearSelectedFacets() {
        this.selectedREV_CDE = [];
        this.selectedREV_DSC = [];
    }

    searchRevenueCode() {
        this.clearSelectedFacets();
        this.searchRevenueCodeRecordsWithFilters(1);
        this.searchRevenueCodeFacets();
    }

    searchRevenueCodeRecordsWithFilters(pageNum: number) {
        this.pageNumber = pageNum;
        let selectedREV_CDEString: string = "";
        let selectedREV_DSCString: string = "";
        if (this.selectedREV_CDE && this.selectedREV_CDE.length > 0)
            selectedREV_CDEString = this.selectedREV_CDE.join();
        if (this.selectedREV_DSC && this.selectedREV_DSC.length > 0)
            selectedREV_DSCString = this.selectedREV_DSC.join();

        this.asyncRevenueCode = this.fdaService.searchRevenueCodeRecordsWithFilter(this.searchRevenueCodeText, selectedREV_CDEString, selectedREV_DSCString,
            this.pageNumber, this.recordsToFetch)
            .do((res: any) => {
                if (res.success) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.totalRecords;
                        this.rersRevenueCode = res.response.results || [];
                        this.foundRecord = res.response.totalRecords > 0;
                    });
                }
                else {
                    this.zone.run(() => {
                        this.totalRecords = 0;
                        this.rersRevenueCode = [];
                    });
                }
            })
            .map(res => res.success ? res.response.results : []);
    }

    ngOnInit(): void {
        this.searchRevenueCodeRecordsWithFilters(1);
        this.searchRevenueCodeFacets();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    }
}

/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals: string[], page: number): Observable<IServerResponse> {
    const perPage = 10;
    const start = (page - 1) * perPage;
    const end = start + perPage;

    return Observable
        .of({
            items: meals.slice(start, end),
            total: 100
        }).delay(1000);
}
