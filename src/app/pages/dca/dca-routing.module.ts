import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DCAComponent } from './dca.component';

const routes: Routes = [
    {
        path: '',
        component: DCAComponent,
        data: {
            title: 'DCA'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DCARoutingModule { }
