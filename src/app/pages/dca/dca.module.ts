import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule } from '@angular/forms';

import { DCAComponent } from './dca.component';
import { DCARoutingModule } from './dca-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [
        FormsModule,
        DCARoutingModule,
        ChartsModule
    ],
    declarations: []
})
export class DCAModule { }
