import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FDAService } from '../../services/fda.service';
import { Observable } from 'rxjs';

interface IServerResponse {
    items: string[];
    total: number;
}

declare var jQuery: any;
@Component({
    templateUrl: 'dca.component.html',
    providers: [FDAService]
})
export class DCAComponent implements OnInit {
    selectedProductId: string = "";
    selectedDCA: any = {};
    pageNumber: number = 1;
    recordsToFetch: number = 10;
    totalRecords: number = 0;
    resFacets: any = {};
    asyncDCA: Observable<any[]>;
    loading: boolean;
    searchDCAText: string = "";
    selectedAN: string[] = [];
    selectedLT: string[] = [];
    selectedSC: string[] = [];
    selectedLS: string[] = [];
    rersDCA: any[] = [];
    foundRecord: boolean = true;
    constructor(private zone: NgZone, private fdaService: FDAService, private _elmRef: ElementRef) {

    }

    searchDCAFacets() {
        this.fdaService.searchDCAFacets(this.searchDCAText)
            .subscribe(res => {
                if (res.success) {
                    this.zone.run(() => {
                        this.resFacets = res.response;
                    });
                }
            }
            , error => {
            });
    }

    changeSelectFacet(objFacet, index) {
        this.zone.run(() => {
            let changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    this.selectedAN.push(objFacet.name);
                else if (index == 2)
                    this.selectedLT.push(objFacet.name);
                else if (index == 3)
                    this.selectedSC.push(objFacet.name);
                else if (index == 4)
                    this.selectedLS.push(objFacet.name);
            }
            else {
                let arrayIndex: number = -1;
                if (index == 1) {
                    arrayIndex = this.selectedAN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedAN.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = this.selectedLT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedLT.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = this.selectedSC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedSC.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = this.selectedLS.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedLS.splice(arrayIndex, 1);
                }
            }
        });
        this.searchDCARecordsWithFilters(1);
    }

    removeSelectedFacet(objFacet, index) {
        this.zone.run(() => {
            objFacet.selected = false;
            let arrayIndex: number = -1;
            if (index == 1) {
                arrayIndex = this.selectedAN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedAN.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = this.selectedLT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedLT.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = this.selectedSC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedSC.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = this.selectedLS.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedLS.splice(arrayIndex, 1);
            }
        });
        this.searchDCARecordsWithFilters(1);
    }

    clearSelectedFacets() {
        this.selectedAN = [];
        this.selectedLT = [];
        this.selectedSC = [];
        this.selectedLS = [];
        this.resFacets.agencyNames.forEach(objFacet => {
            objFacet.selected = false;
        });
        this.resFacets.licenseTypes.forEach(objFacet => {
            objFacet.selected = false;
        });
        this.resFacets.specialityCodes.forEach(objFacet => {
            objFacet.selected = false;
        });
        this.resFacets.licenseStatuses.forEach(objFacet => {
            objFacet.selected = false;
        });
    }

    searchDCA() {
        this.clearSelectedFacets();
        this.searchDCARecordsWithFilters(1);
        this.searchDCAFacets();
    }

    searchDCARecordsWithFilters(pageNum: number) {
        this.pageNumber = pageNum;
        let selectedANString: string = "";
        let selectedLTString: string = "";
        let selectedSCString: string = "";
        let selectedLSString: string = "";
        if (this.selectedAN && this.selectedAN.length > 0)
            selectedANString = this.selectedAN.join();
        if (this.selectedLT && this.selectedLT.length > 0)
            selectedLTString = this.selectedLT.join();
        if (this.selectedSC && this.selectedSC.length > 0)
            selectedSCString = this.selectedSC.join();
        if (this.selectedLS && this.selectedLS.length > 0)
            selectedLSString = this.selectedLS.join();

        this.asyncDCA = this.fdaService.searchDCARecordsWithFilter(this.searchDCAText, selectedANString, selectedLTString,
            selectedSCString, selectedLSString, this.pageNumber, this.recordsToFetch)
            .do((res: any) => {
                if (res.success) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.totalRecords;
                        this.rersDCA = res.response.results || [];
                        this.foundRecord = res.response.totalRecords > 0;
                    });
                }
                else {
                    this.zone.run(() => {
                        this.totalRecords = 0;
                        this.rersDCA = [];
                    });
                }
            })
            .map(res => res.success ? res.response.results : []);
    }

    ngOnInit(): void {
        this.searchDCARecordsWithFilters(1);
        this.searchDCAFacets();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    }
}

/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals: string[], page: number): Observable<IServerResponse> {
    const perPage = 10;
    const start = (page - 1) * perPage;
    const end = start + perPage;

    return Observable
        .of({
            items: meals.slice(start, end),
            total: 100
        }).delay(1000);
}
