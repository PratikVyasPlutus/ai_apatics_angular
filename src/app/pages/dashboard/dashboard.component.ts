import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router, NavigationEnd } from "@angular/router";
import { FDAService } from '../../services/fda.service';
import { Jsonp, Headers } from '@angular/http';

import { SessionService } from '../../services/session.service';

@Component({
    templateUrl: 'dashboard.component.html',
    providers: [FDAService, SessionService]
})
export class DashboardComponent implements OnInit {
    loggedIn: boolean = false;
    constructor(private router: Router, private _sessionService: SessionService, private fdaService: FDAService, private _jsonp: Jsonp) {

    }

    public disabled: boolean = false;
    public status: { isopen: boolean } = { isopen: false };

    public toggleDropdown($event: MouseEvent): void {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    }
    logOut() {
        this._sessionService.clearUserSession().subscribe(
            res => {
                this.loggedIn = false;
                this.router.navigate(['/login']);
            },
            err => {
                console.log('Error while clearing session data.')
            }
        )
    }
    getAuthenticationToken() {
        this.fdaService.getAuthToken()
            .subscribe(res => {
                if (res.access_token) {
                    window.localStorage.setItem('apaticsAuthToken', res.access_token);
                }
            }
            , error => {
            });
    }

    ngOnInit(): void {
        //this.getAuthenticationToken();
    }
}
