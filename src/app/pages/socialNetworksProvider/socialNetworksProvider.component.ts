import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//
import { ProviderOutcomeService } from '../../services/provider-outcome.service';



declare var jQuery: any;
declare var vis: any;
@Component({
    templateUrl: 'socialNetworksProvider.component.html',
    providers: [ProviderOutcomeService],
})

export class SocialNetworksProviderComponent implements OnInit {

    activeFilters: any = {};
    isShowLoad: boolean = false;
    searchQuery: any = "";
    tableOneData: any = [];
    tableTwoData: any = [];
    tableThreeData: any = [];

    constructor(public providerOutcomeService: ProviderOutcomeService, private element: ElementRef, private route: ActivatedRoute) {
        this.initFilter();
    }

    ngOnInit() {

        this.providerOutcomeService.getAllSideBarFilterForProviderNetwork('').subscribe(res => {
            console.log('getAllSideBarFilterForClaimant res', res);
            this.activeFilters['providers'] = res.providers;
            this.activeFilters['secondary_providers'] = res.secondary_providers;
            this.activeFilters['patients'] = res.patients;
            this.route
                .queryParams
                .subscribe(params => {
                    if (params.provider_firstname) {
                        var provider_name = params.provider_firstname + ' ' + params.provider_lastname;
                        console.log('provider_name', provider_name);
                        for (var key in res.providers) {
                            console.log('key', res.providers[key].name);
                            if (res.providers[key].name == provider_name) {
                                console.log('INNN', res.providers[key].name);
                                this.changeProvSelection(res.providers[key]);
                            }
                        }
                    } else {
                        this.getAllProviderDataWithFilter();
                    }
                });

        }, err => {

        });

    }

    ngAfterViewInit() {

    }

    initFilter() {
        this.activeFilters['providers'] = [];
        this.activeFilters['secondary_providers'] = [];
        this.activeFilters['patients'] = [];
        this.activeFilters['searchQuery'] = "";
    }

    changeProvSelection(prov) {
        let changedSelection = !prov.selected;
        prov.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    changeSecProvSelection(sprov) {
        let changedSelection = !sprov.selected;
        sprov.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    changePatientSelection(patient) {
        let changedSelection = !patient.selected;
        patient.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    searchProviderSummary() {
        this.activeFilters['searchQuery'] = this.searchQuery;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    removeSearchQuery() {
        this.activeFilters['searchQuery'] = "";
        this.searchQuery = "";
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    }

    getAllProviderDataWithFilter() {
        this.isShowLoad = true;
        this.providerOutcomeService.getAllProviderNetworkData(this.activeFilters).subscribe(res => {
            console.log('getAllClaimantData res', res);
            var nodes = [];
            res.nodes.forEach(element => {
                nodes.push({
                    id: element.id,
                    label: element.label,
                    title: element.title,
                    level: element.level,
                    color: {
                        background: element.background,
                        highlight: element.highlight,
                        hover: element.hover,
                    }
                });
            });

            var edges = [];
            res.edges.forEach(element => {
                edges.push(element);
            });

            this.loadGraph(nodes, edges);
            this.tableOneData = res.table_one;
            this.tableTwoData = res.table_two;
            this.tableThreeData = res.table_three;
            this.isShowLoad = false;
        }, err => {
            console.log('getAllClaimantData err', err);
            this.isShowLoad = false;
        });
    }

    loadGraph(nodes, edges) {
        var container = document.querySelector('#network-graph');
        var data = {
            nodes: nodes,
            edges: edges
        };
        var options = {
            nodes: {
                shape: 'dot',
                font: { size: 10 }
            },
            interaction: {
                zoomView: true,
                hover: true,
                navigationButtons: true,
            }
        }
        let network = new vis.Network(container, data, options);
    }
}
