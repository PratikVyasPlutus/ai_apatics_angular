import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SecretaryOfStateComponent } from './secretaryofstate.component';

const routes: Routes = [
    {
        path: '',
        component: SecretaryOfStateComponent,
        data: {
            title: 'Secretary of State'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SecretaryOfStateRoutingModule { }
