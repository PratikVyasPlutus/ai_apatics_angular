import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule } from '@angular/forms';

import { SecretaryOfStateComponent } from './secretaryofstate.component';
import { SecretaryOfStateRoutingModule } from './secretaryofstate-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [
        FormsModule,
        SecretaryOfStateRoutingModule,
        ChartsModule
    ],
    declarations: []
})
export class SecretaryOfStateModule { }
