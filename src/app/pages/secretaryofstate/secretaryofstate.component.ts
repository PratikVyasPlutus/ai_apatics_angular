import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FDAService } from '../../services/fda.service';
import { Observable } from 'rxjs';

interface IServerResponse {
    items: string[];
    total: number;
}

declare var jQuery: any;
@Component({
    templateUrl: 'secretaryofstate.component.html',
    providers: [FDAService]
})
export class SecretaryOfStateComponent implements OnInit {
    selectedProductId: string = "";
    selectedSecretaryOfState: any = {};
    pageNumber: number = 1;
    recordsToFetch: number = 10;
    totalRecords: number = 0;
    resFacets: any = {};
    asyncSecretaryOfState: Observable<any[]>;
    loading: boolean;
    searchSecretaryOfStateText: string = "";
    selectedCN: string[] = [];
    selectedCOL: string[] = [];
    selectedCT: string[] = [];
    selectedCC: string[] = [];
    rerSecretaryOfState: any[] = [];
    foundRecord: boolean = true;
    constructor(private zone: NgZone, private fdaService: FDAService, private _elmRef: ElementRef) {

    }

    searchSecretaryOfStateFacets() {
        this.fdaService.searchSecretaryOfStateFacets(this.searchSecretaryOfStateText)
            .subscribe(res => {
                if (res.success) {
                    this.zone.run(() => {
                        this.resFacets = res.response;
                    });
                }
            }
            , error => {
            });
    }

    changeSelectFacet(objFacet, index) {
        this.zone.run(() => {
            let changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    this.selectedCN.push(objFacet.name);
                else if (index == 2)
                    this.selectedCOL.push(objFacet.name);
                else if (index == 3)
                    this.selectedCT.push(objFacet.name);
                else if (index == 4)
                    this.selectedCC.push(objFacet.name);
            }
            else {
                let arrayIndex: number = -1;
                if (index == 1) {
                    arrayIndex = this.selectedCN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedCN.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = this.selectedCOL.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedCOL.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = this.selectedCT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedCT.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = this.selectedCC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedCC.splice(arrayIndex, 1);
                }
            }
        });
        this.searchSecretaryOfStateRecordsWithFilters(1);
    }

    removeSelectedFacet(objFacet, index) {
        this.zone.run(() => {
            objFacet.selected = false;
            let arrayIndex: number = -1;
            if (index == 1) {
                arrayIndex = this.selectedCN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedCN.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = this.selectedCOL.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedCOL.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = this.selectedCT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedCT.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = this.selectedCC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedCC.splice(arrayIndex, 1);
            }
        });
        this.searchSecretaryOfStateRecordsWithFilters(1);
    }

    clearSelectedFacets() {
        this.selectedCN = [];
        this.selectedCOL = [];
        this.selectedCT = [];
        this.selectedCC = [];
    }

    searchSecretaryOfState() {
        this.clearSelectedFacets();
        this.searchSecretaryOfStateRecordsWithFilters(1);
        this.searchSecretaryOfStateFacets();
    }

    searchSecretaryOfStateRecordsWithFilters(pageNum: number) {
        this.pageNumber = pageNum;
        let selectedCNString: string = "";
        let selectedCOLString: string = "";
        let selectedCTString: string = "";
        let selectedCCString: string = "";
        if (this.selectedCN && this.selectedCN.length > 0)
            selectedCNString = this.selectedCN.join();
        if (this.selectedCOL && this.selectedCOL.length > 0)
            selectedCOLString = this.selectedCOL.join();
        if (this.selectedCT && this.selectedCT.length > 0)
            selectedCTString = this.selectedCT.join();
        if (this.selectedCC && this.selectedCC.length > 0)
            selectedCCString = this.selectedCC.join();

        this.asyncSecretaryOfState = this.fdaService.searchSecretaryOfStateRecordsWithFilter(this.searchSecretaryOfStateText, selectedCNString, selectedCOLString,
            selectedCTString, selectedCCString, this.pageNumber, this.recordsToFetch)
            .do((res: any) => {
                if (res.success) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.totalRecords;
                        if (res.response.results)
                            res.response.results.forEach(objSOS => {
                                objSOS.mailAddressString = this.generateAddressString(objSOS.mailAddressLine1, objSOS.mailAddressLine2, objSOS.mailAddressCity, objSOS.mailAddressState, objSOS.mailAddressZipCode);
                                objSOS.agentAddressString = this.generateAddressString(objSOS.agentAddress1, objSOS.agentAddress2, objSOS.agentAddressCity, objSOS.agentAddressState, objSOS.agentAddressZipCode);
                                objSOS.cEOPartnerAddressString = this.generateAddressString(objSOS.cEOPartnerAddress1, objSOS.cEOPartnerAddress2, objSOS.cEOPartnerCity, objSOS.cEOPartnerState, objSOS.cEOPartnerZipCode);
                                objSOS.generalPartner1AddressString = this.generateAddressString(objSOS.generalPartner1Address, null, objSOS.generalPartner1City, objSOS.generalPartner1State, objSOS.generalPartner1ZipCode);
                                objSOS.generalPartner2AddressString = this.generateAddressString(objSOS.generalPartner2Address, null, objSOS.generalPartner2City, objSOS.generalPartner2State, objSOS.generalPartner2ZipCode);
                                objSOS.californiaAddressString = this.generateAddressString(objSOS.californiaAddress, null, objSOS.californiaCity, null, objSOS.californiaZipCode);

                            });
                        this.rerSecretaryOfState = res.response.results || [];
                        this.foundRecord = res.response.totalRecords > 0;
                    });
                }
                else {
                    this.zone.run(() => {
                        this.totalRecords = 0;
                        this.rerSecretaryOfState = [];
                    });
                }
            })
            .map(res => res.success ? res.response.results : []);
    }

    generateAddressString(line1, line2, city, state, zipcode) {
        let address: string = "";
        if (line1)
            address += line1 + ", ";
        if (line2)
            address += line2 + ", ";
        if (city)
            address += city + ", ";
        if (state)
            address += state + ", ";
        if (zipcode)
            address += zipcode + ", ";
        if (address.length > 0)
            address = address.substring(0, address.length - 2);
        return address;
    }

    ngOnInit(): void {
        this.searchSecretaryOfStateRecordsWithFilters(1);
        this.searchSecretaryOfStateFacets();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    }
}

/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals: string[], page: number): Observable<IServerResponse> {
    const perPage = 10;
    const start = (page - 1) * perPage;
    const end = start + perPage;

    return Observable
        .of({
            items: meals.slice(start, end),
            total: 100
        }).delay(1000);
}
