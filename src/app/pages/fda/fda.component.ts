import { ChangeDetectionStrategy, Component, Input, OnInit, NgZone, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FDAService } from '../../services/fda.service';
import { Observable } from 'rxjs';

interface IServerResponse {
    items: string[];
    total: number;
}

declare var jQuery: any;
@Component({
    templateUrl: 'fda.component.html',
    providers: [FDAService]
})
export class FDAComponent implements OnInit {
    selectedProductId: string = "";
    selectedFDA: any = {};
    pageNumber: number = 1;
    recordsToFetch: number = 10;
    totalRecords: number = 0;
    resFacets: any = {};
    asyncFDA: Observable<any[]>;
    loading: boolean;
    searchFDATecxt: string = "";
    selectedPNDC: string[] = [];
    selectedPTN: string[] = [];
    selectedPN: string[] = [];
    selectedNPN: string[] = [];
    selectedDEAS: string[] = [];
    selectedPNDCString: string = "";
    selectedPTNString: string = "";
    selectedPNString: string = "";
    selectedNPNString: string = "";
    selectedDEASString: string = "";
    rersFDA: any[] = [];
    foundRecord: boolean = true;
    constructor(private zone: NgZone, private fdaService: FDAService, private _elmRef: ElementRef) {

    }

    searchFDARecords(pageNum: number) {
        this.pageNumber = pageNum;
        this.asyncFDA = this.fdaService.searchFDARecords(this.searchFDATecxt, this.pageNumber, this.recordsToFetch)
            .do((res: any) => {
                if (res.success) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.totalRecords;
                        this.rersFDA = res.response.results || [];
                    });
                }
                else {
                    this.totalRecords = 0;
                    this.rersFDA = [];
                }
                this.zone.run(() => {
                    if (this.totalRecords > 0) this.foundRecord = true;
                    else this.foundRecord = false;
                });
            })
            .map(res => res.success ? res.response.results : []);
    }

    searchFDAFacets() {
        this.fdaService.searchFDAFacets(this.searchFDATecxt)
            .subscribe(res => {
                if (res.success) {
                    this.zone.run(() => {
                        this.resFacets = res.response;
                    });
                }
            }
            , error => {
            });
    }

    changeSelectFacet(objFacet, index) {
        this.zone.run(() => {
            let changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    this.selectedPNDC.push(objFacet.name);
                else if (index == 2)
                    this.selectedPTN.push(objFacet.name);
                else if (index == 3)
                    this.selectedPN.push(objFacet.name);
                else if (index == 4)
                    this.selectedNPN.push(objFacet.name);
                else if (index == 5)
                    this.selectedDEAS.push(objFacet.name);
            }
            else {
                let arrayIndex: number = -1;
                if (index == 1) {
                    arrayIndex = this.selectedPNDC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPNDC.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = this.selectedPTN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPTN.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = this.selectedPN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedPN.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = this.selectedNPN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedNPN.splice(arrayIndex, 1);
                }
                else if (index == 5) {
                    arrayIndex = this.selectedDEAS.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        this.selectedDEAS.splice(arrayIndex, 1);
                }
            }
        });
        this.searchFDARecordsWithFilters(1);
    }

    removeSelectedFacet(objFacet, index) {
        this.zone.run(() => {
            objFacet.selected = false;
            let arrayIndex: number = -1;
            if (index == 1) {
                arrayIndex = this.selectedPNDC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPNDC.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = this.selectedPTN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPTN.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = this.selectedPN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedPN.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = this.selectedNPN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedNPN.splice(arrayIndex, 1);
            }
            else if (index == 5) {
                arrayIndex = this.selectedDEAS.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    this.selectedDEAS.splice(arrayIndex, 1);
            }
        });
        this.searchFDARecordsWithFilters(1);
    }

    clearSelectedFacets() {
        this.selectedPNDC = [];
        this.selectedPTN = [];
        this.selectedPN = [];
        this.selectedNPN = [];
        this.selectedDEAS = [];
        this.resFacets.productNDCs.forEach(objFacet => {
            objFacet.selected = false;
        });
        this.resFacets.productTypeNames.forEach(objFacet => {
            objFacet.selected = false;
        });
        this.resFacets.proprietaryNames.forEach(objFacet => {
            objFacet.selected = false;
        });
        this.resFacets.nonProprietaryNames.forEach(objFacet => {
            objFacet.selected = false;
        });
        this.resFacets.dEASchedules.forEach(objFacet => {
            objFacet.selected = false;
        });
    }

    searchFDA() {
        this.searchFDARecords(1);
        this.searchFDAFacets();
    }

    searchFDARecordsWithFilters(pageNum: number) {
        this.pageNumber = pageNum;
        let selectedPNDCString = "";
        let selectedPTNString = "";
        let selectedPNString = "";
        let selectedNPNString = "";
        let selectedDEASString = "";
        if (this.selectedPNDC && this.selectedPNDC.length > 0)
            selectedPNDCString = this.selectedPNDC.join();
        if (this.selectedPTN && this.selectedPTN.length > 0)
            selectedPTNString = this.selectedPTN.join();
        if (this.selectedPN && this.selectedPN.length > 0)
            selectedPNString = this.selectedPN.join();
        if (this.selectedNPN && this.selectedNPN.length > 0)
            selectedNPNString = this.selectedNPN.join();
        if (this.selectedDEAS && this.selectedDEAS.length > 0)
            selectedDEASString = this.selectedDEAS.join();

        this.asyncFDA = this.fdaService.searchFDARecordsWithFilter(this.searchFDATecxt, selectedPNDCString, selectedPTNString,
            selectedPNString, selectedNPNString, selectedDEASString, this.pageNumber, this.recordsToFetch)
            .do((res: any) => {
                if (res.success) {
                    this.zone.run(() => {
                        this.totalRecords = res.response.totalRecords;
                        this.rersFDA = res.response.results || [];
                    });
                }
                else {
                    this.zone.run(() => {
                        this.totalRecords = 0;
                        this.rersFDA = [];
                    });
                }
            })
            .map(res => res.success ? res.response.results : []);
    }

    ngOnInit(): void {
        this.searchFDARecords(1);
        this.searchFDAFacets();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    }
}

/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals: string[], page: number): Observable<IServerResponse> {
    const perPage = 10;
    const start = (page - 1) * perPage;
    const end = start + perPage;

    return Observable
        .of({
            items: meals.slice(start, end),
            total: 100
        }).delay(1000);
}
