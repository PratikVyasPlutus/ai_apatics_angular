import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FDAComponent } from './fda.component';

const routes: Routes = [
    {
        path: '',
        component: FDAComponent,
        data: {
            title: 'FDA'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FDARoutingModule { }
