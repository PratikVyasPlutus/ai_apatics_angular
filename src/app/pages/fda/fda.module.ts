import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule } from '@angular/forms';

import { FDAComponent } from './fda.component';
import { FDARoutingModule } from './fda-routing.module';
import { BreadcrumbsComponent } from '../../shared/breadcrumb.component';

@NgModule({
    imports: [
        FormsModule,
        FDARoutingModule,
        ChartsModule
    ],
    declarations: []
})
export class FDAModule { }
