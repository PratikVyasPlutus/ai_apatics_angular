webpackJsonp([1,5],{

/***/ 1007:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(140)();
// imports


// module
exports.push([module.i, ".wrapper {    \r\n\tmargin-top: 80px;\r\n\tmargin-bottom: 20px;\r\n}\r\n\r\n.form-signin {\r\n  max-width: 420px;\r\n  padding: 30px 38px 66px;\r\n  margin: 0 auto;\r\n  background-color: #eee;\r\n  }\r\n\r\n.form-signin-heading {\r\n  text-align:center;\r\n  margin-bottom: 30px;\r\n}\r\n\r\n.form-control {\r\n  position: relative;\r\n  font-size: 16px;\r\n  height: auto;\r\n  padding: 10px;\r\n}\r\n\r\ninput[type=\"text\"] {\r\n  margin-bottom: 0px;\r\n  border-bottom-left-radius: 0;\r\n  border-bottom-right-radius: 0;\r\n}\r\n\r\ninput[type=\"password\"] {\r\n  margin-bottom: 20px;\r\n  border-top-left-radius: 0;\r\n  border-top-right-radius: 0;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 1009:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 498,
	"./af.js": 498,
	"./ar": 504,
	"./ar-dz": 499,
	"./ar-dz.js": 499,
	"./ar-ly": 500,
	"./ar-ly.js": 500,
	"./ar-ma": 501,
	"./ar-ma.js": 501,
	"./ar-sa": 502,
	"./ar-sa.js": 502,
	"./ar-tn": 503,
	"./ar-tn.js": 503,
	"./ar.js": 504,
	"./az": 505,
	"./az.js": 505,
	"./be": 506,
	"./be.js": 506,
	"./bg": 507,
	"./bg.js": 507,
	"./bn": 508,
	"./bn.js": 508,
	"./bo": 509,
	"./bo.js": 509,
	"./br": 510,
	"./br.js": 510,
	"./bs": 511,
	"./bs.js": 511,
	"./ca": 512,
	"./ca.js": 512,
	"./cs": 513,
	"./cs.js": 513,
	"./cv": 514,
	"./cv.js": 514,
	"./cy": 515,
	"./cy.js": 515,
	"./da": 516,
	"./da.js": 516,
	"./de": 518,
	"./de-at": 517,
	"./de-at.js": 517,
	"./de.js": 518,
	"./dv": 519,
	"./dv.js": 519,
	"./el": 520,
	"./el.js": 520,
	"./en-au": 521,
	"./en-au.js": 521,
	"./en-ca": 522,
	"./en-ca.js": 522,
	"./en-gb": 523,
	"./en-gb.js": 523,
	"./en-ie": 524,
	"./en-ie.js": 524,
	"./en-nz": 525,
	"./en-nz.js": 525,
	"./eo": 526,
	"./eo.js": 526,
	"./es": 528,
	"./es-do": 527,
	"./es-do.js": 527,
	"./es.js": 528,
	"./et": 529,
	"./et.js": 529,
	"./eu": 530,
	"./eu.js": 530,
	"./fa": 531,
	"./fa.js": 531,
	"./fi": 532,
	"./fi.js": 532,
	"./fo": 533,
	"./fo.js": 533,
	"./fr": 536,
	"./fr-ca": 534,
	"./fr-ca.js": 534,
	"./fr-ch": 535,
	"./fr-ch.js": 535,
	"./fr.js": 536,
	"./fy": 537,
	"./fy.js": 537,
	"./gd": 538,
	"./gd.js": 538,
	"./gl": 539,
	"./gl.js": 539,
	"./he": 540,
	"./he.js": 540,
	"./hi": 541,
	"./hi.js": 541,
	"./hr": 542,
	"./hr.js": 542,
	"./hu": 543,
	"./hu.js": 543,
	"./hy-am": 544,
	"./hy-am.js": 544,
	"./id": 545,
	"./id.js": 545,
	"./is": 546,
	"./is.js": 546,
	"./it": 547,
	"./it.js": 547,
	"./ja": 548,
	"./ja.js": 548,
	"./jv": 549,
	"./jv.js": 549,
	"./ka": 550,
	"./ka.js": 550,
	"./kk": 551,
	"./kk.js": 551,
	"./km": 552,
	"./km.js": 552,
	"./ko": 553,
	"./ko.js": 553,
	"./ky": 554,
	"./ky.js": 554,
	"./lb": 555,
	"./lb.js": 555,
	"./lo": 556,
	"./lo.js": 556,
	"./lt": 557,
	"./lt.js": 557,
	"./lv": 558,
	"./lv.js": 558,
	"./me": 559,
	"./me.js": 559,
	"./mi": 560,
	"./mi.js": 560,
	"./mk": 561,
	"./mk.js": 561,
	"./ml": 562,
	"./ml.js": 562,
	"./mr": 563,
	"./mr.js": 563,
	"./ms": 565,
	"./ms-my": 564,
	"./ms-my.js": 564,
	"./ms.js": 565,
	"./my": 566,
	"./my.js": 566,
	"./nb": 567,
	"./nb.js": 567,
	"./ne": 568,
	"./ne.js": 568,
	"./nl": 570,
	"./nl-be": 569,
	"./nl-be.js": 569,
	"./nl.js": 570,
	"./nn": 571,
	"./nn.js": 571,
	"./pa-in": 572,
	"./pa-in.js": 572,
	"./pl": 573,
	"./pl.js": 573,
	"./pt": 575,
	"./pt-br": 574,
	"./pt-br.js": 574,
	"./pt.js": 575,
	"./ro": 576,
	"./ro.js": 576,
	"./ru": 577,
	"./ru.js": 577,
	"./se": 578,
	"./se.js": 578,
	"./si": 579,
	"./si.js": 579,
	"./sk": 580,
	"./sk.js": 580,
	"./sl": 581,
	"./sl.js": 581,
	"./sq": 582,
	"./sq.js": 582,
	"./sr": 584,
	"./sr-cyrl": 583,
	"./sr-cyrl.js": 583,
	"./sr.js": 584,
	"./ss": 585,
	"./ss.js": 585,
	"./sv": 586,
	"./sv.js": 586,
	"./sw": 587,
	"./sw.js": 587,
	"./ta": 588,
	"./ta.js": 588,
	"./te": 589,
	"./te.js": 589,
	"./tet": 590,
	"./tet.js": 590,
	"./th": 591,
	"./th.js": 591,
	"./tl-ph": 592,
	"./tl-ph.js": 592,
	"./tlh": 593,
	"./tlh.js": 593,
	"./tr": 594,
	"./tr.js": 594,
	"./tzl": 595,
	"./tzl.js": 595,
	"./tzm": 597,
	"./tzm-latn": 596,
	"./tzm-latn.js": 596,
	"./tzm.js": 597,
	"./uk": 598,
	"./uk.js": 598,
	"./uz": 599,
	"./uz.js": 599,
	"./vi": 600,
	"./vi.js": 600,
	"./x-pseudo": 601,
	"./x-pseudo.js": 601,
	"./yo": 602,
	"./yo.js": 602,
	"./zh-cn": 603,
	"./zh-cn.js": 603,
	"./zh-hk": 604,
	"./zh-hk.js": 604,
	"./zh-tw": 605,
	"./zh-tw.js": 605
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 1009;


/***/ }),

/***/ 103:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var Observable_1 = __webpack_require__(1);
//export var serviceBaseUrl='http://localhost/TaskLiveRestAPI/v1/';
var GlobalClass = (function () {
    function GlobalClass() {
        this.BaseUrl = 'http://localhost/TasksLiveApp/';
        this.roleEnum = {
            "SuperAdmin": 1,
            "Admin": 2,
            "MyClient": 3,
            "Employee": 4
        };
        //console.log('Global class constructor called');
    }
    GlobalClass.prototype.extractSolrData = function (res) {
        console.log('resextractSolrData', res);
        var body = res.json();
        return body || {};
    };
    GlobalClass.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    GlobalClass.prototype.extractError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    GlobalClass.prototype.convertToMSJSON = function (objDate) {
        var date = '/Date(' + objDate.getTime() + ')/'; //CHANGED LINE
        return date;
    };
    return GlobalClass;
}());
GlobalClass = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], GlobalClass);
exports.GlobalClass = GlobalClass;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/global.js.map

/***/ }),

/***/ 104:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var Observable_1 = __webpack_require__(1);
var common_util_1 = __webpack_require__(450);
var angular2_uuid_1 = __webpack_require__(855);
var SessionService = (function () {
    function SessionService(commonutil) {
        this.commonutil = commonutil;
    }
    SessionService.prototype.createUserSession = function (req) {
        var _this = this;
        return new Observable_1.Observable(function (observer) {
            var session = {
                sessionId: angular2_uuid_1.UUID.UUID(),
                name: req.username
            };
            _this.commonutil.setSessionStorageItem('userSession', session, true);
            observer.next(session);
            observer.complete();
        });
    };
    SessionService.prototype.getUserSession = function () {
        var _this = this;
        return new Observable_1.Observable(function (observer) {
            var session = _this.commonutil.getSessionStorageItem('userSession', true);
            observer.next(session);
            observer.complete();
        });
    };
    SessionService.prototype.clearUserSession = function () {
        var _this = this;
        return new Observable_1.Observable(function (observer) {
            _this.commonutil.removeSessionStorageItem('userSession');
            observer.next(true);
            observer.complete();
        });
    };
    return SessionService;
}());
SessionService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof common_util_1.Commonutil !== "undefined" && common_util_1.Commonutil) === "function" && _a || Object])
], SessionService);
exports.SessionService = SessionService;
var _a;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/session.service.js.map

/***/ }),

/***/ 1045:
/***/ (function(module, exports) {

module.exports = "<router-outlet>\r\n</router-outlet>"

/***/ }),

/***/ 1046:
/***/ (function(module, exports) {

module.exports = "<header class=\"navbar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header>\r\n\r\n<div class=\"sidebar\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"#\">Product NDC</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <input class=\"form-control\" type=\"text\" placeholder=\"Search\" />\r\n                    </li>\r\n                    <li class=\"nav-item\">\r\n                        <a class=\"nav-link\" routerLinkActive=\"active\">hi</a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"#\">Product Type Name</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <input class=\"form-control\" type=\"text\" placeholder=\"Search\" />\r\n                    </li>\r\n                    <li class=\"nav-item\">\r\n                        <a class=\"nav-link\" routerLinkActive=\"active\">hi</a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"#\">Proprietary Name</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <input class=\"form-control\" type=\"text\" placeholder=\"Search\" />\r\n                    </li>\r\n                    <li class=\"nav-item\">\r\n                        <a class=\"nav-link\" routerLinkActive=\"active\">hi</a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"#\">Non Proprietary Name</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <input class=\"form-control\" type=\"text\" placeholder=\"Search\" />\r\n                    </li>\r\n                    <li class=\"nav-item\">\r\n                        <a class=\"nav-link\" routerLinkActive=\"active\">hi</a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"#\">DEA Schedule</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <input class=\"form-control\" type=\"text\" placeholder=\"Search\" />\r\n                    </li>\r\n                    <li class=\"nav-item\">\r\n                        <a class=\"nav-link\" routerLinkActive=\"active\">hi</a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n\r\n<!-- Main content -->\r\n<main class=\"main\">\r\n\r\n    <!-- Breadcrumb -->\r\n    <ol class=\"breadcrumb\">\r\n        <breadcrumbs></breadcrumbs>\r\n    </ol>\r\n\r\n    <div class=\"container-fluid\">\r\n        <router-outlet></router-outlet>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n\r\n<footer class=\"footer\">\r\n    <span class=\"text-left\">\r\n        <a>SolrPOC</a> &copy; 2017 creativeLabs.\r\n    </span>\r\n    <span class=\"float-xs-right\">\r\n        Powered by <a>SolrPOC</a>\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1047:
/***/ (function(module, exports) {

module.exports = "<header class=\"navbar apatics-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid custom-container\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <!--<a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>-->\r\n            </li>\r\n        </ul>\r\n        <ul class=\"nav navbar-nav pull-right hidden-md-down right-nav\">\r\n            <li class=\"nav-item\">\r\n                <a style=\"cursor:pointer\" class=\"pointer ap-blue nav-link\" (click)=\"logOut()\">LOG OUT</a>\r\n            </li>\r\n\r\n        </ul>\r\n    </div>\r\n</header>"

/***/ }),

/***/ 1048:
/***/ (function(module, exports) {

module.exports = "<header class=\"navbar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header>\r\n\r\n\r\n<!-- Main content -->\r\n<main class=\"main\">\r\n\r\n    <!-- Breadcrumb -->\r\n    <ol class=\"breadcrumb\">\r\n        <breadcrumbs></breadcrumbs>\r\n    </ol>\r\n\r\n    <div class=\"container-fluid\">\r\n\r\n        <router-outlet></router-outlet>\r\n    </div>\r\n</main>\r\n\r\n\r\n\r\n<footer class=\"footer\">\r\n    <span class=\"text-left\">\r\n        <a>SolrPOC</a> &copy; 2017 creativeLabs.\r\n    </span>\r\n    <span class=\"float-xs-right\">\r\n        Powered by <a>SolrPOC</a>\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1049:
/***/ (function(module, exports) {

module.exports = "<header class=\"navbar kdark-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n        <ul class=\"nav navbar-nav pull-right hidden-md-down right-nav\">\r\n            <li class=\"nav-item\">\r\n                <a style=\"cursor:pointer\" class=\"pointer ap-blue nav-link\" (click)=\"logOut()\">LOG OUT</a>\r\n            </li>\r\n\r\n        </ul>\r\n    </div>\r\n</header>"

/***/ }),

/***/ 1050:
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n\r\n<!-- Main content -->\r\n<main class=\"main kdark full-main\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\">\r\n            <a [routerLink]=\"['../home']\">Home</a>\r\n        </li>\r\n        <li class=\"breadcrumb-item active\">\r\n            <span>Claim Segmentation</span>\r\n        </li>\r\n        <!-- Breadcrumb Menu-->\r\n    </ol>\r\n\r\n    <div class=\"container-fluid\">\r\n        <div class=\"animated fadeIn mb-2 charts-container\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12\">\r\n\r\n                    <img src=\"assets/img/claim.png\">\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n\r\n\r\n<footer class=\"footer kdark full-footer\">\r\n    <span class=\"text-left\">\r\n        &copy; 2019 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1051:
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar sidebar-kdark\">\r\n    <ul class=\"nav\">\r\n        <li class=\"divider\"></li>\r\n        <li class=\"nav-item\" (click)=\"activeSideNavLink(1)\">\r\n            <a class=\"nav-link\" [ngClass]=\"{'active': isShowAtRisk}\">\r\n                AT RISK\r\n            </a>\r\n        </li>\r\n        <li class=\"nav-item\" (click)=\"activeSideNavLink(2)\">\r\n            <a class=\"nav-link\" [ngClass]=\"{'active': isShowMediumRisk}\">\r\n                MEDIUM RISK\r\n            </a>\r\n        </li>\r\n    </ul>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main kdark\" id=\"main-container\">\r\n    <div *ngIf=\"isShowLoad\" class=\"loading\">Loading&#8230;</div>\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\">\r\n            <a [routerLink]=\"['../home']\">Home</a>\r\n        </li>\r\n        <li class=\"breadcrumb-item active\">\r\n            <span>Opioid</span>\r\n        </li>\r\n        <!-- Breadcrumb Menu-->\r\n    </ol>\r\n\r\n    <div class=\"container-fluid\">\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"m-t-2\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-12\" style=\"color:#1f486a;font-size: 20px;text-align: center;padding-bottom: 10px;\">\r\n                        <div>OPIOID AT-RISK</div>\r\n                        <div>CASE MANAGEMENT – Powered by AI</div>\r\n                    </div>\r\n                </div>\r\n                <div *ngIf=\"isShowAtRisk\" class=\"row\">\r\n                    <div class=\"col-sm-12\">\r\n                        <div class=\"card br-5 mb-2 card-accent-primary mb-0\">\r\n                            <div class=\"card-header\">\r\n                                New Claimants – AT RISK\r\n                            </div>\r\n                            <div class=\"card-block\">\r\n                                <div class=\"report-table-container\">\r\n                                    <table class=\"table table-bordered ktable\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th># Claimant</th>\r\n                                                <th>Claimant Name</th>\r\n                                                <th>Comments</th>\r\n                                                <th width=\"30%\">Action</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n                                            <tr *ngFor=\"let ran of riskData.AtRisk.NewClaimants; let i=index\">\r\n                                                <td>{{ran.id}}</td>\r\n                                                <td>{{ran.name}}</td>\r\n                                                <td>{{ran.comments}}</td>\r\n                                                <td>\r\n                                                    <button type=\"button\" class=\"btn btn-primary btn-sm\" (click)=\"openInformProviderModel()\">Inform Provider</button>\r\n                                                    <button type=\"button\" class=\"btn btn-primary btn-sm\" (click)=\"openAssignManagerModel()\">Assign Case Manager</button>\r\n                                                </td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-12\">\r\n                        <div class=\"card br-5 mb-2 card-accent-primary mb-0\">\r\n                            <div class=\"card-header\">\r\n                                Assigned Claimants\r\n                            </div>\r\n                            <div class=\"card-block\">\r\n                                <div class=\"report-table-container\">\r\n                                    <table class=\"table table-bordered ktable\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th># Claimant</th>\r\n                                                <th>Claimant Name</th>\r\n                                                <th>Case Manager</th>\r\n                                                <th>Notes</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n                                            <tr *ngFor=\"let raa of riskData.AtRisk.AssignedClaimants; let i=index\">\r\n                                                <td>{{raa.id}}</td>\r\n                                                <td>{{raa.name}}</td>\r\n                                                <td>{{raa.caseManager}}</td>\r\n                                                <td>{{raa.note}}</td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div *ngIf=\"isShowMediumRisk\" class=\"row\">\r\n                    <div class=\"col-sm-12\">\r\n                        <div class=\"card br-5 mb-2 card-accent-primary mb-0\">\r\n                            <div class=\"card-header\">\r\n                                New Claimants – Medium Risk\r\n                            </div>\r\n                            <div class=\"card-block\">\r\n                                <div class=\"report-table-container\">\r\n                                    <table class=\"table table-bordered ktable\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th># Claimant</th>\r\n                                                <th>Claimant Name</th>\r\n                                                <th>Comments</th>\r\n                                                <th width=\"30%\">Action</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n                                            <tr *ngFor=\"let rmn of riskData.MediumRisk.NewClaimants; let i=index\">\r\n                                                <td>{{rmn.id}}</td>\r\n                                                <td>{{rmn.name}}</td>\r\n                                                <td>{{rmn.comments}}</td>\r\n                                                <td>\r\n                                                    <button type=\"button\" class=\"btn btn-primary btn-sm\" (click)=\"openInformProviderModel()\">Inform Provider</button>\r\n                                                    <button type=\"button\" class=\"btn btn-primary btn-sm\" (click)=\"openAssignManagerModel()\">Assign Case Manager</button>\r\n                                                </td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-12\">\r\n                        <div class=\"card br-5 mb-2 card-accent-primary mb-0\">\r\n                            <div class=\"card-header\">\r\n                                Assigned Claimants\r\n                            </div>\r\n                            <div class=\"card-block\">\r\n                                <div class=\"report-table-container\">\r\n                                    <table class=\"table table-bordered ktable\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th># Claimant</th>\r\n                                                <th>Claimant Name</th>\r\n                                                <th>Case Manager</th>\r\n                                                <th>Notes</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n                                            <tr *ngFor=\"let rma of riskData.MediumRisk.AssignedClaimants; let i=index\">\r\n                                                <td>{{rma.id}}</td>\r\n                                                <td>{{rma.name}}</td>\r\n                                                <td>{{rma.caseManager}}</td>\r\n                                                <td>{{rma.note}}</td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n\r\n<!-- /.modal -->\r\n<div bsModal #InformProviderModel=\"bs-modal\" [config]=\"{backdrop: 'static'}\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\"\r\n    aria-labelledby=\"myModalLabel\" aria-hidden=\"false\">\r\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n\r\n            <div class=\"card-header\">\r\n                <strong>Select Provider</strong>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <div class=\"alert alert-success alert-dismissable\" id=\"ProvideMsgScccess1\" style=\"display:none\">\r\n                    <strong>Success!</strong> Mail sent successfully.\r\n                </div>\r\n\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-12\">\r\n                        <div class=\"row\">\r\n                            <div class=\"form-group col-sm-12\">\r\n                                <label for=\"name\">Select Provider</label>\r\n                                <ng-select [multiple]=\"true\" [(ngModel)]=\"selectedProvider\" [items]=\"provideList\" placeholder=\"No Provider selected\"></ng-select>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <h1 *ngIf=\"0\">{{selectedProvider|json}}</h1>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"modal-footer text-center\">\r\n                <button type=\"button\" [disabled]=\"!(selectedProvider && selectedProvider.length>0)\" (click)=\"sendMailToProvider()\" class=\"btn btn-primary\">Send Mail</button>\r\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"closeInformProviderModel()\">Cancel</button>\r\n            </div>\r\n\r\n        </div>\r\n        <!-- /.modal-content -->\r\n    </div>\r\n    <!-- /.modal-dialog -->\r\n</div>\r\n<div bsModal #AssignManagerModel=\"bs-modal\" [config]=\"{backdrop: 'static'}\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\"\r\n    aria-labelledby=\"myModalLabel\" aria-hidden=\"false\">\r\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n\r\n            <div class=\"card-header\">\r\n                <strong>Select Cash Manager</strong>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <div class=\"alert alert-success alert-dismissable\" id=\"ProvideMsgScccess2\" style=\"display:none\">\r\n                    <strong>Success!</strong> Assign Manager successfully.\r\n                </div>\r\n\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-12\">\r\n                        <div class=\"row\">\r\n                            <div class=\"form-group col-sm-12\">\r\n                                <label for=\"name\">Select Cash Manager</label>\r\n                                <ng-select [multiple]=\"true\" [(ngModel)]=\"selectedManager\" [items]=\"managerList\" placeholder=\"No Manager selected\"></ng-select>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <h1 *ngIf=\"0\">{{selectedManager|json}}</h1>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"modal-footer text-center\">\r\n                <button type=\"button\" [disabled]=\"!(selectedManager && selectedManager.length>0)\" (click)=\"AssignManager()\" class=\"btn btn-primary\">Assign</button>\r\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"closeAssignManagerModel()\">Cancel</button>\r\n            </div>\r\n\r\n        </div>\r\n        <!-- /.modal-content -->\r\n    </div>\r\n    <!-- /.modal-dialog -->\r\n</div>\r\n<!-- /.modal -->\r\n\r\n<footer class=\"footer kdark\">\r\n    <span class=\"text-left\">\r\n        &copy; 2019 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1052:
/***/ (function(module, exports) {

module.exports = "<!-- <header class=\"navbar apatics-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header> -->\r\n\r\n<app-header-default></app-header-default>\r\n<div id=\"facet-sidebar\" class=\"sidebar apatics-sidebar\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Data Title</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group1\" name=\"input2-group1\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedDT\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedDT || searchedDT.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedDT=''\" *ngIf=\"searchedDT && searchedDT.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.dataTitles | searchFacets:searchedDT\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 1)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">License No</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedLN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedLN || searchedLN.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedLN=''\" *ngIf=\"searchedLN && searchedLN.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.licenseNos | searchFacets:searchedLN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 2)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Alert Date</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedDOD\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedDOD || searchedDOD.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedDOD=''\" *ngIf=\"searchedDOD && searchedDOD.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.datesOfDataCreated | searchFacets:searchedDOD\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 3)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">State</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedSTATE\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedSTATE || searchedSTATE.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedSTATE=''\" *ngIf=\"searchedSTATE && searchedSTATE.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.states | searchFacets:searchedSTATE\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 4)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main apatics-container\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../home']\">Home</a></li>\r\n        <li class=\"breadcrumb-item active\"><span>Chiropractor</span></li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"card m-t-2\">\r\n                <!--<h3 class=\"card-header\">Chiropractor</h3>-->\r\n                <div class=\"card-block\">\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group mb-0 col-md-12\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search Chiropractor\" [(ngModel)]=\"searchChiropractorText\"\r\n                                    (keyup.enter)=\"searchChiropractor()\">\r\n                                <span class=\"input-group-btn\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchChiropractor()\"><i class=\"fa fa-search\"></i> Search</button>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 pt-1 text-xs-right page-info\">\r\n                        <span *ngIf=\"totalRecords>0\" [textContent]=\"(pageNumber*recordsToFetch-recordsToFetch+1) + ' to ' + (pageNumber*recordsToFetch-recordsToFetch+rersChiropractor.length) + ' of ' + totalRecords + ' items'\"></span>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.dataTitles | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 1)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Data Titles':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.licenseNos | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 2)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty License Nos':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.datesOfDataCreated | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 3)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Dates of Data Created':objFacet.name\"></span>                           \r\n                        </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.states | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 4)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty State':objFacet.name\"></span>                           \r\n                        </button>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 text-lg-center\" *ngIf=\"foundRecord==false\">\r\n                        <h4>No result found.</h4>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 fda-list-container\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"card m-b-1\" *ngFor=\"let objChiropractor of asyncChiropractor | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\">\r\n                            <div class=\"card-block p-1\">\r\n                                <div class=\"col-xs-12 p-0\">\r\n                                    <button type=\"button\" class=\"btn btn-primary pull-left mr-1\" (click)=\"objChiropractor.isOpen=!objChiropractor.isOpen\">\r\n                                        <i class=\"fa\" [ngClass]=\"objChiropractor.isOpen ? 'fa-minus' : 'fa-plus'\"></i>\r\n                                    </button>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.dataTitle\">\r\n                                        <span class=\"data-title\">Data Title:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.dataTitle | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.nameAndCity\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Name and City:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.nameAndCity | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.state\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">State:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.state | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.licenseNo\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">License No:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.licenseNo | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.dateOfDataCreated\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Date Of Data Created:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.dateOfDataCreated.substring(6, objChiropractor.dateOfDataCreated.length-7) | date : 'MM-dd-yyyy' | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.dateFiled\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Date Filed:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.dateFiled.substring(6, objChiropractor.dateFiled.length-7) | date : 'MM-dd-yyyy' | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.revocationDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Revocation Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.revocationDate.substring(6, objChiropractor.revocationDate.length-7) | date : 'MM-dd-yyyy' | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.probationEndDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Probation End Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.probationEndDate.substring(6, objChiropractor.probationEndDate.length-7) | date : 'MM-dd-yyyy' | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.effectiveDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Effective Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.effectiveDate.substring(6, objChiropractor.effectiveDate.length-7) | date : 'MM-dd-yyyy' | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.fineAmount\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Fine Amount:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.fineAmount | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.dateIssued\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Date Issued:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.dateIssued.substring(6, objChiropractor.dateIssued.length-7) | date : 'MM-dd-yyyy' | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-xs-12 p-0\" *ngIf=\"objChiropractor.isOpen\">\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.action\">\r\n                                        <span class=\"data-title\">Action:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.action | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.allegedViolations\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Alleged Violations :</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.allegedViolations | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objChiropractor.violations\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Violations:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objChiropractor.violations | highlight:searchChiropractorText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"spinner\" [ngClass]=\"{ 'hidden': !loading }\"></div>\r\n                        <div class=\"pagination-container\">\r\n                            <pagination-controls (pageChange)=\"searchChiropractorRecordsWithFilters($event)\" id=\"server\"></pagination-controls>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n\r\n<footer class=\"footer\">\r\n    <span class=\"text-left\">\r\n        &copy; 2017 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1053:
/***/ (function(module, exports) {

module.exports = "<header class=\"navbar kdark-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav pull-right hidden-md-down right-nav\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"pointer ap-blue nav-link\" (click)=\"logOut()\" style=\"cursor:pointer;margin-right: 10px;\">LOG OUT</a>\r\n            </li>\r\n            <!-- <li *ngIf=\"!loggedIn\" class=\"nav-item\">\r\n                <a class=\"pointer ap-blue nav-link\" (click)=\"logIn()\">LOG IN</a>\r\n            </li> -->\r\n        </ul>\r\n\r\n    </div>\r\n</header>\r\n<div class=\"container d-table\" id=\"dashboard\">\r\n    <h1 class=\"logo-section\">\r\n        <img src=\"assets/img/logo_large.png\">\r\n        <br>\r\n        <span id=\"header_tabline\"> Leveraging Big Data Analytics In Healthcare</span>\r\n    </h1>\r\n    <div class=\"d-table\">\r\n        <div class=\"d-100vh-va-middle\">\r\n            <div *ngIf=\"0\" class=\"row\">\r\n                <div class=\"col-lg-8 offset-lg-2 col-md-10 offset-md-1\">\r\n                    <button class=\"btn btn-primary btn-lg btn-block\" type=\"button\">External Sources</button>\r\n                    <button class=\"btn btn-primary btn-block active\" type=\"button\">Red Flags</button>\r\n                    <div class=\"dashboard-block-container\">\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <button class=\"btn btn-block active\" type=\"button\" [routerLink]=\"['../providerdashboard']\">Provider Profile</button>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <button class=\"btn btn-block active\" type=\"button\" [routerLink]=\"['../esi']\">Pharmacy</button>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <a target=\"_blank\" href=\"http://fraudlynx.apatics.net:82/\">\r\n                                <button class=\"btn btn-block active\" type=\"button\">Provider Cluster</button>\r\n                            </a>\r\n\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <button class=\"btn btn-block active\" type=\"button\">Dental</button>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\" dropdown>\r\n                            <button class=\"btn btn-block active\" type=\"button\" data-toggle=\"dropdown\" dropdownToggle aria-haspopup=\"true\" aria-expanded=\"false\">Alerts</button>\r\n                            <div class=\"dropdown-menu dropdown-menu-left dropdown-menu-dashboard\" dropdownMenu aria-labelledby=\"alerts-dropdown\">\r\n                                <a class=\"dropdown-item\" [routerLink]=\"['../nicb']\">NICB</a>\r\n                                <a class=\"dropdown-item\" [routerLink]=\"['../mbcoig']\">MBC/OIG</a>\r\n                                <a class=\"dropdown-item\" [routerLink]=\"['../chiropractor']\">Chiropractor alert</a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <a target=\"_blank\" href=\"http://fraudlynx.apatics.net:7474/\">\r\n                                <button class=\"btn btn-block active\" type=\"button\">Social Network</button>\r\n                            </a>\r\n                        </div>\r\n                        <!--<div class=\"dashboard-btn-container\">\r\n                            <button class=\"btn btn-block active\" type=\"button\" [routerLink]=\"['../esi']\">ESI</button>\r\n                        </div>-->\r\n                    </div>\r\n                    <button class=\"btn btn-primary btn-lg btn-block\" type=\"button\">Internal Sources</button>\r\n                    <div class=\"dashboard-block-container\">\r\n                        <div class=\"dashboard-btn-container\" dropdown>\r\n                            <button class=\"btn btn-block active\" type=\"button\" data-toggle=\"dropdown\" dropdownToggle aria-haspopup=\"true\" aria-expanded=\"false\">License/NPI</button>\r\n                            <div class=\"dropdown-menu dropdown-menu-left dropdown-menu-dashboard\" dropdownMenu aria-labelledby=\"license-dropdown\">\r\n                                <a class=\"dropdown-item\" [routerLink]=\"['../dca']\">DCA</a>\r\n                                <a class=\"dropdown-item\" [routerLink]=\"['../secretaryofstate']\">Secretary of State</a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <button class=\"btn btn-block active\" type=\"button\">Injury</button>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\" dropdown>\r\n                            <button class=\"btn btn-block active\" type=\"button\" data-toggle=\"dropdown\" dropdownToggle aria-haspopup=\"true\" aria-expanded=\"false\">Medical Codes</button>\r\n                            <div class=\"dropdown-menu dropdown-menu-left dropdown-menu-dashboard\" dropdownMenu aria-labelledby=\"medical-codes-dropdown\">\r\n                                <a class=\"dropdown-item\" [routerLink]=\"['../icd9']\">ICD9</a>\r\n                                <a class=\"dropdown-item\" [routerLink]=\"['../revenuecode']\">Revenue Code</a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <button class=\"btn btn-block active\" type=\"button\" [routerLink]=\"['../dnb']\">DNB</button>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <button class=\"btn btn-block active\" type=\"button\">Medicaid</button>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <button class=\"btn btn-block active\" type=\"button\" [routerLink]=\"['../fda']\">FDA</button>\r\n                        </div>\r\n                        <!--<div class=\"dashboard-btn-container button-lg\">\r\n                            <button class=\"btn btn-block active\" type=\"button\" [routerLink]=\"['../gd']\">Claimant and Provider Distance</button>\r\n                        </div>-->\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n                <div class=\"col-lg-8 offset-lg-2 col-md-10 offset-md-1\">\r\n                    <button class=\"btn btn-primary btn-lg btn-block\" type=\"button\">NETWORK OPTIMIZATION</button>\r\n                    <div class=\"dashboard-block-container\">\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <button class=\"btn btn-block active\" type=\"button\" [routerLink]=\"['../providerdashboard']\">Provider Profile</button>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <button class=\"btn btn-block active\" type=\"button\" [routerLink]=\"['../provider-outcome']\">Provider Outcome</button>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\" dropdown>\r\n                            <button class=\"btn btn-block active\" type=\"button\" data-toggle=\"dropdown\" dropdownToggle aria-haspopup=\"true\" aria-expanded=\"false\">Provider Predictive Analytics</button>\r\n                            <div class=\"dropdown-menu dropdown-menu-left dropdown-menu-dashboard\" dropdownMenu aria-labelledby=\"alerts-dropdown\">\r\n                                <a class=\"dropdown-item\" [routerLink]=\"['../predictive-analytics/provider-map']\">Map View</a>\r\n                                <a class=\"dropdown-item\" [routerLink]=\"['../predictive-analytics/provider-reports']\">Reports View</a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\" dropdown>\r\n                            <button class=\"btn btn-block active\" type=\"button\" data-toggle=\"dropdown\" dropdownToggle aria-haspopup=\"true\" aria-expanded=\"false\">Social Networks</button>\r\n                            <div class=\"dropdown-menu dropdown-menu-left dropdown-menu-dashboard\" dropdownMenu aria-labelledby=\"alerts-dropdown\">\r\n                                <a class=\"dropdown-item\" [routerLink]=\"['../social-networks/provider']\">Provider Network</a>\r\n                                <a class=\"dropdown-item\" [routerLink]=\"['../social-networks/claimant']\">Claimant Network</a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <button class=\"btn btn-block active\" type=\"button\" [routerLink]=\"['../opioid']\">OPIOID</button>\r\n                        </div>\r\n                        <div class=\"dashboard-btn-container\">\r\n                            <button class=\"btn btn-block active\" type=\"button\" [routerLink]=\"['../claim-segmentation']\">Claim Segmentation</button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<footer class=\"footer\" id=\"dashboard-footer\" style=\"left:0;\">\r\n    <span class=\"text-left\">\r\n        &copy; 2019 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1054:
/***/ (function(module, exports) {

module.exports = "<!-- <header class=\"navbar apatics-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header> -->\r\n<app-header-default></app-header-default>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar apatics-sidebar\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Agency Name</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group1\" name=\"input2-group1\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedAN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedAN || searchedAN.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedAN=''\" *ngIf=\"searchedAN && searchedAN.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.agencyNames | searchFacets:searchedAN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 1)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">License Type</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedLT\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedLT || searchedLT.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedLT=''\" *ngIf=\"searchedLT && searchedLT.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.licenseTypes | searchFacets:searchedLT\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 2)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Speciality Code</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedSC\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedSC || searchedSC.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedSC=''\" *ngIf=\"searchedSC && searchedSC.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.specialityCodes | searchFacets:searchedSC\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 3)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">License Status</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedLS\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedLS || searchedLS.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedLS=''\" *ngIf=\"searchedLS && searchedLS.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.licenseStatuses | searchFacets:searchedLS\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 4)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main apatics-container\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../home']\">Home</a></li>\r\n        <li class=\"breadcrumb-item active\"><span>DCA</span></li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"card m-t-2\">\r\n                <!--<h3 class=\"card-header\">DCA</h3>-->\r\n                <div class=\"card-block\">\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group mb-0 col-md-12\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search DCA\" [(ngModel)]=\"searchDCAText\"\r\n                                    (keyup.enter)=\"searchDCA()\">\r\n                                <span class=\"input-group-btn\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchDCA()\"><i class=\"fa fa-search\"></i> Search</button>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 pt-1 text-xs-right page-info\">\r\n                        <span *ngIf=\"totalRecords>0\" [textContent]=\"(pageNumber*recordsToFetch-recordsToFetch+1) + ' to ' + (pageNumber*recordsToFetch-recordsToFetch+rersDCA.length) + ' of ' + totalRecords + ' items'\"></span>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.agencyNames | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 1)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Agency Names':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.licenseTypes | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 2)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty License Types':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.specialityCodes | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 3)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Speciality Codes':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.licenseStatuses | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 4)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty License Statuses':objFacet.name\"></span>                           \r\n                       </button>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 text-lg-center\" *ngIf=\"foundRecord==false\">\r\n                        <h4>No result found.</h4>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 fda-list-container\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"card m-b-1\" *ngFor=\"let objDCA of asyncDCA | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\">\r\n                            <div class=\"card-block p-1\">\r\n                                <div class=\"col-xs-12 p-0\">\r\n                                    <button type=\"button\" class=\"btn btn-primary pull-left mr-1\" (click)=\"objDCA.isOpen=!objDCA.isOpen\">\r\n                                        <i class=\"fa\" [ngClass]=\"objDCA.isOpen ? 'fa-minus' : 'fa-plus'\"></i>\r\n                                    </button>\r\n                                    <div class=\"pull-left\" *ngIf=\"objDCA.agencyName\">\r\n                                        <span class=\"data-title\">Agency Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objDCA.agencyName | highlight:searchDCAText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objDCA.licenseType\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">License Type:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objDCA.licenseType | highlight:searchDCAText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objDCA.specialityCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Speciality Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objDCA.specialityCode | highlight:searchDCAText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objDCA.licenseNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">License Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objDCA.licenseNumber | highlight:searchDCAText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objDCA.licenseStatus\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">License Status:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objDCA.licenseStatus | highlight:searchDCAText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objDCA.firstName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"(objDCA.firstName + ' ' + objDCA.middleName + ' ' + objDCA.lastName + ' ' + objDCA.suffix) | highlight:searchDCAText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objDCA.address\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Address:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objDCA.address | highlight:searchDCAText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-xs-12 p-0\" *ngIf=\"objDCA.isOpen\">\r\n                                    <div class=\"pull-left\" *ngIf=\"objDCA.individualOrganization\">\r\n                                        <span class=\"data-title\">Individual Organization:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objDCA.individualOrganization | highlight:searchDCAText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objDCA.county\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">County:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objDCA.county | highlight:searchDCAText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objDCA.originalIssueDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Original Issue Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objDCA.originalIssueDate.substring(6, objDCA.originalIssueDate.length-7) | date : 'MM-dd-yyyy' | highlight:searchDCAText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objDCA.expirationDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Expiration Date :</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objDCA.expirationDate.substring(6, objDCA.expirationDate.length-7) | date : 'MM-dd-yyyy' | highlight:searchDCAText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objDCA.degree\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Degree:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objDCA.degree | highlight:searchDCAText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"spinner\" [ngClass]=\"{ 'hidden': !loading }\"></div>\r\n                        <div class=\"pagination-container\">\r\n                            <pagination-controls (pageChange)=\"searchDCARecordsWithFilters($event)\" id=\"server\"></pagination-controls>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n\r\n<footer class=\"footer\">\r\n    <span class=\"text-left\">\r\n        &copy; 2017 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1055:
/***/ (function(module, exports) {

module.exports = "<app-header-default></app-header-default>\r\n<!--\r\n<div id=\"facet-sidebar\" class=\"sidebar\">\r\n    <nav class=\"sidebar-nav\">\r\n\r\n    </nav>\r\n</div>-->\r\n<!-- Main content -->\r\n<main class=\"main pl-0 apatics-container\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../home']\">Home</a></li>\r\n        <li class=\"breadcrumb-item active\"><span>DNB</span></li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"card m-t-2\">\r\n                <!--<h3 class=\"card-header\">DNB</h3>-->\r\n                <div class=\"card-block\">\r\n                    <div class=\"container\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-xs-12\">\r\n                                <!--<a (click)=\"pdfShow=!pdfShow\">click me to view PDF in Iframe!</a>-->\r\n                                <div class=\"thumbnail\" style=\"display:block;\" *ngIf=\"!pdfShow\">\r\n                                    <pdf-viewer [src]=\"pdfSrc\" [show-all]=\"true\" [original-size]=\"false\" style=\"display: block;\"></pdf-viewer>\r\n                                </div>\r\n                                <br>\r\n                            </div>\r\n                            <div class=\"col-xs-12\" *ngIf=\"pdfShow\">\r\n                                <object width=\"500\" height=\"600\" [data]=\"pageurl\" type=\"application/pdf\"></object>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n<footer class=\"footer\" style=\"left:0;\">\r\n    <span class=\"text-left\">\r\n        &copy; 2017 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1056:
/***/ (function(module, exports) {

module.exports = "<!-- <header class=\"navbar kdark-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header> -->\r\n<app-header-default></app-header-default>\r\n<div id=\"facet-sidebar\" class=\"sidebar sidebar-kdark\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Vendor Name 1</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group1\" name=\"input2-group1\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedVN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedVN || searchedVN.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedVN=''\" *ngIf=\"searchedVN && searchedVN.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.vendorName1 | searchFacets:searchedVN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 1)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <!-- <li [hidden]=\"true\" class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Date of Service</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedDOS\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedDOS || searchedDOS.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedDOS=''\" *ngIf=\"searchedDOS && searchedDOS.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.dateOfService | searchFacets:searchedDOS:'date'\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 2)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty': (objFacet.name | date:'dd-MMM-yyyy')\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li> -->\r\n            <li class=\"nav-item nav-dropdown range-silider-container\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Billed Amount 1</a>\r\n                <ul class=\"nav-dropdown-items range-silider\">\r\n                    <li class=\"nav-item\" *ngIf=\"resFacets.maxBilledAmount && resFacets.maxBilledAmount>0\">\r\n                        <nouislider class=\"nav-link\" name=\"form_value\" [(ngModel)]=\"billedAmountRange\" [config]=\"billedAmountConfig\" (ngModelChange)=\"onChangeBilledAmount($event)\"></nouislider>\r\n                        <div class=\"range-text\" [textContent]=\"billedAmountRange\"></div>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown range-silider-container\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Paid Amount 1</a>\r\n                <ul class=\"nav-dropdown-items range-silider\">\r\n                    <li class=\"nav-item\" *ngIf=\"resFacets.maxPaidAmount && resFacets.maxPaidAmount>0\">\r\n                        <nouislider class=\"nav-link\" name=\"form_value\" [(ngModel)]=\"paidAmountRange\" [config]=\"paidAmountConfig\" (ngModelChange)=\"onChangePaidAmount($event)\"></nouislider>\r\n                        <div class=\"range-text\" [textContent]=\"paidAmountRange\"></div>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main kdark\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../home']\">Home</a></li>\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../providerdashboard']\">Provider Dashboard</a></li>\r\n        <li class=\"breadcrumb-item active\">\r\n            <span [textContent]=\"pageName\">Double Billing</span>\r\n        </li>\r\n        <!-- Breadcrumb Menu-->\r\n        <li class=\"breadcrumb-menu\">\r\n            <div class=\"btn-group\" role=\"group\" aria-label=\"Button group with nested dropdown\">\r\n                <a class=\"btn btn-clear\" title=\"Download CSV\" href=\"javascript:void(0)\" (click)=\"exportCSV()\">\r\n                    <i class=\"icon-cloud-download\"></i>\r\n                    Download\r\n                </a>\r\n            </div>\r\n        </li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"m-t-2\">\r\n\r\n                <!--<h3 class=\"card-header\">DuplicateBilling</h3>-->\r\n                <div class=\"row\">\r\n                    <div class=\"form-group mb-0 col-md-12\">\r\n                        <div class=\"input-group\">\r\n                            <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search Duplicate Billing\" [(ngModel)]=\"searchDuplicateBillingText\"\r\n                                (keyup.enter)=\"searchDuplicateBilling()\">\r\n                            <span class=\"input-group-btn\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchDuplicateBilling()\"><i class=\"fa fa-search\"></i> Search</button>\r\n                                </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xs-12 p-0 pt-1 text-xs-right page-info\" [hidden]=\"true\">\r\n                    <span *ngIf=\"totalRecords>0\" [textContent]=\"(pageNumber*recordsToFetch-recordsToFetch+1) + ' to ' + (pageNumber*recordsToFetch-recordsToFetch+rersDuplicateBilling.length) + ' of ' + totalRecords + ' items'\"></span>\r\n                </div>\r\n                <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                    <button class=\"btn btn-secondary active\" type=\"button\" *ngIf=\"billedAmountRange && billedAmountRange.length==2 && (billedAmountRange[0]>0 || billedAmountRange[1]<resFacets.maxBilledAmount)\"\r\n                        (click)=\"billedAmountRange=[0,resFacets.maxBilledAmount]\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"'Billed Amount 1(' + billedAmountRange[0] + ' - ' + billedAmountRange[1] + ')'\"></span>                           \r\n                    </button>\r\n                    <button class=\"btn btn-secondary active\" type=\"button\" *ngIf=\"paidAmountRange && paidAmountRange.length==2 && (paidAmountRange[0]>0 || paidAmountRange[1]<resFacets.maxPaidAmount)\"\r\n                        (click)=\"paidAmountRange=[0,resFacets.maxPaidAmount]\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"'Paid Amount 1(' + paidAmountRange[0] + ' - ' + paidAmountRange[1] + ')'\"></span>                           \r\n                    </button>\r\n                    <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.vendorName1 | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 1)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Vendor name 1':objFacet.name\"></span>                           \r\n                    </button>\r\n                    <button [hidden]=\"true\" class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.dateOfService | selectedFacets\"\r\n                        (click)=\"removeSelectedFacet(objFacet, 2)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Date of servive':(objFacet.name | date:'dd-MMM-yyyy')\"></span>                           \r\n                    </button>\r\n                </div>\r\n                <div class=\"col-xs-12 p-0 text-lg-center\" *ngIf=\"foundRecord==false\">\r\n                    <h4>No result found.</h4>\r\n                </div>\r\n            </div>\r\n            <div class=\"row mt-1\">\r\n                <div class=\"col-xs-12\" [hidden]=\"totalRecords==0\">\r\n                    <div class=\"spinner\" [ngClass]=\"{ 'hidden': !loading }\"></div>\r\n                    <div class=\"pagination-container\">\r\n                        <pagination-controls autoHide=\"true\" (pageChange)=\"searchDuplicateBillingRecordsWithFilters($event)\" id=\"server\"></pagination-controls>\r\n                        <ul class=\"pagination\" [hidden]=\"true\">\r\n                            <li class=\"page-item pagination-previous\" [class.disabled]=\"pageNumber<=1\">\r\n                                <a class=\"page-link\" (click)=\"searchDuplicateBillingRecordsWithFilters(pageNumber-1)\">Previous</a>\r\n                            </li>\r\n                            <li class=\"page-item\">\r\n                                <span *ngIf=\"totalRecords>0\" [textContent]=\"'Record ' + pageNumber + ' of ' + totalRecords\"></span>\r\n                            </li>\r\n                            <li class=\"page-item pagination-next\" [class.disabled]=\"pageNumber==(totalRecords/recordsToFetch).toFixed(0)\">\r\n                                <a class=\"page-link\" (click)=\"searchDuplicateBillingRecordsWithFilters(pageNumber+1)\">Next</a>\r\n                            </li>\r\n                        </ul>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div *ngFor=\"let objDuplicateBilling of asyncDuplicateBilling | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\">\r\n            <div class=\"animated fadeIn mb-2 br-5 dup-bill-list\">\r\n                <div class=\"card mb-0\">\r\n                    <div class=\"card-header px-1 py-q\">\r\n                        <div class=\"row m-0\">\r\n                            <div class=\"col-lg-1\">\r\n                                <button class=\"btn btn-link mr-1 mt-0 focus-outline\" (click)=\"objDuplicateBilling.isOpen=!objDuplicateBilling.isOpen\"\r\n                                    type=\"button\">\r\n                                <i class=\"icon-arrow-down\" [hidden]=\"!objDuplicateBilling.isOpen\"></i>\r\n                                <i class=\"icon-arrow-right\" [hidden]=\"objDuplicateBilling.isOpen\"></i>\r\n                            </button>\r\n                            </div>\r\n                            <div class=\"col-lg-11\">\r\n                                <div class=\"pull-left\" *ngIf=\"objDuplicateBilling.billedCPTCode\">\r\n                                    <span class=\"data-title\">CPT CODE:</span>\r\n                                    <div class=\"pull-left\" [innerHTML]=\"objDuplicateBilling.billedCPTCode | highlight:searchDuplicateBillingText\"></div>\r\n                                </div>\r\n                                <div class=\"pull-left\" *ngIf=\"objDuplicateBilling.dateOfServiceYear\">\r\n                                    <div class=\"divider\">|</div>\r\n                                    <span class=\"data-title\">DOSY:</span>\r\n                                    <div class=\"pull-left\" [innerHTML]=\"objDuplicateBilling.dateOfServiceYear | highlight:searchDuplicateBillingText\"></div>\r\n                                </div>\r\n                                <div class=\"pull-left\" *ngIf=\"objDuplicateBilling.dateOfService\">\r\n                                    <div class=\"divider\">|</div>\r\n                                    <span class=\"data-title\">DOS:</span>\r\n                                    <div class=\"pull-left\" [innerHTML]=\"(objDuplicateBilling.dateOfService | date:'dd-MMM-yyyy') | highlight:searchDuplicateBillingText\"></div>\r\n                                </div>\r\n                                <div class=\"pull-left\" *ngIf=\"objDuplicateBilling.typeOfBillCode\">\r\n                                    <div class=\"divider\">|</div>\r\n                                    <span class=\"data-title\">BILL CODE:</span>\r\n                                    <div class=\"pull-left\" [innerHTML]=\"objDuplicateBilling.typeOfBillCode | highlight:searchDuplicateBillingText\"></div>\r\n                                </div>\r\n                                <div class=\"pull-left\" *ngIf=\"objDuplicateBilling.typeOfBillDescription\">\r\n                                    <div class=\"divider\">|</div>\r\n                                    <span class=\"data-title\">TYPE OF DESC.:</span>\r\n                                    <div class=\"pull-left\" [innerHTML]=\"objDuplicateBilling.typeOfBillDescription | highlight:searchDuplicateBillingText\"></div>\r\n                                </div>\r\n                                <div class=\"pull-left\" *ngIf=\"objDuplicateBilling.billedCPTDescription\">\r\n                                    <div class=\"divider\">|</div>\r\n                                    <span class=\"data-title\">CPT DESC.:</span>\r\n                                    <div class=\"pull-left\" [innerHTML]=\"objDuplicateBilling.billedCPTDescription | highlight:searchDuplicateBillingText\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div id=\"slide-down-de\" class=\"card-block\" [hidden]=\"!objDuplicateBilling.isOpen\">\r\n                        <div (click)=\"$event.preventDefault()\">\r\n                            <tabset class=\"kdark\">\r\n                                <tab heading=\"BILL-1\">\r\n                                    <div class=\"row m-0\">\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>TAX ID NUMBER-1</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.taxIDNumber1 | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>VENDOR NAME-1</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.vendorName1 | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>PAID CPT CODE-1</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.paidCPTCode1 | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>BILLED AMOUNT-1</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.billedAmount1 | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>PAID AMOUNT-1</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.paidAmount1 | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>BILL RECEIVED DATE-1</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.billReceivedDate1 | date:'dd-MMM-yyyy' | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>PAID CPT DESCRIPTION-1</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.paidCPTDescription1 | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </tab>\r\n                                <tab heading=\"BILL-2\">\r\n                                    <div class=\"row m-0\">\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>TAX ID NUMBER-2</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.taxIDNumber2 | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>VENDOR NAME-2</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.vendorName2 | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>PAID CPT CODE-2</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.paidCPTCode2 | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>BILLED AMOUNT-2</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.billedAmount2 | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>PAID AMOUNT-2</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.paidAmount2 | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>BILL RECEIVED DATE-2</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.billReceivedDate2 | date:'dd-MMM-yyyy' | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-xs-4\">\r\n                                            <div><b>PAID CPT DESCRIPTION-2</b></div>\r\n                                            <div>\r\n                                                <div class=\"pull-left\" [innerHtml]=\"objDuplicateBilling.paidCPTDescription2 | highlight:searchDuplicateBillingText\"></div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </tab>\r\n                            </tabset>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n<footer class=\"footer kdark\">\r\n    <span class=\"text-left\">\r\n        &copy; 2017 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1057:
/***/ (function(module, exports) {

module.exports = "<!-- <header class=\"navbar apatics-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header> -->\r\n<app-header-default></app-header-default>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar apatics-sidebar\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Pharmacy Name</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group1\" name=\"input2-group1\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPHN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPHN || searchedPHN.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPHN=''\" *ngIf=\"searchedPHN && searchedPHN.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.pharmacyNames | searchFacets:searchedPHN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 1)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Pharmacy Location City</a>\r\n                <ul class=\"nav-dropdown-items\" [hidden]=\"ad\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPLC\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPLC || searchedPLC.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPLC=''\" *ngIf=\"searchedPLC && searchedPLC.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.pharmacyLocationCities | searchFacets:searchedPLC\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 2)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Pharmacy Zip Code</a>\r\n                <ul class=\"nav-dropdown-items\" [hidden]=\"ad\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPZC\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPZC || searchedPZC.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPZC=''\" *ngIf=\"searchedPZC && searchedPZC.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.pharmacyZipCodes | searchFacets:searchedPZC\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 3)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown range-silider-container\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Billed Amount</a>\r\n                <ul class=\"nav-dropdown-items range-silider\">\r\n                    <li class=\"nav-item \">\r\n                        <nouislider class=\"nav-link\" name=\"form_value\" [(ngModel)]=\"billedAmountRange\" [config]=\"billedAmountConfig\" (ngModelChange)=\"onChangeBilledAmount($event)\"></nouislider>\r\n                        <div class=\"range-text\" [textContent]=\"billedAmountRange\"></div>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Prescriber Last Name</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPLN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPLN || searchedPLN.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPLN=''\" *ngIf=\"searchedPLN && searchedPLN.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.prescriberLastNames | searchFacets:searchedPLN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 4)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Claim Counts</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedCC\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedCC || searchedCC.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedCC=''\" *ngIf=\"searchedCC && searchedCC.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.claimCounts | searchFacets:searchedCC\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 5)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown range-silider-container\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Claim Amount</a>\r\n                <ul class=\"nav-dropdown-items range-silider\">\r\n                    <li class=\"nav-item\">\r\n                        <nouislider class=\"nav-link\" name=\"form_value\" [(ngModel)]=\"claimAmountRange\" [config]=\"claimAmountConfig\" (ngModelChange)=\"onChangeClaimAmount($event)\"></nouislider>\r\n                        <div class=\"range-text\" [textContent]=\"claimAmountRange\"></div>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Dea Type</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedDT\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedDT || searchedDT.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedDT=''\" *ngIf=\"searchedDT && searchedDT.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.deaTypes | searchFacets:searchedDT\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 6)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main apatics-container\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../home']\">Home</a></li>\r\n        <li class=\"breadcrumb-item active\"><span>ESI</span></li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"card m-t-2\">\r\n                <!--<h3 class=\"card-header\">ESI</h3>-->\r\n                <div class=\"card-block\">\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group mb-0 col-md-12\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search ESI\" [(ngModel)]=\"searchESIText\"\r\n                                    (keyup.enter)=\"searchESI()\">\r\n                                <span class=\"input-group-btn\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchESI()\"><i class=\"fa fa-search\"></i> Search</button>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.pharmacyNames | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 1)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Pharmacy Names':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.pharmacyLocationCities | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 2)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Pharmacy Location Cities':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.pharmacyZipCodes | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 3)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Pharmacy Zip Codes':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.prescriberLastNames | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 4)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Prescriber Last Names':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.claimCounts | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 5)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Claim Counts':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.deaTypes | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 6)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Dea Types':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngIf=\"billedAmountRange && billedAmountRange.length==2 && (billedAmountRange[0]>0 || billedAmountRange[1]<5000)\"\r\n                            (click)=\"billedAmountRange=[0,5000]\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"billedAmountRange[0] + ' - ' + billedAmountRange[1]\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngIf=\"claimAmountRange && claimAmountRange.length==2 && (claimAmountRange[0]>0 || claimAmountRange[1]<90000)\"\r\n                            (click)=\"claimAmountRange=[0,90000]\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"claimAmountRange[0] + ' - ' + claimAmountRange[1]\"></span>                           \r\n                       </button>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 text-xs-right page-info\">\r\n                        <span *ngIf=\"totalRecords>0\" [textContent]=\"(pageNumber*recordsToFetch-recordsToFetch+1) + ' to ' + (pageNumber*recordsToFetch-recordsToFetch+rersESI.length) + ' of ' + totalRecords + ' items'\"></span>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 text-lg-center\" *ngIf=\"foundRecord==false\">\r\n                        <h4>No result found.</h4>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 fda-list-container\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"card m-b-1\" *ngFor=\"let objESI of asyncESI | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\">\r\n                            <div class=\"card-block p-1\">\r\n                                <div class=\"col-xs-12 p-0\">\r\n                                    <button type=\"button\" class=\"btn btn-primary pull-left mr-1\" (click)=\"objESI.isOpen=!objESI.isOpen\">\r\n                                        <i class=\"fa\" [ngClass]=\"objESI.isOpen ? 'fa-minus' : 'fa-plus'\"></i>\r\n                                    </button>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.pharmacyName\">\r\n                                        <span class=\"data-title\">Pharmacy Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.pharmacyName | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.pharmacyLocationCity\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Pharmacy Location City:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.pharmacyLocationCity | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.pharmacyZipCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Pharmacy Zip Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.pharmacyZipCode | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.billedAmount\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Billed Amount:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.billedAmount | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.prescriberLastName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Prescriber Last Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.prescriberLastName | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.claimCount\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Claim Count:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.claimCount | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.claimAmount\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Claim Amount:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.claimAmount | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.deaType\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Dea Type:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.deaType | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-xs-12 p-0\" *ngIf=\"objESI.isOpen\">\r\n                                    <!---->\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.pharmacyAddress\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Pharmacy Address:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.pharmacyAddress | highlight:searchESIText\"></div>\r\n                                    </div>\r\n\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.pharmacyLocationState\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Pharmacy Location State:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.pharmacyLocationState | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.pharmacyTelephoneNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Pharmacy Telephone Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.pharmacyTelephoneNumber | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.pharmCorpAddress\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Pharm Corp Address:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.pharmCorpAddress | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.pharmCorpAddress2\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Pharm Corp Address2 :</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.pharmCorpAddress2 | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.pharmCorpCity\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Pharm Corp City:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.pharmCorpCity | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.pharmCorpState\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Pharm Corp State:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.pharmCorpState | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.pharmCorpZipCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Pharm Corp Zip Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.pharmCorpZipCode | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.taxID\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Tax ID:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.taxID | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.dateFilled\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Date Filled:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.dateFilled.substring(6, objESI.dateFilled.length-7) | date : 'MM-dd-yyyy' | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.nDCNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">NDC Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.nDCNumber | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.drugName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Drug Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.drugName | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.quantity\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Quantity:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.quantity | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.daysSupply\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Days Supply:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.daysSupply | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.costBasis\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Cost Basis:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.costBasis | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.ingrediantCost\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Ingrediant Cost:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.ingrediantCost | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.salesTax\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Sales Tax:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.salesTax | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.patientFirstName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Patient Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.patientFirstName + ' ' + objESI.patientLastName | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.groupNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Group Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.groupNumber | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.prescriberId\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Prescriber Id:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.prescriberId | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.diagnosisCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Diagnosis Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.diagnosisCode | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.cardholderFirstName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Cardholder Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.cardholderFirstName + ' ' + objESI.cardholderLastName | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.datePrescriptionWritten\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Date Prescription Written:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.datePrescriptionWritten | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.dispenseAsWritten\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Dispense As Written:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.dispenseAsWritten | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.eligibilityClarificationCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Eligibility Clarification Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.eligibilityClarificationCode | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.compoundCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Compound Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.compoundCode | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.numberOfRefillsAuthorized\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Number Of Refills Authorized:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.numberOfRefillsAuthorized | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.levelOfService\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Level Of Service:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.levelOfService | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.prescriptionOriginCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Prescription Origin Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.prescriptionOriginCode | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.primaryPrescriber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Primary Prescriber:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.primaryPrescriber | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.drugType\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Drug Type:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.drugType | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.unitDoseIndicator\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Unit Dose Indicator:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.unitDoseIndicator | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.basisOfDaysSupplyDetermination\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Basis Of Days Supply Determination:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.basisOfDaysSupplyDetermination | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.fullAWP\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Full AWP:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.fullAWP | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.claimType\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Claim Type:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.claimType | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.adjudicationDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">AdjudicationDate:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.adjudicationDate.substring(6, objESI.adjudicationDate.length-7) | date : 'MM-dd-yyyy' | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.adminFee\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Admin Fee:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.adminFee | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.incentiveFee\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Incentive Fee:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.incentiveFee | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.formulary Flag\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">FormularyFlag:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.formularyFlag | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.therapeuticClassAhfs\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Therapeutic Class Ahfs:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.therapeuticClassAhfs | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.pharmacyType\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Pharmacy Type:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.pharmacyType | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.billedBasisCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Billed Basis Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.billedBasisCode | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.paidDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Paid Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.paidDate.substring(6, objESI.paidDate.length-7) | date : 'MM-dd-yyyy' | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.drugStrength\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Drug Strength:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.drugStrength | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.dOI\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">DOI:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.dOI.substring(6, objESI.dOI.length-7) | date : 'MM-dd-yyyy' | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.feeAmount\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Fee Amount:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.feeAmount | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.claimNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Claim Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.claimNumber | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.totalClaimCount\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Total Claim Count:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.totalClaimCount | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objESI.compoundValue\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Compound Value:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objESI.compoundValue | highlight:searchESIText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"spinner\" [ngClass]=\"{ 'hidden': !loading }\"></div>\r\n                        <div class=\"pagination-container\">\r\n                            <pagination-controls (pageChange)=\"searchESIRecordsWithFilters($event)\" id=\"server\"></pagination-controls>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n<footer class=\"footer\">\r\n    <span class=\"text-left\">\r\n        &copy; 2017 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1058:
/***/ (function(module, exports) {

module.exports = "<!-- <header class=\"navbar apatics-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header> -->\r\n<app-header-default></app-header-default>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar apatics-sidebar\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Product NDC</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group1\" name=\"input2-group1\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPNDC\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPNDC || searchedPNDC.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPNDC=''\" *ngIf=\"searchedPNDC && searchedPNDC.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.productNDCs | searchFacets:searchedPNDC\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 1)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Product Type Name</a>\r\n                <ul class=\"nav-dropdown-items\" [hidden]=\"ad\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPTN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPTN || searchedPTN.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPTN=''\" *ngIf=\"searchedPTN && searchedPTN.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.productTypeNames | searchFacets:searchedPTN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 2)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Proprietary Name</a>\r\n                <ul class=\"nav-dropdown-items\" [hidden]=\"ad\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPN || searchedPN.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPN=''\" *ngIf=\"searchedPN && searchedPN.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.proprietaryNames | searchFacets:searchedPN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 3)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Non Proprietary Name</a>\r\n                <ul class=\"nav-dropdown-items\" [hidden]=\"ad\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group4\" name=\"input2-group4\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedNPN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedNPN || searchedNPN.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedNPN=''\" *ngIf=\"searchedNPN && searchedNPN.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.nonProprietaryNames | searchFacets:searchedNPN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 4)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">DEA Schedule</a>\r\n                <ul class=\"nav-dropdown-items\" [hidden]=\"ad\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group5\" name=\"input2-group5\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedDEAS\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedDEAS || searchedDEAS.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedDEAS=''\" *ngIf=\"searchedDEAS && searchedDEAS.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.dEASchedules | searchFacets:searchedDEAS\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 5)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main apatics-container\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../home']\">Home</a></li>\r\n        <li class=\"breadcrumb-item active\"><span>FDA</span></li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"card m-t-2\">\r\n                <!--<h3 class=\"card-header\">FDA</h3>-->\r\n                <div class=\"card-block\">\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group mb-0 col-md-12\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search FDA\" [(ngModel)]=\"searchFDATecxt\"\r\n                                    (keyup.enter)=\"searchFDA()\">\r\n                                <span class=\"input-group-btn\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchFDA()\"><i class=\"fa fa-search\"></i> Search</button>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 pt-1 text-xs-right page-info\">\r\n                        <span *ngIf=\"totalRecords>0\" [textContent]=\"(pageNumber*recordsToFetch-recordsToFetch+1) + ' to ' + (pageNumber*recordsToFetch-recordsToFetch+rersFDA.length) + ' of ' + totalRecords + ' items'\"></span>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.productNDCs | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 1)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.productTypeNames | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 2)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.proprietaryNames | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 3)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.nonProprietaryNames | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 4)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.dEASchedules | selectedFacets:searchedDEAS\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 5)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name\"></span>                           \r\n                       </button>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 text-lg-center\" *ngIf=\"foundRecord==false\">\r\n                        <h4>No result found.</h4>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 fda-list-container\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"card m-b-1\" *ngFor=\"let objFDA of asyncFDA | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\">\r\n                            <div class=\"card-block p-1\">\r\n                                <div class=\"col-xs-12 p-0\">\r\n                                    <button type=\"button\" class=\"btn btn-primary pull-left mr-1\" (click)=\"objFDA.isOpen=!objFDA.isOpen\">\r\n                                        <i class=\"fa\" [ngClass]=\"objFDA.isOpen ? 'fa-minus' : 'fa-plus'\"></i>\r\n                                    </button>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.productTypeName\">\r\n                                        <span class=\"data-title\">Product Type Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.productTypeName | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.proprietaryName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Proprietary Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.proprietaryName | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.nonProprietaryName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Non Proprietary Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.nonProprietaryName | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.dosageFormName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Dosage Form Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.dosageFormName | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.startMarketingDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Start Marketing Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.startMarketingDate | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.labelerName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Labeler Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.labelerName | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-xs-12 p-0\" *ngIf=\"objFDA.isOpen\">\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.productNDC\">\r\n                                        <span class=\"data-title\">Product NDC:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.productNDC | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.proprietaryNamesUffix\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Proprietary Name Suffix:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.proprietaryNamesUffix | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.routeName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">RouteName:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.routeName | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.endMarketingDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">End Marketing Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.endMarketingDate | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.marketingCategoryName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Marketing Category Name :</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.marketingCategoryName | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.applicationNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Application Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.applicationNumber | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.substanceName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Substance Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.substanceName | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.activeNumeratorStrength\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Active Numerator Strength:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.activeNumeratorStrength | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.activeIngredUnit\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Active Ingred Unit:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.activeIngredUnit | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.pharmClasses\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Pharm Classes:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.pharmClasses | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objFDA.dEASchedule\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">DEA Schedule:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objFDA.dEASchedule | highlight:searchFDATecxt\"></div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"spinner\" [ngClass]=\"{ 'hidden': !loading }\"></div>\r\n                        <div class=\"pagination-container\">\r\n                            <pagination-controls (pageChange)=\"searchFDARecordsWithFilters($event)\" id=\"server\"></pagination-controls>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n<footer class=\"footer\">\r\n    <span class=\"text-left\">\r\n        &copy; 2017 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1059:
/***/ (function(module, exports) {

module.exports = "<!-- <header class=\"navbar apatics-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header> -->\r\n<app-header-default></app-header-default>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar apatics-sidebar\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown range-silider-container\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Billed Amount</a>\r\n                <ul class=\"nav-dropdown-items range-silider\">\r\n                    <li class=\"nav-item\" *ngIf=\"resFacets.maxBilledAmount && resFacets.maxBilledAmount>0\">\r\n                        <nouislider class=\"nav-link\" name=\"form_value\" [(ngModel)]=\"billedAmountRange\" [config]=\"billedAmountConfig\" (ngModelChange)=\"onChangeBilledAmount($event)\"></nouislider>\r\n                        <div class=\"range-text\" [textContent]=\"billedAmountRange\"></div>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown range-silider-container\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Distance</a>\r\n                <ul class=\"nav-dropdown-items range-silider\">\r\n                    <li class=\"nav-item\" *ngIf=\"resFacets.maxDistance && resFacets.maxDistance>0\">\r\n                        <nouislider class=\"nav-link\" name=\"form_value\" [(ngModel)]=\"distanceRange\" [config]=\"distanceConfig\" (ngModelChange)=\"onChangeDistance($event)\"></nouislider>\r\n                        <div class=\"range-text\" [textContent]=\"distanceRange\"></div>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown range-silider-container\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Paid Amount</a>\r\n                <ul class=\"nav-dropdown-items range-silider\">\r\n                    <li class=\"nav-item\" *ngIf=\"resFacets.maxPaidAmount && resFacets.maxPaidAmount>0\">\r\n                        <nouislider class=\"nav-link\" name=\"form_value\" [(ngModel)]=\"paidAmountRange\" [config]=\"paidAmountConfig\" (ngModelChange)=\"onChangePaidAmount($event)\"></nouislider>\r\n                        <div class=\"range-text\" [textContent]=\"paidAmountRange\"></div>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Provider Name</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group1\" name=\"input2-group1\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPN || searchedPN.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPN=''\" *ngIf=\"searchedPN && searchedPN.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.providerNames | searchFacets:searchedPN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 1)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Tax ID Number</a>\r\n                <ul class=\"nav-dropdown-items\" [hidden]=\"ad\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedTIN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedTIN || searchedTIN.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedTIN=''\" *ngIf=\"searchedTIN && searchedTIN.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.taxIDNumbers | searchFacets:searchedTIN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 2)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown range-silider-container\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Total Billed Amount</a>\r\n                <ul class=\"nav-dropdown-items range-silider\">\r\n                    <li class=\"nav-item \" *ngIf=\"resFacets.maxTotalBilledAmount && resFacets.maxTotalBilledAmount>0\">\r\n                        <nouislider class=\"nav-link\" name=\"form_value\" [(ngModel)]=\"totalBilledAmountRange\" [config]=\"totalBilledAmountConfig\" (ngModelChange)=\"onChangeTotalBilledAmount($event)\"></nouislider>\r\n                        <div class=\"range-text\" [textContent]=\"totalBilledAmountRange\"></div>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown range-silider-container\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Total Paid Amount</a>\r\n                <ul class=\"nav-dropdown-items range-silider\">\r\n                    <li class=\"nav-item\" *ngIf=\"resFacets.maxTotalPaidAmount && resFacets.maxTotalPaidAmount>0\">\r\n                        <nouislider class=\"nav-link\" name=\"form_value\" [(ngModel)]=\"totalPaidAmountRange\" [config]=\"totalPaidAmountConfig\" (ngModelChange)=\"onChangeTotalPaidAmount($event)\"></nouislider>\r\n                        <div class=\"range-text\" [textContent]=\"totalPaidAmountRange\"></div>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main pr-0 apatics-container\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../home']\">Home</a></li>\r\n        <li class=\"breadcrumb-item active\"><span>Claimant and Provider Distance</span></li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"card m-t-2\">\r\n                <!--<h3 class=\"card-header\">GD</h3>-->\r\n                <div class=\"card-block\">\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group mb-0 col-md-12\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search Claimant and Provider Distance\"\r\n                                    [(ngModel)]=\"searchGDText\" (keyup.enter)=\"searchGD()\">\r\n                                <span class=\"input-group-btn\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchGD()\"><i class=\"fa fa-search\"></i> Search</button>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngIf=\"billedAmountRange && billedAmountRange.length==2 && (billedAmountRange[0]>0 || billedAmountRange[1]<resFacets.maxBilledAmount)\"\r\n                            (click)=\"billedAmountRange=[0,resFacets.maxBilledAmount]\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"'Billed Amount (' + billedAmountRange[0] + ' - ' + billedAmountRange[1] + ')'\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngIf=\"distanceRange && distanceRange.length==2 && (distanceRange[0]>0 || distanceRange[1]<resFacets.maxDistance)\"\r\n                            (click)=\"distanceRange=[0,resFacets.maxDistance]\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"'Distance (' + distanceRange[0] + ' - ' + distanceRange[1] + ')'\"></span>                                                       \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngIf=\"paidAmountRange && paidAmountRange.length==2 && (paidAmountRange[0]>0 || paidAmountRange[1]<resFacets.maxPaidAmount)\"\r\n                            (click)=\"paidAmountRange=[0,resFacets.maxPaidAmount]\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"'Paid Amount (' + paidAmountRange[0] + ' - ' + paidAmountRange[1] + ')'\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.providerNames | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 1)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Provider Names':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.taxIDNumbers | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 2)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty TaxID Numbers':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngIf=\"totalBilledAmountRange && totalBilledAmountRange.length==2 && (totalBilledAmountRange[0]>0 || totalBilledAmountRange[1]<resFacets.maxTotalBilledAmount)\"\r\n                            (click)=\"totalBilledAmountRange=[0,resFacets.maxTotalBilledAmount]\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"'Total Billed Amount (' + totalBilledAmountRange[0] + ' - ' + totalBilledAmountRange[1] + ')'\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngIf=\"totalPaidAmountRange && totalPaidAmountRange.length==2 && (totalPaidAmountRange[0]>0 || totalPaidAmountRange[1]<resFacets.maxTotalPaidAmount)\"\r\n                            (click)=\"totalPaidAmountRange=[0,resFacets.maxTotalPaidAmount]\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"'Total Paid Amount (' + totalPaidAmountRange[0] + ' - ' + totalPaidAmountRange[1] + ')'\"></span>                           \r\n                       </button>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 text-xs-right page-info\">\r\n                        <span *ngIf=\"totalRecords>0\" [textContent]=\"(pageNumber*recordsToFetch-recordsToFetch+1) + ' to ' + (pageNumber*recordsToFetch-recordsToFetch+rersGD.length) + ' of ' + totalRecords + ' items'\"></span>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 text-lg-center\" *ngIf=\"foundRecord==false\">\r\n                        <h4>No result found.</h4>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 fda-list-container\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"card m-b-1\" *ngFor=\"let objGD of asyncGD | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\"\r\n                            (click)=\"openCloseMap\">\r\n                            <div class=\"card-block p-1\">\r\n                                <div class=\"col-xs-12 p-0\">\r\n                                    <button type=\"button\" class=\"btn btn-link p-0 font-2xl pull-left mr-1 map-button\" (click)=\"openCloseMap(objGD)\">\r\n                                        <i class=\"icon-location-pin\"></i>\r\n                                    </button>\r\n                                    <div class=\"pull-left\" *ngIf=\"objGD.billedAmount\">\r\n                                        <span class=\"data-title\">Billed Amount:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objGD.billedAmount | highlight:searchGDText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objGD.claimantAddress\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Claimant Address:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objGD.claimantAddress | highlight:searchGDText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objGD.distance\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Distance:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objGD.distance | highlight:searchGDText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objGD.paidAmount\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Paid Amount:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objGD.paidAmount | highlight:searchGDText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objGD.providerName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Provider Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objGD.providerName | highlight:searchGDText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objGD.claimCount\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Claim Count:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objGD.claimCount | highlight:searchGDText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objGD.taxIDNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Tax ID Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objGD.taxIDNumber | highlight:searchGDText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objGD.totalBilledAmount\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Total Billed Amount:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objGD.totalBilledAmount | highlight:searchGDText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objGD.totalPaidAmount\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">TotalPaidAmount:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objGD.totalPaidAmount | highlight:searchGDText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objGD.providerAddress\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Provider Address:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objGD.providerAddress | highlight:searchGDText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"spinner\" [ngClass]=\"{ 'hidden': !loading }\"></div>\r\n                        <div class=\"pagination-container\">\r\n                            <pagination-controls (pageChange)=\"searchGDRecordsWithFilters($event)\" id=\"server\"></pagination-controls>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n\r\n<aside id=\"map-aside\" class=\"aside-menu\" [class.open]=\"showMap\">\r\n    <!--mapShow-->\r\n    <div class=\"m-0 py-h text-muted text-xs-center bg-faded text-uppercase aside-header\">\r\n        <a class=\"pull-left\" style=\"margin-left:5px;cursor:pointer\" (click)=\"closeMap()\">\r\n<i class=\"fa fa-close\"></i>\r\n</a>\r\n        <small><b>Distance between provider and claimant - <span [textContent]=\"distanceString\"></span></b></small>\r\n    </div>\r\n    <div class=\"m-0 aside-body\">\r\n        <sebm-google-map #gMap [latitude]=\"mapCenterLocation.lat\" [longitude]=\"mapCenterLocation.lng\" [fitBounds]=\"mapBounds\">\r\n            <sebm-google-map-marker *ngFor=\"let m of markers; let i = index\" [latitude]=\"m.lat\" [longitude]=\"m.lng\" [label]=\"m.label\"\r\n                [markerDraggable]=\"m.draggable\" [iconUrl]=\"m.icon\">\r\n                <sebm-google-map-info-window>\r\n                    <strong [textContent]=\"m.info\"></strong>\r\n                </sebm-google-map-info-window>\r\n            </sebm-google-map-marker>\r\n            <sebm-google-map-polyline *ngIf=\"markers && markers.length>=2\">\r\n                <sebm-google-map-polyline-point [latitude]=\"point.lat\" [longitude]=\"point.lng\" *ngFor=\"let point of markers\">\r\n                </sebm-google-map-polyline-point>\r\n            </sebm-google-map-polyline>\r\n        </sebm-google-map>\r\n    </div>\r\n</aside>\r\n<footer class=\"footer\">\r\n    <span class=\"text-left\">\r\n        &copy; 2017 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1060:
/***/ (function(module, exports) {

module.exports = "<!-- <header class=\"navbar apatics-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header> -->\r\n<app-header-default></app-header-default>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar apatics-sidebar\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">ICD9 CDE</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group1\" name=\"input2-group1\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedCDE\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedCDE || searchedCDE.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedCDE=''\" *ngIf=\"searchedCDE && searchedCDE.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.iCD9_CDEs | searchFacets:searchedCDE\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 1)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">ICD9 CDE DSC</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedCDE_DSC\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedCDE_DSC || searchedCDE_DSC.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedCDE_DSC=''\" *ngIf=\"searchedCDE_DSC && searchedCDE_DSC.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.iCD9_CDE_DSCs | searchFacets:searchedCDE_DSC\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 2)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">ICD9 CDE TYP</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedCDE_TYP\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedCDE_TYP || searchedCDE_TYP.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedCDE_TYP=''\" *ngIf=\"searchedCDE_TYP && searchedCDE_TYP.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.iCD9_CDE_TYPs | searchFacets:searchedCDE_TYP\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 3)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">ICD9 CDE TYP DSC</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedCDE_TYP_DSC\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedCDE_TYP_DSC || searchedCDE_TYP_DSC.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedCDE_TYP_DSC=''\" *ngIf=\"searchedCDE_TYP_DSC && searchedCDE_TYP_DSC.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.iCD9_CDE_TYP_DSCs | searchFacets:searchedCDE_TYP_DSC\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 4)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main apatics-container\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../home']\">Home</a></li>\r\n        <li class=\"breadcrumb-item active\"><span>ICD9</span></li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"card m-t-2\">\r\n                <!--<h3 class=\"card-header\">ICD9</h3>-->\r\n                <div class=\"card-block\">\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group mb-0 col-md-12\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search ICD9\" [(ngModel)]=\"searchICD9Text\"\r\n                                    (keyup.enter)=\"searchICD9()\">\r\n                                <span class=\"input-group-btn\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchICD9()\"><i class=\"fa fa-search\"></i> Search</button>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 pt-1 text-xs-right page-info\">\r\n                        <span *ngIf=\"totalRecords>0\" [textContent]=\"(pageNumber*recordsToFetch-recordsToFetch+1) + ' to ' + (pageNumber*recordsToFetch-recordsToFetch+rersICD9.length) + ' of ' + totalRecords + ' items'\"></span>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.iCD9_CDEs | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 1)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty CDE':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.iCD9_CDE_DSCs | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 2)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty CDE DSC':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.iCD9_CDE_TYPs | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 3)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty CDE TYP':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.iCD9_CDE_TYP_DSCs | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 4)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty CDE TYP DSC':objFacet.name\"></span>                           \r\n                       </button>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 text-lg-center\" *ngIf=\"foundRecord==false\">\r\n                        <h4>No result found.</h4>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 fda-list-container\" [hidden]=\"totalRecords==0\">\r\n                        <!--<div class=\"card m-b-1\" *ngFor=\"let objICD9 of asyncICD9 | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\">\r\n                            <div class=\"card-block p-1\">\r\n                                <div class=\"col-xs-12 p-0\">\r\n                                    <div class=\"pull-left\" *ngIf=\"objICD9.iCD9_CDE\">\r\n                                        <span class=\"data-title\">CDE:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objICD9.iCD9_CDE | highlight:searchICD9Text\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objICD9.iCD9_CDE_DSC\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">CDE DSC:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objICD9.iCD9_CDE_DSC | highlight:searchICD9Text\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objICD9.iCD9_CDE_TYP\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">CDE TYP:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objICD9.iCD9_CDE_TYP | highlight:searchICD9Text\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objICD9.iCD9_CDE_TYP_DSC\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">CDE TYP DSC:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objICD9.iCD9_CDE_TYP_DSC | highlight:searchICD9Text\"></div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>-->\r\n                        <div class=\"col-xs-12 p-0\">\r\n                            <table class=\"table table-bordered table-striped table-condensed\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th>CDE</th>\r\n                                        <th>CDE DSC</th>\r\n                                        <th>CDE TYP</th>\r\n                                        <th>CDE TYP DSC</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr *ngFor=\"let objICD9 of asyncICD9 | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\">\r\n                                        <td [innerHTML]=\"objICD9.iCD9_CDE | highlight:searchICD9Text\"></td>\r\n                                        <td [innerHTML]=\"objICD9.iCD9_CDE_DSC | highlight:searchICD9Text\">2012/02/01</td>\r\n                                        <td [innerHTML]=\"objICD9.iCD9_CDE_TYP | highlight:searchICD9Text\"></td>\r\n                                        <td [innerHTML]=\"objICD9.iCD9_CDE_TYP_DSC | highlight:searchICD9Text\">\r\n                                        </td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"spinner\" [ngClass]=\"{ 'hidden': !loading }\"></div>\r\n                        <div class=\"pagination-container\">\r\n                            <pagination-controls (pageChange)=\"searchICD9RecordsWithFilters($event)\" id=\"server\"></pagination-controls>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n<footer class=\"footer\">\r\n    <span class=\"text-left\">\r\n        &copy; 2017 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1061:
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n\r\n  <div class=\"wrapper\">\r\n    <form name=\"Login_Form\" class=\"form-signin\" #loginForm=\"ngForm\" (ngSubmit)=\"login(username.value, password.value)\">\r\n      <h3 class=\"form-signin-heading\">\r\n        <span>Sign In</span>\r\n      </h3>\r\n\r\n      <input type=\"text\" class=\"form-control\" name=\"username\" placeholder=\"Username\" autofocus=\"\" #username ngModel required/>\r\n      <input type=\"password\" class=\"form-control\" name=\"password\" placeholder=\"Password\" #password ngModel required/>\r\n\r\n      <button class=\"btn btn-lg btn-primary btn-block\" name=\"Submit\" value=\"Login\" type=\"submit\" [disabled]=\"!loginForm.valid\">Login</button>\r\n      <div *ngIf=\"loginError\" class=\"error-mssg-section m-t-10 text-center\">\r\n        <alert type=\"danger\" dismissOnTimeout=\"3000\" (onClose)=\"onClose()\">{{ loginError }}</alert>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div>"

/***/ }),

/***/ 1062:
/***/ (function(module, exports) {

module.exports = "<!-- <header class=\"navbar apatics-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header> -->\r\n<app-header-default></app-header-default>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar apatics-sidebar\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Date of Action Year</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group1\" name=\"input2-group1\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedDOAY\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedDOAY || searchedDOAY.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedDOAY=''\" *ngIf=\"searchedDOAY && searchedDOAY.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.dateOfActionYears | searchFacets:searchedDOAY\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 1)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Action Type</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedAT\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedAT || searchedAT.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedAT=''\" *ngIf=\"searchedAT && searchedAT.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.actionTypes | searchFacets:searchedAT\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 2)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Specialty</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedSP\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedSP || searchedSP.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedSP=''\" *ngIf=\"searchedSP && searchedSP.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.specialties | searchFacets:searchedSP\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 3)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Business Type</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedBT\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedBT || searchedBT.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedBT=''\" *ngIf=\"searchedBT && searchedBT.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.businessTypes | searchFacets:searchedBT\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 4)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main apatics-container\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../home']\">Home</a></li>\r\n        <li class=\"breadcrumb-item active\"><span>MBCOIG</span></li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"card m-t-2\">\r\n                <!--<h3 class=\"card-header\">MBCOIG</h3>-->\r\n                <div class=\"card-block\">\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group mb-0 col-md-12\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search MBCOIG\" [(ngModel)]=\"searchMBCOIGText\"\r\n                                    (keyup.enter)=\"searchMBCOIG()\">\r\n                                <span class=\"input-group-btn\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchMBCOIG()\"><i class=\"fa fa-search\"></i> Search</button>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 pt-1 text-xs-right page-info\">\r\n                        <span *ngIf=\"totalRecords>0\" [textContent]=\"(pageNumber*recordsToFetch-recordsToFetch+1) + ' to ' + (pageNumber*recordsToFetch-recordsToFetch+rersMBCOIG.length) + ' of ' + totalRecords + ' items'\"></span>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.dateOfActionYears | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 1)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Date of Action Years':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.actionTypes | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 2)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Action Types':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.specialties | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 3)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Specialties':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.businessTypes | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 4)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Business Types':objFacet.name\"></span>                           \r\n                       </button>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 text-lg-center\" *ngIf=\"foundRecord==false\">\r\n                        <h4>No result found.</h4>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 fda-list-container\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"card m-b-1\" *ngFor=\"let objMBCOIG of asyncMBCOIG | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\">\r\n                            <div class=\"card-block p-1\">\r\n                                <div class=\"col-xs-12 p-0\">\r\n                                    <button type=\"button\" class=\"btn btn-primary pull-left mr-1\" (click)=\"objMBCOIG.isOpen=!objMBCOIG.isOpen\">\r\n                                        <i class=\"fa\" [ngClass]=\"objMBCOIG.isOpen ? 'fa-minus' : 'fa-plus'\"></i>\r\n                                    </button>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.alertSource\">\r\n                                        <span class=\"data-title\">Alert Source:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.alertSource | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.dateOfActionYear\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Date Of Action Year:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.dateOfActionYear | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.actionType\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Action Type:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.actionType | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.specialty\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Specialty:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.specialty | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.businessType\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">business Type:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.businessType | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.monthYear\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Month Year:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.monthYear | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.matchName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Match Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.matchName | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.actionDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Action Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.actionDate.substring(6, objMBCOIG.actionDate.length-7) | date : 'MM-dd-yyyy' | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.reinsistDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Reinsist Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.reinsistDate.substring(6, objMBCOIG.reinsistDate.length-7) | date : 'MM-dd-yyyy' | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.waiverDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Waiver Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.waiverDate.substring(6, objMBCOIG.actionDate.length-7) | date : 'MM-dd-yyyy' | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-xs-12 p-0\" *ngIf=\"objMBCOIG.isOpen\">\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.licenseNumber\">\r\n                                        <span class=\"data-title\">License Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.licenseNumber | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.nPI\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">NPI:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.nPI | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.cityState\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">City State :</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.cityState | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.zipCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Zip Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.zipCode | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.businessName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Business Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.businessName | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objMBCOIG.address\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Address:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objMBCOIG.address | highlight:searchMBCOIGText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"spinner\" [ngClass]=\"{ 'hidden': !loading }\"></div>\r\n                        <div class=\"pagination-container\">\r\n                            <pagination-controls (pageChange)=\"searchMBCOIGRecordsWithFilters($event)\" id=\"server\"></pagination-controls>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n<footer class=\"footer\">\r\n    <span class=\"text-left\">\r\n        &copy; 2017 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1063:
/***/ (function(module, exports) {

module.exports = "<!-- <header class=\"navbar apatics-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header> -->\r\n<app-header-default></app-header-default>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar apatics-sidebar\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Provider Specialty</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group1\" name=\"input2-group1\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPS\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPS || searchedPS.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPS=''\" *ngIf=\"searchedPS && searchedPS.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.providerSpecialties | searchFacets:searchedPS\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 1)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Professional License Type</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPLT\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPLT || searchedPLT.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPLT=''\" *ngIf=\"searchedPLT && searchedPLT.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.professionalLicenseTypes | searchFacets:searchedPLT\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 2)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Professional License Action</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPLA\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPLA || searchedPLA.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPLA=''\" *ngIf=\"searchedPLA && searchedPLA.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.professionalLicenseActions | searchFacets:searchedPLA\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 3)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main apatics-container\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../home']\">Home</a></li>\r\n        <li class=\"breadcrumb-item active\"><span>NICB</span></li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"card m-t-2\">\r\n                <!--<h3 class=\"card-header\">NICB</h3>-->\r\n                <div class=\"card-block\">\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group mb-0 col-md-12\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search NICB\" [(ngModel)]=\"searchNICBText\"\r\n                                    (keyup.enter)=\"searchNICB()\">\r\n                                <span class=\"input-group-btn\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchNICB()\"><i class=\"fa fa-search\"></i> Search</button>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 pt-1 text-xs-right page-info\">\r\n                        <span *ngIf=\"totalRecords>0\" [textContent]=\"(pageNumber*recordsToFetch-recordsToFetch+1) + ' to ' + (pageNumber*recordsToFetch-recordsToFetch+rersNICB.length) + ' of ' + totalRecords + ' items'\"></span>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.providerSpecialties | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 1)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Provider Specialties':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.professionalLicenseTypes | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 2)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Professional License Types':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.professionalLicenseActions | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 3)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Professional License Actions':objFacet.name\"></span>                           \r\n                       </button>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 text-lg-center\" *ngIf=\"foundRecord==false\">\r\n                        <h4>No result found.</h4>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 fda-list-container\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"card m-b-1\" *ngFor=\"let objNICB of asyncNICB | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\">\r\n                            <div class=\"card-block p-1\">\r\n                                <div class=\"col-xs-12 p-0\">\r\n                                    <button type=\"button\" class=\"btn btn-primary pull-left mr-1\" (click)=\"objNICB.isOpen=!objNICB.isOpen\">\r\n                                        <i class=\"fa\" [ngClass]=\"objNICB.isOpen ? 'fa-minus' : 'fa-plus'\"></i>\r\n                                    </button>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.alertNumber\">\r\n                                        <span class=\"data-title\">Alert Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.alertNumber | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.partyFirstName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Party Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.partyFirstName + ' ' +  objNICB.partyLastName  | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.providerSpecialty\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Provider Specialty:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.providerSpecialty | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.providerIndividualNPI\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Provider Individual NPI:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.providerIndividualNPI | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.professionalLicenseNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Professional License Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.professionalLicenseNumber | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.professionalLicenseState\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Professional License State:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.professionalLicenseState | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.professionalLicenseType\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Professional License Type:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.professionalLicenseType | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.professionalLicenseAction\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Professional License Action:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.professionalLicenseAction | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.businessName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Business Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.businessName | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-xs-12 p-0\" *ngIf=\"objNICB.isOpen\">\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.clinicNPI\">\r\n                                        <span class=\"data-title\">Clinic NPI:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.clinicNPI | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.streetNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Street Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.streetNumber | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.streetName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Street Name :</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.streetName | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.streetType\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Street Type:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.streetType | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.city\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">City:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.city | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.state\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">State:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.state | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.zipCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Zip Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.zipCode | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objNICB.alertInformation\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Alert Information:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objNICB.alertInformation | highlight:searchNICBText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"spinner\" [ngClass]=\"{ 'hidden': !loading }\"></div>\r\n                        <div class=\"pagination-container\">\r\n                            <pagination-controls (pageChange)=\"searchNICBRecordsWithFilters($event)\" id=\"server\"></pagination-controls>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n<footer class=\"footer\">\r\n    <span class=\"text-left\">\r\n        &copy; 2017 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1064:
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar sidebar-kdark\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">City</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedCity\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedCity || searchedCity.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedCity=''\" *ngIf=\"searchedCity && searchedCity.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let city of activeFilters['citys']|searchFacets:searchedCity\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeCitySelection(city)\">\r\n                            <i class=\"fa\" [ngClass]=\"city.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"city.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"city.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Zip</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedZip\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedZip || searchedZip.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedZip=''\" *ngIf=\"searchedZip && searchedZip.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let zip of activeFilters['zips'] | searchFacets:searchedZip\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeZipSelection(zip)\">\r\n                            <i class=\"fa\" [ngClass]=\"zip.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"zip.name==''?'Empty':zip.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"zip.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Providers</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedProv\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedProv || searchedProv.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedProv=''\" *ngIf=\"searchedProv && searchedProv.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let prov of activeFilters['providers'] | searchFacets:searchedProv\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeProvSelection(prov)\">\r\n                            <i class=\"fa\" [ngClass]=\"prov.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"prov.name==''?'Empty':prov.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"prov.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">TIN Number</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedTIN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedTIN || searchedTIN.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedTIN=''\" *ngIf=\"searchedTIN && searchedTIN.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let tin of activeFilters['tin_nums'] | searchFacets:searchedTIN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeTinSelection(tin)\">\r\n                            <i class=\"fa\" [ngClass]=\"tin.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"tin.name==''?'Empty':tin.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"tin.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Provider Value</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPV\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPV || searchedPV.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPV=''\" *ngIf=\"searchedPV && searchedPV.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let pv of activeFilters['providers_values'] | searchFacets:searchedPV\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changePvSelection(pv)\">\r\n                            <i class=\"fa\" [ngClass]=\"pv.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"pv.name==''?'Empty':pv.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"pv.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main kdark\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\">\r\n            <a [routerLink]=\"['../../home']\">Home</a>\r\n        </li>\r\n        <li class=\"breadcrumb-item active\">\r\n            <span>Provider Map</span>\r\n        </li>\r\n        <!-- Breadcrumb Menu-->\r\n    </ol>\r\n\r\n    <div class=\"container-fluid\">\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"m-t-2\">\r\n                <div class=\"card-block\">\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group mb-0 col-md-12\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search ProviderSummary\" [(ngModel)]=\"searchProviderSummaryText\"\r\n                                    (keyup.enter)=\"searchProviderSummary()\">\r\n                                <span class=\"input-group-btn\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchProviderSummary()\">\r\n                                        <i class=\"fa fa-search\"></i> Search</button>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                            <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['citys'] | selectedFacets\" (click)=\"changeCitySelection(objFacet)\">\r\n                                <i class=\"fa fa-close\"></i>\r\n                                <span [textContent]=\"objFacet.name==''?'Empty Provider Name':objFacet.name\"></span>\r\n                            </button>\r\n                            <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['zips'] | selectedFacets\" (click)=\"changeZipSelection(objFacet)\">\r\n                                <i class=\"fa fa-close\"></i>\r\n                                <span [textContent]=\"objFacet.name==''?'Empty Provider Type':objFacet.name\"></span>\r\n                            </button>\r\n                            <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['providers'] | selectedFacets\"\r\n                                (click)=\"changeProvSelection(objFacet)\">\r\n                                <i class=\"fa fa-close\"></i>\r\n                                <span [textContent]=\"objFacet.name==''?'Empty Provider Type':objFacet.name\"></span>\r\n                            </button>\r\n                            <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['tin_nums'] | selectedFacets\"\r\n                                (click)=\"changeTinSelection(objFacet)\">\r\n                                <i class=\"fa fa-close\"></i>\r\n                                <span [textContent]=\"objFacet.name==''?'Empty Provider Type':objFacet.name\"></span>\r\n                            </button>\r\n                            <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['providers_values'] | selectedFacets\"\r\n                                (click)=\"changePvSelection(objFacet)\">\r\n                                <i class=\"fa fa-close\"></i>\r\n                                <span [textContent]=\"objFacet.name==''?'Empty Provider Type':objFacet.name\"></span>\r\n                            </button>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <div class=\"col-sm-12\">\r\n                            <div class=\"col-sm-12\" style=\"padding: 0;\">\r\n                                <div id=\"map\" style=\"position: relative; height: 470px;\"></div>\r\n                                <div class=\"\" id=\"map-legend\" style=\"z-index: 0; position: absolute; top: 0px; left: 0px;\">\r\n                                    <h5 class=\"m-t-0\">Legend</h5>\r\n                                    <div>\r\n                                        <img class=\"map-legend-icons\" src=\"assets/img/gmap-icons/turquoise-blue.png\"> Most Valued\r\n                                    </div>\r\n                                    <div>\r\n                                        <img class=\"map-legend-icons\" src=\"assets/img/gmap-icons/yellow.png\"> Medium Valued\r\n                                    </div>\r\n                                    <div>\r\n                                        <img class=\"map-legend-icons\" src=\"assets/img/gmap-icons/red-dark.png\"> Least Valued\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\" style=\"margin-top:10px\">\r\n                        <div class=\"col-sm-12\">\r\n                            <div class=\"\">\r\n                                <table class=\"table table-bordered\" style=\"color:#1f486a\">\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th>Provider</th>\r\n                                            <th>TIN Number</th>\r\n                                            <th>Address</th>\r\n                                            <th>City</th>\r\n                                            <th>Zip Code</th>\r\n                                            <th>Rank within the cluster</th>\r\n                                            <th>Provider value</th>\r\n                                        </tr>\r\n                                    </thead>\r\n                                    <tbody>\r\n                                        <tr *ngFor=\"let md of mapData\">\r\n                                            <td *ngIf=\"md.is_redirect==1\">\r\n                                                <a [routerLink]=\"['../../providerdashboard']\" [queryParams]=\"{ provider_firstname: md.first_name,provider_lastname:md.last_name }\">{{md.first_name}} {{md.last_name}}</a>\r\n                                            </td>\r\n                                            <td *ngIf=\"md.is_redirect==0\">\r\n                                                {{md.first_name}} {{md.last_name}}\r\n                                            </td>\r\n                                            <td>{{md.TIN_NUM}}</td>\r\n                                            <td>{{md.Address }}</td>\r\n                                            <td>{{md.City}}</td>\r\n                                            <td>{{md.Zip}}</td>\r\n                                            <td>{{md.rank_order}}</td>\r\n                                            <td style=\"text-align: center;\">\r\n                                                <img style=\"width:20px\" *ngIf=\"md.PROVIDER_VALUE=='Most Valued'\" src=\"assets/img/gmap-icons/turquoise-blue.png\">\r\n                                                <img style=\"width:20px\" *ngIf=\"md.PROVIDER_VALUE=='Medium Valued'\" src=\"assets/img/gmap-icons/yellow.png\">\r\n                                                <img style=\"width:20px\" *ngIf=\"md.PROVIDER_VALUE=='Least Valued'\" src=\"assets/img/gmap-icons/red-dark.png\">\r\n                                            </td>\r\n\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n\r\n\r\n<footer class=\"footer kdark\">\r\n    <span class=\"text-left\">\r\n        &copy; 2019 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1065:
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar sidebar-kdark\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">MPN</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedMNP\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedMNP || searchedMNP.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedMNP=''\" *ngIf=\"searchedMNP && searchedMNP.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let mnp of activeFilters['mnp']|searchFacets:searchedMNP\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeMnpSelection(mnp)\">\r\n                            <i class=\"fa\" [ngClass]=\"mnp.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"mnp.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"mnp.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Work Day</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPS\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPS || searchedPS.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPS=''\" *ngIf=\"searchedPS && searchedPS.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let wd of activeFilters['work_day'] | searchFacets:searchedPS\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeWorkDaySelection(wd)\">\r\n                            <i class=\"fa\" [ngClass]=\"wd.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"wd.name==''?'Empty':wd.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"wd.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Modified job day</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPS\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPS || searchedPS.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPS=''\" *ngIf=\"searchedPS && searchedPS.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let mjd of activeFilters['modified_day'] | searchFacets:searchedPS\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeModifiedDaySelection(mjd)\">\r\n                            <i class=\"fa\" [ngClass]=\"mjd.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"mjd.name==''?'Empty':mjd.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"mjd.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <li *ngIf=\"0\" class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Patient Age</a>\r\n                <ul class=\"nav-dropdown-items m-t-0\">\r\n                    <li class=\"nav-item age-slider-li onAgeChange\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <nouislider [connect]=\"true\" [min]=\"0\" [max]=\"15\" [step]=\"1\" [(ngModel)]=\"someRange\" (ngModelChange)=\"onAgeChange($event)\"></nouislider>\r\n                            </div>\r\n                            <div>{{someRange[0]}} - {{someRange[1]}}</div>\r\n                        </div>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main kdark\" id=\"main-container\">\r\n    <div *ngIf=\"isShowLoad\" class=\"loading\">Loading&#8230;</div>\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\">\r\n            <a [routerLink]=\"['../home']\">Home</a>\r\n        </li>\r\n        <li class=\"breadcrumb-item active\">\r\n            <span>Provider Outcome</span>\r\n        </li>\r\n        <!-- Breadcrumb Menu-->\r\n    </ol>\r\n    <div class=\"container-fluid text-white\">\r\n        <div class=\"row m-b-1\" style=\"padding: 0px 13px 0px 2px;\">\r\n            <div *ngIf=\"activeFilters['default'].city\" class=\"col-md-2\">City :\r\n                <span class=\"edit-address-selection-line\" (click)=\"changeAddressInfoModel()\">{{activeFilters['default'].city}}</span>\r\n            </div>\r\n            <div *ngIf=\"activeFilters['default'].zip\" class=\"col-md-2\">Zip Code :\r\n                <span class=\"edit-address-selection-line\" (click)=\"changeAddressInfoModel()\">{{activeFilters['default'].zip}}</span>\r\n            </div>\r\n            <div *ngIf=\"activeFilters['default'].icd\" class=\"col-md-4\">ICD Code/ICD Description :\r\n                <span class=\"edit-address-selection-line\" (click)=\"changeAddressInfoModel()\">{{activeFilters['default'].icd}}</span>\r\n            </div>\r\n            <div *ngIf=\"activeFilters['default'].speciality\" class=\"col-md-4\">Speciality :\r\n                <span class=\"edit-address-selection-line\" (click)=\"changeAddressInfoModel()\">{{activeFilters['default'].speciality}}</span>\r\n            </div>\r\n            <div class=\"col-md-4\" class=\"send-sms-to-claimant\">\r\n                <button (click)=\"openSendSMSModel()\" type=\"button\" class=\"btn btn-primary\">Send providers list to claimant</button>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n    <div class=\"container-fluid\">\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"m-t-2\">\r\n                <div class=\"row\">\r\n                    <div class=\"form-group mb-0 col-md-12\">\r\n                        <div class=\"input-group\">\r\n                            <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search ProviderSummary\" [(ngModel)]=\"searchProviderSummaryText\">\r\n                            <span class=\"input-group-btn\">\r\n                                <button type=\"button\" class=\"btn btn-primary\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                    Search\r\n                                </button>\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"\">\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['mnp'] | selectedFacets\" (click)=\"changeMnpSelection(objFacet)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Provider Name':objFacet.name\"></span>\r\n                        </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['work_day'] | selectedFacets\"\r\n                            (click)=\"changeWorkDaySelection(objFacet)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Provider Type':objFacet.name\"></span>\r\n                        </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['modified_day'] | selectedFacets\"\r\n                            (click)=\"changeModifiedDaySelection(objFacet)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Provider Type':objFacet.name\"></span>\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-12\">\r\n                        <div class=\"card br-5 mb-2 card-accent-primary mb-0\">\r\n                            <div class=\"card-header\">\r\n                                Top 5 Providers in the area\r\n                            </div>\r\n                            <div class=\"card-block\">\r\n                                <table class=\"table ktable treatment-table table-responsive\">\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th>\r\n                                                <input class=\"pr-check-all\" title=\"Select All\" (change)=\"selectAllProvider($event);\" type=\"checkbox\">\r\n                                            </th>\r\n                                            <th width=\"25%\">Provider</th>\r\n\r\n                                            <th>ICD Code</th>\r\n                                            <th>Avg duration to Modified Job (Days)</th>\r\n                                            <th>Avg duration Return to Work (Days)</th>\r\n                                            <th>Avg amount per Patient</th>\r\n                                            <th>MPN</th>\r\n                                            <th>Distance (Miles)</th>\r\n                                        </tr>\r\n                                    </thead>\r\n                                    <tbody class=\"text-center\">\r\n                                        <tr *ngFor=\"let pod of providerOutcomeData\">\r\n                                            <td>\r\n                                                <input class=\"pr-check-all\" [(ngModel)]=\"pod.selected\" title=\"Select All\" type=\"checkbox\">\r\n                                            </td>\r\n                                            <td>\r\n                                                <address class=\"text-left\">\r\n                                                    <strong [innerHTML]=\"pod.PROVIDER_NAME | highlight:searchProviderSummaryText\"></strong>\r\n                                                    <br>\r\n                                                    <span [innerHTML]=\"pod.SPECIALITY | highlight:searchProviderSummaryText\"></span>\r\n                                                    <br>\r\n                                                    <i class=\"icon-home icons font-1xl\"></i>\r\n                                                    <span [innerHTML]=\"pod.PROVIDER_ADDRESS+', '+pod.VENDOR_CITY+' '+pod.STATE+' '+pod.VENDOR_ZIP | highlight:searchProviderSummaryText\"></span>\r\n                                                    <br>\r\n                                                    <i class=\"icon-phone icons font-1xl\"></i>\r\n                                                    <span [innerHTML]=\"pod.PHONE_NUMBER | highlight:searchProviderSummaryText\"></span>\r\n                                                </address>\r\n                                            </td>\r\n\r\n                                            <td>{{pod.ICD_CODE1}}</td>\r\n                                            <td>{{pod.RMJ_DAYS}}</td>\r\n                                            <td>{{pod.RTW_DAYS}}</td>\r\n                                            <td>${{pod.TOT_PAID_AMT}}</td>\r\n                                            <td>{{pod.MPN_YN}}</td>\r\n                                            <td>{{pod.DISTANCE}}</td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n\r\n                                <div class=\"chart-section row\">\r\n                                    <div class=\"col-sm-12\">\r\n                                        <div class=\"ap-light-blue-bg\">\r\n                                            <nvd3 [options]=\"paidChartOptions\" [data]=\"paidChartData\"></nvd3>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n\r\n<!-- /.modal -->\r\n<div bsModal #addressInfoModel=\"bs-modal\" [config]=\"{backdrop: 'static'}\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\"\r\n    aria-labelledby=\"myModalLabel\" aria-hidden=\"false\">\r\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n\r\n            <div class=\"card-header\">\r\n                <strong>Select a City and Zip Code</strong>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-12\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-6\">\r\n                                <div class=\"form-group\">\r\n                                    <label for=\"name\">City</label>\r\n                                    <ng-select name=\"city\" [(ngModel)]=\"addressSelectObj.city\" (selected)=\"selectedCity($event)\" #selCity [items]=\"cityList\"\r\n                                        placeholder=\"Search For City\">\r\n                                    </ng-select>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-sm-6\">\r\n                                <div class=\"form-group\">\r\n                                    <label for=\"name\">Zip Code</label>\r\n                                    <ng-select name=\"zip\" [(ngModel)]=\"addressSelectObj.zip\" (selected)=\"selectedZip($event)\" #selZipCode [items]=\"zipCodeList\"\r\n                                        placeholder=\"Search For Zip Code\">\r\n                                    </ng-select>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"row\">\r\n                            <div class=\"form-group col-sm-12\">\r\n                                <label for=\"name\">Please select either ICD</label>\r\n                                <ng-select name=\"icd\" [(ngModel)]=\"addressSelectObj.icd\" (selected)=\"selectedICD($event)\" #selICDCode [items]=\"icdCodeList\"\r\n                                    placeholder=\"Search For Zip Code\">\r\n                                </ng-select>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-12 text-center\">OR</div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"form-group col-sm-12\">\r\n                                <label for=\"name\">Speciality</label>\r\n                                <ng-select name=\"speciality\" [(ngModel)]=\"addressSelectObj.speciality\" (selected)=\"selectedSpeciality($event)\" #selSpeciality\r\n                                    [items]=\"specialityList\" placeholder=\"Search For Zip Code\">\r\n                                </ng-select>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <h1 *ngIf=\"0 && !((addressSelectObj.city && addressSelectObj.city.length>0 && addressSelectObj.city[0].hasOwnProperty('text')) && (addressSelectObj.zip && addressSelectObj.zip.length>0 && addressSelectObj.zip[0].hasOwnProperty('text')) && ((addressSelectObj.icd && addressSelectObj.icd.length>0 && addressSelectObj.icd[0].hasOwnProperty('text')) || (addressSelectObj.speciality && addressSelectObj.speciality.length>0 && addressSelectObj.speciality[0].hasOwnProperty('text'))))\">All Not Valid</h1>\r\n\r\n            <div class=\"modal-footer text-center\">\r\n                <button type=\"button\" (click)=\"submitAddressModelData()\" [disabled]=\"!((addressSelectObj.city && addressSelectObj.city.length>0 && addressSelectObj.city[0].hasOwnProperty('text')) && (addressSelectObj.zip && addressSelectObj.zip.length>0 && addressSelectObj.zip[0].hasOwnProperty('text')) && ((addressSelectObj.icd && addressSelectObj.icd.length>0 && addressSelectObj.icd[0].hasOwnProperty('text')) || (addressSelectObj.speciality && addressSelectObj.speciality.length>0 && addressSelectObj.speciality[0].hasOwnProperty('text'))))\"\r\n                    class=\"btn btn-primary\">Submit</button>\r\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"closeAddressInfoModel()\">Cancel</button>\r\n            </div>\r\n\r\n        </div>\r\n        <!-- /.modal-content -->\r\n    </div>\r\n    <!-- /.modal-dialog -->\r\n</div>\r\n<div bsModal #sendSmsInfo=\"bs-modal\" [config]=\"{backdrop: 'static'}\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"\r\n    aria-hidden=\"false\">\r\n    <div class=\"modal-dialog modal-lg\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n\r\n            <div class=\"card-header\">\r\n                <strong>Select claimants</strong>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <div class=\"alert alert-success alert-dismissable\" id=\"ProvideMsgScccess\" style=\"display:none\">\r\n                    <strong>Success!</strong> Massage sent successfully.\r\n                </div>\r\n\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-12\">\r\n                        <div class=\"row\">\r\n                            <div class=\"form-group col-sm-12\">\r\n                                <label for=\"name\">Select claimant</label>\r\n                                <ng-select [multiple]=\"true\" [(ngModel)]=\"sendSmsSelectObj.sender\" [items]=\"sendersList\" placeholder=\"No senders selected\"></ng-select>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <h1 *ngIf=\"0\">{{sendSmsSelectObj|json}}</h1>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"modal-footer text-center\">\r\n                <button type=\"button\" [disabled]=\"!(sendSmsSelectObj.sender && sendSmsSelectObj.sender.length>0)\" (click)=\"sendSMStoSenders()\"\r\n                    class=\"btn btn-primary\">Send</button>\r\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"closeSMSInfoModel()\">Cancel</button>\r\n            </div>\r\n\r\n        </div>\r\n        <!-- /.modal-content -->\r\n    </div>\r\n    <!-- /.modal-dialog -->\r\n</div>\r\n<!-- /.modal -->\r\n\r\n<footer class=\"footer kdark\">\r\n    <span class=\"text-left\">\r\n        &copy; 2019 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1066:
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n\r\n<!-- Main content -->\r\n<main class=\"main kdark full-main\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\">\r\n            <a [routerLink]=\"['../../home']\">Home</a>\r\n        </li>\r\n        <li class=\"breadcrumb-item active\">\r\n            <span>Provider Reports</span>\r\n        </li>\r\n        <!-- Breadcrumb Menu-->\r\n    </ol>\r\n\r\n    <div class=\"container-fluid\">\r\n        <div class=\"animated fadeIn mb-2 charts-container\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6\">\r\n                    <div style=\"display:none\" class=\"card\">\r\n                        <div class=\"card-header\">\r\n                            Cluster of Providers\r\n                        </div>\r\n                        <div class=\"card-block p-0\">\r\n                            <div class=\"chart-wrapper\">\r\n                                <div class=\"ap-light-blue-bg\">\r\n                                    <nvd3 [options]=\"scatterOptions\" [data]=\"scatterData\"></nvd3>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <img src=\"assets/img/Provider Reports 11.png\">\r\n                </div>\r\n\r\n                <div class=\"col-sm-6\">\r\n                    <div style=\"display:none\" class=\"card\">\r\n                        <div class=\"card-header\">\r\n                            Amount Paid vs PD CPT Difference Count\r\n                        </div>\r\n                        <div class=\"card-block p-0\">\r\n                            <div class=\"chart-wrapper\">\r\n                                <div class=\"ap-light-blue-bg\">\r\n                                    <nvd3 [options]=\"boxPlotOptions\" [data]=\"boxPlotData\"></nvd3>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <img src=\"assets/img/Provider Reports 22.png\">\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n\r\n\r\n<footer class=\"footer kdark full-footer\">\r\n    <span class=\"text-left\">\r\n        &copy; 2019 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1067:
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar sidebar-kdark\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Provider Name</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group1\" name=\"input2-group1\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPN || searchedPN.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPN=''\" *ngIf=\"searchedPN && searchedPN.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.PARTY_FIRST_NAME | searchFacets:searchedPN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 1)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.count + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Provider Type</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPS\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPS || searchedPS.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPS=''\" *ngIf=\"searchedPS && searchedPS.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.PROVIDER_SPECIALTY | searchFacets:searchedPS\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 2)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.count + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Zip</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedZC\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedZC || searchedZC.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedZC=''\" *ngIf=\"searchedZC && searchedZC.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.ZIP_CODE | searchFacets:searchedZC\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 3)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.count + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">TIN</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedTIN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedTIN || searchedTIN.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedTIN=''\" *ngIf=\"searchedTIN && searchedTIN.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.TAX_ID_NUMBER | searchFacets:searchedTIN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 4)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.count + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main kdark\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\">\r\n            <a [routerLink]=\"['../home']\">Home</a>\r\n        </li>\r\n        <li class=\"breadcrumb-item active\">\r\n            <span>Provider dashboard</span>\r\n        </li>\r\n        <!-- Breadcrumb Menu-->\r\n        <li class=\"breadcrumb-menu\">\r\n            <div class=\"btn-group\" role=\"group\" aria-label=\"Button group with nested dropdown\" *ngIf=\"rersProviderSummary && rersProviderSummary.length>0\">\r\n                <a class=\"btn btn-clear\" title=\"Download JSON\" [href]=\"downloadJsonHref\" [download]=\"rersProviderSummary[0].PARTY_FIRST_NAME + '.json'\">\r\n                    <i class=\"icon-cloud-download\"></i> Download JSON\r\n                </a>\r\n                <a class=\"btn btn-clear\" title=\"Download CSV\" href=\"javascript:void(0)\" (click)=\"exportCSV()\">\r\n                    <i class=\"icon-cloud-download\"></i> Download\r\n                </a>\r\n            </div>\r\n        </li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"m-t-2\">\r\n                <!--<h3 class=\"card-header\">ProviderSummary</h3>-->\r\n                <div class=\"row\">\r\n                    <div class=\"form-group mb-0 col-md-12\">\r\n                        <div class=\"input-group\">\r\n                            <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search ProviderSummary\" [(ngModel)]=\"searchProviderSummaryText\"\r\n                                (keyup.enter)=\"searchProviderSummary()\">\r\n                            <span class=\"input-group-btn\">\r\n                                <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchProviderSummary()\">\r\n                                    <i class=\"fa fa-search\"></i> Search</button>\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xs-12 p-0 pt-1 text-xs-right page-info\" [hidden]=\"true\">\r\n                    <span *ngIf=\"totalRecords>0\" [textContent]=\"(pageNumber*recordsToFetch-recordsToFetch+1) + ' to ' + (pageNumber*recordsToFetch-recordsToFetch+rersProviderSummary.length) + ' of ' + totalRecords + ' items'\"></span>\r\n                </div>\r\n                <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                    <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.PARTY_FIRST_NAME | selectedFacets\"\r\n                        (click)=\"removeSelectedFacet(objFacet, 1)\">\r\n                        <i class=\"fa fa-close\"></i>\r\n                        <span [textContent]=\"objFacet.name==''?'Empty Provider Name':objFacet.name\"></span>\r\n                    </button>\r\n                    <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.PROVIDER_SPECIALTY | selectedFacets\"\r\n                        (click)=\"removeSelectedFacet(objFacet, 2)\">\r\n                        <i class=\"fa fa-close\"></i>\r\n                        <span [textContent]=\"objFacet.name==''?'Empty Provider Type':objFacet.name\"></span>\r\n                    </button>\r\n                    <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.ZIP_CODE | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 3)\">\r\n                        <i class=\"fa fa-close\"></i>\r\n                        <span [textContent]=\"objFacet.name==''?'Empty Zip':objFacet.name\"></span>\r\n                    </button>\r\n                    <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.TAX_ID_NUMBER | selectedFacets\"\r\n                        (click)=\"removeSelectedFacet(objFacet, 4)\">\r\n                        <i class=\"fa fa-close\"></i>\r\n                        <span [textContent]=\"objFacet.name==''?'Empty TIN':objFacet.name\"></span>\r\n                    </button>\r\n                </div>\r\n                <div class=\"col-xs-12 p-0 text-lg-center\" *ngIf=\"foundRecord==false\">\r\n                    <h4>No result found.</h4>\r\n                </div>\r\n            </div>\r\n            <div class=\"row mt-1\">\r\n                <!---->\r\n                <div class=\"col-xs-12\" [hidden]=\"totalRecords==0\">\r\n                    <!--<div class=\"spinner\" [ngClass]=\"{ 'hidden': !loading }\"></div>-->\r\n                    <div class=\"pagination-container\" [hidden]=\"totalRecords<=1\">\r\n                        <!--<pagination-controls [hidden]=\"true\" autoHide=\"true\" (pageChange)=\"searchProviderSummaryRecordsWithFilters($event)\" id=\"server\"></pagination-controls>-->\r\n                        <!-- [class.disabled]=\"fetchLimit<=1\"-->\r\n                        <ul class=\"pagination\">\r\n                            <li class=\"page-item pagination-previous\" [class.disabled]=\"pageNumber<=1\">\r\n                                <a class=\"page-link\" (click)=\"searchProviderSummaryRecordsWithFilters(pageNumber-1)\">Previous</a>\r\n                                <!--<a class=\"page-link\" style=\"width: 100px\" (click)=\"fetchLimit=fetchLimit-1;getProviderRecordsWithFilters()\">Previous</a>-->\r\n                            </li>\r\n                            <li class=\"page-item\">\r\n                                <span *ngIf=\"totalRecords>0\" [textContent]=\"'Record ' + pageNumber + ' of ' + totalRecords\"></span>\r\n                            </li>\r\n                            <!--[class.disabled]=\"totalRecords==fetchLimit\"-->\r\n                            <li class=\"page-item pagination-next\" [class.disabled]=\"pageNumber==(totalRecords/recordsToFetch).toFixed(0)\">\r\n                                <a class=\"page-link\" (click)=\"searchProviderSummaryRecordsWithFilters(pageNumber+1)\">Next</a>\r\n                                <!--<a class=\"page-link\" style=\"width: 100px\" (click)=\"fetchLimit=fetchLimit+1;getProviderRecordsWithFilters()\">Next</a>-->\r\n                            </li>\r\n                        </ul>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!--asyncProviderSummary-->\r\n        <div *ngFor=\"let objProviderSummary of asyncProviderSummary | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\">\r\n            <div class=\"animated fadeIn provider-dash\">\r\n                <div class=\"row mb-2\">\r\n                    <!-- tabview start -->\r\n                    <div *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\">\r\n                            <div class=\"col-sm-6 col-md-12\">\r\n                                <div class=\"card-accent-primary mb-0\">\r\n                                    <div class=\"card-header blue-hedear font-large\">\r\n                                        <div class=\"row m-0\">\r\n\r\n                                            <div class=\"\">\r\n                                                <div class=\"pull-left\">\r\n                                                    <button class=\"btn btn-link focus-outline\" (click)=\"objProviderSummary.isOpen=!objProviderSummary.isOpen\" type=\"button\">\r\n                                                        <i class=\"icon-arrow-down\" [hidden]=\"!objProviderSummary.isOpen\"></i>\r\n                                                        <i class=\"icon-arrow-right\" [hidden]=\"objProviderSummary.isOpen\"></i>\r\n                                                    </button>\r\n                                                    <b class=\"card-inverse\">Provider NPI:</b>\r\n                                                    <span  class=\"card-inverse\" [textContent]=\"'' + objProviderSummary.PROVIDER_INDIVIDUAL_NPI\">\r\n                                                        8743535559 </span> |\r\n                                                    <b  class=\"card-inverse\">Provider Name:</b> <span  class=\"card-inverse\"> {{objProviderSummary.PARTY_FIRST_NAME + ' ' + objProviderSummary.PARTY_LAST_NAME}} </span>\r\n                                                    |\r\n                                                    <b  class=\"card-inverse\">License Number: </b><span class=\"card-inverse\">{{objProviderSummary.PROFESSIONAL_LICENSE_NUMBER}}</span>\r\n                                                    <b  class=\"card-inverse\">{{objProviderSummary.PROVIDER_LICENSE_STATUS}}</b>\r\n                                                    <!-- <b [textContent]=\"'License Number: : ' + objProviderSummary.PROFESSIONAL_LICENSE_NUMBER + ' STATUS : ' + objProviderSummary.PROVIDER_LICENSE_STATUS\">LICENCE NO : 34432 STATUS : VALID </b> -->\r\n                                                    <!-- <span class=\"data-title\">Hello :</span>\r\n                                                    <div class=\"pull-left\"> hello 1</div> -->\r\n                                                </div>\r\n\r\n\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div id=\"slide-down-de\" [hidden]=\"!objProviderSummary.isOpen\">\r\n                                        <div *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\">\r\n                                            <div class=\"col-sm-6 col-md-4\">\r\n                                                <div class=\"card card-accent-primary mb-0\">\r\n                                                    <div class=\"card-header\">\r\n                                                        <span [textContent]=\"(objProviderSummary.PARTY_FIRST_NAME) | uppercase\">DIAZ</span>\r\n                                                        <!-- <button class=\"btn btn-primary float-xs-right\" type=\"button\" *ngIf=\"!caseOpened\" (click)=\"generateCase(objProviderSummary)\">Open Case</button> -->\r\n                                                        <span class=\"tag tag-success float-xs-right case-status\" *ngIf=\"caseOpened\" [textContent]=\"caseStatus\" (click)=\"openCaseDetailInCaseManagement()\"></span>\r\n                                                    </div>\r\n                                                    <div class=\"card-block\">\r\n                                                            <div class=\"text-lg-center\">\r\n                                                                <img    [src]=\"objProviderSummary.PROFILE_PICTURE\" class=\"mt-q md-card-image round-element provideImage\"\r\n                                                                    alt=\"Teresa\">\r\n                                                                <div class=\"mt-1\"><b>{{objProviderSummary.PARTY_FIRST_NAME + ' ' + objProviderSummary.PARTY_LAST_NAME}}</b>\r\n                                                                     <!-- <a *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\"\r\n                                                                    style=\"text-decoration: none;cursor:pointer\" href=\"javascript:;\"\r\n                                                                    style=\"color:#fff;text-decoration: none;\" class=\" pull-right\"\r\n                                                                    (click)=\"ongetmoreinfo('subjectSection')\">AKAInfo...</a>  -->\r\n                                                                </div>\r\n\r\n                                                            </div>\r\n                                                            <!-- <div class=\"card mt-1\"> -->\r\n                                                            <div class=\"mt-1\">\r\n                                                                <div class=\"\">\r\n                                                                    <span [textContent]=\"'Speciality: Chiropractor'\">Speciality: </span>\r\n                                                                </div>\r\n                                                            </div>\r\n                                                            <div class=\"mt-1\">\r\n                                                                <div class=\"\">\r\n                                                                    <span [textContent]=\"'NICB Alerts: ' + objProviderSummary.NICB_ALERTS\">NICB Alerts: </span>\r\n                                                                </div>\r\n                                                            </div>\r\n                                                            <div class=\"row lg-d-flex mt-1\">\r\n                                                                <div class=\"col-sm-6 col-md-6\">\r\n                                                                    <div class=\"card mb-0\">\r\n                                                                        <div class=\"card-block p-q\">\r\n                                                                            <a target=\"_blank\" [href]=\"objProviderSummary.DNB_REPORT_LINK\">\r\n                                                                                <img src=\"/assets/img/pdf.png\">\r\n                                                                            </a> D&B report\r\n                                                                        </div>\r\n                                                                    </div>\r\n                                                                </div>\r\n                                                                <br/>\r\n                                                                <div class=\"col-sm-6 col-md-6\">\r\n                                                                    <div class=\"card mb-0\">\r\n                                                                        <div class=\"card-block p-q\">\r\n                                                                            <a target=\"_blank\" [href]=\"objProviderSummary.LEXIS_NEXIS_REPORT\">\r\n                                                                                <img src=\"/assets/img/pdf.png\">\r\n                                                                            </a> Lexis report\r\n                                                                        </div>\r\n                                                                    </div>\r\n                                                                </div>\r\n                                                            </div>\r\n                                                        \r\n\r\n\r\n                                                       \r\n                                                        \r\n\r\n\r\n                                                    </div>\r\n                                                </div>\r\n                                                <!-- <div class=\"card card-accent-primary mb-0\">\r\n                                                    <div class=\"card-header p-q\">\r\n                                                         <b [textContent]=\"'LICENCE NO : ' + objProviderSummary.PROFESSIONAL_LICENSE_NUMBER + ' STATUS : ' + objProviderSummary.PROVIDER_LICENSE_STATUS\">LICENCE NO : 34432 STATUS : VALID </b> \r\n                                                        <a *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\" style=\"color:#fff;text-decoration: none;cursor:pointer\" href=\"javascript:;\"\r\n                                                            class=\"\" (click)=\"ongetmoreinfo('LicenseSection')\">LICENCE Info...</a>\r\n                                                    </div>\r\n                                                </div> -->\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-sm-6 col-md-8\" (click)=\"$event.preventDefault()\">\r\n                                            <tabset class=\"kdark provider-custom-tab\">\r\n                                                <tab style=\"border-top:0px;\" heading=\"Aliases\">\r\n                                                    <div style=\"width:100%;max-height:300px;overflow:auto;\">\r\n                                                        <table class=\"table ktable\">\r\n                                                            <thead>\r\n                                                                <tr>\r\n                                                                    <th>#</th>\r\n                                                                    <th>First Name</th>\r\n                                                                    <th>Middle Name</th>\r\n                                                                    <th>Last Name</th>\r\n                                                                    <th>Birth Date</th>\r\n                                                                    <th>Person SSN</th>\r\n                                                                </tr>\r\n                                                            </thead>\r\n                                                            <tr *ngFor=\"let subject of responcesubjectInfo; let i=index\">\r\n                                                                <td>{{i+1}}</td>\r\n                                                                <td>{{subject.PersonName.FirstName}}</td>\r\n                                                                <td>{{subject.PersonName.MiddleName}}</td>\r\n                                                                <td>{{subject.PersonName.LastName}}</td>\r\n                                                                <td>\r\n                                                                    <span *ngIf=\"subject.PersonProfile.PersonBirthDate\">{{subject.PersonProfile.PersonBirthDate}}</span>\r\n                                                                </td>\r\n                                                                <td>\r\n                                                                    <span *ngIf=\"subject.PersonSSN\">{{subject.PersonSSN.SSN}}</span>\r\n                                                                </td>\r\n                                                            </tr>\r\n\r\n                                                        </table>\r\n                                                    </div>\r\n                                                </tab>\r\n                                                <tab style=\"border-top:0px;\" heading=\"Multiple SSN\">\r\n                                                    <div style=\"width:100%;max-height:300px;overflow:auto;\">\r\n                                                        <table class=\"table ktable\">\r\n                                                            <thead>\r\n                                                                <tr>\r\n                                                                    <th width=\"5%\">#</th>\r\n                                                                    <th width=\"20%\">SSN</th>\r\n                                                                    <th width=\"25%\">Address</th>\r\n                                                                    <th width=\"25%\">Alert Indicator</th>\r\n                                                                    <th width=\"25%\">Alert Description</th>\r\n                                                                </tr>\r\n                                                            </thead>\r\n                                                            <tr *ngFor=\"let SSNAddressFraud of responceSSNAddressFraudSectionInfo; let i=index\">\r\n                                                                <td>{{i+1}}</td>\r\n                                                                <td>{{SSNAddressFraud[\"SSNAlert\"][\"SSN\"][\"SSN\"]}}</td>\r\n                                                                <td>{{SSNAddressFraud.AddressAlert.Address.Street}}\r\n                                                                    {{SSNAddressFraud.AddressAlert.Address.City}}\r\n                                                                    {{SSNAddressFraud.AddressAlert.Address.State}}\r\n                                                                    {{SSNAddressFraud.AddressAlert.Address.ZipCode}}\r\n\r\n                                                                </td>\r\n                                                                <td>{{SSNAddressFraud.SSNAlert.AlertIndicator}}</td>\r\n                                                                <td>{{SSNAddressFraud.SSNAlert.AlertDescription}}</td>\r\n\r\n                                                            </tr>\r\n\r\n                                                        </table>\r\n                                                    </div>\r\n                                                </tab>\r\n                                                <tab style=\"border-top:0px;\" heading=\"Address\">\r\n                                                    <div style=\"width:100%;max-height:300px;overflow:auto;\">\r\n                                                        <table class=\"table ktable\">\r\n                                                            <thead>\r\n                                                                <tr>\r\n                                                                    <th width=\"5%\">#</th>\r\n                                                                    <th width=\"10%\">First Name</th>\r\n                                                                    <th width=\"10%\">Last Name</th>\r\n                                                                    <th width=\"55%\">Address</th>\r\n                                                                    <th width=\"20%\">DUNS Number</th>\r\n                                                                </tr>\r\n                                                            </thead>\r\n                                                            <tr *ngFor=\"let bsubjectaddress of responceSubjectAddressInfo; let i=index\">\r\n                                                                <td>{{i+1}}</td>\r\n                                                                <td>{{bsubjectaddress.DunBradstreetRecord.PersonInfo.PersonName.FirstName}}</td>\r\n                                                                <td>{{bsubjectaddress.DunBradstreetRecord.PersonInfo.PersonName.LastName}}</td>\r\n                                                                <td>{{bsubjectaddress.SubjectAddress.Street}} {{bsubjectaddress.SubjectAddress.City}}\r\n                                                                    {{bsubjectaddress.SubjectAddress.State}} {{bsubjectaddress.SubjectAddress.ZipCode}}\r\n                                                                    {{bsubjectaddress.SubjectAddress.County}}\r\n                                                                </td>\r\n                                                                <td>\r\n                                                                    <span *ngIf=\"bsubjectaddress.DunBradstreetRecord.BusinessInfo\">{{bsubjectaddress.DunBradstreetRecord.BusinessInfo.DUNSNumber}}</span>\r\n                                                                </td>\r\n\r\n                                                            </tr>\r\n\r\n                                                        </table>\r\n                                                    </div>\r\n                                                </tab>\r\n                                                <tab style=\"border-top:0px;\" heading=\"Red Flag Alert\">\r\n                                                    <!-- <div class=\"col-sm-6 col-md-12 mb-2\">\r\n                                                        <div class=\"br-5\">\r\n                                                            <div class=\"card card-accent-danger mb-0\">\r\n                                                                 <div class=\"card-header font-large\">\r\n                                                                            Red Flag Alert\r\n                                                                        </div> \r\n                                                                \r\n                                                            </div>\r\n                                                        </div>\r\n                                                    </div> -->\r\n                                                    <div class=\"card-block p-0 redflagalert-container\">\r\n                                                        <div class=\"col-md-6 p-1\" *ngFor=\"let objRedFlagAlert of resRedFlagAlerts\">\r\n                                                            <span [textContent]=\"objRedFlagAlert.AlertType + ' (' + objRedFlagAlert.AlertValue + ')'\">Denied Bills (114)</span>\r\n                                                            <button class=\"btn btn-warning pull-right\" type=\"button\" (click)=\"viewRedFlagAlertDetails(objProviderSummary.TAX_ID_NUMBER, objRedFlagAlert.AlertType)\">View Details</button>\r\n                                                        </div>\r\n                                                        <!-- <div class=\"col-md-6 p-1\">\r\n                                                                                <span> Quick Analysis Flag</span>\r\n                                                                                <button class=\"btn btn-warning pull-right\" type=\"button\" (click)=\"ongetmoreinfo('QuickAnalysisFlagSection')\">View Details</button>\r\n                                                                            </div> -->\r\n                                                        <div class=\"col-md-6 p-1\">\r\n                                                            <span>Criminal Records</span>\r\n                                                            <button class=\"btn btn-warning pull-right\" type=\"button\" (click)=\"ongetmoreinfo('CriminalSection')\">View Details</button>\r\n                                                        </div>\r\n                                                    </div>\r\n                                                </tab>\r\n                                                <tab style=\"border-top:0px;\" heading=\"Quick Analysis Flag\">\r\n                                                    <div *ngIf=\"responceQuickAnalysisFlagSectionInfo\" style=\"width:100%;max-height:300px;overflow:auto;\">\r\n                                                        <table class=\"table ktable\">\r\n                                                            <thead>\r\n                                                                <tr>\r\n                                                                    <th>#</th>\r\n                                                                    <th>Quick Analysis Flag Name</th>\r\n                                                                    <th>Risk Flag</th>\r\n                                                                </tr>\r\n                                                            </thead>\r\n                                                            <tr *ngFor=\"let quickanalysis of responceQuickAnalysisFlagSectionInfo; let i=index\">\r\n\r\n                                                                <td>{{i+1}}</td>\r\n                                                                <td>{{quickanalysis.riskname}}</td>\r\n                                                                <td>{{quickanalysis.riskvalue}}</td>\r\n\r\n                                                            </tr>\r\n\r\n                                                        </table>\r\n                                                    </div>\r\n                                                </tab>\r\n                                                <tab style=\"border-top:0px;\" heading=\"LICENCE Info\">\r\n                                                    <div style=\"width:100%;max-height:300px;overflow:auto;\">\r\n                                                        <table class=\"table ktable\">\r\n                                                            <thead>\r\n                                                                <tr>\r\n                                                                    <th>#</th>\r\n                                                                    <th>Full Name</th>\r\n                                                                    <th>Address</th>\r\n                                                                    <th>License Number</th>\r\n                                                                    <th>License State</th>\r\n                                                                    <th>Licensing Agency</th>\r\n                                                                </tr>\r\n                                                            </thead>\r\n                                                            <tr *ngFor=\"let professionalLicense of responceLicenseSectionInfo; let i=index\">\r\n                                                                <td>{{i+1}}</td>\r\n                                                                <!-- <td>{{professionalLicense.PersonInfo.PersonName.FirstName}}</td>\r\n                    <td>{{professionalLicense.PersonInfo.PersonName.LastName}}</td> -->\r\n                                                                <td>{{professionalLicense.PersonInfo.PersonName.FullName}}</td>\r\n                                                                <td>{{professionalLicense.PersonalAddresses.Address.Street}}\r\n                                                                    {{professionalLicense.PersonalAddresses.Address.City}}\r\n                                                                    {{professionalLicense.PersonalAddresses.Address.State}}\r\n                                                                    {{professionalLicense.PersonalAddresses.Address.ZipCode}}\r\n                                                                    {{professionalLicense.PersonalAddresses.Address.County}}\r\n                                                                </td>\r\n                                                                <td>{{professionalLicense.ProLicenseInfo.CertificationInfo.LicenseNumber}}</td>\r\n                                                                <td>{{professionalLicense.ProLicenseInfo.LicenseState}}</td>\r\n                                                                <td>{{professionalLicense.ProLicenseInfo.LicensingAgency}}</td>\r\n                                                            </tr>\r\n\r\n                                                        </table>\r\n                                                    </div>\r\n                                                </tab>\r\n                                            </tabset>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                    </div>\r\n                    <!-- tabview end -->\r\n                    <div *ngIf=\"objProviderSummary.PARTY_FIRST_NAME != 'Bryan'\" class=\"col-sm-6 col-md-4\">\r\n                        <div class=\"br-5 mb-2\">\r\n                            <div class=\"card card-accent-primary mb-0\">\r\n                                <div class=\"card-header\">\r\n                                    <span [textContent]=\"(objProviderSummary.PARTY_FIRST_NAME) | uppercase\">DIAZ</span>\r\n                                    <button class=\"btn btn-primary float-xs-right\" type=\"button\" *ngIf=\"!caseOpened\" (click)=\"generateCase(objProviderSummary)\" style=\"\r\n                                    visibility: hidden;\">Open Case</button>\r\n                                    <span class=\"tag tag-success float-xs-right case-status\" *ngIf=\"caseOpened\" [textContent]=\"caseStatus\" (click)=\"openCaseDetailInCaseManagement()\"></span>\r\n                                </div>\r\n                                <div class=\"card-block\">\r\n                                    <div class=\"text-lg-center\">\r\n                                        <img [src]=\"objProviderSummary.PROFILE_PICTURE\" class=\"md-card-image round-element provideImage\" alt=\"Teresa\">\r\n                                        <div class=\"mt-1\">\r\n                                            <b>{{objProviderSummary.PARTY_FIRST_NAME + ' ' + objProviderSummary.PARTY_LAST_NAME}}</b>\r\n                                            <a *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\" style=\"text-decoration: none;cursor:pointer\" href=\"javascript:;\"\r\n                                                style=\"color:#fff;text-decoration: none;\" class=\" pull-right\" (click)=\"ongetmoreinfo('subjectSection')\">AKAInfo...</a>\r\n                                        </div>\r\n\r\n                                        <!-- <br *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\" /> -->\r\n                                    </div>\r\n\r\n                                    <div class=\"mt-1\">\r\n                                        <div class=\"\">\r\n                                            <span [textContent]=\"'Speciality: Chiropractor'\">Speciality: </span>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"mt-1\">\r\n                                        <div class=\"\">\r\n                                            <span [textContent]=\"'NICB Alerts: ' + objProviderSummary.NICB_ALERTS\">NICB Alerts: </span>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"row mt-1\">\r\n                                        <div class=\"col-sm-6 col-md-6\">\r\n                                            <div class=\"card mb-0\">\r\n                                                <div class=\"card-block p-q\">\r\n                                                    <a target=\"_blank\" [href]=\"objProviderSummary.DNB_REPORT_LINK\">\r\n                                                        <img src=\"/assets/img/pdf.png\">\r\n                                                    </a> D&B report\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-sm-6 col-md-6\">\r\n                                            <div class=\"card mb-0\">\r\n                                                <div class=\"card-block p-q\">\r\n                                                    <a target=\"_blank\" [href]=\"objProviderSummary.LEXIS_NEXIS_REPORT\">\r\n                                                        <img src=\"/assets/img/pdf.png\">\r\n                                                    </a> Lexis report\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"card card-accent-primary mb-0\">\r\n                                <div class=\"card-header p-1\">\r\n                                    <b [textContent]=\"'ADDRESS : ' + objProviderSummary.STREET_ADDRESS + ' ' + objProviderSummary.CITY + ' ' + objProviderSummary.STATE + ' ' + objProviderSummary.ZIP_CODE\">52-Ohio CO 86544 </b>\r\n                                    <a *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\" style=\"color:#fff;text-decoration: none;cursor:pointer\" href=\"javascript:;\"\r\n                                        class=\" pull-right\" (click)=\"ongetmoreinfo('BusinessAtSubjectAddressSection')\">More Info...</a>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"card card-accent-primary mb-0\">\r\n                                <div class=\"card-header p-1\">\r\n                                    <b [textContent]=\"'TIN : ' + objProviderSummary.TAX_ID_NUMBER\">TIN : 264647617 </b>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"card card-accent-primary mb-0\">\r\n                                <div class=\"card-header p-1\">\r\n                                    <b [textContent]=\"'LICENCE NO : ' + objProviderSummary.PROFESSIONAL_LICENSE_NUMBER + ' STATUS : ' + objProviderSummary.PROVIDER_LICENSE_STATUS\">LICENCE NO : 34432 STATUS : VALID </b>\r\n                                    <a *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\" style=\"color:#fff;text-decoration: none;cursor:pointer\" href=\"javascript:;\"\r\n                                        class=\" pull-right\" (click)=\"ongetmoreinfo('LicenseSection')\">More Info...</a>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"card card-accent-primary mb-0\" *ngIf=\"objProviderSummary.PROVIDER_INDIVIDUAL_NPI\">\r\n                                <div class=\"card-header p-1\">\r\n                                    <b [textContent]=\"'NPI : ' + objProviderSummary.PROVIDER_INDIVIDUAL_NPI\">NPI : 8743535559 </b>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"card card-accent-primary mb-0\" *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\">\r\n                                <div class=\"card-header p-1\">\r\n                                    <b [textContent]=\"'SSN and Fraud alerts'\">SSN and Fraud alerts </b>\r\n                                    <a *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\" style=\"color:#fff;text-decoration: none;cursor:pointer\" href=\"javascript:;\"\r\n                                        class=\" pull-right\" (click)=\"ongetmoreinfo('SSNAddressFraudSection')\">More Info...</a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div *ngIf=\"objProviderSummary.PARTY_FIRST_NAME != 'Bryan'\" class=\"col-sm-6 col-md-8\">\r\n                        <div class=\"br-5 mb-2\">\r\n                            <div class=\"card card-accent-success mb-0\">\r\n                                <div class=\"card-header font-large card-panel-text\">\r\n                                    Bill Statistics\r\n                                </div>\r\n                                <div class=\"card-block\">\r\n                                    <table class=\"table ktable\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th>YEAR</th>\r\n                                                <th>BILL COUNT</th>\r\n                                                <th>BILLED AMT</th>\r\n                                                <th>PAID AMT</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n                                            <tr *ngFor=\"let objBillStatistics of resBillStatistics\">\r\n                                                <td [textContent]=\"objBillStatistics.BILL_YEAR\">2011</td>\r\n                                                <td [textContent]=\"objBillStatistics.TOTAL_MEDICAL_BILLS\">3400</td>\r\n                                                <td [textContent]=\"objBillStatistics.TOTAL_BILLED_AMOUNT\">$56,853,722.00</td>\r\n                                                <td [textContent]=\"objBillStatistics.TOTAL_PAID_AMOUNT\">$56,853,722.00</td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div *ngIf=\"objProviderSummary.PARTY_FIRST_NAME != 'Bryan'\" class=\"col-sm-6 col-md-8 mb-2\">\r\n                        <div class=\"br-5\">\r\n                            <div class=\"card card-accent-danger mb-0\">\r\n                                <div class=\"card-header font-large\">\r\n                                    Red Flag Alert\r\n                                </div>\r\n                                <div class=\"card-block p-0 redflagalert-container\">\r\n                                    <div class=\"col-md-6 p-1\" *ngFor=\"let objRedFlagAlert of resRedFlagAlerts\">\r\n                                        <span [textContent]=\"objRedFlagAlert.AlertType + ' (' + objRedFlagAlert.AlertValue + ')'\">Denied Bills (114)</span>\r\n                                        <button class=\"btn btn-warning pull-right\" type=\"button\" (click)=\"viewRedFlagAlertDetails(objProviderSummary.TAX_ID_NUMBER, objRedFlagAlert.AlertType)\">View Details</button>\r\n                                    </div>\r\n                                    <div *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\" class=\"col-md-6 p-1\">\r\n                                        <span> Quick Analysis Flag</span>\r\n                                        <button class=\"btn btn-warning pull-right\" type=\"button\" (click)=\"ongetmoreinfo('QuickAnalysisFlagSection')\">View Details</button>\r\n                                    </div>\r\n                                    <div *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\" class=\"col-md-6 p-1\">\r\n                                        <span>Criminal Records</span>\r\n                                        <button class=\"btn btn-warning pull-right\" type=\"button\" (click)=\"ongetmoreinfo('CriminalSection')\">View Details</button>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div *ngIf=\"objProviderSummary.PARTY_FIRST_NAME == 'Bryan'\" class=\"col-sm-6 col-md-12 mb-2\">\r\n                        <div class=\"br-5 mb-2\">\r\n                            <div class=\"card card-accent-success mb-0\">\r\n                                <div class=\"card-header font-large\">\r\n                                    Bill Statistics\r\n                                </div>\r\n                                <div class=\"card-block\">\r\n                                    <table class=\"table ktable\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th>YEAR</th>\r\n                                                <th>BILL COUNT</th>\r\n                                                <th>BILLED AMT</th>\r\n                                                <th>PAID AMT</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n                                            <tr *ngFor=\"let objBillStatistics of resBillStatistics\">\r\n                                                <td [textContent]=\"objBillStatistics.BILL_YEAR\">2011</td>\r\n                                                <td [textContent]=\"objBillStatistics.TOTAL_MEDICAL_BILLS\">3400</td>\r\n                                                <td [textContent]=\"objBillStatistics.TOTAL_BILLED_AMOUNT\">$56,853,722.00</td>\r\n                                                <td [textContent]=\"objBillStatistics.TOTAL_PAID_AMOUNT\">$56,853,722.00</td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-6 col-md-12 mb-2\">\r\n                        <div class=\"br-5\">\r\n                            <div class=\"card card-accent-warning mb-0\">\r\n                                <div class=\"card-header font-large\">\r\n                                    Chiropractic Alerts\r\n                                </div>\r\n                                <div class=\"card-block\">\r\n                                    <div class=\"col-xs-12 p-0\" *ngFor=\"let objChiropracticAlert of resChiropracticAlerts\">\r\n                                        <div class=\"mv-2\">\r\n                                            <b [textContent]=\"objChiropracticAlert.dataTitle | uppercase\">Twitter Trends(200)</b>\r\n                                        </div>\r\n                                        <div class=\"mb-q\" *ngIf=\"objChiropracticAlert.action\">\r\n                                            <b [textContent]=\"objChiropracticAlert.action\">Latest tweet</b>\r\n                                        </div>\r\n                                        <div class=\"mb-2\">\r\n                                            <small [textContent]=\"objChiropracticAlert.nameAndCity\">January 26 2016</small>\r\n                                        </div>\r\n                                        <div class=\"mb-q\" *ngIf=\"objChiropracticAlert.allegedViolations\">\r\n                                            \"\r\n                                            <span [textContent]=\"objChiropracticAlert.allegedViolations\"></span>\"\r\n                                        </div>\r\n                                        <div class=\"mb-2\" *ngIf=\"objChiropracticAlert.dateFiled\">\r\n                                            <small [textContent]=\"objChiropracticAlert.dateFiled | date : 'MMMM dd yyyy'\">January 26 2016</small>\r\n                                        </div>\r\n                                        <div class=\"mb-q\" *ngIf=\"objChiropracticAlert.violations\">\r\n                                            \"\r\n                                            <span [textContent]=\"objChiropracticAlert.violations\"></span>\"\r\n                                        </div>\r\n                                        <div class=\"mb-2\" *ngIf=\"objChiropracticAlert.effectiveDate\">\r\n                                            <small [textContent]=\"objChiropracticAlert.effectiveDate | date : 'MMMM dd yyyy'\">January 26 2016</small>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-6 col-md-12 mb-2\" *ngIf=\"this.resSocial && (this.resSocial.twitter || this.resSocial.instagram)\">\r\n                        <div class=\"br-5\">\r\n                            <div class=\"card card-accent-info mb-0\">\r\n                                <div class=\"card-header font-large\">\r\n                                    Social\r\n                                </div>\r\n                                <div class=\"card-block\">\r\n                                    <div class=\"col-md-6 col-xs-12 pl-0\" [class.col-lg-4]=\"this.resSocial.rssFeed && this.resSocial.rssFeed.length>=(pageNumber*4)\"\r\n                                        *ngIf=\"this.resSocial.twitter && this.resSocial.twitter.length>(pageNumber*3)\">\r\n                                        <div class=\"mb-q\">\r\n                                            <b style=\"font-size: 1rem;\">Twitter</b>\r\n                                        </div>\r\n                                        <div *ngFor=\"let objSocial of this.resSocial.twitter.slice((pageNumber*3)-3, pageNumber*3)\">\r\n                                            <div>\r\n                                                <!-- <small [textContent]=\"objSocial.created_at_std | dateutc | date : 'MMMM dd yyyy hh:mm a'\"></small> -->\r\n                                                <small [textContent]=\"objSocial.created_at_std | dateutc | date : 'longDate'\"></small>\r\n                                                <small [textContent]=\"objSocial.created_at_std | dateutc | date : 'shortTime'\"></small>\r\n                                            </div>\r\n                                            <div class=\"mb-1\" *ngIf=\"objSocial.content\" [innerHTML]=\"objSocial.content.content_body\">\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-md-6 col-xs-12 pl-0\" [class.col-lg-4]=\"this.resSocial.rssFeed && this.resSocial.rssFeed.length>=(pageNumber*4)\"\r\n                                        *ngIf=\"this.resSocial.instagram && this.resSocial.instagram.length>=(pageNumber*2)\">\r\n                                        <div class=\"mb-q\">\r\n                                            <b style=\"font-size: 1rem;\">Instagram</b>\r\n                                        </div>\r\n                                        <div *ngFor=\"let objSocial of this.resSocial.instagram.slice((pageNumber*2)-2, pageNumber*2)\">\r\n                                            <div>\r\n                                                <!-- <small [textContent]=\"objSocial.created_at_std | dateutc | date : 'MMMM dd yyyy hh:mm a'\"></small> -->\r\n                                                <small [textContent]=\"objSocial.created_at_std | dateutc | date : 'longDate'\"></small>\r\n                                                <small [textContent]=\"objSocial.created_at_std | dateutc | date : 'shortTime'\"></small>\r\n                                            </div>\r\n                                            <div class=\"mb-1\" *ngIf=\"objSocial.content\" [innerHTML]=\"objSocial.content.content_body\">\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-md-4 col-xs-12 pl-0\" *ngIf=\"this.resSocial.rssFeed && this.resSocial.rssFeed.length>=(pageNumber*4)\">\r\n                                        <div class=\"mb-q\">\r\n                                            <b style=\"font-size: 1rem;\">RSS</b>\r\n                                        </div>\r\n                                        <div *ngFor=\"let objSocial of this.resSocial.rssFeed.slice((pageNumber*4)-4, pageNumber*4)\">\r\n                                            <div>\r\n                                                <!-- <small [textContent]=\"objSocial.created_at_std | dateutc | date : 'MMMM dd yyyy hh:mm a'\"></small> -->\r\n                                                <small [textContent]=\"objSocial.created_at_std | dateutc | date : 'longDate'\"></small>\r\n                                                <small [textContent]=\"objSocial.created_at_std | dateutc | date : 'shortTime'\"></small>\r\n                                            </div>\r\n                                            <div class=\"mb-1\" *ngIf=\"objSocial.content\" [innerHTML]=\"objSocial.content.content_title? objSocial.content.content_title : objSocial.content.content_body\">\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-6 col-md-12 mb-2\" *ngIf=\"this.resSocial && !(this.resSocial.twitter || this.resSocial.instagram) && (this.resSocial.twitterHealthCareFraud || this.resSocial.twitterMedicalBillings)\">\r\n                        <div class=\"br-5\">\r\n                            <div class=\"card card-accent-info mb-0\">\r\n                                <div class=\"card-header font-large\">\r\n                                    Social\r\n                                    <button [routerLink]=\"['../social-networks/provider']\" [queryParams]=\"{ provider_firstname: objProviderSummary.PARTY_FIRST_NAME,provider_lastname:objProviderSummary.PARTY_LAST_NAME }\"\r\n                                        class=\"btn btn-primary float-xs-right\" type=\"button\">Open Social Graph</button>\r\n                                </div>\r\n                                <div class=\"card-block\">\r\n                                    <div class=\"col-xs-12 pl-0\">\r\n                                        <div class=\"mb-q\">\r\n                                            <b style=\"font-size: 1rem;\">Tweets</b>\r\n                                        </div>\r\n                                        <div *ngIf=\"this.resSocial.twitterHealthCareFraud && (this.resSocial.twitterHealthCareFraud.length>=(pageNumber*2))\">\r\n                                            <div *ngFor=\"let objSocial of this.resSocial.twitterHealthCareFraud.slice((pageNumber*2)-2, pageNumber*2)\">\r\n                                                <div class=\"mt-1\">\r\n                                                    <small [textContent]=\"objSocial.createdDate | date : 'MMMM dd yyyy hh:mm a'\"></small>\r\n                                                </div>\r\n                                                <div class=\"mb-1\" *ngIf=\"objSocial.text\" [innerHTML]=\"objSocial.text\">\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div *ngIf=\"this.resSocial.twitterMedicalBillings && this.resSocial.twitterMedicalBillings.length>=(pageNumber*2)\">\r\n                                            <div *ngFor=\"let objSocial of this.resSocial.twitterMedicalBillings.slice((pageNumber*2)-2, pageNumber*2)\">\r\n                                                <div class=\"mt-1\">\r\n                                                    <small [textContent]=\"objSocial.createdDate | date : 'MMMM dd yyyy hh:mm a'\"></small>\r\n                                                </div>\r\n                                                <div class=\"mb-1\" *ngIf=\"objSocial.text\" [innerHTML]=\"objSocial.text\">\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n    <!-- moreInformation model start -->\r\n    <modal #moreInfoModal>\r\n        <modal-header>\r\n            <h1>Bryan Aun</h1>\r\n        </modal-header>\r\n        <modal-content>\r\n            <div style=\"width:100%;height:400px;overflow:auto;\">\r\n                <table class=\"table\">\r\n                    <tr>\r\n                        <th>#</th>\r\n                        <th>FirstName</th>\r\n                        <th>MiddleName</th>\r\n                        <th>LastName</th>\r\n                        <th>BirthDate</th>\r\n                        <th>PersonSSN</th>\r\n                    </tr>\r\n                    <tr *ngFor=\"let subject of responcesubjectInfo; let i=index\">\r\n                        <td>{{i+1}}</td>\r\n                        <td>{{subject.PersonName.FirstName}}</td>\r\n                        <td>{{subject.PersonName.MiddleName}}</td>\r\n                        <td>{{subject.PersonName.LastName}}</td>\r\n                        <td>\r\n                            <span *ngIf=\"subject.PersonProfile.PersonBirthDate\">{{subject.PersonProfile.PersonBirthDate}}</span>\r\n                        </td>\r\n                        <td>\r\n                            <span *ngIf=\"subject.PersonSSN\">{{subject.PersonSSN.SSN}}</span>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n            </div>\r\n\r\n        </modal-content>\r\n        <!-- <modal-footer>\r\n            <button class=\"btn btn-primary\" (click)=\"moreInfoModal.close()\">close</button>\r\n        </modal-footer> -->\r\n    </modal>\r\n    <!-- moreInformation model end -->\r\n    <!-- BusinessAtSubjectAddressSection Model start -->\r\n    <modal #BusinessAtSubjectAddressSectionModal>\r\n        <modal-header>\r\n            <h1>Bryan Aun</h1>\r\n        </modal-header>\r\n        <modal-content>\r\n            <div style=\"width:100%;height:400px;overflow:auto;\">\r\n                <table class=\"table\">\r\n                    <tr>\r\n                        <th>#</th>\r\n                        <th>FirstName</th>\r\n                        <th>LastName</th>\r\n                        <th>Address</th>\r\n                        <th>DUNSNumber</th>\r\n                    </tr>\r\n                    <tr *ngFor=\"let bsubjectaddress of responceSubjectAddressInfo; let i=index\">\r\n                        <td>{{i+1}}</td>\r\n                        <td>{{bsubjectaddress.DunBradstreetRecord.PersonInfo.PersonName.FirstName}}</td>\r\n                        <td>{{bsubjectaddress.DunBradstreetRecord.PersonInfo.PersonName.LastName}}</td>\r\n                        <td>{{bsubjectaddress.SubjectAddress.Street}} {{bsubjectaddress.SubjectAddress.City}} {{bsubjectaddress.SubjectAddress.State}}\r\n                            {{bsubjectaddress.SubjectAddress.ZipCode}} {{bsubjectaddress.SubjectAddress.County}}</td>\r\n                        <td>\r\n                            <span *ngIf=\"bsubjectaddress.DunBradstreetRecord.BusinessInfo\">{{bsubjectaddress.DunBradstreetRecord.BusinessInfo.DUNSNumber}}</span>\r\n                        </td>\r\n\r\n                    </tr>\r\n\r\n                </table>\r\n            </div>\r\n        </modal-content>\r\n        <!-- <modal-footer>\r\n            <button class=\"btn btn-primary\" (click)=\"BusinessAtSubjectAddressSectionModal.close()\">close</button>\r\n        </modal-footer> -->\r\n    </modal>\r\n    <!-- BusinessAtSubjectAddressSection Model end -->\r\n    <!-- LicenseSection Model start -->\r\n    <modal #LicenseSectionModal>\r\n        <modal-header>\r\n            <h1>Bryan Aun</h1>\r\n        </modal-header>\r\n        <modal-content>\r\n            <div style=\"width:100%;height:400px;overflow:auto;\">\r\n                <table class=\"table\">\r\n                    <tr>\r\n                        <th>#</th>\r\n\r\n                        <th>FullName</th>\r\n                        <th>Address</th>\r\n                        <th>LicenseNumber</th>\r\n                        <th>LicenseState</th>\r\n                        <th>LicensingAgency</th>\r\n                    </tr>\r\n                    <tr *ngFor=\"let professionalLicense of responceLicenseSectionInfo; let i=index\">\r\n                        <td>{{i+1}}</td>\r\n                        <!-- <td>{{professionalLicense.PersonInfo.PersonName.FirstName}}</td>\r\n                    <td>{{professionalLicense.PersonInfo.PersonName.LastName}}</td> -->\r\n                        <td>{{professionalLicense.PersonInfo.PersonName.FullName}}</td>\r\n                        <td>{{professionalLicense.PersonalAddresses.Address.Street}} {{professionalLicense.PersonalAddresses.Address.City}}\r\n                            {{professionalLicense.PersonalAddresses.Address.State}} {{professionalLicense.PersonalAddresses.Address.ZipCode}}\r\n                            {{professionalLicense.PersonalAddresses.Address.County}}\r\n                        </td>\r\n                        <td>{{professionalLicense.ProLicenseInfo.CertificationInfo.LicenseNumber}}</td>\r\n                        <td>{{professionalLicense.ProLicenseInfo.LicenseState}}</td>\r\n                        <td>{{professionalLicense.ProLicenseInfo.LicensingAgency}}</td>\r\n                    </tr>\r\n\r\n                </table>\r\n            </div>\r\n\r\n        </modal-content>\r\n        <!-- <modal-footer>\r\n            <button class=\"btn btn-primary\" (click)=\"LicenseSectionModal.close()\">close</button>\r\n        </modal-footer> -->\r\n    </modal>\r\n    <!-- LicenseSection Model end -->\r\n    <!-- SSNAddressFraudSection Modal start -->\r\n    <modal #SSNAddressFraudSectionModal>\r\n        <modal-header>\r\n            <h1>Bryan Aun</h1>\r\n        </modal-header>\r\n        <modal-content>\r\n            <div style=\"width:100%;height:400px;overflow:auto;\">\r\n                <table class=\"table\">\r\n                    <tr>\r\n                        <th>#</th>\r\n                        <th>SSN</th>\r\n                        <th>Address</th>\r\n                        <th>AlertIndicator</th>\r\n                        <th>AlertDescription</th>\r\n                    </tr>\r\n                    <tr *ngFor=\"let SSNAddressFraud of responceSSNAddressFraudSectionInfo; let i=index\">\r\n                        <td>1 </td>\r\n                        <td>{{SSNAddressFraud[\"SSNAlert\"][\"SSN\"][\"SSN\"]}}</td>\r\n                        <td>{{SSNAddressFraud.AddressAlert.Address.Street}} {{SSNAddressFraud.AddressAlert.Address.City}}\r\n                            {{SSNAddressFraud.AddressAlert.Address.State}} {{SSNAddressFraud.AddressAlert.Address.ZipCode}}\r\n\r\n                        </td>\r\n                        <td>{{SSNAddressFraud.SSNAlert.AlertIndicator}}</td>\r\n                        <td>{{SSNAddressFraud.SSNAlert.AlertDescription}}</td>\r\n\r\n                    </tr>\r\n\r\n                </table>\r\n            </div>\r\n\r\n        </modal-content>\r\n        <!-- <modal-footer>\r\n            <button class=\"btn btn-primary\" (click)=\"SSNAddressFraudSectionModal.close()\">close</button>\r\n        </modal-footer> -->\r\n    </modal>\r\n    <!-- SSNAddressFraudSection Modal end -->\r\n    <!-- QuickAnalysisFlagSection Modal Start -->\r\n    <modal #QuickAnalysisFlagSectionModal>\r\n        <modal-header>\r\n            <h1>Bryan Aun</h1>\r\n        </modal-header>\r\n        <modal-content>\r\n            <div *ngIf=\"responceQuickAnalysisFlagSectionInfo\" style=\"width:100%;height:400px;overflow:auto;\">\r\n                <table class=\"table\">\r\n                    <tr>\r\n                        <th>#</th>\r\n                        <th>Quick Analysis Flag Name</th>\r\n                        <th>Risk Flag</th>\r\n                    </tr>\r\n\r\n                    <tr *ngFor=\"let quickanalysis of responceQuickAnalysisFlagSectionInfo; let i=index\">\r\n\r\n                        <td>{{i+1}}</td>\r\n                        <td>{{quickanalysis.riskname}}</td>\r\n                        <td>{{quickanalysis.riskvalue}}</td>\r\n\r\n                    </tr>\r\n\r\n                </table>\r\n            </div>\r\n\r\n        </modal-content>\r\n        <!-- <modal-footer>\r\n            <button class=\"btn btn-primary\" (click)=\"QuickAnalysisFlagSectionModal.close()\">close</button>\r\n        </modal-footer> -->\r\n    </modal>\r\n    <!-- QuickAnalysisFlagSection Modal End -->\r\n\r\n    <!-- CriminalSection Modal Start -->\r\n    <modal #CriminalSectionModal>\r\n        <modal-header>\r\n            <h1>Bryan Aun</h1>\r\n        </modal-header>\r\n        <modal-content>\r\n            <div *ngIf=\"responceCriminalSectionInfo\" style=\"width:100%;height:400px;overflow:auto;\">\r\n                <table class=\"table\">\r\n                    <tr>\r\n                        <th>#</th>\r\n                        <th>FirstName</th>\r\n                        <th>LastName</th>\r\n                        <th>BirthDate</th>\r\n                        <th>SourceName</th>\r\n                        <th>SourceCounty</th>\r\n                        <th>SourceState</th>\r\n                        <th>Source</th>\r\n                    </tr>\r\n\r\n                    <tr *ngFor=\"let criminal of responceCriminalSectionInfo; let i=index\">\r\n\r\n                        <td>{{i+1}}</td>\r\n                        <td>{{criminal.DefendantInfo.PersonInfo.PersonName.FirstName}}</td>\r\n                        <td>{{criminal.DefendantInfo.PersonInfo.PersonName.LastName}}</td>\r\n                        <td>{{criminal.DefendantInfo.PersonInfo.PersonProfile.PersonBirthDate}}</td>\r\n                        <td>{{criminal.SourceName}}</td>\r\n                        <td>{{criminal.SourceCounty}}</td>\r\n                        <td>{{criminal.SourceState}}</td>\r\n                        <td>{{criminal.Source}}</td>\r\n                    </tr>\r\n\r\n                </table>\r\n            </div>\r\n        </modal-content>\r\n        <!-- <modal-footer>\r\n            <button class=\"btn btn-primary\" (click)=\"CriminalSectionModal.close()\">close</button>\r\n        </modal-footer> -->\r\n    </modal>\r\n    <!-- CriminalSection Modal End -->\r\n\r\n</main>\r\n\r\n<footer class=\"footer kdark\">\r\n    <span class=\"text-left\">\r\n        &copy; 2019 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1068:
/***/ (function(module, exports) {

module.exports = "<!-- <header class=\"navbar apatics-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header> -->\r\n<app-header-default></app-header-default>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar apatics-sidebar\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Revenue Code</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group1\" name=\"input2-group1\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedREV_CDE\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedREV_CDE || searchedREV_CDE.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedREV_CDE=''\" *ngIf=\"searchedREV_CDE && searchedREV_CDE.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.rEV_CDEs | searchFacets:searchedREV_CDE\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 1)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">REV DSC</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedREV_DSC\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedREV_DSC || searchedREV_DSC.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedREV_DSC=''\" *ngIf=\"searchedREV_DSC && searchedREV_DSC.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.rEV_DSCs | searchFacets:searchedREV_DSC\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 2)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main apatics-container\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../home']\">Home</a></li>\r\n        <li class=\"breadcrumb-item active\"><span>Revenue Code</span></li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"card m-t-2\">\r\n                <!--<h3 class=\"card-header\">RevenueCode</h3>-->\r\n                <div class=\"card-block\">\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group mb-0 col-md-12\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search Revenue Code\" [(ngModel)]=\"searchRevenueCodeText\"\r\n                                    (keyup.enter)=\"searchRevenueCode()\">\r\n                                <span class=\"input-group-btn\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchRevenueCode()\"><i class=\"fa fa-search\"></i> Search</button>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 pt-1 text-xs-right page-info\">\r\n                        <span *ngIf=\"totalRecords>0\" [textContent]=\"(pageNumber*recordsToFetch-recordsToFetch+1) + ' to ' + (pageNumber*recordsToFetch-recordsToFetch+rersRevenueCode.length) + ' of ' + totalRecords + ' items'\"></span>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.rEV_CDEs | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 1)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty REV CDE':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.rEV_DSCs | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 2)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty REV DSC':objFacet.name\"></span>                           \r\n                       </button>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 text-lg-center\" *ngIf=\"foundRecord==false\">\r\n                        <h4>No result found.</h4>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 fda-list-container\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"col-xs-12 p-0\">\r\n                            <table class=\"table table-bordered table-striped table-condensed\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th>REV CDE</th>\r\n                                        <th>REV DSC</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr *ngFor=\"let objRevenueCode of asyncRevenueCode | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\">\r\n                                        <td [innerHTML]=\"objRevenueCode.rEV_CDE | highlight:searchRevenueCodeText\"></td>\r\n                                        <td [innerHTML]=\"objRevenueCode.rEV_DSC | highlight:searchRevenueCodeText\"></td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"spinner\" [ngClass]=\"{ 'hidden': !loading }\"></div>\r\n                        <div class=\"pagination-container\">\r\n                            <pagination-controls (pageChange)=\"searchRevenueCodeRecordsWithFilters($event)\" id=\"server\"></pagination-controls>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n<footer class=\"footer\">\r\n    <span class=\"text-left\">\r\n        &copy; 2017 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1069:
/***/ (function(module, exports) {

module.exports = "<!-- <header class=\"navbar apatics-navbar\" id=\"header-bar\">\r\n    <div class=\"container-fluid\">\r\n        <button class=\"navbar-toggler hidden-lg-up\" type=\"button\" mobile-nav-toggle>&#9776;</button>\r\n        <a class=\"navbar-brand\" href=\"#\"></a>\r\n        <ul class=\"nav navbar-nav hidden-md-down\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link navbar-toggler sidebar-toggle\" href=\"#\">&#9776;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</header> -->\r\n<app-header-default></app-header-default>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar apatics-sidebar\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Corporation Name</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group1\" name=\"input2-group1\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedCN\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedCN || searchedCN.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedCN=''\" *ngIf=\"searchedCN && searchedCN.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.corporationNames | searchFacets:searchedCN\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 1)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Corp or LLC</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedCOL\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedCOL || searchedCOL.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedCOL=''\" *ngIf=\"searchedCOL && searchedCOL.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.corpOrLLCs | searchFacets:searchedCOL\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 2)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Corporation Type</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedCT\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedCT || searchedCT.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedCT=''\" *ngIf=\"searchedCT && searchedCT.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.corporationTypes | searchFacets:searchedCT\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 3)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n            <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">California City</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group3\" name=\"input2-group3\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedCC\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedCC || searchedCC.length==0\"><i class=\"fa fa-search\"></i></span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedCC=''\" *ngIf=\"searchedCC && searchedCC.length>0\"><i class=\"fa fa-times\"></i></span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let objFacet of resFacets.californiaCities | searchFacets:searchedCC\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSelectFacet(objFacet, 4)\">\r\n                            <i class=\"fa\" [ngClass]=\"objFacet.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty':objFacet.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"'(' + objFacet.recordCount + ')'\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main apatics-container\" id=\"main-container\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\"><a [routerLink]=\"['../home']\">Home</a></li>\r\n        <li class=\"breadcrumb-item active\"><span>Secretary of State</span></li>\r\n    </ol>\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"card m-t-2\">\r\n                <!--<h3 class=\"card-header\">SecretaryOfState</h3>-->\r\n                <div class=\"card-block\">\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group mb-0 col-md-12\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search SecretaryOfState\" [(ngModel)]=\"searchSecretaryOfStateText\"\r\n                                    (keyup.enter)=\"searchSecretaryOfState()\">\r\n                                <span class=\"input-group-btn\">\r\n                                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchSecretaryOfState()\"><i class=\"fa fa-search\"></i> Search</button>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 pt-1 text-xs-right page-info\">\r\n                        <span *ngIf=\"totalRecords>0\" [textContent]=\"(pageNumber*recordsToFetch-recordsToFetch+1) + ' to ' + (pageNumber*recordsToFetch-recordsToFetch+rerSecretaryOfState.length) + ' of ' + totalRecords + ' items'\"></span>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.corporationNames | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 1)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Corporation Names':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.corpOrLLCs | selectedFacets\" (click)=\"removeSelectedFacet(objFacet, 2)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Corp or LLC':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.corporationTypes | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 3)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Corporation Types':objFacet.name\"></span>                           \r\n                       </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of resFacets.californiaCities | selectedFacets\"\r\n                            (click)=\"removeSelectedFacet(objFacet, 4)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty California Cities':objFacet.name\"></span>                           \r\n                       </button>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 text-lg-center\" *ngIf=\"foundRecord==false\">\r\n                        <h4>No result found.</h4>\r\n                    </div>\r\n                    <div class=\"col-xs-12 p-0 fda-list-container\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"card m-b-1\" *ngFor=\"let objSecretaryOfState of asyncSecretaryOfState | async | paginate: { id: 'server', itemsPerPage: recordsToFetch, currentPage: pageNumber, totalItems: totalRecords }\">\r\n                            <div class=\"card-block p-1\">\r\n                                <div class=\"col-xs-12 p-0\">\r\n                                    <button type=\"button\" class=\"btn btn-primary pull-left mr-1\" (click)=\"objSecretaryOfState.isOpen=!objSecretaryOfState.isOpen\">\r\n                                        <i class=\"fa\" [ngClass]=\"objSecretaryOfState.isOpen ? 'fa-minus' : 'fa-plus'\"></i>\r\n                                    </button>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.corporationName\">\r\n                                        <span class=\"data-title\">Corporation Names:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.corporationName | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.corpOrLLC\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Corp or LLC:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.corpOrLLC | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.corporationType\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Corporation Type:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.corporationType | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.californiaCity\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">California City:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.californiaCity | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.incorporationDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Incorporation Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.incorporationDate.substring(6, objSecretaryOfState.incorporationDate.length-7) | date : 'MM-dd-yyyy'  | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.incorporationStatus\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Incorporation Status:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.incorporationStatus | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.mailAddressString\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Mail Address:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.mailAddressString | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-xs-12 p-0\" *ngIf=\"objSecretaryOfState.isOpen\">\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.transactionDate\">\r\n                                        <span class=\"data-title\">Transaction Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.transactionDate.substring(6, objSecretaryOfState.transactionDate.length-7) | date : 'MM-dd-yyyy' | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.transactionCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Tansaction Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.transactionCode | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.fTBSuspensionStatusCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">FTB Suspension Status Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.fTBSuspensionStatusCode | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.fTBSuspensionDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">FTB Suspension Date :</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.fTBSuspensionDate.substring(6, objSecretaryOfState.fTBSuspensionDate.length-7) | date : 'MM-dd-yyyy' | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.corporationNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Corporation Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.corporationNumber | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.agentName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Agent Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.agentName | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.agentAddressString\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Agent Address:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.agentAddressString | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.cEOPartnerName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">CEO / Partner Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.cEOPartnerName | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.cEOPartnerAddressString\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">CEO / Partner Address:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.cEOPartnerAddressString | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.generalPartner1Name\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">General Partner 1 Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.generalPartner1Name | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.generalPartner1AddressString\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">General Partner 1 Address:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.generalPartner1AddressString | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.generalPartner2Name\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">General Partner 2 Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.generalPartner2Name | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.generalPartner2AddressString\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">General Partner 2 Address:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.generalPartner2AddressString | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.californiaAddressString\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">California Address:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.californiaAddressString | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.cONameIfPresent\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">C / O Name (If Present):</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.cONameIfPresent | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.comments\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Comments:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.comments | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.numberOfAmendments\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Number of Amendments:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.numberOfAmendments | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.numberOfGPToAmendment\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Number of GP to Amendment:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.numberOfGPToAmendment | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.additionalGPs\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Additional GPs:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.additionalGPs | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.documentPageCount\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Document Page Count:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.documentPageCount | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.documentNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Document Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.documentNumber | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.term\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Term:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.term | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.corporationClassification\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Corporation Classification:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.corporationClassification | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.jurisdiction\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Jurisdiction:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.jurisdiction | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.oldCorporationName\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Old Corporation Name:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.oldCorporationName | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.filingType\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Filing Type:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.filingType | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.originalFileDate\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Original File Date:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.originalFileDate.substring(6, objSecretaryOfState.originalFileDate.length-7) | date : 'MM-dd-yyyy'  | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.nameCorporationNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Name Corporation Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.nameCorporationNumber | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.stateOrForeignCountry\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">State or Foreign Country:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.stateOrForeignCountry | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.lLCManagementCode\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">LLC Management Code:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.lLCManagementCode | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                    <div class=\"pull-left\" *ngIf=\"objSecretaryOfState.originalFileNumber\">\r\n                                        <div class=\"divider\">|</div>\r\n                                        <span class=\"data-title\">Original File Number:</span>\r\n                                        <div class=\"pull-left\" [innerHTML]=\"objSecretaryOfState.originalFileNumber | highlight:searchSecretaryOfStateText\"></div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12\" [hidden]=\"totalRecords==0\">\r\n                        <div class=\"spinner\" [ngClass]=\"{ 'hidden': !loading }\"></div>\r\n                        <div class=\"pagination-container\">\r\n                            <pagination-controls (pageChange)=\"searchSecretaryOfStateRecordsWithFilters($event)\" id=\"server\"></pagination-controls>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n<footer class=\"footer\">\r\n    <span class=\"text-left\">\r\n        &copy; 2017 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1070:
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar sidebar-kdark\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Provider</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedProv\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedProv || searchedProv.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedProv=''\" *ngIf=\"searchedProv && searchedProv.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let prov of activeFilters['providers']|searchFacets:searchedProv\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeProvSelection(prov)\">\r\n                            <i class=\"fa\" [ngClass]=\"prov.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"prov.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"prov.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Attorney</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedAttorney\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedAttorney || searchedAttorney.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedAttorney=''\" *ngIf=\"searchedAttorney && searchedAttorney.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let atto of activeFilters['attorneys'] | searchFacets:searchedAttorney\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeAttorneySelection(atto)\">\r\n                            <i class=\"fa\" [ngClass]=\"atto.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"atto.name==''?'Empty':atto.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"atto.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Claimant</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedClaim\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedClaim || searchedClaim.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedClaim=''\" *ngIf=\"searchedClaim && searchedClaim.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let claim of activeFilters['claimant'] | searchFacets:searchedClaim\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeClaimantSelection(claim)\">\r\n                            <i class=\"fa\" [ngClass]=\"claim.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"claim.name==''?'Empty':claim.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"claim.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Claim Number</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedCNO\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedCNO || searchedCNO.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedCNO=''\" *ngIf=\"searchedCNO && searchedCNO.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let cno of activeFilters['claimnumber'] | searchFacets:searchedCNO\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeClaimNumberSelection(cno)\">\r\n                            <i class=\"fa\" [ngClass]=\"cno.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"cno.name==''?'Empty':cno.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"cno.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main kdark\" id=\"main-container\">\r\n    <div *ngIf=\"isShowLoad\" class=\"loading\">Loading&#8230;</div>\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\">\r\n            <a [routerLink]=\"['../../home']\">Home</a>\r\n        </li>\r\n        <li class=\"breadcrumb-item active\">\r\n            <span>Social Networks</span>\r\n        </li>\r\n        <!-- Breadcrumb Menu-->\r\n    </ol>\r\n\r\n    <div class=\"container-fluid\">\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"m-t-2\">\r\n                <div class=\"row\">\r\n                    <div class=\"form-group mb-0 col-md-12\">\r\n                        <div class=\"input-group\">\r\n                            <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search ProviderSummary\" [(ngModel)]=\"searchQuery\">\r\n                            <span class=\"input-group-btn\">\r\n                                <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchProviderSummary()\">\r\n                                    <i class=\"fa fa-search\"></i> Search</button>\r\n                            </span>\r\n                            <span class=\"input-group-btn\">\r\n                                <button type=\"button\" class=\"btn btn-primary\" (click)=\"removeSearchQuery()\">\r\n                                    <i class=\"fa fa-refresh\"></i>Reset</button>\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['providers'] | selectedFacets\"\r\n                            (click)=\"changeProvSelection(objFacet)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Provider Name':objFacet.name\"></span>\r\n                        </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['attorneys'] | selectedFacets\"\r\n                            (click)=\"changeAttorneySelection(objFacet)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Provider Type':objFacet.name\"></span>\r\n                        </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['claimant'] | selectedFacets\"\r\n                            (click)=\"changeClaimantSelection(objFacet)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Provider Type':objFacet.name\"></span>\r\n                        </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['claimnumber'] | selectedFacets\"\r\n                            (click)=\"changeClaimNumberSelection(objFacet)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Provider Type':objFacet.name\"></span>\r\n                        </button>\r\n\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngIf=\"this.activeFilters['searchQuery']\" (click)=\"removeSearchQuery()\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"this.activeFilters['searchQuery']\"></span>\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-12 mb-2\">\r\n                        <!-- Nav tabs -->\r\n                        <tabset class=\"kdark tab-container\">\r\n                            <tab heading=\"Graph View\">\r\n                                <div class=\"pull-right\" id=\"network-legend\">\r\n                                    <div class=\"legend\">\r\n                                        <p class=\"node-name\">\r\n                                            <span class=\"key-dot\" style=\"background: rgb(109, 206, 158);\"></span>\r\n                                            <label class=\"valign-top\">Provider</label>\r\n                                        </p>\r\n                                    </div>\r\n                                    <div class=\"legend\">\r\n                                        <p class=\"node-name\">\r\n                                            <span class=\"key-dot\" style=\"background: rgb(255, 117, 110);\"></span>\r\n                                            <label class=\"valign-top\">Attorney</label>\r\n                                        </p>\r\n                                    </div>\r\n                                    <div class=\"legend\">\r\n                                        <p class=\"node-name\">\r\n                                            <span class=\"key-dot\" style=\"background: rgb(104, 189, 246);\"></span>\r\n                                            <label class=\"valign-top\">Claimant</label>\r\n                                        </p>\r\n                                    </div>\r\n                                    <div class=\"legend\">\r\n                                        <p class=\"node-name\">\r\n                                            <span class=\"key-dot\" style=\"background: rgb(222, 155, 249);\"></span>\r\n                                            <label class=\"valign-top\">Claimnumber</label>\r\n                                        </p>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"clear\"></div>\r\n                                <div id=\"network-graph\">Network Graph</div>\r\n                            </tab>\r\n                            <tab heading=\"Report View\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"card br-5 mb-2 card-accent-primary mb-0\">\r\n                                        <div class=\"card-header\"># of times a Claimant has collaborated with the same Attorney</div>\r\n                                        <div class=\"card-block\">\r\n                                            <div class=\"report-table-container\">\r\n                                                <table class=\"table table-bordered ktable\">\r\n                                                    <thead>\r\n                                                        <tr>\r\n                                                            <th>Claimant</th>\r\n                                                            <th>Attorney</th>\r\n                                                            <th>Count</th>\r\n                                                            <th>Year</th>\r\n                                                        </tr>\r\n                                                    </thead>\r\n                                                    <tbody class=\"text-center\">\r\n                                                        <tr *ngFor=\"let tod of tableOneData\">\r\n                                                            <td>{{tod.Claimant}}</td>\r\n                                                            <td>{{tod.Attorney}}</td>\r\n                                                            <td>{{tod.count_no}}</td>\r\n                                                            <td>{{tod.min_year}}-{{tod.max_year}}</td>\r\n                                                        </tr>\r\n                                                    </tbody>\r\n                                                </table>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"card br-5 mb-2 card-accent-primary mb-0\">\r\n                                        <div class=\"card-header\"># of times a Claimant has collaborated with the same Provider</div>\r\n                                        <div class=\"card-block\">\r\n                                            <div class=\"report-table-container\">\r\n                                                <table class=\"table table-bordered ktable\">\r\n                                                    <thead>\r\n                                                        <tr>\r\n                                                            <th>Claimant</th>\r\n                                                            <th>Provider</th>\r\n                                                            <th>Count</th>\r\n                                                            <th>Year</th>\r\n                                                        </tr>\r\n                                                    </thead>\r\n                                                    <tbody class=\"text-center\">\r\n                                                        <tr *ngFor=\"let tod of tableTwoData\">\r\n                                                            <td>{{tod.Claimant}}</td>\r\n                                                            <td>{{tod.Provider}}</td>\r\n                                                            <td>{{tod.count_no}}</td>\r\n                                                            <td>{{tod.min_year}}-{{tod.max_year}}</td>\r\n                                                        </tr>\r\n                                                    </tbody>\r\n                                                </table>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"card br-5 mb-2 card-accent-primary mb-0\">\r\n                                        <div class=\"card-header\"># of times a Provider and an Attorney collaborated in a Claim</div>\r\n                                        <div class=\"card-block\">\r\n                                            <div class=\"report-table-container\">\r\n                                                <table class=\"table table-bordered ktable\">\r\n                                                    <thead>\r\n                                                        <tr>\r\n                                                            <th>Provider</th>\r\n                                                            <th>Attorney</th>\r\n                                                            <th>Count</th>\r\n                                                            <th>Year</th>\r\n                                                        </tr>\r\n                                                    </thead>\r\n                                                    <tbody class=\"text-center\">\r\n                                                        <tr *ngFor=\"let tod of tableThreeData\">\r\n                                                            <td>{{tod.Provider}}</td>\r\n                                                            <td>{{tod.Attorney}}</td>\r\n                                                            <td>{{tod.count_no}}</td>\r\n                                                            <td>{{tod.min_year}}-{{tod.max_year}}</td>\r\n                                                        </tr>\r\n                                                    </tbody>\r\n                                                </table>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"card br-5 mb-2 card-accent-primary mb-0\">\r\n                                        <div class=\"card-header\"># of times a Claimant , Provider and an Attorney collaborated in a Claim</div>\r\n                                        <div class=\"card-block\">\r\n                                            <div class=\"report-table-container\">\r\n                                                <table class=\"table table-bordered ktable\">\r\n                                                    <thead>\r\n                                                        <tr>\r\n                                                            <th>Claimant</th>\r\n                                                            <th>Provider</th>\r\n                                                            <th>Attorney</th>\r\n                                                            <th>Count</th>\r\n                                                            <th>Year</th>\r\n                                                        </tr>\r\n                                                    </thead>\r\n                                                    <tbody class=\"text-center\">\r\n                                                        <tr *ngFor=\"let tod of tableFourData\">\r\n                                                            <td>{{tod.Claimant}}</td>\r\n                                                            <td>{{tod.Provider}}</td>\r\n                                                            <td>{{tod.Attorney}}</td>\r\n                                                            <td>{{tod.count_no}}</td>\r\n                                                            <td>{{tod.min_year}}-{{tod.max_year}}</td>\r\n                                                        </tr>\r\n                                                    </tbody>\r\n                                                </table>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </tab>\r\n                        </tabset>\r\n                    </div>\r\n                    <!--/.col-->\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n\r\n\r\n<footer class=\"footer kdark\">\r\n    <span class=\"text-left\">\r\n        &copy; 2019 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1071:
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n\r\n<div id=\"facet-sidebar\" class=\"sidebar sidebar-kdark\">\r\n    <nav class=\"sidebar-nav\">\r\n        <ul class=\"nav\">\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Primary Provider</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedProv\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedProv || searchedProv.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedProv=''\" *ngIf=\"searchedProv && searchedProv.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let prov of activeFilters['providers']|searchFacets:searchedProv\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeProvSelection(prov)\">\r\n                            <i class=\"fa\" [ngClass]=\"prov.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"prov.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"prov.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Secondary Provider</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedSProv\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedSProv || searchedSProv.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedSProv=''\" *ngIf=\"searchedSProv && searchedSProv.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let sprov of activeFilters['secondary_providers']|searchFacets:searchedSProv\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changeSecProvSelection(sprov)\">\r\n                            <i class=\"fa\" [ngClass]=\"sprov.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"sprov.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"sprov.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n            <li class=\"nav-item nav-dropdown\" [class.open]=\"true\" routerLinkActive=\"open\">\r\n                <a class=\"nav-link nav-dropdown-toggle\" href=\"javascript:void(0)\">Patient</a>\r\n                <ul class=\"nav-dropdown-items\">\r\n                    <li class=\"nav-item searchbox-container\">\r\n                        <div class=\"form-group m-0\">\r\n                            <div class=\"input-group\">\r\n                                <input type=\"text\" id=\"input2-group2\" name=\"input2-group2\" class=\"form-control\" placeholder=\"Search\" [(ngModel)]=\"searchedPatient\">\r\n                                <span class=\"input-group-addon\" *ngIf=\"!searchedPatient || searchedPatient.length==0\">\r\n                                    <i class=\"fa fa-search\"></i>\r\n                                </span>\r\n                                <span class=\"input-group-addon cursor-pointer\" (click)=\"searchedPatient=''\" *ngIf=\"searchedPatient && searchedPatient.length>0\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngFor=\"let patient of activeFilters['patients']|searchFacets:searchedPatient\">\r\n                        <a class=\"nav-link p-r-5\" (click)=\"changePatientSelection(patient)\">\r\n                            <i class=\"fa\" [ngClass]=\"patient.selected ? 'fa-check-square':'fa-square'\"></i>\r\n                            <span [textContent]=\"patient.name\"></span>\r\n                            <span class=\"tag tag-info\" [textContent]=\"patient.item_count\"></span>\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n\r\n        </ul>\r\n    </nav>\r\n</div>\r\n<!-- Main content -->\r\n<main class=\"main kdark\" id=\"main-container\">\r\n    <div *ngIf=\"isShowLoad\" class=\"loading\">Loading&#8230;</div>\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\">\r\n            <a [routerLink]=\"['../../home']\">Home</a>\r\n        </li>\r\n        <li class=\"breadcrumb-item active\">\r\n            <span>Social Networks</span>\r\n        </li>\r\n        <!-- Breadcrumb Menu-->\r\n    </ol>\r\n\r\n    <div class=\"container-fluid\">\r\n        <div class=\"animated fadeIn\">\r\n            <div class=\"m-t-2\">\r\n                <div class=\"row\">\r\n                    <div class=\"form-group mb-0 col-md-12\">\r\n                        <div class=\"input-group\">\r\n                            <input type=\"text\" id=\"input1-group2\" name=\"input1-group2\" class=\"form-control\" placeholder=\"Search ProviderSummary\" [(ngModel)]=\"searchQuery\">\r\n                            <span class=\"input-group-btn\">\r\n                                <button type=\"button\" class=\"btn btn-primary\" (click)=\"searchProviderSummary()\">\r\n                                    <i class=\"fa fa-search\"></i> Search</button>\r\n                            </span>\r\n                            <span class=\"input-group-btn\">\r\n                                <button type=\"button\" class=\"btn btn-primary\" (click)=\"removeSearchQuery()\">\r\n                                    <i class=\"fa fa-refresh\"></i>Reset</button>\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-xs-12 p-0 selected-facet-container\">\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['providers'] | selectedFacets\"\r\n                            (click)=\"changeProvSelection(objFacet)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Provider Name':objFacet.name\"></span>\r\n                        </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['secondary_providers'] | selectedFacets\"\r\n                            (click)=\"changeSecProvSelection(objFacet)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Provider Type':objFacet.name\"></span>\r\n                        </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngFor=\"let objFacet of activeFilters['patients'] | selectedFacets\"\r\n                            (click)=\"changePatientSelection(objFacet)\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"objFacet.name==''?'Empty Provider Type':objFacet.name\"></span>\r\n                        </button>\r\n                        <button class=\"btn btn-secondary active\" type=\"button\" *ngIf=\"this.activeFilters['searchQuery']\" (click)=\"removeSearchQuery()\">\r\n                            <i class=\"fa fa-close\"></i>\r\n                            <span [textContent]=\"this.activeFilters['searchQuery']\"></span>\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-12 mb-2\">\r\n                        <!-- Nav tabs -->\r\n                        <tabset class=\"kdark tab-container\">\r\n                            <tab heading=\"Graph View\">\r\n                                <div class=\"pull-right\" id=\"network-legend\">\r\n                                    <div class=\"legend\">\r\n                                        <p class=\"node-name\">\r\n                                            <span class=\"key-dot\" style=\"background: rgb(109, 206, 158);\"></span>\r\n                                            <label class=\"valign-top\">Primary Provider</label>\r\n                                        </p>\r\n                                    </div>\r\n                                    <div class=\"legend\">\r\n                                        <p class=\"node-name\">\r\n                                            <span class=\"key-dot\" style=\"background: rgb(255, 117, 110);\"></span>\r\n                                            <label class=\"valign-top\">Secondary Provider</label>\r\n                                        </p>\r\n                                    </div>\r\n                                    <div class=\"legend\">\r\n                                        <p class=\"node-name\">\r\n                                            <span class=\"key-dot\" style=\"background: rgb(104, 189, 246);\"></span>\r\n                                            <label class=\"valign-top\">Patient</label>\r\n                                        </p>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"clear\"></div>\r\n                                <div id=\"network-graph\">Network Graph</div>\r\n                            </tab>\r\n                            <tab heading=\"Report View\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"card br-5 mb-2 card-accent-primary mb-0\">\r\n                                        <div class=\"card-header\"># of times a Patient has collaborated with the same Provider</div>\r\n                                        <div class=\"card-block\">\r\n                                            <div class=\"report-table-container\">\r\n                                                <table class=\"table table-bordered ktable\">\r\n                                                    <thead>\r\n                                                        <tr>\r\n                                                            <th>Patient</th>\r\n                                                            <th>Provider</th>\r\n                                                            <th>Count</th>\r\n                                                            <th>Year</th>\r\n                                                        </tr>\r\n                                                    </thead>\r\n                                                    <tbody class=\"text-center\">\r\n                                                        <tr *ngFor=\"let tod of tableOneData\">\r\n                                                            <td>{{tod.Patient}}</td>\r\n                                                            <td>{{tod.Provider}}</td>\r\n                                                            <td>{{tod.count_no}}</td>\r\n                                                            <td>{{tod.min_year}}-{{tod.max_year}}</td>\r\n                                                        </tr>\r\n                                                    </tbody>\r\n                                                </table>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"card br-5 mb-2 card-accent-primary mb-0\">\r\n                                        <div class=\"card-header\"># of times a Patient has collaborated with the same Secondary Provider</div>\r\n                                        <div class=\"card-block\">\r\n                                            <div class=\"report-table-container\">\r\n                                                <table class=\"table table-bordered ktable\">\r\n                                                    <thead>\r\n                                                        <tr>\r\n                                                            <th>Patient</th>\r\n                                                            <th>Secondary Provider</th>\r\n                                                            <th>Count</th>\r\n                                                            <th>Year</th>\r\n                                                        </tr>\r\n                                                    </thead>\r\n                                                    <tbody class=\"text-center\">\r\n                                                        <tr *ngFor=\"let tod of tableTwoData\">\r\n                                                            <td>{{tod.Patient}}</td>\r\n                                                            <td>{{tod.Secondary_Provider}}</td>\r\n                                                            <td>{{tod.count_no}}</td>\r\n                                                            <td>{{tod.min_year}}-{{tod.max_year}}</td>\r\n                                                        </tr>\r\n                                                    </tbody>\r\n                                                </table>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"card br-5 mb-2 card-accent-primary mb-0\">\r\n                                        <div class=\"card-header\"># of times a Provider and an Secondary Provider collaborated in a Patient</div>\r\n                                        <div class=\"card-block\">\r\n                                            <div class=\"report-table-container\">\r\n                                                <table class=\"table table-bordered ktable\">\r\n                                                    <thead>\r\n                                                        <tr>\r\n                                                            <th>Patient</th>\r\n                                                            <th>Provider</th>\r\n                                                            <th>Secondary Provider</th>\r\n                                                            <th>Count</th>\r\n                                                            <th>Year</th>\r\n                                                        </tr>\r\n                                                    </thead>\r\n                                                    <tbody class=\"text-center\">\r\n                                                        <tr *ngFor=\"let tod of tableThreeData\">\r\n                                                            <td>{{tod.Patient}}</td>\r\n                                                            <td>{{tod.Provider}}</td>\r\n                                                            <td>{{tod.Secondary_Provider}}</td>\r\n                                                            <td>{{tod.count_no}}</td>\r\n                                                            <td>{{tod.min_year}}-{{tod.max_year}}</td>\r\n                                                        </tr>\r\n                                                    </tbody>\r\n                                                </table>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </tab>\r\n                        </tabset>\r\n                    </div>\r\n                    <!--/.col-->\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.conainer-fluid -->\r\n</main>\r\n\r\n\r\n<footer class=\"footer kdark\">\r\n    <span class=\"text-left\">\r\n        &copy; 2019 Apatics, Inc. All rights reserved\r\n    </span>\r\n</footer>"

/***/ }),

/***/ 1329:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(712);


/***/ }),

/***/ 186:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(37);
var HttpClient = (function () {
    function HttpClient(http) {
        // private ApiBaseUrl = 'http://localhost/SolrPOCAPI/v1/';
        this.ApiBaseUrl = 'http://ai.apatics.net/api/SolrPOCService.svc/'; //Production
        this.headers = new http_1.Headers();
        this.http = http;
        //this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
        // this.headers.append('access-control-allow-origin','*');
        // this.headers.append('access-control-max-age','1728000');
        // this.headers.append('access-control-allow-methods','GET, HEAD, POST, PUT, DELETE, OPTIONS');
        // this.headers.append('access-control-allow-headers','Content-Type');
        //  this.headers.append('access-control-expose-headers','WWW-Authenticate, Server-Authorization');
    }
    HttpClient.prototype.get = function (url) {
        var fullUrl = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.get(fullUrl, { headers: this.headers });
    };
    HttpClient.prototype.post = function (url, data) {
        var fullUrl = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.post(fullUrl, data, { headers: this.headers });
    };
    HttpClient.prototype.getWithHeaders = function (url, headers) {
        var fullUrl = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.get(fullUrl, { headers: headers });
    };
    HttpClient.prototype.postWithHeaders = function (url, data, headers) {
        var fullUrl = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.post(fullUrl, data, { headers: headers });
    };
    return HttpClient;
}());
HttpClient = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
], HttpClient);
exports.HttpClient = HttpClient;
var _a;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/http.client.js.map

/***/ }),

/***/ 270:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var Subject_1 = __webpack_require__(13);
__webpack_require__(206);
__webpack_require__(63);
var core_1 = __webpack_require__(0);
var BroadcastService = (function () {
    function BroadcastService() {
        this._eventBus = new Subject_1.Subject();
    }
    BroadcastService.prototype.broadcast = function (key, data) {
        this._eventBus.next({ key: key, data: data });
    };
    BroadcastService.prototype.on = function (key) {
        return this._eventBus.asObservable()
            .filter(function (event) { return event.key === key; })
            .map(function (event) { return event.data; });
    };
    return BroadcastService;
}());
BroadcastService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], BroadcastService);
exports.BroadcastService = BroadcastService;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/broadcast.service.js.map

/***/ }),

/***/ 39:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(37);
var common_1 = __webpack_require__(10);
var global_1 = __webpack_require__(103);
var http_client_1 = __webpack_require__(186);
__webpack_require__(63);
var FDAService = (function () {
    function FDAService(httpClient, _jsonp, datePipe, globals) {
        this.httpClient = httpClient;
        this._jsonp = _jsonp;
        this.datePipe = datePipe;
        this.globals = globals;
    }
    FDAService.prototype.getSelectedFDA = function (productId) {
        if (!productId || productId.length == 0)
            productId = "0002-1975_6bf7a335-bf6b-4f7f-b8cf-c7b6ee1ba089";
        return this.httpClient.get('FDA/select/?q=' + productId).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchFDAFacets = function (searchText) {
        if (!searchText)
            searchText = "";
        return this.httpClient.get('FDA/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchFDARecords = function (searchText, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "";
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('FDA/search/?q=' + searchText + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchFDARecordsWithFilter = function (searchText, pndc, ptn, pn, npn, deas, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "";
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('FDA/searchwithfilter/?q=' + searchText + '&f1=' + pndc + '&f2=' + ptn + '&f3=' + pn + '&f4=' + npn + '&f5=' + deas + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchDCAFacets = function (searchText) {
        if (!searchText)
            searchText = "";
        return this.httpClient.get('DCA/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchDCARecordsWithFilter = function (searchText, an, lt, sc, ls, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "";
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('DCA/searchwithfilter/?q=' + searchText + '&f1=' + an + '&f2=' + lt + '&f3=' + sc + '&f4=' + ls + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchESIFacets = function (searchText) {
        if (!searchText)
            searchText = "";
        else
            searchText = searchText.replace(/&/g, '`').replace(/#/g, '^');
        return this.httpClient.get('ESI/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchESIRecordsWithFilter = function (searchText, phn, plc, pzc, pln, cc, dt, bar, car, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "";
        else
            searchText = searchText.replace(/&/g, '`').replace(/#/g, '^');
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('ESI/searchwithfilter/?q=' + searchText + '&f1=' + phn + '&f2=' + plc + '&f3=' + pzc + '&f4=' + pln + '&f5=' + cc + '&f6=' + dt + '&f7=' + bar + '&f8=' + car + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchICD9Facets = function (searchText) {
        if (!searchText)
            searchText = "";
        else
            searchText = searchText.replace(/&/g, '`').replace(/#/g, '^');
        return this.httpClient.get('ICD9/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchICD9RecordsWithFilter = function (searchText, cde, cdedsc, cdetyp, cdetypdsc, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "";
        else
            searchText = searchText.replace(/&/g, '`').replace(/#/g, '^');
        if (cde)
            cde = cde.replace(/&/g, '`').replace(/#/g, '^');
        if (cdedsc)
            cdedsc = cdedsc.replace(/&/g, '`').replace(/#/g, '^');
        if (cdetyp)
            cdetyp = cdetyp.replace(/&/g, '`').replace(/#/g, '^');
        if (cdetypdsc)
            cdetypdsc = cdetypdsc.replace(/&/g, '`').replace(/#/g, '^');
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('ICD9/searchwithfilter/?q=' + searchText + '&f1=' + cde + '&f2=' + cdedsc + '&f3=' + cdetyp + '&f4=' + cdetypdsc + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchRevenueCodeFacets = function (searchText) {
        if (!searchText)
            searchText = "";
        else
            searchText = searchText.replace(/&/g, '`').replace(/#/g, '^');
        return this.httpClient.get('RevenueCode/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchRevenueCodeRecordsWithFilter = function (searchText, cde, cdedsc, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "";
        else
            searchText = searchText.replace(/&/g, '`').replace(/#/g, '^');
        if (cde)
            cde = cde.replace(/&/g, '`').replace(/#/g, '^');
        if (cdedsc)
            cdedsc = cdedsc.replace(/&/g, '`').replace(/#/g, '^');
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('RevenueCode/searchwithfilter/?q=' + searchText + '&f1=' + cde + '&f2=' + cdedsc + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchSecretaryOfStateFacets = function (searchText) {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        return this.httpClient.get('SecretaryOfState/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchSecretaryOfStateRecordsWithFilter = function (searchText, cn, col, ct, cc, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        if (cn)
            cn = encodeURIComponent(cn);
        if (col)
            col = encodeURIComponent(col);
        if (ct)
            ct = encodeURIComponent(ct);
        if (cc)
            cc = encodeURIComponent(cc);
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('SecretaryOfState/searchwithfilter/?q=' + searchText + '&f1=' + cn + '&f2=' + col + '&f3=' + ct + '&f4=' + cc + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchNICBFacets = function (searchText) {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        return this.httpClient.get('NICB/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchNICBRecordsWithFilter = function (searchText, ps, plt, pla, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        if (ps)
            ps = encodeURIComponent(ps);
        if (plt)
            plt = encodeURIComponent(plt);
        if (pla)
            pla = encodeURIComponent(pla);
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('NICB/searchwithfilter/?q=' + searchText + '&f1=' + ps + '&f2=' + plt + '&f3=' + pla + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchMBCOIGFacets = function (searchText) {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        return this.httpClient.get('MBCOIG/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchMBCOIGRecordsWithFilter = function (searchText, doay, at, sp, bt, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        if (doay)
            doay = encodeURIComponent(doay);
        if (at)
            at = encodeURIComponent(at);
        if (sp)
            sp = encodeURIComponent(sp);
        if (bt)
            bt = encodeURIComponent(bt);
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('MBCOIG/searchwithfilter/?q=' + searchText + '&f1=' + doay + '&f2=' + at + '&f3=' + sp + '&f4=' + bt + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchChiropractorFacets = function (searchText) {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        return this.httpClient.get('CED/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchChiropractorRecordsWithFilter = function (searchText, dt, ln, dod, state, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        if (dt)
            dt = encodeURIComponent(dt);
        if (ln)
            ln = encodeURIComponent(ln);
        if (dod)
            dod = encodeURIComponent(dod);
        if (state)
            state = encodeURIComponent(state);
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        return this.httpClient.get('CED/searchwithfilter/?q=' + searchText + '&f1=' + dt + '&f2=' + ln + '&f3=' + dod + '&f4=' + state + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchGDFacets = function (searchText) {
        if (!searchText)
            searchText = "";
        else
            searchText = encodeURIComponent(searchText);
        return this.httpClient.get('GeographicDiscrepancy/facets/search/?q=' + searchText).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchGDRecordsWithFilter = function (searchText, bar, dr, par, pn, tin, tbar, tpar, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";
        if (bar)
            bar = encodeURIComponent(bar);
        if (dr)
            dr = encodeURIComponent(dr);
        if (par)
            par = encodeURIComponent(par);
        if (pn)
            pn = encodeURIComponent(pn);
        if (tin)
            tin = encodeURIComponent(tin);
        if (tbar)
            tbar = encodeURIComponent(tbar);
        if (tpar)
            tpar = encodeURIComponent(tpar);
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        var params = new http_1.URLSearchParams();
        params.set('indent', 'on');
        params.set('q', encodeURIComponent(searchText)); // the user's search value
        params.set('wt', 'json');
        var startRecordNumber = (pageNumber * recordsToFetch) - recordsToFetch;
        var url = 'http://35.160.49.120:8983/solr/claimant_provider_distance_core/select?indent=on&q=' + encodeURIComponent(searchText) + '&rows=' + recordsToFetch + '&start=' + startRecordNumber + '&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
        // return this.httpClient.get('GeographicDiscrepancy/searchwithfilter/?q=' + searchText + '&f1=' + bar + '&f2=' + dr + '&f3=' + par + '&f4=' + pn + '&f5=' + tin + '&f6=' + tbar + '&f7=' + tpar + '&pageNumber=' + pageNumber + '&recordsToFetch=' + recordsToFetch).map(this.globals.extractData)
        //     .catch(this.globals.extractError);
        //indent=on&q=' + encodeURIComponent(searchText) + '&rows=20&start=0&wt=json&format=jsonP&callback=__ng_jsonp__.__req0.finished';
        // return this._jsonp.get(url, { search: params })
        //     .map(data => data.json());
    };
    FDAService.prototype.searchProviderSummaryFacets = function (searchText) {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";
        var url = 'http://data.apatics.net:8983/solr/provider_summary_core/select?q=' + encodeURIComponent(searchText) + '&rows=0&wt=json&indent=true&facet=true&facet.sort=index' +
            '&facet.field=PARTY_FIRST_NAME&facet.field=PROVIDER_SPECIALTY&facet.field=ZIP_CODE&facet.field=TAX_ID_NUMBER';
        return this.httpClient.get(url).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.searchProviderSummaryRecordsWithFilter = function (searchText, bar, dr, par, pn, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";
        if (bar)
            bar = encodeURIComponent(bar);
        if (dr)
            dr = encodeURIComponent(dr);
        if (par)
            par = encodeURIComponent(par);
        if (pn)
            pn = encodeURIComponent(pn);
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        var startRecordNumber = (pageNumber * recordsToFetch) - recordsToFetch;
        var url = 'http://data.apatics.net:8983/solr/provider_summary_core/select?indent=on&q=' + encodeURIComponent(searchText) + '&rows=' + recordsToFetch + '&start=' + startRecordNumber + '&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.getAuthToken = function () {
        var headers = new http_1.Headers();
        headers.append('authorization', 'Basic QXBhdGljc0FQSWFwcDpteS1zZWNyZXQtdG9rZW4tdG8tY2hhbmdlLWluLXByb2R1Y3Rpb24=');
        var body = new http_1.URLSearchParams();
        body.set('username', 'admin');
        body.set('password', 'admin');
        body.set('grant_type', 'password');
        body.set('scope', 'read write');
        body.set('client_secret', 'my-secret-token-to-change-in-production');
        body.set('client_id', 'ApaticsAPIapp');
        // let urlSearchParams = new URLSearchParams();
        // for (let key in obj) {
        //     urlSearchParams.append(key, obj[key]);
        // }
        return this.httpClient.postWithHeaders('http://54.191.44.17/oauth/token', body, headers)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.getProviderFacets = function (searchText) {
        var headers = new http_1.Headers();
        headers.append('authorization', 'Bearer ' + window.localStorage.getItem('apaticsAuthToken'));
        var objData = {};
        objData.q = searchText ? (searchText + "*") : "*";
        objData.fields = ["partyFirstName", "providerSpeciality", "zipCode", "taxIdNumber"];
        //let body = {"q": "*", "fields": ["partyFirstName", "providerSpeciality", "zipCode", "taxIdNumber"]};
        return this.httpClient.postWithHeaders('http://54.191.44.17/api/provider/get_facets', objData, headers)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.saveCaseStatus = function (objData) {
        return this.httpClient.post('CaseStatus', { data: objData }).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.getCaseStatus = function (taxIdNumber) {
        if (!taxIdNumber)
            taxIdNumber = "";
        // return this.httpClient.get('CaseStatus/' + taxIdNumber).map(this.globals.extractData)
        //     .catch(this.globals.extractError);
        //var url = "http://localhost/ApaticsTaskLiveRestAPI/v1/";
        var url = "http://casemgmt.apatics.net/api/v1/";
        return this.httpClient.get(url + 'CaseStatus/' + taxIdNumber).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.saveCase = function (objData) {
        //var url = "http://localhost/ApaticsTaskLiveRestAPI/v1/";
        var url = "http://casemgmt.apatics.net/api/v1/";
        return this.httpClient.post(url + 'CaseSave', { data: objData }).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    FDAService.prototype.getTwitterHashtagData = function () {
        return this.httpClient.get('Twitter/SearchHashtag').map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    return FDAService;
}());
FDAService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_client_1.HttpClient !== "undefined" && http_client_1.HttpClient) === "function" && _a || Object, typeof (_b = typeof http_1.Jsonp !== "undefined" && http_1.Jsonp) === "function" && _b || Object, typeof (_c = typeof common_1.DatePipe !== "undefined" && common_1.DatePipe) === "function" && _c || Object, typeof (_d = typeof global_1.GlobalClass !== "undefined" && global_1.GlobalClass) === "function" && _d || Object])
], FDAService);
exports.FDAService = FDAService;
var _a, _b, _c, _d;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/fda.service.js.map

/***/ }),

/***/ 421:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var GlobalEvents;
(function (GlobalEvents) {
    GlobalEvents[GlobalEvents["LOGIN_UPDATE"] = "LOGIN_UPDATE"] = "LOGIN_UPDATE";
})(GlobalEvents = exports.GlobalEvents || (exports.GlobalEvents = {}));
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/global.events.js.map

/***/ }),

/***/ 422:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Global = {
    staticRoot: 'assets/',
    userList: [
        //{ username: 'user1', password: 'pwd1' },
        //{ username: 'user2', password: 'pwd2' },
        { username: 'tbirur1', password: 'apatics@net' },
        { username: 'darges1', password: 'apatics@net' },
        { username: 'demo', password: 'demo' },
    ]
};
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/global.js.map

/***/ }),

/***/ 423:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(833));
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/index.js.map

/***/ }),

/***/ 424:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
//
var provider_outcome_service_1 = __webpack_require__(87);
var ClaimSegmentationComponent = (function () {
    function ClaimSegmentationComponent(providerOutcomeService) {
        this.providerOutcomeService = providerOutcomeService;
    }
    ClaimSegmentationComponent.prototype.ngOnInit = function () {
    };
    return ClaimSegmentationComponent;
}());
ClaimSegmentationComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1050),
        providers: [provider_outcome_service_1.ProviderOutcomeService],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof provider_outcome_service_1.ProviderOutcomeService !== "undefined" && provider_outcome_service_1.ProviderOutcomeService) === "function" && _a || Object])
], ClaimSegmentationComponent);
exports.ClaimSegmentationComponent = ClaimSegmentationComponent;
var _a;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/ClaimSegmentation.component.js.map

/***/ }),

/***/ 425:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
//
var provider_outcome_service_1 = __webpack_require__(87);
var timers_1 = __webpack_require__(709);
var OpioidComponent = (function () {
    function OpioidComponent(providerOutcomeService) {
        this.providerOutcomeService = providerOutcomeService;
        this.provideList = [];
        this.selectedProvider = [];
        this.selectedManager = [];
        this.managerList = [];
        this.tooltipData = [
            {
                "color": "#f7766d",
                "number": 1,
                "content": "#tooltip_1",
                "contentId": "tooltip_1",
                "tableData": [
                    {
                        "DERIVED_SCHEDULE": "GENERAL : 0",
                        "AGE_GROUP_1": "BELOW 50 : 0",
                        "GENDER": "FEMALE : 0",
                    },
                    {
                        "DERIVED_SCHEDULE": "OPIOID : 1930",
                        "AGE_GROUP_1": "OVER 50 :1930",
                        "GENDER": "MALE : 1930",
                    }
                ]
            },
            {
                "color": "#cc9600",
                "number": 2,
                "content": "#tooltip_2",
                "contentId": "tooltip_2",
                "tableData": [
                    {
                        "DERIVED_SCHEDULE": "GENERAL : 2046",
                        "AGE_GROUP_1": "BELOW 50 : 0",
                        "GENDER": "FEMALE : 0",
                    },
                    {
                        "DERIVED_SCHEDULE": "OPIOID : 0",
                        "AGE_GROUP_1": "OVER 50 : 2046",
                        "GENDER": "MALE  : 2046",
                    }
                ]
            },
            {
                "color": "#7cad00",
                "number": 3,
                "content": "#tooltip_3",
                "contentId": "tooltip_3",
                "tableData": [
                    {
                        "DERIVED_SCHEDULE": "GENERAL : 0",
                        "AGE_GROUP_1": "BELOW 50 : 878",
                        "GENDER": "FEMALE : 0",
                    },
                    {
                        "DERIVED_SCHEDULE": "OPIOID : 878",
                        "AGE_GROUP_1": "OVER 50 : 0",
                        "GENDER": "MALE  : 878",
                    }
                ]
            },
            {
                "color": "#009752",
                "number": 4,
                "content": "#tooltip_4",
                "contentId": "tooltip_4",
                "tableData": [
                    {
                        "DERIVED_SCHEDULE": "GENERAL : 1090",
                        "AGE_GROUP_1": "BELOW 50 : 1090",
                        "GENDER": "FEMALE : 0",
                    },
                    {
                        "DERIVED_SCHEDULE": "OPIOID : 0",
                        "AGE_GROUP_1": "OVER 50 : 0",
                        "GENDER": "MALE  : 1090",
                    }
                ]
            },
            {
                "color": "#00bec3",
                "number": 5,
                "content": "#tooltip_5",
                "contentId": "tooltip_5",
                "tableData": [
                    {
                        "DERIVED_SCHEDULE": "GENERAL : 1903",
                        "AGE_GROUP_1": "BELOW 50 : 0",
                        "GENDER": "FEMALE : 1903",
                    },
                    {
                        "DERIVED_SCHEDULE": "OPIOID : 0",
                        "AGE_GROUP_1": "OVER 50 : 1903",
                        "GENDER": "MALE  : 0",
                    }
                ]
            },
            {
                "color": "#00a8fe",
                "number": 6,
                "content": "#tooltip_6",
                "contentId": "tooltip_6",
                "tableData": [
                    {
                        "DERIVED_SCHEDULE": "GENERAL : 0",
                        "AGE_GROUP_1": "BELOW 50 : 0",
                        "GENDER": "FEMALE : 1524",
                    },
                    {
                        "DERIVED_SCHEDULE": "OPIOID : 1524",
                        "AGE_GROUP_1": "OVER 50 : 1524",
                        "GENDER": "MALE  : 0",
                    }
                ]
            },
            {
                "color": "#c67cfe",
                "number": 7,
                "content": "#tooltip_7",
                "contentId": "tooltip_7",
                "tableData": [
                    {
                        "DERIVED_SCHEDULE": "GENERAL : 0",
                        "AGE_GROUP_1": "BELOW 50 : 342",
                        "GENDER": "FEMALE : 342",
                    },
                    {
                        "DERIVED_SCHEDULE": "OPIOID : 342",
                        "AGE_GROUP_1": "OVER 50 : 0",
                        "GENDER": "MALE  : 0",
                    }
                ]
            },
            {
                "color": "#fe61cb",
                "number": 8,
                "content": "#tooltip_8",
                "contentId": "tooltip_8",
                "tableData": [
                    {
                        "DERIVED_SCHEDULE": "GENERAL : 536",
                        "AGE_GROUP_1": "BELOW 50 : 536",
                        "GENDER": "FEMALE : 536",
                    },
                    {
                        "DERIVED_SCHEDULE": "OPIOID : 0",
                        "AGE_GROUP_1": "OVER 50 : 0",
                        "GENDER": "MALE  : 0",
                    }
                ]
            }
        ];
        this.riskData = {
            'AtRisk': {
                'NewClaimants': [
                    { 'name': 'Alvis Klimpt', 'id': '1985459', 'comments': 'Prior Hepatitis C, Age, Demographic' },
                    { 'name': 'Charisse Swetmore', 'id': '1940266', 'comments': 'Prior AA, criminal history, age' },
                    { 'name': 'Kevin Bestiman', 'id': '3628763', 'comments': 'Prior ICD, unit exceeded' }
                ],
                'AssignedClaimants': [
                    { 'name': 'Antonius Arnout', 'id': '3736645', 'caseManager': 'Delia', 'note': 'Patient is getting better. Provider prescribed Tylenol.' },
                    { 'name': 'Fraser Dytham', 'id': '3391799', 'caseManager': 'Will', 'note': 'Patient willing to go for rehab after the injury treatment.' },
                    { 'name': 'Mureil Ivison', 'id': '2810867', 'caseManager': 'Frank', 'note': 'Patient admitted to the hospital for overdose.' }
                ]
            },
            'MediumRisk': {
                'NewClaimants': [
                    { 'name': 'Colas MacCook', 'id': '2093649', 'comments': 'Age, Demographic' },
                    { 'name': 'Glen Haggath', 'id': '1573234', 'comments': 'age, prolonged injury' },
                    { 'name': 'Livvy Dedrick', 'id': '1746951', 'comments': 'Third claim in 1 year' }
                ],
                'AssignedClaimants': [
                    { 'name': 'Trueman Tabrett', 'id': '3736645', 'caseManager': 'Delia', 'note': 'Patient is getting better. Provider prescribed Tylenol.' },
                    { 'name': 'Wenonah Pace', 'id': '3391799', 'caseManager': 'Will', 'note': 'Patient willing to go for rehab after the injury treatment.' },
                    { 'name': 'Ida Ho', 'id': '2814741', 'caseManager': 'Frank', 'note': 'Patient admitted to the hospital for overdose.' }
                ]
            }
        };
        this.isShowAtRisk = true;
        this.isShowMediumRisk = false;
        this.isShowLoad = false;
    }
    OpioidComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('this.tooltipData', this.tooltipData);
        console.log('this.riskData', this.riskData);
        this.providerOutcomeService.getAllSideBarFilterForClaimant('').subscribe(function (res) {
            //console.log('getAllSideBarFilterForClaimant res', res);
            var tempArr1 = [];
            for (var i = 0; i < res.providers.length; i++) {
                tempArr1.push({ id: res.providers[i]['id'], text: res.providers[i]['name'] });
            }
            _this.provideList = tempArr1;
            var tempArr2 = [];
            for (var i = 0; i < res.attorneys.length; i++) {
                tempArr2.push({ id: res.attorneys[i]['id'], text: res.attorneys[i]['name'] });
            }
            _this.managerList = tempArr2;
            console.log('this.provideList', _this.provideList);
            console.log('this.managerList', _this.managerList);
        }, function (err) {
        });
    };
    OpioidComponent.prototype.ngAfterViewInit = function () {
        jQuery(document).ready(function () {
            jQuery('.opioid-tooltip').tooltipster();
        });
    };
    OpioidComponent.prototype.activeSideNavLink = function (index) {
        if (index == 1) {
            this.isShowAtRisk = true;
            this.isShowMediumRisk = false;
        }
        else {
            this.isShowAtRisk = false;
            this.isShowMediumRisk = true;
        }
    };
    OpioidComponent.prototype.openInformProviderModel = function () {
        this.InformProviderModel.show();
    };
    OpioidComponent.prototype.closeInformProviderModel = function () {
        this.InformProviderModel.hide();
    };
    OpioidComponent.prototype.sendMailToProvider = function () {
        var _this = this;
        this.isShowLoad = true;
        timers_1.setTimeout(function () {
            _this.selectedProvider = [];
            _this.isShowLoad = false;
            jQuery('#ProvideMsgScccess1').show().delay(3000).fadeOut();
            timers_1.setTimeout(function () {
                _this.InformProviderModel.hide();
            }, 5000);
        }, 3500);
    };
    OpioidComponent.prototype.openAssignManagerModel = function () {
        this.AssignManagerModel.show();
    };
    OpioidComponent.prototype.closeAssignManagerModel = function () {
        this.AssignManagerModel.hide();
    };
    OpioidComponent.prototype.AssignManager = function () {
        var _this = this;
        this.isShowLoad = true;
        timers_1.setTimeout(function () {
            _this.selectedManager = [];
            _this.isShowLoad = false;
            jQuery('#ProvideMsgScccess2').show().delay(3000).fadeOut();
            timers_1.setTimeout(function () {
                _this.AssignManagerModel.hide();
            }, 5000);
        }, 3500);
    };
    return OpioidComponent;
}());
__decorate([
    core_1.ViewChild('InformProviderModel'),
    __metadata("design:type", Object)
], OpioidComponent.prototype, "InformProviderModel", void 0);
__decorate([
    core_1.ViewChild('AssignManagerModel'),
    __metadata("design:type", Object)
], OpioidComponent.prototype, "AssignManagerModel", void 0);
OpioidComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1051),
        providers: [provider_outcome_service_1.ProviderOutcomeService],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof provider_outcome_service_1.ProviderOutcomeService !== "undefined" && provider_outcome_service_1.ProviderOutcomeService) === "function" && _a || Object])
], OpioidComponent);
exports.OpioidComponent = OpioidComponent;
var _a;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/Opioid.component.js.map

/***/ }),

/***/ 426:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var fda_service_1 = __webpack_require__(39);
var rxjs_1 = __webpack_require__(31);
var ChiropractorComponent = (function () {
    function ChiropractorComponent(zone, fdaService, _elmRef) {
        this.zone = zone;
        this.fdaService = fdaService;
        this._elmRef = _elmRef;
        this.selectedProductId = "";
        this.selectedChiropractor = {};
        this.pageNumber = 1;
        this.recordsToFetch = 10;
        this.totalRecords = 0;
        this.resFacets = {};
        this.searchChiropractorText = "";
        this.selectedDT = [];
        this.selectedLN = [];
        this.selectedDOD = [];
        this.selectedSTATE = [];
        this.rersChiropractor = [];
        this.foundRecord = true;
    }
    ChiropractorComponent.prototype.searchChiropractorFacets = function () {
        var _this = this;
        this.fdaService.searchChiropractorFacets(this.searchChiropractorText)
            .subscribe(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.resFacets = res.response;
                });
            }
        }, function (error) {
        });
    };
    ChiropractorComponent.prototype.changeSelectFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            var changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    _this.selectedDT.push(objFacet.name);
                else if (index == 2)
                    _this.selectedLN.push(objFacet.name);
                else if (index == 3)
                    _this.selectedDOD.push(objFacet.name);
                else if (index == 4)
                    _this.selectedSTATE.push(objFacet.name);
            }
            else {
                var arrayIndex = -1;
                if (index == 1) {
                    arrayIndex = _this.selectedDT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedDT.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = _this.selectedLN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedLN.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = _this.selectedDOD.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedDOD.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = _this.selectedSTATE.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedSTATE.splice(arrayIndex, 1);
                }
            }
        });
        this.searchChiropractorRecordsWithFilters(1);
    };
    ChiropractorComponent.prototype.removeSelectedFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            objFacet.selected = false;
            var arrayIndex = -1;
            if (index == 1) {
                arrayIndex = _this.selectedDT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedDT.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = _this.selectedLN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedLN.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = _this.selectedDOD.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedDOD.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = _this.selectedSTATE.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedSTATE.splice(arrayIndex, 1);
            }
        });
        this.searchChiropractorRecordsWithFilters(1);
    };
    ChiropractorComponent.prototype.clearSelectedFacets = function () {
        this.selectedDT = [];
        this.selectedLN = [];
        this.selectedDOD = [];
        this.selectedSTATE = [];
    };
    ChiropractorComponent.prototype.searchChiropractor = function () {
        this.clearSelectedFacets();
        this.searchChiropractorRecordsWithFilters(1);
        this.searchChiropractorFacets();
    };
    ChiropractorComponent.prototype.searchChiropractorRecordsWithFilters = function (pageNum) {
        var _this = this;
        this.pageNumber = pageNum;
        var selectedDTString = "";
        var selectedLNString = "";
        var selectedDODString = "";
        var selectedSTATEString = "";
        if (this.selectedDT && this.selectedDT.length > 0)
            selectedDTString = this.selectedDT.join();
        if (this.selectedLN && this.selectedLN.length > 0)
            selectedLNString = this.selectedLN.join();
        if (this.selectedDOD && this.selectedDOD.length > 0)
            selectedDODString = this.selectedDOD.join();
        if (this.selectedSTATE && this.selectedSTATE.length > 0)
            selectedSTATEString = this.selectedSTATE.join();
        this.asyncChiropractor = this.fdaService.searchChiropractorRecordsWithFilter(this.searchChiropractorText, selectedDTString, selectedLNString, selectedDODString, selectedSTATEString, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.totalRecords;
                    _this.rersChiropractor = res.response.results || [];
                    _this.foundRecord = res.response.totalRecords > 0;
                });
            }
            else {
                _this.zone.run(function () {
                    _this.totalRecords = 0;
                    _this.rersChiropractor = [];
                });
            }
        })
            .map(function (res) { return res.success ? res.response.results : []; });
    };
    ChiropractorComponent.prototype.ngOnInit = function () {
        this.searchChiropractorRecordsWithFilters(1);
        this.searchChiropractorFacets();
    };
    return ChiropractorComponent;
}());
ChiropractorComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1052),
        providers: [fda_service_1.FDAService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.NgZone !== "undefined" && core_1.NgZone) === "function" && _a || Object, typeof (_b = typeof fda_service_1.FDAService !== "undefined" && fda_service_1.FDAService) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object])
], ChiropractorComponent);
exports.ChiropractorComponent = ChiropractorComponent;
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
var _a, _b, _c;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/chiropractor.component.js.map

/***/ }),

/***/ 427:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(38);
var fda_service_1 = __webpack_require__(39);
var http_1 = __webpack_require__(37);
var session_service_1 = __webpack_require__(104);
var DashboardComponent = (function () {
    function DashboardComponent(router, _sessionService, fdaService, _jsonp) {
        this.router = router;
        this._sessionService = _sessionService;
        this.fdaService = fdaService;
        this._jsonp = _jsonp;
        this.loggedIn = false;
        this.disabled = false;
        this.status = { isopen: false };
    }
    DashboardComponent.prototype.toggleDropdown = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    };
    DashboardComponent.prototype.logOut = function () {
        var _this = this;
        this._sessionService.clearUserSession().subscribe(function (res) {
            _this.loggedIn = false;
            _this.router.navigate(['/login']);
        }, function (err) {
            console.log('Error while clearing session data.');
        });
    };
    DashboardComponent.prototype.getAuthenticationToken = function () {
        this.fdaService.getAuthToken()
            .subscribe(function (res) {
            if (res.access_token) {
                window.localStorage.setItem('apaticsAuthToken', res.access_token);
            }
        }, function (error) {
        });
    };
    DashboardComponent.prototype.ngOnInit = function () {
        //this.getAuthenticationToken();
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1053),
        providers: [fda_service_1.FDAService, session_service_1.SessionService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _a || Object, typeof (_b = typeof session_service_1.SessionService !== "undefined" && session_service_1.SessionService) === "function" && _b || Object, typeof (_c = typeof fda_service_1.FDAService !== "undefined" && fda_service_1.FDAService) === "function" && _c || Object, typeof (_d = typeof http_1.Jsonp !== "undefined" && http_1.Jsonp) === "function" && _d || Object])
], DashboardComponent);
exports.DashboardComponent = DashboardComponent;
var _a, _b, _c, _d;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/dashboard.component.js.map

/***/ }),

/***/ 428:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var fda_service_1 = __webpack_require__(39);
var rxjs_1 = __webpack_require__(31);
var DCAComponent = (function () {
    function DCAComponent(zone, fdaService, _elmRef) {
        this.zone = zone;
        this.fdaService = fdaService;
        this._elmRef = _elmRef;
        this.selectedProductId = "";
        this.selectedDCA = {};
        this.pageNumber = 1;
        this.recordsToFetch = 10;
        this.totalRecords = 0;
        this.resFacets = {};
        this.searchDCAText = "";
        this.selectedAN = [];
        this.selectedLT = [];
        this.selectedSC = [];
        this.selectedLS = [];
        this.rersDCA = [];
        this.foundRecord = true;
    }
    DCAComponent.prototype.searchDCAFacets = function () {
        var _this = this;
        this.fdaService.searchDCAFacets(this.searchDCAText)
            .subscribe(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.resFacets = res.response;
                });
            }
        }, function (error) {
        });
    };
    DCAComponent.prototype.changeSelectFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            var changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    _this.selectedAN.push(objFacet.name);
                else if (index == 2)
                    _this.selectedLT.push(objFacet.name);
                else if (index == 3)
                    _this.selectedSC.push(objFacet.name);
                else if (index == 4)
                    _this.selectedLS.push(objFacet.name);
            }
            else {
                var arrayIndex = -1;
                if (index == 1) {
                    arrayIndex = _this.selectedAN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedAN.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = _this.selectedLT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedLT.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = _this.selectedSC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedSC.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = _this.selectedLS.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedLS.splice(arrayIndex, 1);
                }
            }
        });
        this.searchDCARecordsWithFilters(1);
    };
    DCAComponent.prototype.removeSelectedFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            objFacet.selected = false;
            var arrayIndex = -1;
            if (index == 1) {
                arrayIndex = _this.selectedAN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedAN.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = _this.selectedLT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedLT.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = _this.selectedSC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedSC.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = _this.selectedLS.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedLS.splice(arrayIndex, 1);
            }
        });
        this.searchDCARecordsWithFilters(1);
    };
    DCAComponent.prototype.clearSelectedFacets = function () {
        this.selectedAN = [];
        this.selectedLT = [];
        this.selectedSC = [];
        this.selectedLS = [];
        this.resFacets.agencyNames.forEach(function (objFacet) {
            objFacet.selected = false;
        });
        this.resFacets.licenseTypes.forEach(function (objFacet) {
            objFacet.selected = false;
        });
        this.resFacets.specialityCodes.forEach(function (objFacet) {
            objFacet.selected = false;
        });
        this.resFacets.licenseStatuses.forEach(function (objFacet) {
            objFacet.selected = false;
        });
    };
    DCAComponent.prototype.searchDCA = function () {
        this.clearSelectedFacets();
        this.searchDCARecordsWithFilters(1);
        this.searchDCAFacets();
    };
    DCAComponent.prototype.searchDCARecordsWithFilters = function (pageNum) {
        var _this = this;
        this.pageNumber = pageNum;
        var selectedANString = "";
        var selectedLTString = "";
        var selectedSCString = "";
        var selectedLSString = "";
        if (this.selectedAN && this.selectedAN.length > 0)
            selectedANString = this.selectedAN.join();
        if (this.selectedLT && this.selectedLT.length > 0)
            selectedLTString = this.selectedLT.join();
        if (this.selectedSC && this.selectedSC.length > 0)
            selectedSCString = this.selectedSC.join();
        if (this.selectedLS && this.selectedLS.length > 0)
            selectedLSString = this.selectedLS.join();
        this.asyncDCA = this.fdaService.searchDCARecordsWithFilter(this.searchDCAText, selectedANString, selectedLTString, selectedSCString, selectedLSString, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.totalRecords;
                    _this.rersDCA = res.response.results || [];
                    _this.foundRecord = res.response.totalRecords > 0;
                });
            }
            else {
                _this.zone.run(function () {
                    _this.totalRecords = 0;
                    _this.rersDCA = [];
                });
            }
        })
            .map(function (res) { return res.success ? res.response.results : []; });
    };
    DCAComponent.prototype.ngOnInit = function () {
        this.searchDCARecordsWithFilters(1);
        this.searchDCAFacets();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    };
    return DCAComponent;
}());
DCAComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1054),
        providers: [fda_service_1.FDAService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.NgZone !== "undefined" && core_1.NgZone) === "function" && _a || Object, typeof (_b = typeof fda_service_1.FDAService !== "undefined" && fda_service_1.FDAService) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object])
], DCAComponent);
exports.DCAComponent = DCAComponent;
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
var _a, _b, _c;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/dca.component.js.map

/***/ }),

/***/ 429:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var platform_browser_1 = __webpack_require__(85);
var DNBComponent = (function () {
    function DNBComponent(domSanitizer) {
        this.domSanitizer = domSanitizer;
        this.pdfSrc = '/assets/pdf/DNB_364667645.pdf';
        // or pass options as object
        // pdfSrc: any = {
        //   url: './pdf-test.pdf',
        //   withCredentials: true,
        //// httpHeaders: { // cross domain
        ////   'Access-Control-Allow-Credentials': true
        //// }
        // };
        this.page = 1;
        this.rotation = 0;
        this.zoom = 1.0;
        this.originalSize = false;
        this.showAll = true;
        this.renderText = true;
        this.afterLoadComplete = this.afterLoadComplete.bind(this);
    }
    DNBComponent.prototype.incrementPage = function (amount) {
        this.page += amount;
    };
    DNBComponent.prototype.incrementZoom = function (amount) {
        this.zoom += amount;
    };
    DNBComponent.prototype.rotate = function (angle) {
        this.rotation += angle;
    };
    /**
     * Render PDF preview on selecting file
     */
    DNBComponent.prototype.onFileSelected = function () {
        var _this = this;
        var $img = document.querySelector('#file');
        if (typeof (FileReader) !== 'undefined') {
            var reader = new FileReader();
            reader.onload = function (e) {
                _this.pdfSrc = e.target.result;
            };
            reader.readAsArrayBuffer($img.files[0]);
        }
    };
    /**
     * Get pdf information after it's loaded
     * @param pdf
     */
    DNBComponent.prototype.afterLoadComplete = function (pdf) {
        this.pdf = pdf;
    };
    DNBComponent.prototype.ngOnInit = function () {
        this.pageurl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.pdfSrc);
    };
    return DNBComponent;
}());
DNBComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1055)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof platform_browser_1.DomSanitizer !== "undefined" && platform_browser_1.DomSanitizer) === "function" && _a || Object])
], DNBComponent);
exports.DNBComponent = DNBComponent;
var _a;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/dnb.component.js.map

/***/ }),

/***/ 430:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(38);
var solr_service_1 = __webpack_require__(449);
var rxjs_1 = __webpack_require__(31);
var common_1 = __webpack_require__(10);
var Angular2_csv_1 = __webpack_require__(451);
var DuplicateBillingComponent = (function () {
    function DuplicateBillingComponent(zone, solrervice, _elmRef, route, datePipe) {
        this.zone = zone;
        this.solrervice = solrervice;
        this._elmRef = _elmRef;
        this.route = route;
        this.datePipe = datePipe;
        this.pageNumber = 1;
        this.recordsToFetch = 10;
        this.totalRecords = 0;
        this.resFacets = {};
        this.searchDuplicateBillingText = "";
        this.selectedVN = [];
        this.selectedDOS = [];
        this.rersDuplicateBilling = [];
        this.foundRecord = true;
        this.gPanelShow = -1;
        this.pageName = "Double Billing";
        // //dynamic Tabs
        // public angular2TabsExample: Array<any> = [
        //     { title: 'Angular Tab 1', content: 'Angular 2 Tabs are navigable windows, each window (called tab) contains content', disabled: false, removable: true },
        //     { title: 'Angular Tab 2', content: 'generally we categorize contents depending on the theme', disabled: false, removable: true },
        //     { title: 'Angular Tab 3', content: 'Angular 2 Tabs Content', disabled: false, removable: true }
        // ];
        // //on select a tab do something
        // public doOnTabSelect(currentTab: any) {
        //     console.log("doOnTabSelect" + currentTab);
        // };
        // //on remove Tab do something
        // public removeThisTabHandler(tab: any) {
        //     console.log('Remove Tab handler' + tab);
        // };
        this.billedAmountRange = [0, 150000];
        this.billedAmountConfig = {
            connect: true,
            start: [0, 150000],
            //step: 15000,
            pageSteps: 10,
            range: {
                min: 0,
                max: 150000
            },
            format: {
                to: function (value) {
                    return parseFloat(value.toFixed());
                },
                from: function (value) {
                    return parseFloat(value).toFixed(2);
                }
            }
        };
        this.paidAmountRange = [0, 150000];
        this.paidAmountConfig = {
            connect: true,
            start: [0, 150000],
            //step: 15000,
            pageSteps: 10,
            range: {
                min: 0,
                max: 150000
            },
            format: {
                to: function (value) {
                    return parseFloat(value.toFixed());
                },
                from: function (value) {
                    return parseFloat(value).toFixed(2);
                }
            }
        };
        this.taxIdNumber = -1;
    }
    DuplicateBillingComponent.prototype.exportCSV = function () {
        var options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            fields: ["vendorName1", "vendorName2", "billReceivedDate2", "vendorName1", "billedCPTDescription", "billReceivedDate1", "paidCPTCode2",
                "billedAmount1", "typeOfBillCode", "paidCPTCode1", "billedAmount2", "taxIDNumber2", "dateOfServiceYear", "taxIDNumber1", "paidAmount2",
                "typeOfBillDescription", "paidAmount1", "billedCPTCode", "dateOfService", "paidCPTDescription1", "paidCPTDescription2"
            ],
            fieldNames: ["Vendor Name 1", "Vendor Name 2", "Bill Received Date 2", "Vendor Name 1", "Billed CPT Description", "Bill Received Date 1", "Paid CPT Code 2",
                "Billed Amount 1", "Type Of Bill Code", "Paid CPT Code 1", "Billed Amount 2", "Tax ID Number 2", "Date of Service Year", "Tax ID Number 1", "Paid Amount 2",
                "Type of Bill Description", "Paid Amount 1", "Billed CPT Code", "Date of Service", "Paid CPT Description 1", "Paid CPT Description 2"
            ]
        };
        var date = new Date();
        var objCSV = [];
        // objCSV.push('Duplicate Billing');
        objCSV.push(JSON.parse(JSON.stringify(this.rersDuplicateBilling)));
        objCSV.forEach(function (element) {
            if (Object.prototype.toString.call(element) === '[object Array]') {
                element.forEach(function (element1) {
                    for (var field in element1) {
                        if ('_version_,id,DNB_REPORT_LINK,LEXIS_NEXIS_REPORT,PROFILE_PICTURE'.indexOf(field) > -1) {
                            delete element1[field];
                        }
                    }
                });
            }
        });
        var res = new Angular2_csv_1.Angular2Csv(objCSV, 'Duplicate Billing - ' + date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate(), options);
        //console.info('res', res);
    };
    DuplicateBillingComponent.prototype.onChangeBilledAmount = function (event) {
        this.searchDuplicateBillingRecordsWithFilters(1);
    };
    DuplicateBillingComponent.prototype.onChangePaidAmount = function (event) {
        this.searchDuplicateBillingRecordsWithFilters(1);
    };
    DuplicateBillingComponent.prototype.searchDuplicateBillingFacets = function () {
        var _this = this;
        this.solrervice.searchDuplicateBillingFacets(this.taxIdNumber, this.searchDuplicateBillingText)
            .subscribe(function (res) {
            if (res.facet_counts) {
                _this.zone.run(function () {
                    var facetArray = [];
                    _this.resFacets = {};
                    if (res.facet_counts.facet_fields) {
                        if (res.facet_counts.facet_fields["dateOfService"] && res.facet_counts.facet_fields["dateOfService"].length > 0) {
                            for (var index = 0; index < res.facet_counts.facet_fields["dateOfService"].length; index++) {
                                if (res.facet_counts.facet_fields["dateOfService"][index + 1] > 0)
                                    facetArray.push({ name: _this.datePipe.transform((res.facet_counts.facet_fields["dateOfService"][index]), "dd-MMM-yyyy"), recordCount: res.facet_counts.facet_fields["dateOfService"][++index] });
                            }
                            _this.resFacets.dateOfService = facetArray;
                        }
                        if (res.facet_counts.facet_fields["vendorName1"] && res.facet_counts.facet_fields["vendorName1"].length > 0) {
                            facetArray = [];
                            for (var index = 0; index < res.facet_counts.facet_fields["vendorName1"].length; index++) {
                                if (res.facet_counts.facet_fields["vendorName1"][index + 1] > 0)
                                    facetArray.push({ name: res.facet_counts.facet_fields["vendorName1"][index], recordCount: res.facet_counts.facet_fields["vendorName1"][++index] });
                            }
                            _this.resFacets.vendorName1 = facetArray;
                        }
                        if (res.facet_counts.facet_fields["billedAmount1"] && res.facet_counts.facet_fields["billedAmount1"].length > 0) {
                            var maxAmount = 0;
                            for (var index = 0; index < res.facet_counts.facet_fields["billedAmount1"].length; index++) {
                                if (parseFloat(res.facet_counts.facet_fields["billedAmount1"][index]) > maxAmount)
                                    maxAmount = parseFloat(res.facet_counts.facet_fields["billedAmount1"][index]);
                                index++;
                            }
                            _this.resFacets.maxBilledAmount = maxAmount;
                        }
                        if (res.facet_counts.facet_fields["paidAmount1"] && res.facet_counts.facet_fields["paidAmount1"].length > 0) {
                            var maxAmount = 0;
                            for (var index = 0; index < res.facet_counts.facet_fields["paidAmount1"].length; index++) {
                                if (parseFloat(res.facet_counts.facet_fields["paidAmount1"][index]) > maxAmount)
                                    maxAmount = parseFloat(res.facet_counts.facet_fields["billedAmount1"][index]);
                                index++;
                            }
                            _this.resFacets.maxPaidAmount = maxAmount;
                        }
                        if (_this.resFacets.maxBilledAmount) {
                            _this.billedAmountConfig.range = { min: 0, max: _this.resFacets.maxBilledAmount };
                            _this.billedAmountRange = [0, _this.resFacets.maxBilledAmount];
                        }
                        if (_this.resFacets.maxPaidAmount) {
                            _this.paidAmountConfig.range.max = _this.resFacets.maxPaidAmount;
                            _this.paidAmountRange = [0, _this.resFacets.maxPaidAmount];
                        }
                        //console.log('this.resFacets', this.resFacets);
                    }
                });
            }
        }, function (error) {
        });
    };
    DuplicateBillingComponent.prototype.changeSelectFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            var changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    _this.selectedVN.push(objFacet.name);
                else if (index == 2)
                    _this.selectedDOS.push(objFacet.name);
            }
            else {
                var arrayIndex = -1;
                if (index == 1) {
                    arrayIndex = _this.selectedVN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedVN.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = _this.selectedDOS.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedDOS.splice(arrayIndex, 1);
                }
            }
        });
        this.searchDuplicateBillingRecordsWithFilters(1);
    };
    DuplicateBillingComponent.prototype.removeSelectedFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            objFacet.selected = false;
            var arrayIndex = -1;
            if (index == 1) {
                arrayIndex = _this.selectedVN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedVN.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = _this.selectedDOS.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedDOS.splice(arrayIndex, 1);
            }
        });
        this.searchDuplicateBillingRecordsWithFilters(1);
    };
    DuplicateBillingComponent.prototype.clearSelectedFacets = function () {
        this.selectedVN = [];
        this.selectedDOS = [];
        this.billedAmountRange = [0, 0];
        this.paidAmountRange = [0, 0];
    };
    DuplicateBillingComponent.prototype.searchDuplicateBilling = function () {
        this.clearSelectedFacets();
        this.searchDuplicateBillingRecordsWithFilters(1);
        this.searchDuplicateBillingFacets();
    };
    DuplicateBillingComponent.prototype.searchDuplicateBillingRecordsWithFilters = function (pageNum) {
        var _this = this;
        this.pageNumber = pageNum;
        var selectedVNString = "";
        var selectedDOSString = "";
        var selectedBARtring = "";
        var selectedPARString = "";
        if (this.selectedVN && this.selectedVN.length > 0)
            selectedVNString = this.selectedVN.join('"OR"');
        if (this.selectedDOS && this.selectedDOS.length > 0) {
            var dosArray = [];
            for (var index = 0; index < this.selectedDOS.length; index++) {
                dosArray.push(this.datePipe.transform(this.selectedDOS[index], "yyyy-MM-dd") + 'T00:00:00.000Z');
            }
            //console.log('this.selectedDOS', dosArray);
            selectedDOSString = dosArray.join('"OR"');
        }
        if (this.resFacets.maxBilledAmount && this.resFacets.maxBilledAmount > 0 && this.billedAmountRange && this.billedAmountRange.length > 1
            && this.billedAmountRange[0] > 0 || this.billedAmountRange[1] < this.resFacets.maxBilledAmount)
            selectedBARtring = '[' + this.billedAmountRange.join(' TO ') + ']';
        if (this.resFacets.maxPaidAmount && this.resFacets.maxPaidAmount > 0 && this.paidAmountRange && this.paidAmountRange.length > 1
            && this.paidAmountRange[0] > 0 || this.paidAmountRange[1] < this.resFacets.maxPaidAmount)
            selectedPARString = '[' + this.paidAmountRange.join(' TO ') + ']';
        this.asyncDuplicateBilling = this.solrervice.searchDuplicateBillingRecordsWithFilter(this.taxIdNumber, this.searchDuplicateBillingText, selectedVNString, selectedDOSString, selectedBARtring, selectedPARString, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            if (res.response) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.numFound;
                    _this.rersDuplicateBilling = res.response.docs || [];
                    _this.foundRecord = res.response.numFound > 0;
                    //console.info('rersDuplicateBilling', this.rersDuplicateBilling)
                    //console.info('this.asyncDuplicateBilling', this.asyncDuplicateBilling);
                });
            }
            else {
                _this.zone.run(function () {
                    _this.totalRecords = 0;
                    _this.rersDuplicateBilling = [];
                });
            }
            _this;
        })
            .map(function (res) { return res.response ? res.response.docs : []; });
        //console.info('this.asyncDuplicateBilling', this.asyncDuplicateBilling);
    };
    DuplicateBillingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            console.log('queryParams', params);
            if (params["taxIdNumber"]) {
                _this.taxIdNumber = params["taxIdNumber"];
                //setting static number
                _this.taxIdNumber = 954885742;
                _this.searchDuplicateBillingFacets();
                _this.searchDuplicateBillingRecordsWithFilters(1);
            }
            if (params['p']) {
                switch (params['p'].toString()) {
                    case "1":
                        _this.pageName = "Upcoding";
                        break;
                    case "2":
                        _this.pageName = "Excessive Testing";
                        break;
                    case "3":
                        _this.pageName = "Denied Claims";
                        break;
                    case "4":
                        _this.pageName = "Duplicate Billing";
                        break;
                    case "5":
                        _this.pageName = "Excessive DME Prescription";
                        break;
                    case "6":
                        _this.pageName = "Excess Imaging Services";
                        break;
                    case "7":
                        _this.pageName = "Cluster Billing";
                        break;
                    case "8":
                        _this.pageName = "Pain Pumps";
                        break;
                    case "9":
                        _this.pageName = "Autologous Transfusion";
                        break;
                    default:
                        break;
                }
            }
        });
    };
    return DuplicateBillingComponent;
}());
DuplicateBillingComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1056),
        providers: [solr_service_1.SOLRService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.NgZone !== "undefined" && core_1.NgZone) === "function" && _a || Object, typeof (_b = typeof solr_service_1.SOLRService !== "undefined" && solr_service_1.SOLRService) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object, typeof (_d = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _d || Object, typeof (_e = typeof common_1.DatePipe !== "undefined" && common_1.DatePipe) === "function" && _e || Object])
], DuplicateBillingComponent);
exports.DuplicateBillingComponent = DuplicateBillingComponent;
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
var _a, _b, _c, _d, _e;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/duplicatebilling.component.js.map

/***/ }),

/***/ 431:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var fda_service_1 = __webpack_require__(39);
var rxjs_1 = __webpack_require__(31);
var ESIComponent = (function () {
    function ESIComponent(zone, fdaService, _elmRef) {
        this.zone = zone;
        this.fdaService = fdaService;
        this._elmRef = _elmRef;
        this.selectedProductId = "";
        this.selectedESI = {};
        this.pageNumber = 1;
        this.recordsToFetch = 10;
        this.totalRecords = 0;
        this.resFacets = {};
        this.searchESIText = "";
        this.selectedPHN = [];
        this.selectedPLC = [];
        this.selectedPZC = [];
        this.selectedPLN = [];
        this.selectedCC = [];
        this.selectedDT = [];
        this.rersESI = [];
        this.foundRecord = true;
        this.billedAmountRange = [0, 5000];
        this.billedAmountConfig = {
            connect: true,
            start: [0, 5000],
            step: 100,
            pageSteps: 10,
            range: {
                min: 0,
                max: 5000
            },
            format: {
                to: function (value) {
                    return parseFloat(value.toFixed());
                },
                from: function (value) {
                    return parseFloat(value).toFixed(2);
                }
            }
        };
        this.claimAmountRange = [0, 90000];
        this.claimAmountConfig = {
            connect: true,
            start: [0, 90000],
            step: 5000,
            pageSteps: 0,
            range: {
                min: 0,
                max: 90000
            },
            format: {
                to: function (value) {
                    return parseFloat(value.toFixed());
                },
                from: function (value) {
                    return parseFloat(value).toFixed(2);
                }
            }
        };
    }
    ESIComponent.prototype.onChangeBilledAmount = function (event) {
        this.searchESIRecordsWithFilters(1);
    };
    ESIComponent.prototype.onChangeClaimAmount = function (event) {
        this.searchESIRecordsWithFilters(1);
    };
    ESIComponent.prototype.searchESIFacets = function () {
        var _this = this;
        this.fdaService.searchESIFacets(this.searchESIText)
            .subscribe(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.resFacets = res.response;
                });
            }
        }, function (error) {
        });
    };
    ESIComponent.prototype.changeSelectFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            var changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    _this.selectedPHN.push(objFacet.name);
                else if (index == 2)
                    _this.selectedPLC.push(objFacet.name);
                else if (index == 3)
                    _this.selectedPZC.push(objFacet.name);
                else if (index == 4)
                    _this.selectedPLN.push(objFacet.name);
                else if (index == 5)
                    _this.selectedCC.push(objFacet.name);
                else if (index == 6)
                    _this.selectedDT.push(objFacet.name);
            }
            else {
                var arrayIndex = -1;
                if (index == 1) {
                    arrayIndex = _this.selectedPHN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPHN.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = _this.selectedPLC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPLC.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = _this.selectedPZC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPZC.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = _this.selectedPLN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPLN.splice(arrayIndex, 1);
                }
                else if (index == 5) {
                    arrayIndex = _this.selectedCC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedCC.splice(arrayIndex, 1);
                }
                else if (index == 6) {
                    arrayIndex = _this.selectedDT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedDT.splice(arrayIndex, 1);
                }
            }
        });
        this.searchESIRecordsWithFilters(1);
    };
    ESIComponent.prototype.removeSelectedFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            objFacet.selected = false;
            var arrayIndex = -1;
            if (index == 1) {
                arrayIndex = _this.selectedPHN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPHN.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = _this.selectedPLC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPLC.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = _this.selectedPZC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPZC.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = _this.selectedPLN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPLN.splice(arrayIndex, 1);
            }
            else if (index == 5) {
                arrayIndex = _this.selectedCC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedCC.splice(arrayIndex, 1);
            }
            else if (index == 6) {
                arrayIndex = _this.selectedDT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedDT.splice(arrayIndex, 1);
            }
        });
        this.searchESIRecordsWithFilters(1);
    };
    ESIComponent.prototype.clearSelectedFacets = function () {
        this.selectedPHN = [];
        this.selectedPLC = [];
        this.selectedPZC = [];
        this.selectedPLN = [];
        this.selectedCC = [];
        this.selectedDT = [];
        this.billedAmountRange = [0, 5000];
        this.claimAmountRange = [0, 90000];
        // this.resFacets.pharmacyNames.forEach(objFacet => {
        //     objFacet.selected = false;
        // });
        // this.resFacets.pharmacyLocationCities.forEach(objFacet => {
        //     objFacet.selected = false;
        // });
        // this.resFacets.pharmacyZipCodes.forEach(objFacet => {
        //     objFacet.selected = false;
        // });
        // this.resFacets.prescriberLastNames.forEach(objFacet => {
        //     objFacet.selected = false;
        // });
        // this.resFacets.claimCounts.forEach(objFacet => {
        //     objFacet.selected = false;
        // });
        // this.resFacets.deaTypes.forEach(objFacet => {
        //     objFacet.selected = false;
        // });
    };
    ESIComponent.prototype.searchESI = function () {
        this.clearSelectedFacets();
        this.searchESIRecordsWithFilters(1);
        this.searchESIFacets();
    };
    ESIComponent.prototype.searchESIRecordsWithFilters = function (pageNum) {
        var _this = this;
        this.pageNumber = pageNum;
        var selectedPHNString = "";
        var selectedPLCString = "";
        var selectedPZCString = "";
        var selectedPLNString = "";
        var selectedCCString = "";
        var selectedDTString = "";
        var selectedBARtring = "";
        var selectedCARtring = "";
        if (this.selectedPHN && this.selectedPHN.length > 0)
            selectedPHNString = this.selectedPHN.join().replace(/&/g, '`').replace(/#/g, '^');
        if (this.selectedPLC && this.selectedPLC.length > 0)
            selectedPLCString = this.selectedPLC.join().replace(/&/g, '`').replace(/#/g, '^');
        if (this.selectedPZC && this.selectedPZC.length > 0)
            selectedPZCString = this.selectedPZC.join().replace(/&/g, '`').replace(/#/g, '^');
        if (this.selectedPLN && this.selectedPLN.length > 0)
            selectedPLNString = this.selectedPLN.join().replace(/&/g, '`').replace(/#/g, '^');
        if (this.selectedCC && this.selectedCC.length > 0)
            selectedCCString = this.selectedCC.join().replace(/&/g, '`').replace(/#/g, '^');
        if (this.selectedDT && this.selectedDT.length > 0)
            selectedDTString = this.selectedDT.join().replace(/&/g, '`').replace(/#/g, '^');
        if (this.billedAmountRange && this.billedAmountRange.length > 0)
            selectedBARtring = this.billedAmountRange.join();
        if (this.claimAmountRange && this.claimAmountRange.length > 0)
            selectedCARtring = this.claimAmountRange.join();
        this.asyncESI = this.fdaService.searchESIRecordsWithFilter(this.searchESIText, selectedPHNString, selectedPLCString, selectedPZCString, selectedPLNString, selectedCCString, selectedDTString, selectedBARtring, selectedCARtring, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.totalRecords;
                    _this.rersESI = res.response.results || [];
                    _this.foundRecord = res.response.totalRecords > 0;
                });
            }
            else {
                _this.zone.run(function () {
                    _this.totalRecords = 0;
                    _this.rersESI = [];
                });
            }
        })
            .map(function (res) { return res.success ? res.response.results : []; });
    };
    ESIComponent.prototype.ngOnInit = function () {
        this.searchESIRecordsWithFilters(1);
        this.searchESIFacets();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    };
    return ESIComponent;
}());
ESIComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1057),
        providers: [fda_service_1.FDAService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.NgZone !== "undefined" && core_1.NgZone) === "function" && _a || Object, typeof (_b = typeof fda_service_1.FDAService !== "undefined" && fda_service_1.FDAService) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object])
], ESIComponent);
exports.ESIComponent = ESIComponent;
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
var _a, _b, _c;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/esi.component.js.map

/***/ }),

/***/ 432:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var fda_service_1 = __webpack_require__(39);
var rxjs_1 = __webpack_require__(31);
var FDAComponent = (function () {
    function FDAComponent(zone, fdaService, _elmRef) {
        this.zone = zone;
        this.fdaService = fdaService;
        this._elmRef = _elmRef;
        this.selectedProductId = "";
        this.selectedFDA = {};
        this.pageNumber = 1;
        this.recordsToFetch = 10;
        this.totalRecords = 0;
        this.resFacets = {};
        this.searchFDATecxt = "";
        this.selectedPNDC = [];
        this.selectedPTN = [];
        this.selectedPN = [];
        this.selectedNPN = [];
        this.selectedDEAS = [];
        this.selectedPNDCString = "";
        this.selectedPTNString = "";
        this.selectedPNString = "";
        this.selectedNPNString = "";
        this.selectedDEASString = "";
        this.rersFDA = [];
        this.foundRecord = true;
    }
    FDAComponent.prototype.searchFDARecords = function (pageNum) {
        var _this = this;
        this.pageNumber = pageNum;
        this.asyncFDA = this.fdaService.searchFDARecords(this.searchFDATecxt, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.totalRecords;
                    _this.rersFDA = res.response.results || [];
                });
            }
            else {
                _this.totalRecords = 0;
                _this.rersFDA = [];
            }
            _this.zone.run(function () {
                if (_this.totalRecords > 0)
                    _this.foundRecord = true;
                else
                    _this.foundRecord = false;
            });
        })
            .map(function (res) { return res.success ? res.response.results : []; });
    };
    FDAComponent.prototype.searchFDAFacets = function () {
        var _this = this;
        this.fdaService.searchFDAFacets(this.searchFDATecxt)
            .subscribe(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.resFacets = res.response;
                });
            }
        }, function (error) {
        });
    };
    FDAComponent.prototype.changeSelectFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            var changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    _this.selectedPNDC.push(objFacet.name);
                else if (index == 2)
                    _this.selectedPTN.push(objFacet.name);
                else if (index == 3)
                    _this.selectedPN.push(objFacet.name);
                else if (index == 4)
                    _this.selectedNPN.push(objFacet.name);
                else if (index == 5)
                    _this.selectedDEAS.push(objFacet.name);
            }
            else {
                var arrayIndex = -1;
                if (index == 1) {
                    arrayIndex = _this.selectedPNDC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPNDC.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = _this.selectedPTN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPTN.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = _this.selectedPN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPN.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = _this.selectedNPN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedNPN.splice(arrayIndex, 1);
                }
                else if (index == 5) {
                    arrayIndex = _this.selectedDEAS.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedDEAS.splice(arrayIndex, 1);
                }
            }
        });
        this.searchFDARecordsWithFilters(1);
    };
    FDAComponent.prototype.removeSelectedFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            objFacet.selected = false;
            var arrayIndex = -1;
            if (index == 1) {
                arrayIndex = _this.selectedPNDC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPNDC.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = _this.selectedPTN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPTN.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = _this.selectedPN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPN.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = _this.selectedNPN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedNPN.splice(arrayIndex, 1);
            }
            else if (index == 5) {
                arrayIndex = _this.selectedDEAS.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedDEAS.splice(arrayIndex, 1);
            }
        });
        this.searchFDARecordsWithFilters(1);
    };
    FDAComponent.prototype.clearSelectedFacets = function () {
        this.selectedPNDC = [];
        this.selectedPTN = [];
        this.selectedPN = [];
        this.selectedNPN = [];
        this.selectedDEAS = [];
        this.resFacets.productNDCs.forEach(function (objFacet) {
            objFacet.selected = false;
        });
        this.resFacets.productTypeNames.forEach(function (objFacet) {
            objFacet.selected = false;
        });
        this.resFacets.proprietaryNames.forEach(function (objFacet) {
            objFacet.selected = false;
        });
        this.resFacets.nonProprietaryNames.forEach(function (objFacet) {
            objFacet.selected = false;
        });
        this.resFacets.dEASchedules.forEach(function (objFacet) {
            objFacet.selected = false;
        });
    };
    FDAComponent.prototype.searchFDA = function () {
        this.searchFDARecords(1);
        this.searchFDAFacets();
    };
    FDAComponent.prototype.searchFDARecordsWithFilters = function (pageNum) {
        var _this = this;
        this.pageNumber = pageNum;
        var selectedPNDCString = "";
        var selectedPTNString = "";
        var selectedPNString = "";
        var selectedNPNString = "";
        var selectedDEASString = "";
        if (this.selectedPNDC && this.selectedPNDC.length > 0)
            selectedPNDCString = this.selectedPNDC.join();
        if (this.selectedPTN && this.selectedPTN.length > 0)
            selectedPTNString = this.selectedPTN.join();
        if (this.selectedPN && this.selectedPN.length > 0)
            selectedPNString = this.selectedPN.join();
        if (this.selectedNPN && this.selectedNPN.length > 0)
            selectedNPNString = this.selectedNPN.join();
        if (this.selectedDEAS && this.selectedDEAS.length > 0)
            selectedDEASString = this.selectedDEAS.join();
        this.asyncFDA = this.fdaService.searchFDARecordsWithFilter(this.searchFDATecxt, selectedPNDCString, selectedPTNString, selectedPNString, selectedNPNString, selectedDEASString, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.totalRecords;
                    _this.rersFDA = res.response.results || [];
                });
            }
            else {
                _this.zone.run(function () {
                    _this.totalRecords = 0;
                    _this.rersFDA = [];
                });
            }
        })
            .map(function (res) { return res.success ? res.response.results : []; });
    };
    FDAComponent.prototype.ngOnInit = function () {
        this.searchFDARecords(1);
        this.searchFDAFacets();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    };
    return FDAComponent;
}());
FDAComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1058),
        providers: [fda_service_1.FDAService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.NgZone !== "undefined" && core_1.NgZone) === "function" && _a || Object, typeof (_b = typeof fda_service_1.FDAService !== "undefined" && fda_service_1.FDAService) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object])
], FDAComponent);
exports.FDAComponent = FDAComponent;
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
var _a, _b, _c;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/fda.component.js.map

/***/ }),

/***/ 433:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var fda_service_1 = __webpack_require__(39);
var rxjs_1 = __webpack_require__(31);
var core_2 = __webpack_require__(457);
var GDComponent = (function () {
    function GDComponent(zone, fdaService, _elmRef, _loader) {
        this.zone = zone;
        this.fdaService = fdaService;
        this._elmRef = _elmRef;
        this._loader = _loader;
        this.selectedProductId = "";
        this.selectedGD = {};
        this.pageNumber = 1;
        this.recordsToFetch = 10;
        this.totalRecords = 0;
        this.resFacets = {};
        this.searchGDText = "";
        this.selectedPN = [];
        this.selectedTIN = [];
        this.rersGD = [];
        this.foundRecord = true;
        this.showMap = false;
        this.distanceString = "";
        this.billedAmountRange = [0, 150000];
        this.billedAmountConfig = {
            connect: true,
            start: [0, 150000],
            //step: 15000,
            pageSteps: 10,
            range: {
                min: 0,
                max: 150000
            },
            format: {
                to: function (value) {
                    return parseFloat(value.toFixed());
                },
                from: function (value) {
                    return parseFloat(value).toFixed(2);
                }
            }
        };
        this.distanceRange = [0, 5000];
        this.distanceConfig = {
            connect: true,
            start: [0, 5000],
            //step: 500,
            pageSteps: 10,
            range: {
                min: 0,
                max: 5000
            },
            format: {
                to: function (value) {
                    return parseFloat(value.toFixed());
                },
                from: function (value) {
                    return parseFloat(value).toFixed(2);
                }
            }
        };
        this.paidAmountRange = [0, 150000];
        this.paidAmountConfig = {
            connect: true,
            start: [0, 150000],
            //step: 15000,
            pageSteps: 10,
            range: {
                min: 0,
                max: 150000
            },
            format: {
                to: function (value) {
                    return parseFloat(value.toFixed());
                },
                from: function (value) {
                    return parseFloat(value).toFixed(2);
                }
            }
        };
        this.totalBilledAmountRange = [0, 150000];
        this.totalBilledAmountConfig = {
            connect: true,
            start: [0, 150000],
            step: 15000,
            pageSteps: 0,
            range: {
                min: 0,
                max: 150000
            },
            format: {
                to: function (value) {
                    return parseFloat(value.toFixed());
                },
                from: function (value) {
                    return parseFloat(value).toFixed(2);
                }
            }
        };
        this.totalPaidAmountRange = [0, 150000];
        this.totalPaidAmountConfig = {
            connect: true,
            start: [0, 150000],
            step: 15000,
            pageSteps: 0,
            range: {
                min: 0,
                max: 150000
            },
            format: {
                to: function (value) {
                    return parseFloat(value.toFixed());
                },
                from: function (value) {
                    return parseFloat(value).toFixed(2);
                }
            }
        };
        /*map section*/
        this.mapCenterLocation = {
            lat: 51.678418,
            lng: 7.809007
        };
        this.markers = [
            {
                lat: 51.673858,
                lng: 7.815982,
                label: 'C',
                draggable: false,
                icon: '',
                info: 'Info'
            },
            {
                lat: 51.373858,
                lng: 7.815982,
                label: 'P',
                draggable: false,
                icon: '',
                info: 'Info'
            }
        ];
        this.mapZoomLevel = 11;
    }
    GDComponent.prototype.onChangeBilledAmount = function (event) {
        this.searchGDRecordsWithFilters(1);
    };
    GDComponent.prototype.onChangeDistance = function (event) {
        this.searchGDRecordsWithFilters(1);
    };
    GDComponent.prototype.onChangePaidAmount = function (event) {
        this.searchGDRecordsWithFilters(1);
    };
    GDComponent.prototype.onChangeTotalBilledAmount = function (event) {
        this.searchGDRecordsWithFilters(1);
    };
    GDComponent.prototype.onChangeTotalPaidAmount = function (event) {
        this.searchGDRecordsWithFilters(1);
    };
    GDComponent.prototype.searchGDFacets = function () {
        var _this = this;
        this.fdaService.searchGDFacets(this.searchGDText)
            .subscribe(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.resFacets = res.response;
                    if (_this.resFacets.maxBilledAmount) {
                        _this.billedAmountConfig.range = { min: 0, max: _this.resFacets.maxBilledAmount };
                        _this.billedAmountRange = [0, _this.resFacets.maxBilledAmount];
                    }
                    if (_this.resFacets.maxDistance) {
                        _this.distanceConfig.range.max = _this.resFacets.maxDistance;
                        _this.distanceRange = [0, _this.resFacets.maxDistance];
                    }
                    if (_this.resFacets.maxPaidAmount) {
                        _this.paidAmountConfig.range.max = _this.resFacets.maxPaidAmount;
                        _this.paidAmountRange = [0, _this.resFacets.maxPaidAmount];
                    }
                    if (_this.resFacets.maxTotalBilledAmount) {
                        _this.totalBilledAmountConfig.range.max = _this.resFacets.maxTotalBilledAmount;
                        _this.totalBilledAmountRange = [0, _this.resFacets.maxTotalBilledAmount];
                    }
                    if (_this.resFacets.maxTotalPaidAmount) {
                        _this.totalPaidAmountConfig.range.max = _this.resFacets.maxTotalPaidAmount;
                        _this.totalPaidAmountRange = [0, _this.resFacets.maxTotalPaidAmount];
                    }
                });
            }
        }, function (error) {
        });
    };
    GDComponent.prototype.changeSelectFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            var changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    _this.selectedPN.push(objFacet.name);
                else if (index == 2)
                    _this.selectedTIN.push(objFacet.name);
            }
            else {
                var arrayIndex = -1;
                if (index == 1) {
                    arrayIndex = _this.selectedPN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPN.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = _this.selectedTIN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedTIN.splice(arrayIndex, 1);
                }
            }
        });
        this.searchGDRecordsWithFilters(1);
    };
    GDComponent.prototype.removeSelectedFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            objFacet.selected = false;
            var arrayIndex = -1;
            if (index == 1) {
                arrayIndex = _this.selectedPN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPN.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = _this.selectedTIN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedTIN.splice(arrayIndex, 1);
            }
        });
        this.searchGDRecordsWithFilters(1);
    };
    GDComponent.prototype.clearSelectedFacets = function () {
        this.selectedPN = [];
        this.selectedTIN = [];
        this.billedAmountRange = [0, 0];
        this.distanceRange = [0, 0];
        this.paidAmountRange = [0, 0];
        this.totalBilledAmountRange = [0, 0];
        this.totalPaidAmountRange = [0, 0];
        this.resFacets = {};
    };
    GDComponent.prototype.searchGD = function () {
        this.clearSelectedFacets();
        this.searchGDRecordsWithFilters(1);
        this.searchGDFacets();
    };
    GDComponent.prototype.searchGDRecordsWithFilters = function (pageNum) {
        var _this = this;
        this.showMap = false;
        this.selectedGD = {};
        this.pageNumber = pageNum;
        var selectedPNString = "";
        var selectedTINString = "";
        var selectedBARtring = "";
        var selectedDRString = "";
        var selectedPARString = "";
        var selectedTBARString = "";
        var selectedTPARString = "";
        if (this.billedAmountRange && this.billedAmountRange.length > 0)
            selectedBARtring = this.billedAmountRange.join();
        if (this.distanceRange && this.distanceRange.length > 0)
            selectedDRString = this.distanceRange.join();
        if (this.paidAmountRange && this.paidAmountRange.length > 0)
            selectedPARString = this.paidAmountRange.join();
        if (this.selectedPN && this.selectedPN.length > 0)
            selectedPNString = this.selectedPN.join();
        if (this.selectedTIN && this.selectedTIN.length > 0)
            selectedTINString = this.selectedTIN.join();
        if (this.totalBilledAmountRange && this.totalBilledAmountRange.length > 0)
            selectedTBARString = this.totalBilledAmountRange.join();
        if (this.totalPaidAmountRange && this.totalPaidAmountRange.length > 0)
            selectedTPARString = this.totalPaidAmountRange.join();
        this.asyncGD = this.fdaService.searchGDRecordsWithFilter(this.searchGDText, selectedBARtring, selectedDRString, selectedPARString, selectedPNString, selectedTINString, selectedTBARString, selectedTPARString, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            if (res.response) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.numFound;
                    _this.rersGD = res.response.docs || [];
                    _this.foundRecord = res.response.numFound > 0;
                    console.log('resGD', res);
                });
            }
            else {
                _this.zone.run(function () {
                    _this.totalRecords = 0;
                    _this.rersGD = [];
                });
            }
        })
            .map(function (res) { return res.response ? res.response.docs : []; });
    };
    GDComponent.prototype.openCloseMap = function (objGD) {
        if (this.showMap) {
            if (objGD == this.selectedGD)
                this.showMap = false;
            else {
                this.selectedGD = objGD;
                this.setLocationFromAddressInMarker(0, objGD.claimantAddress);
                this.setLocationFromAddressInMarker(1, objGD.providerAddress);
                this.calculateDistance(objGD.claimantAddress, objGD.providerAddress);
                //this.resizeMap()
            }
        }
        else {
            this.selectedGD = objGD;
            this.setLocationFromAddressInMarker(0, objGD.claimantAddress);
            this.setLocationFromAddressInMarker(1, objGD.providerAddress);
            this.calculateDistance(objGD.claimantAddress, objGD.providerAddress);
            //this.resizeMap();
            this.showMap = true;
        }
    };
    GDComponent.prototype.closeMap = function () {
        this.selectedGD = {};
        this.showMap = false;
    };
    GDComponent.prototype.initMap = function () {
        var _this = this;
        this._loader.load().then(function () {
            _this.geocoder = new google.maps.Geocoder();
            _this.serviceDistanceMatrix = new google.maps.DistanceMatrixService();
            _this.mapBounds = new google.maps.LatLngBounds();
            _this.mapOverlayView = new google.maps.OverlayView();
        });
    };
    GDComponent.prototype.resizeMap = function () {
        this.mapBounds = new google.maps.LatLngBounds();
        this.mapBounds.extend(new google.maps.LatLng(this.markers[0].lat, this.markers[0].lng));
        this.mapBounds.extend(new google.maps.LatLng(this.markers[1].lat, this.markers[1].lng));
        this.mapCenterLocation.lat = this.mapBounds.getCenter().lat();
        this.mapCenterLocation.lng = this.mapBounds.getCenter().lng();
        // let gmap = this.mapOverlayView.getMap();
        // console.log('zoom', gmap.getZoom());
    };
    GDComponent.prototype.calculateDistance = function (sourceAddress, destinationAddress) {
        var _this = this;
        this.serviceDistanceMatrix.getDistanceMatrix({
            origins: [sourceAddress],
            destinations: [destinationAddress],
            travelMode: 'DRIVING',
            // transitOptions: TransitOptions,
            // drivingOptions: DrivingOptions,
            // unitSystem: UnitSystem,
            avoidHighways: false,
            avoidTolls: false,
        }, function (response, status) {
            if (response.rows[0] && response.rows[0].elements[0] && response.rows[0].elements[0].duration && response.rows[0].elements[0].distance) {
                _this.zone.run(function () {
                    var distanceInMiles = Number((response.rows[0].elements[0].distance.value / 1609.344).toFixed(2));
                    _this.distanceString = distanceInMiles + ' Miles ' + '(' + response.rows[0].elements[0].duration.text + ')';
                    /* if (distanceInMiles <= 25)
                         this.mapZoomLevel = 11;
                     else if (distanceInMiles <= 50)
                         this.mapZoomLevel = 10;
                     else if (distanceInMiles <= 100)
                         this.mapZoomLevel = 10
                     else if (distanceInMiles <= 300)
                         this.mapZoomLevel = 9
                     else if (distanceInMiles <= 600)
                         this.mapZoomLevel = 8
                     else if (distanceInMiles <= 800)
                         this.mapZoomLevel = 7
                     else if (distanceInMiles <= 1200)
                         this.mapZoomLevel = 6
                     else if (distanceInMiles <= 2000)
                         this.mapZoomLevel = 5
                     else if (distanceInMiles <= 3000)
                         this.mapZoomLevel = 4
                     else if (distanceInMiles <= 4000)
                         this.mapZoomLevel = 2
                     else
                         this.mapZoomLevel = 1*/
                    //console.log('this.mapZoomLevel', this.mapZoomLevel);
                });
            }
        });
    };
    GDComponent.prototype.setLocationFromAddressInMarker = function (markerIndex, address) {
        var _this = this;
        this.geocoder.geocode({ 'address': address }, function (results, status) {
            _this.markers[markerIndex].lat = results[0].geometry.location.lat();
            _this.markers[markerIndex].lng = results[0].geometry.location.lng();
            _this.markers[markerIndex].info = address;
            if (markerIndex > 0) {
                _this.resizeMap();
            }
        });
    };
    GDComponent.prototype.ngOnInit = function () {
        this.searchGDRecordsWithFilters(1);
        this.searchGDFacets();
        this.initMap();
        //this.calculateDistance();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    };
    return GDComponent;
}());
GDComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1059),
        providers: [fda_service_1.FDAService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.NgZone !== "undefined" && core_1.NgZone) === "function" && _a || Object, typeof (_b = typeof fda_service_1.FDAService !== "undefined" && fda_service_1.FDAService) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object, typeof (_d = typeof core_2.MapsAPILoader !== "undefined" && core_2.MapsAPILoader) === "function" && _d || Object])
], GDComponent);
exports.GDComponent = GDComponent;
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
var _a, _b, _c, _d;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/gd.component.js.map

/***/ }),

/***/ 434:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var fda_service_1 = __webpack_require__(39);
var rxjs_1 = __webpack_require__(31);
var ICD9Component = (function () {
    function ICD9Component(zone, fdaService, _elmRef) {
        this.zone = zone;
        this.fdaService = fdaService;
        this._elmRef = _elmRef;
        this.selectedICD9 = {};
        this.pageNumber = 1;
        this.recordsToFetch = 10;
        this.totalRecords = 0;
        this.resFacets = {};
        this.searchICD9Text = "";
        this.selectedCDE = [];
        this.selectedCDE_DSC = [];
        this.selectedCDE_TYP = [];
        this.selectedCDE_TYP_DSC = [];
        this.rersICD9 = [];
        this.foundRecord = true;
    }
    ICD9Component.prototype.searchICD9Facets = function () {
        var _this = this;
        this.fdaService.searchICD9Facets(this.searchICD9Text)
            .subscribe(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.resFacets = res.response;
                });
            }
        }, function (error) {
        });
    };
    ICD9Component.prototype.changeSelectFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            var changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    _this.selectedCDE.push(objFacet.name);
                else if (index == 2)
                    _this.selectedCDE_DSC.push(objFacet.name);
                else if (index == 3)
                    _this.selectedCDE_TYP.push(objFacet.name);
                else if (index == 4)
                    _this.selectedCDE_TYP_DSC.push(objFacet.name);
            }
            else {
                var arrayIndex = -1;
                if (index == 1) {
                    arrayIndex = _this.selectedCDE.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedCDE.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = _this.selectedCDE_DSC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedCDE_DSC.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = _this.selectedCDE_TYP.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedCDE_TYP.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = _this.selectedCDE_TYP_DSC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedCDE_TYP_DSC.splice(arrayIndex, 1);
                }
            }
        });
        this.searchICD9RecordsWithFilters(1);
    };
    ICD9Component.prototype.removeSelectedFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            objFacet.selected = false;
            var arrayIndex = -1;
            if (index == 1) {
                arrayIndex = _this.selectedCDE.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedCDE.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = _this.selectedCDE_DSC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedCDE_DSC.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = _this.selectedCDE_TYP.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedCDE_TYP.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = _this.selectedCDE_TYP_DSC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedCDE_TYP_DSC.splice(arrayIndex, 1);
            }
        });
        this.searchICD9RecordsWithFilters(1);
    };
    ICD9Component.prototype.clearSelectedFacets = function () {
        this.selectedCDE = [];
        this.selectedCDE_DSC = [];
        this.selectedCDE_TYP = [];
        this.selectedCDE_TYP_DSC = [];
    };
    ICD9Component.prototype.searchICD9 = function () {
        this.clearSelectedFacets();
        this.searchICD9RecordsWithFilters(1);
        this.searchICD9Facets();
    };
    ICD9Component.prototype.searchICD9RecordsWithFilters = function (pageNum) {
        var _this = this;
        this.pageNumber = pageNum;
        var selectedCDEString = "";
        var selectedCDE_DSCString = "";
        var selectedCDE_TYPString = "";
        var selectedCDE_TYP_DSCString = "";
        if (this.selectedCDE && this.selectedCDE.length > 0)
            selectedCDEString = this.selectedCDE.join();
        if (this.selectedCDE_DSC && this.selectedCDE_DSC.length > 0)
            selectedCDE_DSCString = this.selectedCDE_DSC.join();
        if (this.selectedCDE_TYP && this.selectedCDE_TYP.length > 0)
            selectedCDE_TYPString = this.selectedCDE_TYP.join();
        if (this.selectedCDE_TYP_DSC && this.selectedCDE_TYP_DSC.length > 0)
            selectedCDE_TYP_DSCString = this.selectedCDE_TYP_DSC.join();
        this.asyncICD9 = this.fdaService.searchICD9RecordsWithFilter(this.searchICD9Text, selectedCDEString, selectedCDE_DSCString, selectedCDE_TYPString, selectedCDE_TYP_DSCString, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.totalRecords;
                    _this.rersICD9 = res.response.results || [];
                    _this.foundRecord = res.response.totalRecords > 0;
                });
            }
            else {
                _this.zone.run(function () {
                    _this.totalRecords = 0;
                    _this.rersICD9 = [];
                });
            }
        })
            .map(function (res) { return res.success ? res.response.results : []; });
    };
    ICD9Component.prototype.ngOnInit = function () {
        this.searchICD9RecordsWithFilters(1);
        this.searchICD9Facets();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    };
    return ICD9Component;
}());
ICD9Component = __decorate([
    core_1.Component({
        template: __webpack_require__(1060),
        providers: [fda_service_1.FDAService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.NgZone !== "undefined" && core_1.NgZone) === "function" && _a || Object, typeof (_b = typeof fda_service_1.FDAService !== "undefined" && fda_service_1.FDAService) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object])
], ICD9Component);
exports.ICD9Component = ICD9Component;
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
var _a, _b, _c;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/icd9.component.js.map

/***/ }),

/***/ 435:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var login_service_1 = __webpack_require__(843);
var router_1 = __webpack_require__(38);
var session_service_1 = __webpack_require__(104);
var broadcast_service_1 = __webpack_require__(270);
var global_events_1 = __webpack_require__(421);
var LoginComponent = (function () {
    function LoginComponent(_loginService, router, _sessionService, _broadcastService, route) {
        this._loginService = _loginService;
        this.router = router;
        this._sessionService = _sessionService;
        this._broadcastService = _broadcastService;
        this.route = route;
    }
    LoginComponent.prototype.ngOnInit = function () {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.checkUserSession();
    };
    LoginComponent.prototype.checkUserSession = function () {
        var _this = this;
        this._sessionService.getUserSession().subscribe(function (res) {
            if (res) {
                console.log('check user session login !!');
                _this.router.navigate([_this.returnUrl]);
                _this.sessionExists = true;
            }
            else {
                _this.sessionExists = false;
            }
        }, function (err) {
            console.log('Error while retrieving session data.');
            _this.sessionExists = false;
        });
    };
    LoginComponent.prototype.onClose = function () {
        this.loginError = null;
    };
    LoginComponent.prototype.broadcastLoginUpdate = function () {
        this._broadcastService.broadcast(global_events_1.GlobalEvents.LOGIN_UPDATE, { loggedIn: true });
    };
    LoginComponent.prototype.login = function (username, password) {
        var _this = this;
        this.loginError = null;
        this._loginService.login({ username: username, password: password }).subscribe(function (res) {
            if (res) {
                _this.router.navigate([_this.returnUrl || '/home']);
                _this.broadcastLoginUpdate();
                _this.loginstatus = true;
            }
            else {
                _this.loginError = 'Invalid credentials. Please try again...';
                _this.loginstatus = false;
            }
        }, function (err) {
            _this.loginError = 'Error while processing log in. Please try again...';
            _this.loginstatus = false;
        }, function () {
        });
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        selector: 'app-login',
        template: __webpack_require__(1061),
        styles: [__webpack_require__(1007)],
        providers: [login_service_1.LoginService, session_service_1.SessionService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof login_service_1.LoginService !== "undefined" && login_service_1.LoginService) === "function" && _a || Object, typeof (_b = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _b || Object, typeof (_c = typeof session_service_1.SessionService !== "undefined" && session_service_1.SessionService) === "function" && _c || Object, typeof (_d = typeof broadcast_service_1.BroadcastService !== "undefined" && broadcast_service_1.BroadcastService) === "function" && _d || Object, typeof (_e = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _e || Object])
], LoginComponent);
exports.LoginComponent = LoginComponent;
var _a, _b, _c, _d, _e;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/login.component.js.map

/***/ }),

/***/ 436:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var fda_service_1 = __webpack_require__(39);
var rxjs_1 = __webpack_require__(31);
var MBCOIGComponent = (function () {
    function MBCOIGComponent(zone, fdaService, _elmRef) {
        this.zone = zone;
        this.fdaService = fdaService;
        this._elmRef = _elmRef;
        this.selectedProductId = "";
        this.selectedMBCOIG = {};
        this.pageNumber = 1;
        this.recordsToFetch = 10;
        this.totalRecords = 0;
        this.resFacets = {};
        this.searchMBCOIGText = "";
        this.selectedDOAY = [];
        this.selectedAT = [];
        this.selectedSP = [];
        this.selectedBT = [];
        this.rersMBCOIG = [];
        this.foundRecord = true;
    }
    MBCOIGComponent.prototype.searchMBCOIGFacets = function () {
        var _this = this;
        this.fdaService.searchMBCOIGFacets(this.searchMBCOIGText)
            .subscribe(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.resFacets = res.response;
                });
            }
        }, function (error) {
        });
    };
    MBCOIGComponent.prototype.changeSelectFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            var changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    _this.selectedDOAY.push(objFacet.name);
                else if (index == 2)
                    _this.selectedAT.push(objFacet.name);
                else if (index == 3)
                    _this.selectedSP.push(objFacet.name);
                else if (index == 4)
                    _this.selectedBT.push(objFacet.name);
            }
            else {
                var arrayIndex = -1;
                if (index == 1) {
                    arrayIndex = _this.selectedDOAY.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedDOAY.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = _this.selectedAT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedAT.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = _this.selectedSP.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedSP.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = _this.selectedBT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedBT.splice(arrayIndex, 1);
                }
            }
        });
        this.searchMBCOIGRecordsWithFilters(1);
    };
    MBCOIGComponent.prototype.removeSelectedFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            objFacet.selected = false;
            var arrayIndex = -1;
            if (index == 1) {
                arrayIndex = _this.selectedDOAY.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedDOAY.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = _this.selectedAT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedAT.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = _this.selectedSP.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedSP.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = _this.selectedBT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedBT.splice(arrayIndex, 1);
            }
        });
        this.searchMBCOIGRecordsWithFilters(1);
    };
    MBCOIGComponent.prototype.clearSelectedFacets = function () {
        this.selectedDOAY = [];
        this.selectedAT = [];
        this.selectedSP = [];
        this.selectedBT = [];
    };
    MBCOIGComponent.prototype.searchMBCOIG = function () {
        this.clearSelectedFacets();
        this.searchMBCOIGRecordsWithFilters(1);
        this.searchMBCOIGFacets();
    };
    MBCOIGComponent.prototype.searchMBCOIGRecordsWithFilters = function (pageNum) {
        var _this = this;
        this.pageNumber = pageNum;
        var selectedDOAYString = "";
        var selectedATString = "";
        var selectedSPString = "";
        var selectedBTString = "";
        if (this.selectedDOAY && this.selectedDOAY.length > 0)
            selectedDOAYString = this.selectedDOAY.join();
        if (this.selectedAT && this.selectedAT.length > 0)
            selectedATString = this.selectedAT.join();
        if (this.selectedSP && this.selectedSP.length > 0)
            selectedSPString = this.selectedSP.join();
        if (this.selectedBT && this.selectedBT.length > 0)
            selectedBTString = this.selectedBT.join();
        this.asyncMBCOIG = this.fdaService.searchMBCOIGRecordsWithFilter(this.searchMBCOIGText, selectedDOAYString, selectedATString, selectedSPString, selectedBTString, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.totalRecords;
                    _this.rersMBCOIG = res.response.results || [];
                    _this.foundRecord = res.response.totalRecords > 0;
                });
            }
            else {
                _this.zone.run(function () {
                    _this.totalRecords = 0;
                    _this.rersMBCOIG = [];
                });
            }
        })
            .map(function (res) { return res.success ? res.response.results : []; });
    };
    MBCOIGComponent.prototype.ngOnInit = function () {
        this.searchMBCOIGRecordsWithFilters(1);
        this.searchMBCOIGFacets();
    };
    return MBCOIGComponent;
}());
MBCOIGComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1062),
        providers: [fda_service_1.FDAService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.NgZone !== "undefined" && core_1.NgZone) === "function" && _a || Object, typeof (_b = typeof fda_service_1.FDAService !== "undefined" && fda_service_1.FDAService) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object])
], MBCOIGComponent);
exports.MBCOIGComponent = MBCOIGComponent;
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
var _a, _b, _c;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/mbcoig.component.js.map

/***/ }),

/***/ 437:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var fda_service_1 = __webpack_require__(39);
var rxjs_1 = __webpack_require__(31);
var NICBComponent = (function () {
    function NICBComponent(zone, fdaService, _elmRef) {
        this.zone = zone;
        this.fdaService = fdaService;
        this._elmRef = _elmRef;
        this.selectedProductId = "";
        this.selectedNICB = {};
        this.pageNumber = 1;
        this.recordsToFetch = 10;
        this.totalRecords = 0;
        this.resFacets = {};
        this.searchNICBText = "";
        this.selectedPS = [];
        this.selectedPLT = [];
        this.selectedPLA = [];
        this.rersNICB = [];
        this.foundRecord = true;
    }
    NICBComponent.prototype.searchNICBFacets = function () {
        var _this = this;
        this.fdaService.searchNICBFacets(this.searchNICBText)
            .subscribe(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.resFacets = res.response;
                });
            }
        }, function (error) {
        });
    };
    NICBComponent.prototype.changeSelectFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            var changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    _this.selectedPS.push(objFacet.name);
                else if (index == 2)
                    _this.selectedPLT.push(objFacet.name);
                else if (index == 3)
                    _this.selectedPLA.push(objFacet.name);
            }
            else {
                var arrayIndex = -1;
                if (index == 1) {
                    arrayIndex = _this.selectedPS.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPS.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = _this.selectedPLT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPLT.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = _this.selectedPLA.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPLA.splice(arrayIndex, 1);
                }
            }
        });
        this.searchNICBRecordsWithFilters(1);
    };
    NICBComponent.prototype.removeSelectedFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            objFacet.selected = false;
            var arrayIndex = -1;
            if (index == 1) {
                arrayIndex = _this.selectedPS.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPS.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = _this.selectedPLT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPLT.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = _this.selectedPLA.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPLA.splice(arrayIndex, 1);
            }
        });
        this.searchNICBRecordsWithFilters(1);
    };
    NICBComponent.prototype.clearSelectedFacets = function () {
        this.selectedPS = [];
        this.selectedPLT = [];
        this.selectedPLA = [];
    };
    NICBComponent.prototype.searchNICB = function () {
        this.clearSelectedFacets();
        this.searchNICBRecordsWithFilters(1);
        this.searchNICBFacets();
    };
    NICBComponent.prototype.searchNICBRecordsWithFilters = function (pageNum) {
        var _this = this;
        this.pageNumber = pageNum;
        var selectedPSString = "";
        var selectedPLTString = "";
        var selectedPLAString = "";
        if (this.selectedPS && this.selectedPS.length > 0)
            selectedPSString = this.selectedPS.join();
        if (this.selectedPLT && this.selectedPLT.length > 0)
            selectedPLTString = this.selectedPLT.join();
        if (this.selectedPLA && this.selectedPLA.length > 0)
            selectedPLAString = this.selectedPLA.join();
        this.asyncNICB = this.fdaService.searchNICBRecordsWithFilter(this.searchNICBText, selectedPSString, selectedPLTString, selectedPLAString, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.totalRecords;
                    _this.rersNICB = res.response.results || [];
                    _this.foundRecord = res.response.totalRecords > 0;
                });
            }
            else {
                _this.zone.run(function () {
                    _this.totalRecords = 0;
                    _this.rersNICB = [];
                });
            }
        })
            .map(function (res) { return res.success ? res.response.results : []; });
    };
    NICBComponent.prototype.ngOnInit = function () {
        this.searchNICBRecordsWithFilters(1);
        this.searchNICBFacets();
    };
    return NICBComponent;
}());
NICBComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1063),
        providers: [fda_service_1.FDAService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.NgZone !== "undefined" && core_1.NgZone) === "function" && _a || Object, typeof (_b = typeof fda_service_1.FDAService !== "undefined" && fda_service_1.FDAService) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object])
], NICBComponent);
exports.NICBComponent = NICBComponent;
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
var _a, _b, _c;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/nicb.component.js.map

/***/ }),

/***/ 438:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
//
var provider_outcome_service_1 = __webpack_require__(87);
var ProviderMapComponent = (function () {
    function ProviderMapComponent(providerOutcomeService) {
        this.providerOutcomeService = providerOutcomeService;
        this.activeFilters = {};
        this.mapData = [];
        this.initFilter();
    }
    ProviderMapComponent.prototype.ngOnInit = function () {
        this.SetUpSideBarFilter();
    };
    ProviderMapComponent.prototype.ngAfterViewInit = function () {
        //this.initMap();
        //this.loadMarkers();
        this.getAllProviderDataWithFilter();
    };
    ProviderMapComponent.prototype.initFilter = function () {
        this.activeFilters['citys'] = [];
        this.activeFilters['zips'] = [];
        this.activeFilters['providers'] = [];
        this.activeFilters['tin_nums'] = [];
        this.activeFilters['providers_values'] = [];
        this.activeFilters['searchQuery'] = "";
    };
    ProviderMapComponent.prototype.changeCitySelection = function (city) {
        var changedSelection = !city.selected;
        city.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    ProviderMapComponent.prototype.changeZipSelection = function (zip) {
        var changedSelection = !zip.selected;
        zip.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    ProviderMapComponent.prototype.changeProvSelection = function (prov) {
        var changedSelection = !prov.selected;
        prov.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    ProviderMapComponent.prototype.changeTinSelection = function (tin) {
        var changedSelection = !tin.selected;
        tin.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    ProviderMapComponent.prototype.changePvSelection = function (pv) {
        var changedSelection = !pv.selected;
        pv.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    ProviderMapComponent.prototype.initMap = function () {
        var ca = { lat: 36.778261, lng: -119.417932 };
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            center: ca,
            styles: [{ elementType: 'geometry', stylers: [{ color: '#023e58' }] },
                { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
                { elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },
                { featureType: 'administrative.locality', elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },
                { featureType: 'poi', elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },
                { featureType: 'poi.park', elementType: 'geometry', stylers: [{ color: '#263c3f' }] },
                { featureType: 'poi.park', elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },
                { featureType: 'road', elementType: 'geometry', stylers: [{ color: '#38414e' }] },
                { featureType: 'road', elementType: 'geometry.stroke', stylers: [{ color: '#212a37' }] },
                { featureType: 'road', elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },
                { featureType: 'road.highway', elementType: 'geometry', stylers: [{ color: '#746855' }] },
                { featureType: 'road.highway', elementType: 'geometry.stroke', stylers: [{ color: '#1f2835' }] },
                { featureType: 'road.highway', elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },
                { featureType: 'transit', elementType: 'geometry', stylers: [{ color: '#2f3948' }] },
                { featureType: 'transit.station', elementType: 'labels.text.fill', stylers: [{ color: '#d59563' }] },
                { featureType: 'water', elementType: 'geometry', stylers: [{ color: '#17263c' }] },
                { featureType: 'water', elementType: 'labels.text.fill', stylers: [{ color: '#8ec2b8' }] },
                { featureType: 'water', elementType: 'labels.text.stroke', stylers: [{ color: '#17263c' }] }
            ]
        });
        this.loadMarkers(map);
    };
    ProviderMapComponent.prototype.loadMarkers = function (map) {
        this.mapData.forEach(function (ele) {
            //console.log('element', ele);
            //parseFloat()
            if (ele.PROVIDER_VALUE == 'Most Valued') {
                var icon = "assets/img/gmap-icons/turquoise-blue.png";
            }
            else if (ele.PROVIDER_VALUE == 'Medium Valued') {
                var icon = "assets/img/gmap-icons/yellow.png";
            }
            else {
                var icon = "assets/img/gmap-icons/red-dark.png";
            }
            var marker = new google.maps.Marker({
                position: { lat: parseFloat(ele.LAT), lng: parseFloat(ele.LNG) },
                icon: icon,
                map: map
            });
        });
    };
    ProviderMapComponent.prototype.SetUpSideBarFilter = function () {
        var _this = this;
        this.providerOutcomeService.getSideBarFilterMap('').subscribe(function (res) {
            console.log('getSideBarFilterMap res', res);
            _this.activeFilters['citys'] = res.citys;
            _this.activeFilters['zips'] = res.zips;
            _this.activeFilters['providers'] = res.providers;
            _this.activeFilters['tin_nums'] = res.tin_nums;
            _this.activeFilters['providers_values'] = res.provider_values;
        }, function (err) {
            console.error('getCityList error', err);
        });
    };
    ProviderMapComponent.prototype.getAllProviderDataWithFilter = function () {
        var _this = this;
        this.providerOutcomeService.getAllMapData(this.activeFilters).subscribe(function (res) {
            console.log('getSideBarFilterMap res', res);
            _this.mapData = res.data;
            _this.initMap();
        }, function (err) {
            console.error('getCityList error', err);
        });
    };
    return ProviderMapComponent;
}());
ProviderMapComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1064),
        providers: [provider_outcome_service_1.ProviderOutcomeService],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof provider_outcome_service_1.ProviderOutcomeService !== "undefined" && provider_outcome_service_1.ProviderOutcomeService) === "function" && _a || Object])
], ProviderMapComponent);
exports.ProviderMapComponent = ProviderMapComponent;
var _a;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/providerMap.component.js.map

/***/ }),

/***/ 439:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(37);
var common_1 = __webpack_require__(10);
var ng2_nouislider_1 = __webpack_require__(661);
//
var provider_outcome_service_1 = __webpack_require__(87);
var ng2_select_1 = __webpack_require__(666);
var ng2_nvd3_1 = __webpack_require__(318);
__webpack_require__(296);
__webpack_require__(672);
var timers_1 = __webpack_require__(709);
var ProviderOutcomeComponent = (function () {
    function ProviderOutcomeComponent(http, providerOutcomeService, _location) {
        this.providerOutcomeService = providerOutcomeService;
        this._location = _location;
        this.activeFilters = {};
        this.addressSelectObj = {};
        this.sendSmsSelectObj = {};
        this.searchProviderSummaryText = "";
        this.cityList = [];
        this.zipCodeList = [];
        this.icdCodeList = [];
        this.specialityList = [];
        this.providerOutcomeData = [];
        this.someRange = [2, 10];
        this.isFirstAddressSelection = true;
        this.isShowLoad = false;
        this.sendersList = [];
        this.initFilter();
    }
    ProviderOutcomeComponent.prototype.ngOnInit = function () {
        //this.getAllProviderDataWithFilter();
        //this.getSideFilter();
    };
    ProviderOutcomeComponent.prototype.ngAfterViewInit = function () {
        //console.log('selCity', this.selCity);
        //console.log('addressInfoModel', this.addressInfoModel);
        // this.selCity.opened.subscribe(res => {
        //     console.log('selCity open fire', res);
        // });
        // this.selCity.selected.subscribe(res => {
        //     console.log('selCity selected fire', res);
        // });
        this.getCityList();
    };
    ProviderOutcomeComponent.prototype.initFilter = function () {
        this.activeFilters['mnp'] = [];
        this.activeFilters['work_day'] = [];
        this.activeFilters['modified_day'] = [];
        this.activeFilters['default'] = { city: "", zip: "", icd: "", speciality: "" };
    };
    ProviderOutcomeComponent.prototype.selectAllProvider = function (ev) {
        //ev.target.checked
        // for (var i = 0; i < this.providerOutcomeData.length; i++) {
        //     this.providerOutcomeData[i].selected = true;
        // }
        this.providerOutcomeData.forEach(function (x) { x.selected = ev.target.checked; });
    };
    ProviderOutcomeComponent.prototype.changeMnpSelection = function (mnp) {
        var changedSelection = !mnp.selected;
        mnp.selected = changedSelection;
        //console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    ProviderOutcomeComponent.prototype.changeWorkDaySelection = function (wd) {
        var changedSelection = !wd.selected;
        wd.selected = changedSelection;
        //console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    ProviderOutcomeComponent.prototype.changeModifiedDaySelection = function (mjd) {
        var changedSelection = !mjd.selected;
        mjd.selected = changedSelection;
        //console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    ProviderOutcomeComponent.prototype.onAgeChange = function (age, flag) {
        //console.log('call onAgeChange...');
    };
    ProviderOutcomeComponent.prototype.setUpDayFilter = function (maxDay) {
        console.log('maxDay ', maxDay);
        var i = 0;
        var DayArrar = [];
        while (i < parseInt(maxDay)) {
            DayArrar.push({ start: i, end: (i + 10) });
            i += 10;
            console.log(i);
        }
        console.log('DayArrar', DayArrar);
        return DayArrar;
    };
    //------------------- address info model selection function --------------------
    ProviderOutcomeComponent.prototype.getCityList = function () {
        var _this = this;
        this.providerOutcomeService.getCityList().subscribe(function (res) {
            //console.log('getCityList res....', res);
            _this.cityList = res.data;
            //this.cityList = [{ id: 6, text: 'Birmingham' }, { id: 7, text: 'Bradford' }, { id: 26, text: 'Leeds' }, { id: 30, text: 'London' }, { id: 34, text: 'Manchester' }, { id: 47, text: 'Sheffield' }];
            _this.addressInfoModel.show();
        }, function (err) {
            //console.error('getCityList error', err);
        });
    };
    ProviderOutcomeComponent.prototype.getZipCodeListByCity = function (city) {
        var _this = this;
        if (city) {
            this.clearDataByEle(this.selZipCode);
            this.clearDataByEle(this.selICDCode);
            this.providerOutcomeService.getZipCodeList(city).subscribe(function (res) {
                //console.log('getZipCodeList res....', res);
                _this.zipCodeList = res.data;
            }, function (err) {
                //console.error('getZipCodeList error', err);
            });
        }
    };
    ProviderOutcomeComponent.prototype.getICDCodeListByzipCode = function (zipCode, cityName) {
        var _this = this;
        if (zipCode) {
            this.clearDataByEle(this.selICDCode);
            this.clearDataByEle(this.selSpeciality);
            this.providerOutcomeService.getICDCodeList(zipCode, cityName).subscribe(function (res) {
                //console.log('getZipCodeList res....', res);
                _this.icdCodeList = res.data.icdList;
                _this.specialityList = res.data.speciality;
            }, function (err) {
                //console.error('getZipCodeList error', err);
            });
        }
    };
    ProviderOutcomeComponent.prototype.selectedCity = function (value) {
        //console.log('value', value.text);
        this.getZipCodeListByCity(value.text);
    };
    ProviderOutcomeComponent.prototype.selectedZip = function (value) {
        //this.addressSelectObj['zipCode'] = value.text;
        //console.log('addressSelectObj', this.addressSelectObj);
        var cityName = (this.addressSelectObj.city && this.addressSelectObj.city.length > 0 && this.addressSelectObj.city[0].hasOwnProperty('id')) ? this.addressSelectObj.city[0].id : "";
        this.getICDCodeListByzipCode(value.text, cityName);
    };
    ProviderOutcomeComponent.prototype.selectedICD = function (value) {
        this.clearDataByEle(this.selSpeciality);
    };
    ProviderOutcomeComponent.prototype.selectedSpeciality = function (value) {
        this.clearDataByEle(this.selICDCode);
    };
    // clearData() {
    //     console.log('clearData this.select1', this.select1);
    //     var activeItems = this.select1.active;
    //     this.select1.active = [];
    //     this.select1.doEvent('removed', activeItems);
    // }
    ProviderOutcomeComponent.prototype.clearDataByEle = function (ele) {
        var activeItems = ele.active;
        ele.clickedOutside();
        ele.active = [];
        //ele.itemObjects = [];
        ele.doEvent('removed', activeItems);
    };
    ProviderOutcomeComponent.prototype.submitAddressModelData = function () {
        console.log('this.addressSelectObj', this.addressSelectObj);
        this.activeFilters['default'].city = (this.addressSelectObj.city && this.addressSelectObj.city.length > 0 && this.addressSelectObj.city[0].hasOwnProperty('id')) ? this.addressSelectObj.city[0].id : "";
        this.activeFilters['default'].zip = (this.addressSelectObj.zip && this.addressSelectObj.zip.length > 0 && this.addressSelectObj.zip[0].hasOwnProperty('id')) ? this.addressSelectObj.zip[0].id : "";
        this.activeFilters['default'].icd = (this.addressSelectObj.icd && this.addressSelectObj.icd.length > 0 && this.addressSelectObj.icd[0].hasOwnProperty('id')) ? this.addressSelectObj.icd[0].id : "";
        this.activeFilters['default'].speciality = (this.addressSelectObj.speciality && this.addressSelectObj.speciality.length > 0 && this.addressSelectObj.speciality[0].hasOwnProperty('id')) ? this.addressSelectObj.speciality[0].id : "";
        if (this.activeFilters['default'].city && this.activeFilters['default'].zip && (this.activeFilters['default'].icd || this.activeFilters['default'].speciality)) {
            this.addressInfoModel.hide();
            this.getSideFilter();
            this.getAllProviderDataWithFilter();
            this.isFirstAddressSelection = false;
        }
    };
    ProviderOutcomeComponent.prototype.closeAddressInfoModel = function () {
        this.addressInfoModel.hide();
        if (this.isFirstAddressSelection) {
            this._location.back();
        }
    };
    ProviderOutcomeComponent.prototype.changeAddressInfoModel = function () {
        this.addressInfoModel.show();
    };
    ProviderOutcomeComponent.prototype.closeSMSInfoModel = function () {
        this.sendSmsInfo.hide();
    };
    ProviderOutcomeComponent.prototype.openSendSMSModel = function () {
        this.sendSmsSelectObj = [];
        this.sendSmsInfo.show();
    };
    ProviderOutcomeComponent.prototype.ShowMsg = function () {
        jQuery('#ProvideMsgScccess').show().delay(3500).fadeOut();
    };
    ProviderOutcomeComponent.prototype.sendSMStoSenders = function () {
        var _this = this;
        console.log('this.sendSmsSelectObj', this.sendSmsSelectObj);
        var selectProviderList = this.providerOutcomeData.filter(function (x) { return x.selected; });
        console.log('selectProviderList', selectProviderList);
        if (this.sendSmsSelectObj.sender && this.sendSmsSelectObj.sender.length > 0 && selectProviderList.length > 0) {
            this.isShowLoad = true;
            this.providerOutcomeService.sendSMS(this.sendSmsSelectObj.sender, selectProviderList).subscribe(function (res) {
                //console.log('sendSMStoSenders res....', res);
                _this.isShowLoad = false;
                _this.providerOutcomeData.forEach(function (x) { x.selected = false; });
                _this.sendSmsSelectObj = [];
                _this.ShowMsg();
                timers_1.setTimeout(function () {
                    _this.sendSmsInfo.hide();
                }, 5000);
            }, function (err) {
                //console.error('sendSMStoSenders error', err);
                _this.isShowLoad = false;
                _this.sendSmsInfo.hide();
            });
        }
    };
    ///----------------End---------------/////
    //---------Pagination Data-----------------////
    ProviderOutcomeComponent.prototype.getAllProviderDataWithFilter = function () {
        var _this = this;
        //console.log('this.activeFilters', this.activeFilters);
        this.isShowLoad = true;
        this.providerOutcomeService.getAllProviderDataWithFilter(this.activeFilters).subscribe(function (res) {
            //console.log('getZipCodeList res....', res);
            _this.providerOutcomeData = res.data;
            _this.loadChart();
            _this.isShowLoad = false;
        }, function (err) {
            //console.error('getZipCodeList error', err);
        });
    };
    ProviderOutcomeComponent.prototype.getSideFilter = function () {
        var _this = this;
        this.providerOutcomeService.getSideFilter(this.activeFilters).subscribe(function (res) {
            _this.activeFilters['mnp'] = res.side_bar_filter.mnp;
            _this.activeFilters['work_day'] = res.side_bar_filter.work_day;
            _this.activeFilters['modified_day'] = res.side_bar_filter.modified_day;
            _this.sendersList = res.side_bar_filter.claimant;
            console.log('this.activeFilters', _this.activeFilters);
        }, function (err) {
            //console.error('getZipCodeList error', err);
        });
    };
    ProviderOutcomeComponent.prototype.loadChart = function () {
        var dollarFormat = function (d) { return '$' + d3.format(',f')(d); };
        this.paidChartOptions = {
            chart: {
                type: 'multiBarHorizontalChart',
                height: 200,
                showLegend: false,
                showControls: false,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 50,
                    left: 100
                },
                x: function (d) { return d.label; },
                y: function (d) { return d.value; },
                showValues: true,
                valueFormat: function (d) {
                    var format = d3.format(",.2f");
                    return "$" + format(d);
                },
                multiTooltipTemplate: function (label) {
                    return label.datasetLabel + ': ' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                },
                //axis.tickFormat;
                axis: function (d) { return "$" + d; },
                duration: 1000,
                yAxis: {
                    axisLabel: 'TOTAL AMOUNT PAID TO PROVIDER',
                    showMaxMin: false,
                    tickFormat: dollarFormat,
                },
                yDomain: [0, 2000]
            }
        };
        var paidData = {
            key: "TOTAL AMOUNT PAID TO PROVIDER",
            values: []
        };
        this.providerOutcomeData.forEach(function (element) {
            //console.log('element', element);
            paidData.values.push({
                "label": element.PROVIDER_NAME,
                "value": element.TOT_PAID_AMT
            });
        });
        this.paidChartData = [
            paidData
        ];
    };
    return ProviderOutcomeComponent;
}());
__decorate([
    core_1.ViewChild('selCity'),
    __metadata("design:type", typeof (_a = typeof ng2_select_1.SelectComponent !== "undefined" && ng2_select_1.SelectComponent) === "function" && _a || Object)
], ProviderOutcomeComponent.prototype, "selCity", void 0);
__decorate([
    core_1.ViewChild('selZipCode'),
    __metadata("design:type", typeof (_b = typeof ng2_select_1.SelectComponent !== "undefined" && ng2_select_1.SelectComponent) === "function" && _b || Object)
], ProviderOutcomeComponent.prototype, "selZipCode", void 0);
__decorate([
    core_1.ViewChild('selICDCode'),
    __metadata("design:type", typeof (_c = typeof ng2_select_1.SelectComponent !== "undefined" && ng2_select_1.SelectComponent) === "function" && _c || Object)
], ProviderOutcomeComponent.prototype, "selICDCode", void 0);
__decorate([
    core_1.ViewChild('selSpeciality'),
    __metadata("design:type", typeof (_d = typeof ng2_select_1.SelectComponent !== "undefined" && ng2_select_1.SelectComponent) === "function" && _d || Object)
], ProviderOutcomeComponent.prototype, "selSpeciality", void 0);
__decorate([
    core_1.ViewChild('addressInfoModel'),
    __metadata("design:type", Object)
], ProviderOutcomeComponent.prototype, "addressInfoModel", void 0);
__decorate([
    core_1.ViewChild('sendSmsInfo'),
    __metadata("design:type", Object)
], ProviderOutcomeComponent.prototype, "sendSmsInfo", void 0);
ProviderOutcomeComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1065),
        providers: [provider_outcome_service_1.ProviderOutcomeService, ng2_nouislider_1.NouisliderModule, ng2_nvd3_1.NvD3Module],
        styles: [__webpack_require__(497)],
        encapsulation: core_1.ViewEncapsulation.None
    }),
    __metadata("design:paramtypes", [typeof (_e = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _e || Object, typeof (_f = typeof provider_outcome_service_1.ProviderOutcomeService !== "undefined" && provider_outcome_service_1.ProviderOutcomeService) === "function" && _f || Object, typeof (_g = typeof common_1.Location !== "undefined" && common_1.Location) === "function" && _g || Object])
], ProviderOutcomeComponent);
exports.ProviderOutcomeComponent = ProviderOutcomeComponent;
var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/providerOutcome.component.js.map

/***/ }),

/***/ 440:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
//
var provider_outcome_service_1 = __webpack_require__(87);
var ng2_nvd3_1 = __webpack_require__(318);
__webpack_require__(296);
__webpack_require__(672);
var ProviderReportsComponent = (function () {
    function ProviderReportsComponent(providerOutcomeService) {
        this.providerOutcomeService = providerOutcomeService;
    }
    ProviderReportsComponent.prototype.ngOnInit = function () {
        //this.loadBoxPlotChart();
        //this.loadScatterChart();
    };
    ProviderReportsComponent.prototype.loadBoxPlotChart = function () {
        this.boxPlotOptions = {
            chart: {
                type: 'boxPlotChart',
                height: 400,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 30,
                    left: 50
                },
                color: ['darkblue', 'darkorange', 'green', 'darkred', 'darkviolet'],
                x: function (d) { return d.label; },
                maxBoxWidth: 55,
                yDomain: [0, 500]
            }
        };
        this.boxPlotData = [
            {
                label: "Sample A",
                values: {
                    Q1: 180,
                    Q2: 200,
                    Q3: 250,
                    whisker_low: 0,
                    whisker_high: 400
                }
            },
            {
                label: "Sample B",
                values: {
                    Q1: 300,
                    Q2: 350,
                    Q3: 400,
                    whisker_low: 0,
                    whisker_high: 425
                }
            },
            {
                label: "Sample C",
                values: {
                    Q1: 100,
                    Q2: 200,
                    Q3: 300,
                    whisker_low: 0,
                    whisker_high: 400
                }
            }
        ];
    };
    ProviderReportsComponent.prototype.loadScatterChart = function () {
        this.scatterOptions = {
            chart: {
                type: 'scatterChart',
                height: 400,
                color: d3.scale.category10().range(),
                scatter: {
                    onlyCircles: false
                },
                showDistX: true,
                showDistY: true,
                duration: 350,
                useInteractiveGuideline: false,
                tooltip: {
                    enabled: false,
                    useInteractiveGuideline: false
                },
                useVoronoi: false,
                xAxis: {
                    axisLabel: 'Combination of Predictors(TIN, Amount Billed/Paid/Reduced, PD CPT DIFF FLAG...)',
                    tickFormat: function (d) {
                        return null;
                    },
                    ticks: 8,
                    axisLabelDistance: 30,
                    tickLength: 0
                },
                yAxis: {
                    axisLabel: 'Combination of Predictors(Holiday/WeekDay, Bills accepted/rejected...)',
                    tickFormat: function (d) {
                        return null;
                    },
                    ticks: 8,
                    axisLabelDistance: 30,
                    tickLength: 0
                },
            }
        };
        var scatterData = [
            {
                color: "#00c5ad",
                key: "Most Valued Provider",
                value: 1,
                values: [
                    { x: 1, y: 1, size: 1, shape: "circle" }
                ]
            },
            {
                color: "#fdd105",
                key: "Medium Valued Provider",
                value: 2,
                values: [
                    { x: 2, y: 2, size: 2, shape: "circle" }
                ]
            },
            {
                color: "#f72b00",
                key: "Least Valued Provider",
                value: 3,
                values: [
                    { x: 2.5, y: 2.5, size: 3, shape: "circle" }
                ]
            }
        ];
        this.scatterData = dummyDataScatter(scatterData);
        console.log('this.scatterData', this.scatterData);
    };
    return ProviderReportsComponent;
}());
ProviderReportsComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1066),
        providers: [provider_outcome_service_1.ProviderOutcomeService, ng2_nvd3_1.NvD3Module],
        styles: [__webpack_require__(497)],
        encapsulation: core_1.ViewEncapsulation.None
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof provider_outcome_service_1.ProviderOutcomeService !== "undefined" && provider_outcome_service_1.ProviderOutcomeService) === "function" && _a || Object])
], ProviderReportsComponent);
exports.ProviderReportsComponent = ProviderReportsComponent;
function generateDataScatter(groups, points) {
    var data = [], shapes = ['circle'], random = d3.random.normal();
    for (var i = 0; i < groups; i++) {
        data.push({
            key: 'Group ' + i,
            values: []
        });
        for (var j = 0; j < points; j++) {
            data[i].values.push({
                x: random(),
                y: random(),
                size: Math.random(),
                shape: shapes[j % 6]
            });
        }
    }
    return data;
}
function dummyDataScatter(dummyData) {
    var data = [], shapes = ['circle'], random = d3.random.normal();
    console.log('dummyData', dummyData);
    for (var i = 0; i < dummyData.length; i++) {
        console.log('dummyData[i]', dummyData[i]);
        for (var j = 0; j < 20; j++) {
            dummyData[i].values.push({
                x: random(),
                y: random(),
                size: 10,
                shape: shapes[j % 6]
            });
        }
    }
    return dummyData;
}
var _a;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/providerReports.component.js.map

/***/ }),

/***/ 441:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(38);
var http_1 = __webpack_require__(37);
var solr_service_1 = __webpack_require__(449);
var scout_service_1 = __webpack_require__(844);
var fda_service_1 = __webpack_require__(39);
var twitter_service_1 = __webpack_require__(845);
var rxjs_1 = __webpack_require__(31);
var dateUTC_pipe_1 = __webpack_require__(446);
var Angular2_csv_1 = __webpack_require__(451);
var platform_browser_1 = __webpack_require__(85);
__webpack_require__(31);
var ProviderDashboardComponent = (function () {
    function ProviderDashboardComponent(zone, solrService, _elmRef, router, route, sanitizer, scoutService, http, fdaService, twitterService) {
        this.zone = zone;
        this.solrService = solrService;
        this._elmRef = _elmRef;
        this.router = router;
        this.route = route;
        this.sanitizer = sanitizer;
        this.scoutService = scoutService;
        this.fdaService = fdaService;
        this.twitterService = twitterService;
        this.pageNumber = 1;
        this.recordsToFetch = 1;
        this.fetchLimit = 1;
        this.totalRecords = 6;
        this.resFacets = {};
        this.providerSummary = [];
        this.searchProviderSummaryText = "";
        this.selectedPN = [];
        this.selectedPS = [];
        this.selectedZC = [];
        this.selectedTIN = [];
        this.rersProviderSummary = [];
        this.resBillStatistics = [];
        this.resRedFlagAlerts = [];
        this.resChiropracticAlerts = [];
        this.foundRecord = true;
        this.scrollId = "";
        this.downloadJsonHref = "";
        this.resJsonResponse = {};
        this.caseOpened = false;
        this.caseStatus = "";
        //dynamic Tabs
        this.angular2TabsExample = [
            { title: 'Angular Tab 1', content: 'Angular 2 Tabs are navigable windows, each window (called tab) contains content', disabled: false, removable: true },
            { title: 'Angular Tab 2', content: 'generally we categorize contents depending on the theme', disabled: false, removable: true },
            { title: 'Angular Tab 3', content: 'Angular 2 Tabs Content', disabled: false, removable: true }
        ];
        this.caseTaskId = '';
        this.caseFirmId = '';
        this.twitterAuccessToken = '';
        this.resSocial = {};
        this.responceQuickAnalysisFlagSectionInfo = [];
        this.httpP = http;
    }
    ProviderDashboardComponent.prototype.exportCSV = function () {
        var options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            fields: ["TOTAL_PAID_AMOUNT", "PROFESSIONAL_LICENSE_ACTION", "ZIP_CODE", "STATE", "PROVIDER_LICENSE_STATUS", "PROVIDER_INDIVIDUAL_NPI",
                "NICB_ALERTS", "TAX_ID_NUMBER", "PARTY_LAST_NAME", "BUSINESS_NAME", "PROFESSIONAL_LICENSE_NUMBER", "PROVIDER_SPECIALTY",
                "AlertsDisciplinaryAction", "SUSPICIOUS_CONNECTIONS", "TOTAL_BILLED_AMOUNT", "CITY", "PARTY_FIRST_NAME", "STREET_ADDRESS",
                "AlertType", "AlertValue", "TOTAL_MEDICAL_BILLS", "BILL_YEAR"],
            fieldNames: ["Total Paid Amount", "Proffessional License Action", "Zip Code", "State", "Provider License Status", "Provider Individual NPI",
                "NICB Alerts", "Tax ID Number", "Party Last Name", "Business Name", "Professional License Number", "Provider Speciality",
                "Alerts Disciplinary Action", "Suspicious Connections", "Total Billed Amount", "City", "Party First Name", "Street Address",
                "Alert Name", "Alert Count", "Total Medical Bills", "Bill Year"]
        };
        var date = new Date();
        var objCSV = [];
        objCSV.push('Provider Summary');
        objCSV.push(JSON.parse(JSON.stringify(this.rersProviderSummary)));
        objCSV.push('Red Flag Alerts');
        objCSV.push(this.resRedFlagAlerts);
        objCSV.push('Bill Statistics');
        objCSV.push(this.resBillStatistics);
        delete objCSV[1][0]['BILL_SATISTICS'];
        delete objCSV[1][0]['RED_FLAG_ALERTS'];
        objCSV.forEach(function (element) {
            if (Object.prototype.toString.call(element) === '[object Array]') {
                element.forEach(function (element1) {
                    for (var field in element1) {
                        if ('_version_,id,DNB_REPORT_LINK,LEXIS_NEXIS_REPORT,PROFILE_PICTURE'.indexOf(field) > -1) {
                            delete element1[field];
                        }
                    }
                });
            }
        });
        var res = new Angular2_csv_1.Angular2Csv(objCSV, 'Povider Summary - ' + date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate(), options);
        //console.info('res', res);
    };
    //on select a tab do something
    ProviderDashboardComponent.prototype.doOnTabSelect = function (currentTab) {
        console.log("doOnTabSelect" + currentTab);
    };
    ;
    //on remove Tab do something
    ProviderDashboardComponent.prototype.removeThisTabHandler = function (tab) {
        console.log('Remove Tab handler' + tab);
    };
    ;
    ProviderDashboardComponent.prototype.searchProviderSummaryFacets = function () {
        var _this = this;
        this.solrService.searchProviderSummaryFacets(this.searchProviderSummaryText)
            .subscribe(function (res) {
            if (res.facet_counts) {
                _this.zone.run(function () {
                    var facetArray = [];
                    _this.resFacets = {};
                    if (res.facet_counts.facet_fields) {
                        if (res.facet_counts.facet_fields["PARTY_FIRST_NAME"] && res.facet_counts.facet_fields["PARTY_FIRST_NAME"].length > 0) {
                            for (var index = 0; index < res.facet_counts.facet_fields["PARTY_FIRST_NAME"].length; index++) {
                                if (res.facet_counts.facet_fields["PARTY_FIRST_NAME"][index + 1] > 0)
                                    facetArray.push({ name: res.facet_counts.facet_fields["PARTY_FIRST_NAME"][index], count: res.facet_counts.facet_fields["PARTY_FIRST_NAME"][index + 1] });
                                index++;
                            }
                            _this.resFacets.PARTY_FIRST_NAME = facetArray;
                        }
                        if (res.facet_counts.facet_fields["PROVIDER_SPECIALTY"] && res.facet_counts.facet_fields["PROVIDER_SPECIALTY"].length > 0) {
                            facetArray = [];
                            for (var index = 0; index < res.facet_counts.facet_fields["PROVIDER_SPECIALTY"].length; index++) {
                                if (res.facet_counts.facet_fields["PROVIDER_SPECIALTY"][index + 1] > 0)
                                    facetArray.push({ name: res.facet_counts.facet_fields["PROVIDER_SPECIALTY"][index], count: res.facet_counts.facet_fields["PROVIDER_SPECIALTY"][index + 1] });
                                index++;
                            }
                            _this.resFacets.PROVIDER_SPECIALTY = facetArray;
                        }
                        if (res.facet_counts.facet_fields["ZIP_CODE"] && res.facet_counts.facet_fields["ZIP_CODE"].length > 0) {
                            facetArray = [];
                            for (var index = 0; index < res.facet_counts.facet_fields["ZIP_CODE"].length; index++) {
                                if (res.facet_counts.facet_fields["ZIP_CODE"][index + 1] > 0)
                                    facetArray.push({ name: res.facet_counts.facet_fields["ZIP_CODE"][index], count: res.facet_counts.facet_fields["ZIP_CODE"][index + 1] });
                                index++;
                            }
                            _this.resFacets.ZIP_CODE = facetArray;
                        }
                        if (res.facet_counts.facet_fields["TAX_ID_NUMBER"] && res.facet_counts.facet_fields["TAX_ID_NUMBER"].length > 0) {
                            facetArray = [];
                            for (var index = 0; index < res.facet_counts.facet_fields["TAX_ID_NUMBER"].length; index++) {
                                if (res.facet_counts.facet_fields["TAX_ID_NUMBER"][index + 1] > 0)
                                    facetArray.push({ name: res.facet_counts.facet_fields["TAX_ID_NUMBER"][index], count: res.facet_counts.facet_fields["TAX_ID_NUMBER"][index + 1] });
                                index++;
                            }
                            _this.resFacets.TAX_ID_NUMBER = facetArray;
                        }
                        _this.route
                            .queryParams
                            .subscribe(function (params) {
                            if (params.provider_firstname) {
                                var provider_firstname = params.provider_firstname;
                                var provider_lastname = params.provider_lastname;
                                //console.log('provider_firstname', provider_firstname);
                                //console.log('provider_lastname', provider_lastname);
                                for (var key in _this.resFacets.PARTY_FIRST_NAME) {
                                    //console.log('key', this.resFacets.PARTY_FIRST_NAME[key].name);
                                    if (_this.resFacets.PARTY_FIRST_NAME[key].name == provider_firstname) {
                                        //console.log('INNN', this.resFacets.PARTY_FIRST_NAME[key]);
                                        _this.changeSelectFacet(_this.resFacets.PARTY_FIRST_NAME[key], 1);
                                    }
                                }
                            }
                        });
                    }
                });
            }
        }, function (error) {
        });
    };
    ProviderDashboardComponent.prototype.getProviderFacets = function () {
        var _this = this;
        this.solrService.getProviderFacets(this.searchProviderSummaryText)
            .subscribe(function (res) {
            if (res.fields) {
                _this.zone.run(function () {
                    _this.resFacets = {};
                    _this.resFacets.PARTY_FIRST_NAME = res.fields.partyFirstName;
                    _this.resFacets.PROVIDER_SPECIALTY = res.fields.providerSpeciality;
                    _this.resFacets.ZIP_CODE = res.fields.zipCode;
                    _this.resFacets.TAX_ID_NUMBER = res.fields.taxIdNumber;
                });
            }
        }, function (error) {
            if (error.toString().indexOf('401') >= 0) {
                _this.getProviderFacetsWithNewToken();
            }
        });
    };
    ProviderDashboardComponent.prototype.getProviderFacetsWithNewToken = function () {
        var _this = this;
        this.solrService.getAuthToken()
            .subscribe(function (res) {
            if (res.access_token) {
                window.localStorage.setItem('apaticsAuthToken', res.access_token);
                _this.solrService.getProviderFacets(_this.searchProviderSummaryText)
                    .subscribe(function (res) {
                    if (res.fields) {
                        _this.zone.run(function () {
                            _this.resFacets = {};
                            _this.resFacets.PARTY_FIRST_NAME = res.fields.partyFirstName;
                            _this.resFacets.PROVIDER_SPECIALTY = res.fields.providerSpeciality;
                            _this.resFacets.ZIP_CODE = res.fields.zipCode;
                            _this.resFacets.TAX_ID_NUMBER = res.fields.taxIdNumber;
                        });
                    }
                }, function (error) {
                });
            }
        }, function (error) {
        });
    };
    ProviderDashboardComponent.prototype.changeSelectFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            var changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    _this.selectedPN.push(objFacet.name);
                else if (index == 2)
                    _this.selectedPS.push(objFacet.name);
                else if (index == 3)
                    _this.selectedZC.push(objFacet.name);
                else if (index == 4)
                    _this.selectedTIN.push(objFacet.name);
            }
            else {
                var arrayIndex = -1;
                if (index == 1) {
                    arrayIndex = _this.selectedPN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPN.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = _this.selectedPS.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedPS.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = _this.selectedZC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedZC.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = _this.selectedTIN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedTIN.splice(arrayIndex, 1);
                }
            }
        });
        this.searchProviderSummaryRecordsWithFilters(1);
        //this.getProviderRecordsWithFilters();
    };
    ProviderDashboardComponent.prototype.removeSelectedFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            objFacet.selected = false;
            var arrayIndex = -1;
            if (index == 1) {
                arrayIndex = _this.selectedPN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPN.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = _this.selectedPS.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedPS.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = _this.selectedZC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedZC.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = _this.selectedTIN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedTIN.splice(arrayIndex, 1);
            }
        });
        this.searchProviderSummaryRecordsWithFilters(1);
        //this.getProviderRecordsWithFilters();
    };
    ProviderDashboardComponent.prototype.clearSelectedFacets = function () {
        this.selectedPN = [];
        this.selectedPS = [];
        this.selectedZC = [];
        this.selectedTIN = [];
    };
    ProviderDashboardComponent.prototype.searchProviderSummary = function () {
        this.clearSelectedFacets();
        this.searchProviderSummaryRecordsWithFilters(1);
        this.searchProviderSummaryFacets();
        //this.getProviderRecordsWithFilters();//new aptics api
        //this.getProviderFacets();//new aptics api
    };
    ProviderDashboardComponent.prototype.getProviderRecordsWithFilters = function () {
        var _this = this;
        var fLimit = 0;
        var selectedPNString = JSON.parse(JSON.stringify(this.searchProviderSummaryText));
        //alert('j' + selectedPNString + 'j');
        if (this.selectedPN && this.selectedPN.length > 0)
            selectedPNString += (selectedPNString.trim().length > 0 ? ' AND ' : '') + 'partyFirstName: ' + this.selectedPN.join(' OR ');
        if (this.selectedPS && this.selectedPS.length > 0)
            selectedPNString += (selectedPNString.trim().length > 0 ? ' AND ' : '') + 'providerSpeciality: ' + this.selectedPS.join(' OR ');
        if (this.selectedZC && this.selectedZC.length > 0)
            selectedPNString += (selectedPNString.trim().length > 0 ? ' AND ' : '') + 'zipCode: ' + this.selectedZC.join(' OR ');
        if (this.selectedTIN && this.selectedTIN.length > 0)
            selectedPNString += (selectedPNString.trim().length > 0 ? ' AND ' : '') + 'taxIdNumber: ' + this.selectedTIN.join(' OR ');
        //alert(selectedPNString);
        //this.fetchLimit = selectedPNString.trim().length > 0 ? 6 : this.fetchLimit;
        this.asyncProviderSummary = this.solrService.getProviderRecordsWithFilter(selectedPNString, this.scrollId, selectedPNString.trim().length > 0 ? 6 : this.fetchLimit)
            .do(function (res) {
            _this.scrollId = '';
            if (res.scrollId) {
                _this.scrollId = res.scrollId;
            }
            _this.totalRecords = selectedPNString.trim().length > 0 ? res.providers.length : 6;
            fLimit = res.providers.length - 1;
            if (res.providers && res.providers.length > 0) {
                _this.bindBillStatistics(res.providers[fLimit].taxIdNumber);
                _this.bindRedFlagAlerts(res.providers[fLimit].taxIdNumber);
                _this.bindChiropracticAlerts(res.providers[fLimit].professionalLicenseNumber);
            }
            // console.info('this.scrollId', this.scrollId);
            // console.info('res', res);
        }).map(function (res) {
            fLimit = res.providers.length - 1;
            if (res.providers && res.providers.length > 0) {
                var providerSummary = {};
                providerSummary.PARTY_FIRST_NAME = res.providers[fLimit].partyFirstName;
                providerSummary.PARTY_LAST_NAME = res.providers[fLimit].partyLastName;
                providerSummary.NICB_ALERTS = res.providers[fLimit].nicbAlerts;
                providerSummary.DNB_REPORT_LINK = res.providers[fLimit].dnbReportLink.replace('prototype', 'assets');
                providerSummary.LEXIS_NEXIS_REPORT = res.providers[fLimit].lexisNexisReport.replace('prototype', 'assets');
                providerSummary.STREET_ADDRESS = res.providers[fLimit].streetAddress || '';
                providerSummary.STATE = res.providers[fLimit].state;
                providerSummary.ZIP_CODE = res.providers[fLimit].zipCode || '';
                providerSummary.TAX_ID_NUMBER = res.providers[fLimit].taxIdNumber || '';
                providerSummary.PROFESSIONAL_LICENSE_NUMBER = res.providers[fLimit].professionalLicenseNumber || '';
                providerSummary.PROVIDER_LICENSE_STATUS = res.providers[fLimit].providerLicenseStatus || '';
                providerSummary.PROVIDER_INDIVIDUAL_NPI = res.providers[fLimit].providerIndividualNPI;
                providerSummary.STATE = res.providers[fLimit].state;
                providerSummary.STATE = res.providers[fLimit].state;
                providerSummary.STATE = res.providers[fLimit].state;
                providerSummary.PROFILE_PICTURE = res.providers[fLimit].profilePicture.replace('prototype', 'assets');
                return [providerSummary];
            }
            else {
                return [];
            }
        });
    };
    ProviderDashboardComponent.prototype.searchProviderSummaryRecordsWithFilters = function (pageNum) {
        var _this = this;
        this.pageNumber = pageNum;
        var selectedPNString = "";
        var selectedPSString = "";
        var selectedZCString = "";
        var selectedTINString = "";
        if (this.selectedPN && this.selectedPN.length > 0)
            selectedPNString = this.selectedPN.join('"OR"');
        if (this.selectedPS && this.selectedPS.length > 0)
            selectedPSString = this.selectedPS.join('"OR"');
        if (this.selectedZC && this.selectedZC.length > 0)
            selectedZCString = this.selectedZC.join('"OR"');
        if (this.selectedTIN && this.selectedTIN.length > 0)
            selectedTINString = this.selectedTIN.join('"OR"');
        this.asyncProviderSummary = this.solrService.searchProviderSummaryRecordsWithFilter(this.searchProviderSummaryText, selectedPNString, selectedPSString, selectedZCString, selectedTINString, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            //console.info('res.response', res.response);
            if (res.response) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.numFound;
                    _this.rersProviderSummary = res.response.docs || [];
                    _this.foundRecord = res.response.numFound > 0;
                });
                if (_this.rersProviderSummary && _this.rersProviderSummary.length > 0) {
                    //generate res for download json
                    _this.generatePersonalInfoResponse();
                    if (_this.rersProviderSummary[0].PARTY_FIRST_NAME && _this.rersProviderSummary[0].PARTY_FIRST_NAME.toLowerCase().indexOf('medical') >= 0) {
                        _this.rersProviderSummary[0].PARTY_FIRST_NAME = 'Rebecca';
                        _this.rersProviderSummary[0].PARTY_LAST_NAME = 'Hall';
                    }
                    _this.bindBillStatistics(_this.rersProviderSummary[0].TAX_ID_NUMBER);
                    _this.bindRedFlagAlerts(_this.rersProviderSummary[0].TAX_ID_NUMBER);
                    _this.bindChiropracticAlerts(_this.rersProviderSummary[0].PROFESSIONAL_LICENSE_NUMBER);
                    //this.checkImportedCase(this.rersProviderSummary[0].TAX_ID_NUMBER);
                }
            }
            else {
                _this.zone.run(function () {
                    _this.totalRecords = 0;
                    _this.rersProviderSummary = [];
                    _this.resBillStatistics = [];
                    _this.resRedFlagAlerts = [];
                    _this.resChiropracticAlerts = [];
                });
            }
            _this;
        })
            .map(function (res) { return res.response ? res.response.docs : []; });
    };
    ProviderDashboardComponent.prototype.bindBillStatistics = function (taxIDNumber) {
        var _this = this;
        this.resBillStatistics = [];
        this.solrService.getProviderSummaryBillStatistics(taxIDNumber)
            .subscribe(function (res) {
            if (res.response && res.response.docs) {
                _this.resBillStatistics = res.response.docs;
                //generate res for download json
                _this.generateBillStatisticsResponse();
            }
        }, function (error) {
        });
    };
    ProviderDashboardComponent.prototype.bindRedFlagAlerts = function (taxIDNumber) {
        var _this = this;
        this.resRedFlagAlerts = [];
        this.solrService.getProviderSummaryRedFlagAlerts(taxIDNumber)
            .subscribe(function (res) {
            if (res.response && res.response.docs) {
                _this.resRedFlagAlerts = res.response.docs;
                //generate res for download json
                _this.generateRedflagAlertsResponse();
            }
        }, function (error) {
        });
    };
    ProviderDashboardComponent.prototype.bindChiropracticAlerts = function (licenseNumber) {
        var _this = this;
        this.resChiropracticAlerts = [];
        //licenseNumber = 25373;
        this.solrService.getProviderSummaryChiropracticAlerts(licenseNumber)
            .subscribe(function (res) {
            if (res.response && res.response.docs) {
                _this.resChiropracticAlerts = res.response.docs;
            }
        }, function (error) {
        });
    };
    ProviderDashboardComponent.prototype.viewRedFlagAlertDetails = function (taxIDNumber, alertType) {
        switch (alertType.replace(/ /g, "").replace(/_/g, "").toLowerCase()) {
            case "upcoding":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 1);
                break;
            case "excessivetesting":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 2);
                break;
            case "deniedclaims":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 3);
                break;
            case "duplicatebilling":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 4);
                break;
            case "excessivedmeprescription":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 5);
                break;
            case "excessimagingservices":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 6);
                break;
            case "clusterbilling":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 7);
                break;
            case "painpumps":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 8);
                break;
            case "autologoustransfusion":
                this.navigateRedFlagAlertDetails("doublebilling", taxIDNumber, 9);
                break;
            default:
                break;
        }
    };
    ProviderDashboardComponent.prototype.navigateRedFlagAlertDetails = function (pagename, taxIDNumber, pageIndex) {
        var navigationExtras = {
            queryParams: {
                "taxIdNumber": taxIDNumber,
                "p": pageIndex
            }
        };
        this.router.navigate(['../' + pagename], navigationExtras);
    };
    ProviderDashboardComponent.prototype.generatePersonalInfoResponse = function () {
        this.resJsonResponse = {};
        if (this.rersProviderSummary && this.rersProviderSummary.length > 0) {
            var objRes = this.rersProviderSummary[0];
            var objPersonalInfo = {};
            objPersonalInfo["Name"] = objRes.PARTY_FIRST_NAME + ' ' + objRes.PARTY_LAST_NAME;
            objPersonalInfo["Speciality"] = "Chiropractor"; //objRes.PARTY_FIRST_NAME;
            objPersonalInfo["NICB Alerts"] = objRes.NICB_ALERTS;
            objPersonalInfo["Address"] = objRes.STREET_ADDRESS + ' ' + objRes.CITY + ' ' + objRes.STATE + ' ' + objRes.ZIP_CODE;
            objPersonalInfo["TIN"] = objRes.TAX_ID_NUMBER;
            objPersonalInfo["Licence No"] = objRes.PROFESSIONAL_LICENSE_NUMBER;
            objPersonalInfo["Licence Status"] = objRes.PROVIDER_LICENSE_STATUS;
            objPersonalInfo["NPI"] = objRes.PROVIDER_INDIVIDUAL_NPI;
            this.resJsonResponse["Personal Info"] = objPersonalInfo;
        }
        this.generateDownloadJsonUri();
    };
    ProviderDashboardComponent.prototype.generateBillStatisticsResponse = function () {
        if (this.resBillStatistics && this.resBillStatistics.length > 0) {
            var objBillStatistics = [];
            this.resBillStatistics.forEach(function (objBS) {
                objBillStatistics.push({
                    "Year": objBS.BILL_YEAR, "Bill Count": objBS.TOTAL_MEDICAL_BILLS,
                    "Billed Amount": objBS.TOTAL_BILLED_AMOUNT, "Paid Amount": objBS.TOTAL_PAID_AMOUNT
                });
            });
            this.resJsonResponse["Bill Statistics"] = objBillStatistics;
        }
        this.generateDownloadJsonUri();
    };
    ProviderDashboardComponent.prototype.generateRedflagAlertsResponse = function () {
        if (this.resRedFlagAlerts && this.resRedFlagAlerts.length > 0) {
            var objRedFlagAlerts = {};
            this.resRedFlagAlerts.forEach(function (objRFA) {
                objRedFlagAlerts[objRFA.AlertType] = objRFA.AlertValue;
            });
            this.resJsonResponse["Red Flag Alerts"] = objRedFlagAlerts;
        }
        this.generateDownloadJsonUri();
    };
    ProviderDashboardComponent.prototype.generateDownloadJsonUri = function () {
        var theJSON = JSON.stringify(this.resJsonResponse);
        var uri = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
        this.downloadJsonHref = uri;
    };
    ProviderDashboardComponent.prototype.checkImportedCase = function (taxIDNumber) {
        var _this = this;
        this.caseTaxIDNumber = taxIDNumber;
        this.caseOpened = false;
        this.caseStatus = '';
        this.caseTaskId = '';
        this.startCaseStatusInterval(taxIDNumber);
        this.fdaService.getCaseStatus(taxIDNumber)
            .subscribe(function (res) {
            if (res.success && res.response) {
                if (res.response.status) {
                    _this.caseOpened = true;
                    _this.caseStatus = res.response.status;
                    _this.caseTaskId = res.response.taskId;
                    _this.caseFirmId = res.response.firmId;
                }
            }
        }, function (error) {
        });
    };
    ProviderDashboardComponent.prototype.startCaseStatusInterval = function (taxIDNumber) {
        this.caseOpened = false;
        this.caseStatus = '';
        //this.setCaseStatusInterval(taxIDNumber);
        // this.caseStatusTimer
        //     .subscribe(res => {
        //         if (res.success && res.response) {
        //             if (res.response.status) {
        //                 this.caseOpened = true;
        //                 this.caseStatus = res.response.status;
        //             }
        //         }
        //     }
        //     , error => { 
        //     });
    };
    ProviderDashboardComponent.prototype.setCaseStatusInterval = function (taxIDNumber) {
        var _this = this;
        if (this.caseStatusTimer)
            //this.caseStatusTimer.unsubscribe();
            this.caseStatusTimer = rxjs_1.Observable.interval(5000)
                .flatMap(function () {
                return _this.fdaService.getCaseStatus(taxIDNumber);
            })
                .subscribe(function (res) {
                if (res.success && res.response) {
                    if (res.response.status) {
                        _this.caseOpened = true;
                        _this.caseStatus = res.response.status;
                    }
                    else {
                        _this.caseOpened = false;
                        _this.caseStatus = '';
                    }
                }
            }, function (error) {
            });
    };
    ProviderDashboardComponent.prototype.importCaseInScout = function (taxIdNumber) {
        var _this = this;
        var objData = {};
        objData.providerBasic = this.rersProviderSummary[0];
        objData.billStatistics = this.resBillStatistics;
        objData.redFlagAlerts = this.resRedFlagAlerts;
        objData.chiropractorAlerts = this.resChiropracticAlerts;
        this.scoutService.importCase(objData)
            .subscribe(function (res) {
            console.log('resSoap', res);
            var resXMLString = res;
            var resSubString = "";
            resSubString = _this.parseXMLTag(resXMLString, "Success");
            ;
            var resSuccess = resSubString == "true";
            console.log("resSuccess", resSuccess);
            if (resSuccess) {
                resSubString = _this.parseXMLTag(resXMLString, "ScoutID");
                var resScoutId = parseInt(resSubString);
                console.log("resScoutId", resScoutId);
                var objJSONData = { 'taxIdNumber': taxIdNumber, "scoutId": resScoutId, "status": "Open" };
                _this.caseOpened = true;
                _this.caseStatus = "Open";
                _this.saveCase(objJSONData);
                //this.changeCaseStatus(resScoutId, 'Pending');
            }
            else {
                resSubString = _this.parseXMLTag(resXMLString, "Message");
                var resErrorMessage = resSubString;
                console.log("resErrorMessage", resErrorMessage);
                alert(resErrorMessage);
                // var objJSONData = { 'taxIdNumber': taxIdNumber, "scoutId": 123, "status": "pending" };
                // this.saveCase(objJSONData);
            }
        }, function (error) {
            console.log('error', error);
        });
        // xml2js.parseString('<foo>bar</foo>', function (err, result) {
        //     console.log(result);
        // });
    };
    ProviderDashboardComponent.prototype.generateCase = function (objProvider) {
        var _this = this;
        var objData = {};
        objData.taxIdNumber = objProvider.TAX_ID_NUMBER;
        objData.partyFirstName = objProvider.PARTY_FIRST_NAME;
        objData.partyLastName = objProvider.PARTY_LAST_NAME;
        objData.profilePicture = objProvider.PROFILE_PICTURE;
        objData.providerSpecialty = objProvider.PROVIDER_SPECIALTY;
        objData.professionalLicenseNumber = objProvider.PROFESSIONAL_LICENSE_NUMBER;
        objData.professionalLicenseAction = objProvider.PROFESSIONAL_LICENSE_ACTION;
        objData.providerLicenseStatus = objProvider.PROVIDER_LICENSE_STATUS;
        objData.providerIndividualNPI = objProvider.PROVIDER_INDIVIDUAL_NPI;
        objData.businessName = objProvider.BUSINESS_NAME;
        objData.nICBAlerts = objProvider.NICB_ALERTS;
        objData.suspiciousConnections = objProvider.SUSPICIOUS_CONNECTIONS;
        objData.alertsDisciplinaryAction = objProvider.AlertsDisciplinaryAction;
        objData.totalPaidAmount = objProvider.TOTAL_PAID_AMOUNT;
        objData.totalBilledAmount = objProvider.TOTAL_BILLED_AMOUNT;
        objData.streetAddress = objProvider.STREET_ADDRESS;
        objData.city = objProvider.CITY;
        objData.state = objProvider.STATE;
        objData.zipCode = objProvider.ZIP_CODE;
        objData.dNBReportLink = objProvider.DNB_REPORT_LINK;
        objData.lexisNexisReportLink = objProvider.LEXIS_NEXIS_REPORT;
        var jsonStringifyObject = "";
        if (this.resRedFlagAlerts && this.resRedFlagAlerts.length > 0)
            jsonStringifyObject = JSON.stringify(this.resRedFlagAlerts);
        objData.redFlagAlerts = jsonStringifyObject;
        jsonStringifyObject = "";
        if (this.resBillStatistics && this.resBillStatistics.length > 0)
            jsonStringifyObject = JSON.stringify(this.resBillStatistics);
        objData.billStatistics = jsonStringifyObject;
        this.fdaService.saveCase(objData)
            .subscribe(function (res) {
            if (res.success) {
                _this.caseOpened = true;
                _this.caseStatus = res.response.status;
                _this.caseTaskId = res.response.taskId;
                _this.caseFirmId = res.response.firmId;
                _this.openCaseDetailInCaseManagement();
            }
        }, function (error) {
        });
    };
    ProviderDashboardComponent.prototype.parseXMLTag = function (resXMLString, tagName) {
        var resTagValue = "";
        var startIndex = -1;
        var endIndex = -1;
        startIndex = resXMLString.indexOf('<' + tagName + '>');
        endIndex = resXMLString.indexOf('</' + tagName + '>');
        if (startIndex >= 0 && endIndex >= 0)
            resTagValue = resXMLString.substring(startIndex, endIndex).replace("<" + tagName + ">", "");
        return resTagValue;
    };
    ProviderDashboardComponent.prototype.saveCase = function (objJSONData) {
        this.fdaService.saveCaseStatus(objJSONData)
            .subscribe(function (res) {
            console.log('resSaveCase', res);
        }, function (error) {
        });
    };
    ProviderDashboardComponent.prototype.changeCaseStatus = function (scoutID, status) {
        var _this = this;
        this.caseOpened = false;
        this.caseStatus = '';
        this.scoutService.changeCaseStatus(scoutID, status)
            .subscribe(function (res) {
            if (res.success && res.response) {
                if (res.response.id > 0) {
                    _this.caseOpened = true;
                    _this.caseStatus = res.response.status;
                }
            }
        }, function (error) {
        });
    };
    ProviderDashboardComponent.prototype.openCaseDetailInCaseManagement = function () {
        //var pageUrl = 'http://localhost/ApaticsTasksLive/portal/tasks.aspx';
        var pageUrl = 'http://casemgmt.apatics.net/portal/tasks.aspx';
        window.open(pageUrl + '?tid=' + this.caseTaskId + '&fid=' + this.caseFirmId, '_blank');
    };
    ProviderDashboardComponent.prototype.gerAccessToken = function () {
        this.twitterService.getAccessToken()
            .subscribe(function (res) {
            console.log('resAccessToken', res);
        }, function (error) {
        });
    };
    ProviderDashboardComponent.prototype.getTwitterData = function () {
        var _this = this;
        this.fdaService.getTwitterHashtagData()
            .subscribe(function (res) {
            if (res.success && res.response) {
                if (res.response.healthCareFrauds) {
                    _this.resSocial.twitterHealthCareFraud = res.response.healthCareFrauds;
                    if (_this.resSocial.twitterHealthCareFraud && _this.resSocial.twitterHealthCareFraud.length > 0 && _this.resSocial.twitterHealthCareFraud.length < 6) {
                        var twitterHealthCareArrayLength = _this.resSocial.twitterHealthCareFraud.length;
                        for (var index = 0; index < 12 - twitterHealthCareArrayLength; index++) {
                            _this.resSocial.twitterHealthCareFraud.push(_this.resSocial.twitterHealthCareFraud[index]);
                        }
                    }
                }
                if (res.response.medicalBillings) {
                    _this.resSocial.twitterMedicalBillings = res.response.medicalBillings;
                    if (_this.resSocial.twitterMedicalBillings && _this.resSocial.twitterMedicalBillings.length > 0 && _this.resSocial.twitterMedicalBillings.length < 12) {
                        var medicalBillingsArrayLength = _this.resSocial.twitterMedicalBillings.length;
                        for (var index = 0; index < 12 - medicalBillingsArrayLength; index++) {
                            _this.resSocial.twitterMedicalBillings.push(_this.resSocial.twitterMedicalBillings[index]);
                        }
                    }
                }
                console.log('this.resSocial', _this.resSocial);
            }
        }, function (error) {
        });
    };
    ProviderDashboardComponent.prototype.getDialogFeedSocialWall = function () {
        var _this = this;
        this.twitterService.getDialogFeedData()
            .subscribe(function (res) {
            console.log('resSocialWall', res);
            if (res.news_feed && res.news_feed.posts && res.news_feed.posts.post && res.news_feed.posts.post.length > 0) {
                var resPosts = res.news_feed.posts.post;
                var resTwitter = [];
                var resInstagram = [];
                var resRssFeed = [];
                res.news_feed.posts.post.forEach(function (objPost) {
                    if (objPost.source && objPost.source.name == 'twitter') {
                        resTwitter.push(objPost);
                    }
                    else if (objPost.source && objPost.source.name == 'instagram') {
                        resInstagram.push(objPost);
                    }
                    else if (objPost.source && objPost.source.name == 'rss') {
                        resRssFeed.push(objPost);
                    }
                });
                if (resTwitter.length > 0)
                    _this.resSocial.twitter = resTwitter;
                if (resInstagram.length > 0)
                    _this.resSocial.instagram = resInstagram;
                if (resRssFeed.length > 0)
                    _this.resSocial.rssFeed = resRssFeed;
                if (_this.resSocial.twitter && _this.resSocial.twitter.length > 0 && _this.resSocial.twitter.length < 18) {
                    var twitterArrayLength = _this.resSocial.twitter.length;
                    for (var index = 0; index < 18 - _this.resSocial.twitter.length; index++) {
                        _this.resSocial.twitter.push(_this.resSocial.twitter[index]);
                    }
                }
                if (_this.resSocial.instagram && _this.resSocial.instagram.length > 0 && _this.resSocial.instagram.length < 12) {
                    var instagramArrayLength = _this.resSocial.instagram.length;
                    for (var index = 0; index < 12 - instagramArrayLength; index++) {
                        _this.resSocial.instagram.push(_this.resSocial.instagram[index]);
                    }
                }
                if (_this.resSocial.rssFeed && _this.resSocial.rssFeed.length > 0 && _this.resSocial.rssFeed.length < 24) {
                    var rssFeedArrayLength = _this.resSocial.rssFeed.length;
                    for (var index = 0; index < 24 - rssFeedArrayLength; index++) {
                        _this.resSocial.rssFeed.push(_this.resSocial.rssFeed[index]);
                    }
                }
                console.log('this.resSocial', _this.resSocial);
            }
        }, function (error) {
        });
    };
    ProviderDashboardComponent.prototype.ngOnInit = function () {
        this.searchProviderSummaryRecordsWithFilters(1);
        //this.getProviderRecordsWithFilters();//new aptics api
        this.searchProviderSummaryFacets();
        //this.getProviderFacets();//new aptics api
        //this.testSoutService();
        //console.log(xml2json("<foo>bar</foo>",""));
        this.getTwitterData();
        //this.getDialogFeedSocialWall();
        this.ongetmoreinfo('subjectSection');
        this.ongetmoreinfo('SSNAddressFraudSection');
        this.ongetmoreinfo('BusinessAtSubjectAddressSection');
        this.ongetmoreinfo('QuickAnalysisFlagSection');
        this.ongetmoreinfo('LicenseSection');
    };
    ProviderDashboardComponent.prototype.ngOnDestroy = function () {
        //this.caseStatusTimer.unsubscribe();
    };
    ProviderDashboardComponent.prototype.ongetmoreinfo = function (sectionName) {
        var _this = this;
        return this.httpP.get("assets/json/bryan.json")
            .subscribe(function (matchdata) {
            var resJson = matchdata.json();
            //console.log("hello");
            if (sectionName === 'subjectSection') {
                _this.responcesubjectInfo = resJson["pr:PersonReportDetails"]['ClearSectionResults'][0]['SectionDetails']["ns2:SubjectSection"]["SubjectRecord"]["PersonAKAInfo"];
                //this.moreInfoModal.open();
            }
            if (sectionName === 'BusinessAtSubjectAddressSection') {
                _this.responceSubjectAddressInfo = resJson["pr:PersonReportDetails"]['ClearSectionResults'][6]['SectionDetails']["ns2:BusinessAtSubjectAddressSection"]["BusinessAtSubjectAddressRecord"];
                //this.BusinessAtSubjectAddressSectionModal.open();
            }
            if (sectionName === 'LicenseSection') {
                _this.responceLicenseSectionInfo = resJson["pr:PersonReportDetails"]['ClearSectionResults'][4]['SectionDetails']["ns2:LicenseSection"]["ProfLicenseRecord"];
                //this.LicenseSectionModal.open();
            }
            if (sectionName === 'SSNAddressFraudSection') {
                _this.responceSSNAddressFraudSectionInfo = resJson["pr:PersonReportDetails"]['ClearSectionResults'][1]['SectionDetails']["ns2:SSNAddressFraudSection"]["SSNAddressFraudRecord"];
                //this.SSNAddressFraudSectionModal.open();
            }
            if (sectionName === 'QuickAnalysisFlagSection') {
                _this.tmpresponceQuickAnalysisFlagSectionInfo = resJson["pr:PersonReportDetails"]['ClearSectionResults'][3]['SectionDetails']["ns2:QuickAnalysisFlagSection"]["QuickAnalysisFlagRecord"];
                _this.tmpresponceQuickAnalysisFlagSectionInfo.forEach(function (element1) {
                    for (var field in element1["RiskFlags"]) {
                        //console.log(field); console.log(element1);
                        if (element1["RiskFlags"][field] === 'Yes' || element1["RiskFlags"][field] === 'Yes') {
                            _this.responceQuickAnalysisFlagSectionInfo.push({ 'riskname': field, 'riskvalue': element1["RiskFlags"][field] });
                        }
                        //this.responceQuickAnalysisFlagSectionInfo["riskvalue"] = element1;
                    }
                });
                // this.QuickAnalysisFlagSectionModal.open();
            }
            if (sectionName === 'CriminalSection') {
                _this.responceCriminalSectionInfo = resJson["pr:PersonReportDetails"]['ClearSectionResults'][5]['SectionDetails']["ns2:CriminalSection"]["CriminalExpansionRecord"];
                _this.CriminalSectionModal.open();
            }
            console.log(_this.responceQuickAnalysisFlagSectionInfo);
        });
    };
    return ProviderDashboardComponent;
}());
__decorate([
    core_1.ViewChild('moreInfoModal'),
    __metadata("design:type", Object)
], ProviderDashboardComponent.prototype, "moreInfoModal", void 0);
__decorate([
    core_1.ViewChild('BusinessAtSubjectAddressSectionModal'),
    __metadata("design:type", Object)
], ProviderDashboardComponent.prototype, "BusinessAtSubjectAddressSectionModal", void 0);
__decorate([
    core_1.ViewChild('LicenseSectionModal'),
    __metadata("design:type", Object)
], ProviderDashboardComponent.prototype, "LicenseSectionModal", void 0);
__decorate([
    core_1.ViewChild('SSNAddressFraudSectionModal'),
    __metadata("design:type", Object)
], ProviderDashboardComponent.prototype, "SSNAddressFraudSectionModal", void 0);
__decorate([
    core_1.ViewChild('QuickAnalysisFlagSectionModal'),
    __metadata("design:type", Object)
], ProviderDashboardComponent.prototype, "QuickAnalysisFlagSectionModal", void 0);
__decorate([
    core_1.ViewChild('CriminalSectionModal'),
    __metadata("design:type", Object)
], ProviderDashboardComponent.prototype, "CriminalSectionModal", void 0);
ProviderDashboardComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1067),
        providers: [solr_service_1.SOLRService, scout_service_1.ScoutService, fda_service_1.FDAService, twitter_service_1.TwitterService, dateUTC_pipe_1.DateUTCPipe]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.NgZone !== "undefined" && core_1.NgZone) === "function" && _a || Object, typeof (_b = typeof solr_service_1.SOLRService !== "undefined" && solr_service_1.SOLRService) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object, typeof (_d = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _d || Object, typeof (_e = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _e || Object, typeof (_f = typeof platform_browser_1.DomSanitizer !== "undefined" && platform_browser_1.DomSanitizer) === "function" && _f || Object, typeof (_g = typeof scout_service_1.ScoutService !== "undefined" && scout_service_1.ScoutService) === "function" && _g || Object, typeof (_h = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _h || Object, typeof (_j = typeof fda_service_1.FDAService !== "undefined" && fda_service_1.FDAService) === "function" && _j || Object, typeof (_k = typeof twitter_service_1.TwitterService !== "undefined" && twitter_service_1.TwitterService) === "function" && _k || Object])
], ProviderDashboardComponent);
exports.ProviderDashboardComponent = ProviderDashboardComponent;
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/providerdashboard.component.js.map

/***/ }),

/***/ 442:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var fda_service_1 = __webpack_require__(39);
var rxjs_1 = __webpack_require__(31);
var RevenueCodeComponent = (function () {
    function RevenueCodeComponent(zone, fdaService, _elmRef) {
        this.zone = zone;
        this.fdaService = fdaService;
        this._elmRef = _elmRef;
        this.selectedProductId = "";
        this.selectedRevenueCode = {};
        this.pageNumber = 1;
        this.recordsToFetch = 10;
        this.totalRecords = 0;
        this.resFacets = {};
        this.searchRevenueCodeText = "";
        this.selectedREV_CDE = [];
        this.selectedREV_DSC = [];
        this.rersRevenueCode = [];
        this.foundRecord = true;
    }
    RevenueCodeComponent.prototype.searchRevenueCodeFacets = function () {
        var _this = this;
        this.fdaService.searchRevenueCodeFacets(this.searchRevenueCodeText)
            .subscribe(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.resFacets = res.response;
                });
            }
        }, function (error) {
        });
    };
    RevenueCodeComponent.prototype.changeSelectFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            var changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    _this.selectedREV_CDE.push(objFacet.name);
                else if (index == 2)
                    _this.selectedREV_DSC.push(objFacet.name);
            }
            else {
                var arrayIndex = -1;
                if (index == 1) {
                    arrayIndex = _this.selectedREV_CDE.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedREV_CDE.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = _this.selectedREV_DSC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedREV_DSC.splice(arrayIndex, 1);
                }
            }
        });
        this.searchRevenueCodeRecordsWithFilters(1);
    };
    RevenueCodeComponent.prototype.removeSelectedFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            objFacet.selected = false;
            var arrayIndex = -1;
            if (index == 1) {
                arrayIndex = _this.selectedREV_CDE.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedREV_CDE.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = _this.selectedREV_DSC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedREV_DSC.splice(arrayIndex, 1);
            }
        });
        this.searchRevenueCodeRecordsWithFilters(1);
    };
    RevenueCodeComponent.prototype.clearSelectedFacets = function () {
        this.selectedREV_CDE = [];
        this.selectedREV_DSC = [];
    };
    RevenueCodeComponent.prototype.searchRevenueCode = function () {
        this.clearSelectedFacets();
        this.searchRevenueCodeRecordsWithFilters(1);
        this.searchRevenueCodeFacets();
    };
    RevenueCodeComponent.prototype.searchRevenueCodeRecordsWithFilters = function (pageNum) {
        var _this = this;
        this.pageNumber = pageNum;
        var selectedREV_CDEString = "";
        var selectedREV_DSCString = "";
        if (this.selectedREV_CDE && this.selectedREV_CDE.length > 0)
            selectedREV_CDEString = this.selectedREV_CDE.join();
        if (this.selectedREV_DSC && this.selectedREV_DSC.length > 0)
            selectedREV_DSCString = this.selectedREV_DSC.join();
        this.asyncRevenueCode = this.fdaService.searchRevenueCodeRecordsWithFilter(this.searchRevenueCodeText, selectedREV_CDEString, selectedREV_DSCString, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.totalRecords;
                    _this.rersRevenueCode = res.response.results || [];
                    _this.foundRecord = res.response.totalRecords > 0;
                });
            }
            else {
                _this.zone.run(function () {
                    _this.totalRecords = 0;
                    _this.rersRevenueCode = [];
                });
            }
        })
            .map(function (res) { return res.success ? res.response.results : []; });
    };
    RevenueCodeComponent.prototype.ngOnInit = function () {
        this.searchRevenueCodeRecordsWithFilters(1);
        this.searchRevenueCodeFacets();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    };
    return RevenueCodeComponent;
}());
RevenueCodeComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1068),
        providers: [fda_service_1.FDAService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.NgZone !== "undefined" && core_1.NgZone) === "function" && _a || Object, typeof (_b = typeof fda_service_1.FDAService !== "undefined" && fda_service_1.FDAService) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object])
], RevenueCodeComponent);
exports.RevenueCodeComponent = RevenueCodeComponent;
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
var _a, _b, _c;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/revenuecode.component.js.map

/***/ }),

/***/ 443:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var fda_service_1 = __webpack_require__(39);
var rxjs_1 = __webpack_require__(31);
var SecretaryOfStateComponent = (function () {
    function SecretaryOfStateComponent(zone, fdaService, _elmRef) {
        this.zone = zone;
        this.fdaService = fdaService;
        this._elmRef = _elmRef;
        this.selectedProductId = "";
        this.selectedSecretaryOfState = {};
        this.pageNumber = 1;
        this.recordsToFetch = 10;
        this.totalRecords = 0;
        this.resFacets = {};
        this.searchSecretaryOfStateText = "";
        this.selectedCN = [];
        this.selectedCOL = [];
        this.selectedCT = [];
        this.selectedCC = [];
        this.rerSecretaryOfState = [];
        this.foundRecord = true;
    }
    SecretaryOfStateComponent.prototype.searchSecretaryOfStateFacets = function () {
        var _this = this;
        this.fdaService.searchSecretaryOfStateFacets(this.searchSecretaryOfStateText)
            .subscribe(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.resFacets = res.response;
                });
            }
        }, function (error) {
        });
    };
    SecretaryOfStateComponent.prototype.changeSelectFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            var changedSelection = !objFacet.selected;
            objFacet.selected = changedSelection;
            if (changedSelection) {
                if (index == 1)
                    _this.selectedCN.push(objFacet.name);
                else if (index == 2)
                    _this.selectedCOL.push(objFacet.name);
                else if (index == 3)
                    _this.selectedCT.push(objFacet.name);
                else if (index == 4)
                    _this.selectedCC.push(objFacet.name);
            }
            else {
                var arrayIndex = -1;
                if (index == 1) {
                    arrayIndex = _this.selectedCN.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedCN.splice(arrayIndex, 1);
                }
                else if (index == 2) {
                    arrayIndex = _this.selectedCOL.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedCOL.splice(arrayIndex, 1);
                }
                else if (index == 3) {
                    arrayIndex = _this.selectedCT.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedCT.splice(arrayIndex, 1);
                }
                else if (index == 4) {
                    arrayIndex = _this.selectedCC.indexOf(objFacet.name);
                    if (arrayIndex > -1)
                        _this.selectedCC.splice(arrayIndex, 1);
                }
            }
        });
        this.searchSecretaryOfStateRecordsWithFilters(1);
    };
    SecretaryOfStateComponent.prototype.removeSelectedFacet = function (objFacet, index) {
        var _this = this;
        this.zone.run(function () {
            objFacet.selected = false;
            var arrayIndex = -1;
            if (index == 1) {
                arrayIndex = _this.selectedCN.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedCN.splice(arrayIndex, 1);
            }
            else if (index == 2) {
                arrayIndex = _this.selectedCOL.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedCOL.splice(arrayIndex, 1);
            }
            else if (index == 3) {
                arrayIndex = _this.selectedCT.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedCT.splice(arrayIndex, 1);
            }
            else if (index == 4) {
                arrayIndex = _this.selectedCC.indexOf(objFacet.name);
                if (arrayIndex > -1)
                    _this.selectedCC.splice(arrayIndex, 1);
            }
        });
        this.searchSecretaryOfStateRecordsWithFilters(1);
    };
    SecretaryOfStateComponent.prototype.clearSelectedFacets = function () {
        this.selectedCN = [];
        this.selectedCOL = [];
        this.selectedCT = [];
        this.selectedCC = [];
    };
    SecretaryOfStateComponent.prototype.searchSecretaryOfState = function () {
        this.clearSelectedFacets();
        this.searchSecretaryOfStateRecordsWithFilters(1);
        this.searchSecretaryOfStateFacets();
    };
    SecretaryOfStateComponent.prototype.searchSecretaryOfStateRecordsWithFilters = function (pageNum) {
        var _this = this;
        this.pageNumber = pageNum;
        var selectedCNString = "";
        var selectedCOLString = "";
        var selectedCTString = "";
        var selectedCCString = "";
        if (this.selectedCN && this.selectedCN.length > 0)
            selectedCNString = this.selectedCN.join();
        if (this.selectedCOL && this.selectedCOL.length > 0)
            selectedCOLString = this.selectedCOL.join();
        if (this.selectedCT && this.selectedCT.length > 0)
            selectedCTString = this.selectedCT.join();
        if (this.selectedCC && this.selectedCC.length > 0)
            selectedCCString = this.selectedCC.join();
        this.asyncSecretaryOfState = this.fdaService.searchSecretaryOfStateRecordsWithFilter(this.searchSecretaryOfStateText, selectedCNString, selectedCOLString, selectedCTString, selectedCCString, this.pageNumber, this.recordsToFetch)
            .do(function (res) {
            if (res.success) {
                _this.zone.run(function () {
                    _this.totalRecords = res.response.totalRecords;
                    if (res.response.results)
                        res.response.results.forEach(function (objSOS) {
                            objSOS.mailAddressString = _this.generateAddressString(objSOS.mailAddressLine1, objSOS.mailAddressLine2, objSOS.mailAddressCity, objSOS.mailAddressState, objSOS.mailAddressZipCode);
                            objSOS.agentAddressString = _this.generateAddressString(objSOS.agentAddress1, objSOS.agentAddress2, objSOS.agentAddressCity, objSOS.agentAddressState, objSOS.agentAddressZipCode);
                            objSOS.cEOPartnerAddressString = _this.generateAddressString(objSOS.cEOPartnerAddress1, objSOS.cEOPartnerAddress2, objSOS.cEOPartnerCity, objSOS.cEOPartnerState, objSOS.cEOPartnerZipCode);
                            objSOS.generalPartner1AddressString = _this.generateAddressString(objSOS.generalPartner1Address, null, objSOS.generalPartner1City, objSOS.generalPartner1State, objSOS.generalPartner1ZipCode);
                            objSOS.generalPartner2AddressString = _this.generateAddressString(objSOS.generalPartner2Address, null, objSOS.generalPartner2City, objSOS.generalPartner2State, objSOS.generalPartner2ZipCode);
                            objSOS.californiaAddressString = _this.generateAddressString(objSOS.californiaAddress, null, objSOS.californiaCity, null, objSOS.californiaZipCode);
                        });
                    _this.rerSecretaryOfState = res.response.results || [];
                    _this.foundRecord = res.response.totalRecords > 0;
                });
            }
            else {
                _this.zone.run(function () {
                    _this.totalRecords = 0;
                    _this.rerSecretaryOfState = [];
                });
            }
        })
            .map(function (res) { return res.success ? res.response.results : []; });
    };
    SecretaryOfStateComponent.prototype.generateAddressString = function (line1, line2, city, state, zipcode) {
        var address = "";
        if (line1)
            address += line1 + ", ";
        if (line2)
            address += line2 + ", ";
        if (city)
            address += city + ", ";
        if (state)
            address += state + ", ";
        if (zipcode)
            address += zipcode + ", ";
        if (address.length > 0)
            address = address.substring(0, address.length - 2);
        return address;
    };
    SecretaryOfStateComponent.prototype.ngOnInit = function () {
        this.searchSecretaryOfStateRecordsWithFilters(1);
        this.searchSecretaryOfStateFacets();
        //let container = jQuery(this._elmRef.nativeElement).find('.main')[0];
    };
    return SecretaryOfStateComponent;
}());
SecretaryOfStateComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1069),
        providers: [fda_service_1.FDAService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.NgZone !== "undefined" && core_1.NgZone) === "function" && _a || Object, typeof (_b = typeof fda_service_1.FDAService !== "undefined" && fda_service_1.FDAService) === "function" && _b || Object, typeof (_c = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _c || Object])
], SecretaryOfStateComponent);
exports.SecretaryOfStateComponent = SecretaryOfStateComponent;
/**
 * Simulate an async HTTP call with a delayed observable.
 */
function serverCall(meals, page) {
    var perPage = 10;
    var start = (page - 1) * perPage;
    var end = start + perPage;
    return rxjs_1.Observable
        .of({
        items: meals.slice(start, end),
        total: 100
    }).delay(1000);
}
var _a, _b, _c;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/secretaryofstate.component.js.map

/***/ }),

/***/ 444:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
//
var provider_outcome_service_1 = __webpack_require__(87);
var SocialNetworksClaimantComponent = (function () {
    function SocialNetworksClaimantComponent(providerOutcomeService, element) {
        this.providerOutcomeService = providerOutcomeService;
        this.element = element;
        this.activeFilters = {};
        this.tableOneData = [];
        this.tableTwoData = [];
        this.tableThreeData = [];
        this.tableFourData = [];
        this.searchQuery = "";
        this.isShowLoad = false;
        this.initFilter();
    }
    SocialNetworksClaimantComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.providerOutcomeService.getAllSideBarFilterForClaimant('').subscribe(function (res) {
            console.log('getAllSideBarFilterForClaimant res', res);
            _this.activeFilters['providers'] = res.providers;
            _this.activeFilters['attorneys'] = res.attorneys;
            _this.activeFilters['claimant'] = res.claimant;
            _this.activeFilters['claimnumber'] = res.claimnumber;
            _this.getAllProviderDataWithFilter();
        }, function (err) {
        });
    };
    SocialNetworksClaimantComponent.prototype.loadGraph = function (nodes, edges) {
        var container = document.querySelector('#network-graph');
        var data = {
            nodes: nodes,
            edges: edges
        };
        var options = {
            nodes: {
                shape: 'dot',
                font: { size: 10 }
            },
            interaction: {
                zoomView: true,
                hover: true,
                navigationButtons: true,
            }
        };
        var network = new vis.Network(container, data, options);
    };
    SocialNetworksClaimantComponent.prototype.ngAfterViewInit = function () {
    };
    SocialNetworksClaimantComponent.prototype.initFilter = function () {
        this.activeFilters['providers'] = [];
        this.activeFilters['attorneys'] = [];
        this.activeFilters['claimant'] = [];
        this.activeFilters['claimnumber'] = [];
        this.activeFilters['searchQuery'] = "";
    };
    SocialNetworksClaimantComponent.prototype.changeProvSelection = function (prov) {
        var changedSelection = !prov.selected;
        prov.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    SocialNetworksClaimantComponent.prototype.changeAttorneySelection = function (atto) {
        var changedSelection = !atto.selected;
        atto.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    SocialNetworksClaimantComponent.prototype.changeClaimantSelection = function (claim) {
        var changedSelection = !claim.selected;
        claim.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    SocialNetworksClaimantComponent.prototype.changeClaimNumberSelection = function (cno) {
        var changedSelection = !cno.selected;
        cno.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    SocialNetworksClaimantComponent.prototype.searchProviderSummary = function () {
        this.activeFilters['searchQuery'] = this.searchQuery;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    SocialNetworksClaimantComponent.prototype.removeSearchQuery = function () {
        this.activeFilters['searchQuery'] = "";
        this.searchQuery = "";
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    SocialNetworksClaimantComponent.prototype.getAllProviderDataWithFilter = function () {
        var _this = this;
        this.isShowLoad = true;
        this.providerOutcomeService.getAllClaimantData(this.activeFilters).subscribe(function (res) {
            console.log('getAllClaimantData res', res);
            var nodes = [];
            res.nodes.forEach(function (element) {
                nodes.push({
                    id: element.id,
                    label: element.label,
                    title: element.title,
                    level: element.level,
                    color: {
                        background: element.background,
                        highlight: element.highlight,
                        hover: element.hover,
                    }
                });
            });
            var edges = [];
            res.edges.forEach(function (element) {
                edges.push(element);
            });
            _this.loadGraph(nodes, edges);
            _this.tableOneData = res.table_one;
            _this.tableTwoData = res.table_two;
            _this.tableThreeData = res.table_three;
            _this.tableFourData = res.table_four;
            _this.isShowLoad = false;
        }, function (err) {
            console.log('getAllProviderDataWithFilter err', err);
            _this.isShowLoad = false;
        });
    };
    return SocialNetworksClaimantComponent;
}());
SocialNetworksClaimantComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1070),
        providers: [provider_outcome_service_1.ProviderOutcomeService],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof provider_outcome_service_1.ProviderOutcomeService !== "undefined" && provider_outcome_service_1.ProviderOutcomeService) === "function" && _a || Object, typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object])
], SocialNetworksClaimantComponent);
exports.SocialNetworksClaimantComponent = SocialNetworksClaimantComponent;
var _a, _b;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/socialNetworksClaimant.component.js.map

/***/ }),

/***/ 445:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(38);
//
var provider_outcome_service_1 = __webpack_require__(87);
var SocialNetworksProviderComponent = (function () {
    function SocialNetworksProviderComponent(providerOutcomeService, element, route) {
        this.providerOutcomeService = providerOutcomeService;
        this.element = element;
        this.route = route;
        this.activeFilters = {};
        this.isShowLoad = false;
        this.searchQuery = "";
        this.tableOneData = [];
        this.tableTwoData = [];
        this.tableThreeData = [];
        this.initFilter();
    }
    SocialNetworksProviderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.providerOutcomeService.getAllSideBarFilterForProviderNetwork('').subscribe(function (res) {
            console.log('getAllSideBarFilterForClaimant res', res);
            _this.activeFilters['providers'] = res.providers;
            _this.activeFilters['secondary_providers'] = res.secondary_providers;
            _this.activeFilters['patients'] = res.patients;
            _this.route
                .queryParams
                .subscribe(function (params) {
                if (params.provider_firstname) {
                    var provider_name = params.provider_firstname + ' ' + params.provider_lastname;
                    console.log('provider_name', provider_name);
                    for (var key in res.providers) {
                        console.log('key', res.providers[key].name);
                        if (res.providers[key].name == provider_name) {
                            console.log('INNN', res.providers[key].name);
                            _this.changeProvSelection(res.providers[key]);
                        }
                    }
                }
                else {
                    _this.getAllProviderDataWithFilter();
                }
            });
        }, function (err) {
        });
    };
    SocialNetworksProviderComponent.prototype.ngAfterViewInit = function () {
    };
    SocialNetworksProviderComponent.prototype.initFilter = function () {
        this.activeFilters['providers'] = [];
        this.activeFilters['secondary_providers'] = [];
        this.activeFilters['patients'] = [];
        this.activeFilters['searchQuery'] = "";
    };
    SocialNetworksProviderComponent.prototype.changeProvSelection = function (prov) {
        var changedSelection = !prov.selected;
        prov.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    SocialNetworksProviderComponent.prototype.changeSecProvSelection = function (sprov) {
        var changedSelection = !sprov.selected;
        sprov.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    SocialNetworksProviderComponent.prototype.changePatientSelection = function (patient) {
        var changedSelection = !patient.selected;
        patient.selected = changedSelection;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    SocialNetworksProviderComponent.prototype.searchProviderSummary = function () {
        this.activeFilters['searchQuery'] = this.searchQuery;
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    SocialNetworksProviderComponent.prototype.removeSearchQuery = function () {
        this.activeFilters['searchQuery'] = "";
        this.searchQuery = "";
        console.log('this.activeFilters', this.activeFilters);
        this.getAllProviderDataWithFilter();
    };
    SocialNetworksProviderComponent.prototype.getAllProviderDataWithFilter = function () {
        var _this = this;
        this.isShowLoad = true;
        this.providerOutcomeService.getAllProviderNetworkData(this.activeFilters).subscribe(function (res) {
            console.log('getAllClaimantData res', res);
            var nodes = [];
            res.nodes.forEach(function (element) {
                nodes.push({
                    id: element.id,
                    label: element.label,
                    title: element.title,
                    level: element.level,
                    color: {
                        background: element.background,
                        highlight: element.highlight,
                        hover: element.hover,
                    }
                });
            });
            var edges = [];
            res.edges.forEach(function (element) {
                edges.push(element);
            });
            _this.loadGraph(nodes, edges);
            _this.tableOneData = res.table_one;
            _this.tableTwoData = res.table_two;
            _this.tableThreeData = res.table_three;
            _this.isShowLoad = false;
        }, function (err) {
            console.log('getAllClaimantData err', err);
            _this.isShowLoad = false;
        });
    };
    SocialNetworksProviderComponent.prototype.loadGraph = function (nodes, edges) {
        var container = document.querySelector('#network-graph');
        var data = {
            nodes: nodes,
            edges: edges
        };
        var options = {
            nodes: {
                shape: 'dot',
                font: { size: 10 }
            },
            interaction: {
                zoomView: true,
                hover: true,
                navigationButtons: true,
            }
        };
        var network = new vis.Network(container, data, options);
    };
    return SocialNetworksProviderComponent;
}());
SocialNetworksProviderComponent = __decorate([
    core_1.Component({
        template: __webpack_require__(1071),
        providers: [provider_outcome_service_1.ProviderOutcomeService],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof provider_outcome_service_1.ProviderOutcomeService !== "undefined" && provider_outcome_service_1.ProviderOutcomeService) === "function" && _a || Object, typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object, typeof (_c = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _c || Object])
], SocialNetworksProviderComponent);
exports.SocialNetworksProviderComponent = SocialNetworksProviderComponent;
var _a, _b, _c;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/socialNetworksProvider.component.js.map

/***/ }),

/***/ 446:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var DateUTCPipe = (function () {
    function DateUTCPipe() {
    }
    DateUTCPipe.prototype.transform = function (value, format) {
        if (format === void 0) { format = ""; }
        // Try and parse the passed value.
        // var formattedDate = new Date(value.replace("UTC", "").trim());
        var formattedDate = new Date(value.replace("UTC", "").trim().replace(" ", "T"));
        // If moment didn't understand the value, return it unformatted.
        if (!formattedDate)
            return value;
        // Otherwise, return the date formatted as requested.
        return (new Date(Date.UTC(formattedDate.getFullYear(), formattedDate.getMonth(), formattedDate.getDate(), formattedDate.getHours(), formattedDate.getMinutes()))).toISOString();
    };
    return DateUTCPipe;
}());
DateUTCPipe = __decorate([
    core_1.Pipe({
        name: 'dateutc'
    })
], DateUTCPipe);
exports.DateUTCPipe = DateUTCPipe;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/dateUTC.pipe.js.map

/***/ }),

/***/ 447:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(37);
var HttpScoutClient = (function () {
    function HttpScoutClient(http) {
        //private ApiBaseUrl = 'http://localhost:8983/solr/';
        //private ApiBaseUrl = 'http://dg.scoutcms.com/';
        this.ApiBaseUrl = 'http://demo.scoutcms.com/Controllers/DataService.asmx?op=';
        //private ApiBaseUrl = '/api/v1/';//Production
        this.headers = new http_1.Headers();
        this.http = http;
        this.headers.append('Content-Type', 'text/xml');
        this.headers.append('Accept', 'application/xml');
    }
    HttpScoutClient.prototype.get = function (url) {
        var fullUrl = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.get(fullUrl, { headers: this.headers });
    };
    HttpScoutClient.prototype.post = function (url, data) {
        var fullUrl = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.post(fullUrl, data, { headers: this.headers });
    };
    HttpScoutClient.prototype.postWithHeaders = function (url, data, headers) {
        var fullUrl = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.post(fullUrl, data, { headers: this.headers });
    };
    return HttpScoutClient;
}());
HttpScoutClient = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
], HttpScoutClient);
exports.HttpScoutClient = HttpScoutClient;
var _a;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/http-scout.client.js.map

/***/ }),

/***/ 448:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(37);
var HttpSOLRClient = (function () {
    function HttpSOLRClient(http) {
        //private ApiBaseUrl = 'http://localhost:8983/solr/';
        this.ApiBaseUrl = 'http://data.apatics.net:8983/solr/';
        //private ApiBaseUrl = '/api/v1/';//Production
        this.headers = new http_1.Headers();
        this.http = http;
    }
    HttpSOLRClient.prototype.get = function (url) {
        var fullUrl = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.get(fullUrl, { headers: this.headers });
    };
    HttpSOLRClient.prototype.post = function (url, data) {
        var fullUrl = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.post(fullUrl, data, { headers: this.headers });
    };
    HttpSOLRClient.prototype.postWithHeaders = function (url, data, headers) {
        var fullUrl = "";
        if (url.indexOf('http:') >= 0 || url.indexOf('https:') >= 0)
            fullUrl = url;
        else
            fullUrl = this.ApiBaseUrl + url;
        return this.http.post(fullUrl, data, { headers: this.headers });
    };
    return HttpSOLRClient;
}());
HttpSOLRClient = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
], HttpSOLRClient);
exports.HttpSOLRClient = HttpSOLRClient;
var _a;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/http-solr.client.js.map

/***/ }),

/***/ 449:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(37);
var common_1 = __webpack_require__(10);
var http_2 = __webpack_require__(37);
var global_1 = __webpack_require__(103);
var http_solr_client_1 = __webpack_require__(448);
__webpack_require__(63);
var SOLRService = (function () {
    function SOLRService(httpClient, _jsonp, datePipe, globals) {
        this.httpClient = httpClient;
        this._jsonp = _jsonp;
        this.datePipe = datePipe;
        this.globals = globals;
    }
    SOLRService.prototype.searchProviderSummaryFacets = function (searchText) {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";
        var url = 'provider_summary_core/select?q=' + encodeURIComponent(searchText) + '&rows=0&wt=json&indent=true&facet=true&facet.sort=index' +
            '&facet.field=PARTY_FIRST_NAME&facet.field=PROVIDER_SPECIALTY&facet.field=ZIP_CODE&facet.field=TAX_ID_NUMBER';
        return this.httpClient.get(url).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    SOLRService.prototype.searchProviderSummaryRecordsWithFilter = function (searchText, bar, dr, par, pn, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";
        var facetSearchString = "";
        if (bar)
            facetSearchString += "&fq=PARTY_FIRST_NAME:\"" + bar + "\"";
        if (dr)
            facetSearchString += "&fq=PROVIDER_SPECIALTY:\"" + dr + "\"";
        if (par)
            facetSearchString += "&fq=ZIP_CODE:\"" + par + "\"";
        if (pn)
            facetSearchString += "&fq=TAX_ID_NUMBER:\"" + pn + "\"";
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        var startRecordNumber = (pageNumber * recordsToFetch) - recordsToFetch;
        var url = 'provider_summary_core/select?indent=on&q=' + encodeURIComponent(searchText) + facetSearchString + '&rows=' + recordsToFetch + '&start=' + startRecordNumber + '&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    SOLRService.prototype.getProviderSummaryBillStatistics = function (taxIDNumber) {
        if (!taxIDNumber)
            taxIDNumber = "*";
        var url = 'ps_bill_statistics/select?indent=on&q=' + encodeURIComponent("TAX_ID_NUMBER:" + taxIDNumber) + '&rows=100&start=0&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    SOLRService.prototype.getProviderSummaryRedFlagAlerts = function (taxIDNumber) {
        if (!taxIDNumber)
            taxIDNumber = "*";
        var url = 'ps_red_flag_alerts/select?indent=on&q=' + encodeURIComponent("TAX_ID_NUMBER:" + taxIDNumber) + '&rows=100&start=0&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    SOLRService.prototype.getProviderSummaryChiropracticAlerts = function (licenseNumber) {
        if (!licenseNumber)
            licenseNumber = "*";
        else
            licenseNumber = "DC" + licenseNumber;
        var url = 'chiropractic_alerts/select?indent=on&q=' + encodeURIComponent("licenseNo:" + licenseNumber) + '&rows=100&start=0&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    SOLRService.prototype.searchDuplicateBillingFacets = function (taxIDNumber, searchText) {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";
        var url = 'duplicate_billing_core/select?q=' + encodeURIComponent(searchText) + '&rows=0&wt=json&indent=true&facet=true&facet.sort=index' +
            '&fq=taxIDNumber1:' + taxIDNumber + ' OR taxIDNumber2:' + taxIDNumber +
            '&facet.field=dateOfService&facet.field=vendorName1&facet.field=billedAmount1&facet.field=paidAmount1';
        return this.httpClient.get(url).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    SOLRService.prototype.searchDuplicateBillingRecordsWithFilter = function (taxIDNumber, searchText, vn1, dos, ba1, pa1, pageNumber, recordsToFetch) {
        if (!searchText)
            searchText = "*:*"; //var search =(query.search==='') ? '*:*' : query.search+"*";    	
        else
            searchText = searchText + "*";
        var facetSearchString = '&fq=taxIDNumber1:' + taxIDNumber + ' OR taxIDNumber2:' + taxIDNumber;
        if (vn1)
            facetSearchString += "&fq=vendorName1:\"" + vn1 + "\"";
        if (dos)
            facetSearchString += "&fq=dateOfService:\"" + dos + "\"";
        if (ba1)
            facetSearchString += "&fq=billedAmount1:" + ba1;
        if (pa1)
            facetSearchString += "&fq=paidAmount1:" + pa1;
        if (!pageNumber)
            pageNumber = 1;
        if (!recordsToFetch)
            recordsToFetch = 10;
        var startRecordNumber = (pageNumber * recordsToFetch) - recordsToFetch;
        var url = 'duplicate_billing_core/select?indent=on&q=' + encodeURIComponent(searchText) + facetSearchString + '&rows=' + recordsToFetch + '&start=' + startRecordNumber + '&wt=json';
        return this.httpClient.get(url)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    SOLRService.prototype.getAuthToken = function () {
        var headers = new http_1.Headers();
        headers.append('authorization', 'Basic QXBhdGljc0FQSWFwcDpteS1zZWNyZXQtdG9rZW4tdG8tY2hhbmdlLWluLXByb2R1Y3Rpb24=');
        var body = new http_1.URLSearchParams();
        body.set('username', 'admin');
        body.set('password', 'admin');
        body.set('grant_type', 'password');
        body.set('scope', 'read write');
        body.set('client_secret', 'my-secret-token-to-change-in-production');
        body.set('client_id', 'ApaticsAPIapp');
        return this.httpClient.postWithHeaders('http://54.191.44.17/oauth/token', body, headers)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    SOLRService.prototype.getProviderFacets = function (searchText) {
        var headers = new http_1.Headers();
        headers.append('authorization', 'Bearer ' + window.localStorage.getItem('apaticsAuthToken'));
        var objData = {};
        objData.q = searchText ? (searchText + "*") : "*";
        objData.fields = ["partyFirstName", "providerSpeciality", "zipCode", "taxIdNumber"];
        //let body = {"q": "*", "fields": ["partyFirstName", "providerSpeciality", "zipCode", "taxIdNumber"]};
        return this.httpClient.postWithHeaders('http://54.191.44.17/api/provider/get_facets', objData, headers)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    SOLRService.prototype.getProviderRecordsWithFilter = function (searchText, scrollId, limit) {
        var headers = new http_1.Headers();
        headers.append('authorization', 'Bearer ' + window.localStorage.getItem('apaticsAuthToken'));
        var objData = {};
        objData.q = searchText ? searchText : "*";
        objData.limit = limit;
        // if (scrollId.length > 0)
        //     objData.scrollId = scrollId;
        return this.httpClient.postWithHeaders('http://54.191.44.17/api/provider/search_providers', objData, headers)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    return SOLRService;
}());
SOLRService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_solr_client_1.HttpSOLRClient !== "undefined" && http_solr_client_1.HttpSOLRClient) === "function" && _a || Object, typeof (_b = typeof http_2.Jsonp !== "undefined" && http_2.Jsonp) === "function" && _b || Object, typeof (_c = typeof common_1.DatePipe !== "undefined" && common_1.DatePipe) === "function" && _c || Object, typeof (_d = typeof global_1.GlobalClass !== "undefined" && global_1.GlobalClass) === "function" && _d || Object])
], SOLRService);
exports.SOLRService = SOLRService;
var _a, _b, _c, _d;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/solr.service.js.map

/***/ }),

/***/ 450:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var Commonutil = (function () {
    function Commonutil() {
    }
    Commonutil.prototype.getLocalStorageItem = function (key, json) {
        if (json === void 0) { json = false; }
        var val = localStorage.getItem(key);
        return json ? JSON.parse(val) : val;
    };
    Commonutil.prototype.setLocalStorageItem = function (key, value, json) {
        if (json === void 0) { json = false; }
        var toBeSetval = json ? JSON.stringify(value) : value;
        localStorage.setItem(key, toBeSetval);
    };
    Commonutil.prototype.removeLocalStorageItem = function (key) {
        localStorage.removeItem(key);
    };
    Commonutil.prototype.getSessionStorageItem = function (key, json) {
        if (json === void 0) { json = false; }
        var val = sessionStorage.getItem(key);
        return json ? JSON.parse(val) : val;
    };
    Commonutil.prototype.setSessionStorageItem = function (key, value, json) {
        if (json === void 0) { json = false; }
        var toBeSetval = json ? JSON.stringify(value) : value;
        sessionStorage.setItem(key, toBeSetval);
    };
    Commonutil.prototype.removeSessionStorageItem = function (key) {
        sessionStorage.removeItem(key);
    };
    Commonutil.prototype.cloneArray = function (old) {
        if (!old) {
            return old;
        }
        return old.slice(0).reverse();
    };
    return Commonutil;
}());
Commonutil = __decorate([
    core_1.Injectable()
], Commonutil);
exports.Commonutil = Commonutil;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/common-util.js.map

/***/ }),

/***/ 711:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 711;


/***/ }),

/***/ 712:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var platform_browser_dynamic_1 = __webpack_require__(800);
var app_module_1 = __webpack_require__(831);
var environment_1 = __webpack_require__(850);
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule);
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/main.js.map

/***/ }),

/***/ 830:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var session_service_1 = __webpack_require__(104);
var core_1 = __webpack_require__(0);
__webpack_require__(842);
var router_1 = __webpack_require__(38);
var broadcast_service_1 = __webpack_require__(270);
var global_1 = __webpack_require__(422);
var global_events_1 = __webpack_require__(421);
var AppComponent = (function () {
    function AppComponent(_sessionService, router, _broadcastService) {
        this._sessionService = _sessionService;
        this.router = router;
        this._broadcastService = _broadcastService;
        this.staticRoot = global_1.Global.staticRoot;
        this.loggedIn = false;
        this.listenToBroadcast();
        this.checkUserSession();
    }
    AppComponent.prototype.checkUserSession = function () {
        var _this = this;
        this._sessionService.getUserSession().subscribe(function (res) {
            _this.loggedIn = res ? true : false;
        }, function (err) {
            console.log('Error while retrieving session data.');
        });
    };
    AppComponent.prototype.goToHome = function () {
        var _this = this;
        this._sessionService.getUserSession().subscribe(function (res) {
            if (res) {
                _this.router.navigate(['/home']);
            }
            else {
                _this.router.navigate(['/login']);
            }
        }, function (err) {
            console.log('Error while retrieving session data.');
        });
    };
    AppComponent.prototype.logOut = function () {
        var _this = this;
        this._sessionService.clearUserSession().subscribe(function (res) {
            _this.loggedIn = false;
            _this.router.navigate(['/login']);
        }, function (err) {
            console.log('Error while clearing session data.');
        });
    };
    AppComponent.prototype.logIn = function () {
        this.router.navigate(['/login']);
    };
    AppComponent.prototype.listenToBroadcast = function () {
        var _this = this;
        this._broadcastService.on(global_events_1.GlobalEvents.LOGIN_UPDATE).subscribe(function (evData) {
            if (evData.loggedIn) {
                _this.loggedIn = evData.loggedIn;
            }
        });
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'body',
        //template: '<router-outlet></router-outlet>'
        template: __webpack_require__(1045),
        //styleUrls: ['./app.component.css'],
        providers: [session_service_1.SessionService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof session_service_1.SessionService !== "undefined" && session_service_1.SessionService) === "function" && _a || Object, typeof (_b = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _b || Object, typeof (_c = typeof broadcast_service_1.BroadcastService !== "undefined" && broadcast_service_1.BroadcastService) === "function" && _c || Object])
], AppComponent);
exports.AppComponent = AppComponent;
var _a, _b, _c;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/app.component.js.map

/***/ }),

/***/ 831:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var platform_browser_1 = __webpack_require__(85);
var forms_1 = __webpack_require__(30);
var common_1 = __webpack_require__(10);
var http_1 = __webpack_require__(37);
var global_1 = __webpack_require__(103);
var http_client_1 = __webpack_require__(186);
var http_solr_client_1 = __webpack_require__(448);
var http_scout_client_1 = __webpack_require__(447);
var app_component_1 = __webpack_require__(830);
var ng2_bootstrap_1 = __webpack_require__(1017);
var nav_dropdown_directive_1 = __webpack_require__(848);
var facets_search_pipe_1 = __webpack_require__(839);
var facets_selected_pipe_1 = __webpack_require__(840);
var highlight_pipe_1 = __webpack_require__(841);
var dateUTC_pipe_1 = __webpack_require__(446);
var ng2_pagination_1 = __webpack_require__(1036);
var ng2_charts_1 = __webpack_require__(1032);
var sidebar_directive_1 = __webpack_require__(849);
var aside_directive_1 = __webpack_require__(846);
var breadcrumb_component_1 = __webpack_require__(847);
var ng2_nouislider_1 = __webpack_require__(661);
var ng2_pdf_viewer_1 = __webpack_require__(1037);
var core_2 = __webpack_require__(457);
var ngx_modal_1 = __webpack_require__(1040);
var modal_1 = __webpack_require__(629);
var ng2_select_1 = __webpack_require__(666);
// Routing Module
var app_routing_1 = __webpack_require__(832);
//Layouts
var full_layout_component_1 = __webpack_require__(834);
var simple_layout_component_1 = __webpack_require__(838);
var header_layout_component_1 = __webpack_require__(836);
var dashboard_component_1 = __webpack_require__(427);
var fda_component_1 = __webpack_require__(432);
var dca_component_1 = __webpack_require__(428);
var esi_component_1 = __webpack_require__(431);
var icd9_component_1 = __webpack_require__(434);
var revenuecode_component_1 = __webpack_require__(442);
var secretaryofstate_component_1 = __webpack_require__(443);
var nicb_component_1 = __webpack_require__(437);
var mbcoig_component_1 = __webpack_require__(436);
var chiropractor_component_1 = __webpack_require__(426);
var dnb_component_1 = __webpack_require__(429);
var gd_component_1 = __webpack_require__(433);
var providerdashboard_component_1 = __webpack_require__(441);
var providerOutcome_component_1 = __webpack_require__(439);
var providerReports_component_1 = __webpack_require__(440);
var providerMap_component_1 = __webpack_require__(438);
var Opioid_component_1 = __webpack_require__(425);
var ClaimSegmentation_component_1 = __webpack_require__(424);
var socialNetworksClaimant_component_1 = __webpack_require__(444);
var socialNetworksProvider_component_1 = __webpack_require__(445);
var duplicatebilling_component_1 = __webpack_require__(430);
var dropdown_1 = __webpack_require__(628);
var tabs_1 = __webpack_require__(646);
var index_1 = __webpack_require__(423);
//import { AuthGuard } from './guards/index';
var login_component_1 = __webpack_require__(435);
// import {LoginComponent} from './components/login/login.component';
var common_util_1 = __webpack_require__(450);
var broadcast_service_1 = __webpack_require__(270);
var header_component_1 = __webpack_require__(837);
var header_default_component_1 = __webpack_require__(835);
var ng2_nvd3_1 = __webpack_require__(318);
// import { Tabset } from "./shared/tabs/tabset.component";
// import { Tab } from "./shared/tabs/tab.directive";
// import {NgTransclude} from './shared/tabs/utils';
// import {TabHeading} from './shared/tabs/tab-heading.directive'
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            app_routing_1.AppRoutingModule,
            ng2_bootstrap_1.Ng2BootstrapModule,
            ng2_pagination_1.Ng2PaginationModule,
            dropdown_1.DropdownModule.forRoot(),
            tabs_1.TabsModule.forRoot(),
            ng2_charts_1.ChartsModule,
            ng2_nvd3_1.NvD3Module,
            ng2_nouislider_1.NouisliderModule,
            http_1.HttpModule, http_1.JsonpModule,
            ngx_modal_1.ModalModule,
            ng2_select_1.SelectModule,
            modal_1.ModalModule.forRoot(),
            core_2.AgmCoreModule.forRoot({
                apiKey: 'AIzaSyAB_dbNjPKgAtscBsOOCwuB8FTFO5szjV0'
            })
        ],
        declarations: [
            app_component_1.AppComponent,
            full_layout_component_1.FullLayoutComponent, simple_layout_component_1.SimpleLayoutComponent, header_layout_component_1.HeaderLayoutComponent,
            providerdashboard_component_1.ProviderDashboardComponent, duplicatebilling_component_1.DuplicateBillingComponent,
            providerOutcome_component_1.ProviderOutcomeComponent, providerReports_component_1.ProviderReportsComponent, providerMap_component_1.ProviderMapComponent, Opioid_component_1.OpioidComponent, ClaimSegmentation_component_1.ClaimSegmentationComponent,
            socialNetworksClaimant_component_1.SocialNetworksClaimantComponent, socialNetworksProvider_component_1.SocialNetworksProviderComponent,
            dashboard_component_1.DashboardComponent, fda_component_1.FDAComponent, facets_search_pipe_1.SearchFacets, facets_selected_pipe_1.SelectedFacets, highlight_pipe_1.HighlightPipe,
            dca_component_1.DCAComponent, secretaryofstate_component_1.SecretaryOfStateComponent,
            esi_component_1.ESIComponent,
            icd9_component_1.ICD9Component, revenuecode_component_1.RevenueCodeComponent,
            nicb_component_1.NICBComponent, mbcoig_component_1.MBCOIGComponent, chiropractor_component_1.ChiropractorComponent,
            dnb_component_1.DNBComponent, ng2_pdf_viewer_1.PdfViewerComponent,
            gd_component_1.GDComponent,
            nav_dropdown_directive_1.NAV_DROPDOWN_DIRECTIVES,
            breadcrumb_component_1.BreadcrumbsComponent,
            sidebar_directive_1.SIDEBAR_TOGGLE_DIRECTIVES,
            aside_directive_1.AsideToggleDirective,
            dateUTC_pipe_1.DateUTCPipe,
            login_component_1.LoginComponent,
            header_component_1.HeaderComponent,
            header_default_component_1.HeaderDefaultComponent,
        ],
        providers: [{
                provide: common_1.LocationStrategy,
                useClass: common_1.HashLocationStrategy
            }, global_1.GlobalClass, http_client_1.HttpClient, http_solr_client_1.HttpSOLRClient, http_scout_client_1.HttpScoutClient, common_1.DatePipe, index_1.AuthGuard, common_util_1.Commonutil, broadcast_service_1.BroadcastService],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/app.module.js.map

/***/ }),

/***/ 832:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(38);
var dashboard_component_1 = __webpack_require__(427);
var fda_component_1 = __webpack_require__(432);
var dca_component_1 = __webpack_require__(428);
var esi_component_1 = __webpack_require__(431);
var icd9_component_1 = __webpack_require__(434);
var revenuecode_component_1 = __webpack_require__(442);
var secretaryofstate_component_1 = __webpack_require__(443);
var nicb_component_1 = __webpack_require__(437);
var mbcoig_component_1 = __webpack_require__(436);
var chiropractor_component_1 = __webpack_require__(426);
var dnb_component_1 = __webpack_require__(429);
var gd_component_1 = __webpack_require__(433);
var providerdashboard_component_1 = __webpack_require__(441);
var providerOutcome_component_1 = __webpack_require__(439);
var providerReports_component_1 = __webpack_require__(440);
var providerMap_component_1 = __webpack_require__(438);
var socialNetworksClaimant_component_1 = __webpack_require__(444);
var socialNetworksProvider_component_1 = __webpack_require__(445);
var Opioid_component_1 = __webpack_require__(425);
var ClaimSegmentation_component_1 = __webpack_require__(424);
var duplicatebilling_component_1 = __webpack_require__(430);
var login_component_1 = __webpack_require__(435);
//guards
var index_1 = __webpack_require__(423);
exports.routes = [
    {
        path: '',
        redirectTo: 'home',
        canActivate: [index_1.AuthGuard],
        pathMatch: 'full',
    },
    {
        path: 'login',
        component: login_component_1.LoginComponent,
        data: {
            title: 'Login'
        }
    },
    {
        path: 'providerdashboard',
        component: providerdashboard_component_1.ProviderDashboardComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Provider Dashboard'
        }
    },
    {
        path: 'provider-outcome',
        component: providerOutcome_component_1.ProviderOutcomeComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Provider Outcome'
        }
    },
    {
        path: 'predictive-analytics/provider-reports',
        component: providerReports_component_1.ProviderReportsComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Provider Reports'
        }
    },
    {
        path: 'predictive-analytics/provider-map',
        component: providerMap_component_1.ProviderMapComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Provider Map'
        }
    },
    {
        path: 'social-networks/claimant',
        component: socialNetworksClaimant_component_1.SocialNetworksClaimantComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Provider Map'
        }
    },
    {
        path: 'social-networks/provider',
        component: socialNetworksProvider_component_1.SocialNetworksProviderComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Provider Map'
        }
    },
    {
        path: 'opioid',
        component: Opioid_component_1.OpioidComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Provider Map'
        }
    },
    {
        path: 'claim-segmentation',
        component: ClaimSegmentation_component_1.ClaimSegmentationComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Claim Segmentation'
        }
    },
    {
        path: 'doublebilling',
        component: duplicatebilling_component_1.DuplicateBillingComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Double Billing'
        }
    },
    {
        path: 'fda',
        component: fda_component_1.FDAComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'FDA'
        }
    },
    {
        path: 'home',
        component: dashboard_component_1.DashboardComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Home'
        } /*,
        children: [
            {
                path: 'dashboard',
                loadChildren: 'app/dashboard/dashboard.module#DashboardModule'
            }
        ]*/
    },
    {
        path: 'dca',
        component: dca_component_1.DCAComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'DCA'
        }
    },
    {
        path: 'esi',
        component: esi_component_1.ESIComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'ESI'
        }
    },
    {
        path: 'icd9',
        component: icd9_component_1.ICD9Component,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'ICD9'
        }
    },
    {
        path: 'revenuecode',
        component: revenuecode_component_1.RevenueCodeComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Revenue Code'
        }
    },
    {
        path: 'secretaryofstate',
        component: secretaryofstate_component_1.SecretaryOfStateComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Secretary of State'
        }
    },
    {
        path: 'nicb',
        component: nicb_component_1.NICBComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'NICB'
        }
    },
    {
        path: 'mbcoig',
        component: mbcoig_component_1.MBCOIGComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'MBC & OIG'
        }
    },
    {
        path: 'chiropractor',
        component: chiropractor_component_1.ChiropractorComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Chiropractor alert'
        }
    },
    {
        path: 'dnb',
        component: dnb_component_1.DNBComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'DNB'
        }
    },
    {
        path: 'gd',
        component: gd_component_1.GDComponent,
        canActivate: [index_1.AuthGuard],
        data: {
            title: 'Gegraphic discrepancy'
        }
    }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forRoot(exports.routes)],
        exports: [router_1.RouterModule]
    })
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/app.routing.js.map

/***/ }),

/***/ 833:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(38);
var AuthGuard = (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (sessionStorage.getItem('userSession')) {
            // logged in so return true
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        console.log('check user session expired !!');
        return false;
    };
    return AuthGuard;
}());
AuthGuard = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _a || Object])
], AuthGuard);
exports.AuthGuard = AuthGuard;
var _a;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/auth.guard.js.map

/***/ }),

/***/ 834:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var FullLayoutComponent = (function () {
    function FullLayoutComponent() {
    }
    FullLayoutComponent.prototype.ngOnInit = function () { };
    return FullLayoutComponent;
}());
FullLayoutComponent = __decorate([
    core_1.Component({
        selector: 'app-dashboard',
        template: __webpack_require__(1046)
    }),
    __metadata("design:paramtypes", [])
], FullLayoutComponent);
exports.FullLayoutComponent = FullLayoutComponent;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/full-layout.component.js.map

/***/ }),

/***/ 835:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var session_service_1 = __webpack_require__(104);
var router_1 = __webpack_require__(38);
var HeaderDefaultComponent = (function () {
    function HeaderDefaultComponent(_sessionService, router) {
        this._sessionService = _sessionService;
        this.router = router;
        this.loggedIn = false;
    }
    HeaderDefaultComponent.prototype.logOut = function () {
        var _this = this;
        this._sessionService.clearUserSession().subscribe(function (res) {
            _this.loggedIn = false;
            _this.router.navigate(['/login']);
        }, function (err) {
            console.log('Error while clearing session data.');
        });
    };
    HeaderDefaultComponent.prototype.ngOnInit = function () { };
    return HeaderDefaultComponent;
}());
HeaderDefaultComponent = __decorate([
    core_1.Component({
        selector: 'app-header-default',
        template: __webpack_require__(1047),
        providers: [session_service_1.SessionService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof session_service_1.SessionService !== "undefined" && session_service_1.SessionService) === "function" && _a || Object, typeof (_b = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _b || Object])
], HeaderDefaultComponent);
exports.HeaderDefaultComponent = HeaderDefaultComponent;
var _a, _b;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/header-default.component.js.map

/***/ }),

/***/ 836:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var HeaderLayoutComponent = (function () {
    function HeaderLayoutComponent() {
    }
    HeaderLayoutComponent.prototype.ngOnInit = function () { };
    return HeaderLayoutComponent;
}());
HeaderLayoutComponent = __decorate([
    core_1.Component({
        selector: 'app-dashboard',
        template: __webpack_require__(1048)
    }),
    __metadata("design:paramtypes", [])
], HeaderLayoutComponent);
exports.HeaderLayoutComponent = HeaderLayoutComponent;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/header-layout.component.js.map

/***/ }),

/***/ 837:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var session_service_1 = __webpack_require__(104);
var router_1 = __webpack_require__(38);
var HeaderComponent = (function () {
    function HeaderComponent(_sessionService, router) {
        this._sessionService = _sessionService;
        this.router = router;
        this.loggedIn = false;
    }
    HeaderComponent.prototype.logOut = function () {
        var _this = this;
        this._sessionService.clearUserSession().subscribe(function (res) {
            _this.loggedIn = false;
            _this.router.navigate(['/login']);
        }, function (err) {
            console.log('Error while clearing session data.');
        });
    };
    HeaderComponent.prototype.ngOnInit = function () { };
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    core_1.Component({
        selector: 'app-header',
        template: __webpack_require__(1049),
        providers: [session_service_1.SessionService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof session_service_1.SessionService !== "undefined" && session_service_1.SessionService) === "function" && _a || Object, typeof (_b = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _b || Object])
], HeaderComponent);
exports.HeaderComponent = HeaderComponent;
var _a, _b;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/header.component.js.map

/***/ }),

/***/ 838:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var SimpleLayoutComponent = (function () {
    function SimpleLayoutComponent() {
    }
    SimpleLayoutComponent.prototype.ngOnInit = function () { };
    return SimpleLayoutComponent;
}());
SimpleLayoutComponent = __decorate([
    core_1.Component({
        selector: 'app-dashboard',
        template: '<router-outlet></router-outlet>',
    }),
    __metadata("design:paramtypes", [])
], SimpleLayoutComponent);
exports.SimpleLayoutComponent = SimpleLayoutComponent;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/simple-layout.component.js.map

/***/ }),

/***/ 839:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var SearchFacets = (function () {
    function SearchFacets() {
    }
    SearchFacets.prototype.transform = function (allFacets, searchText, type) {
        //console.log('allFacets', allFacets);
        //objFacet.name.toLowerCase.indexOf(searchText,0) == 0
        if (allFacets) {
            if (searchText && searchText.length > 0) {
                return allFacets.filter(function (objFacet) { return objFacet.name.toLowerCase().indexOf(searchText) == 0; }).slice(0, 10);
            }
            else {
                return allFacets.slice(0, 10);
                //return allFacets;
            }
        }
    };
    return SearchFacets;
}());
SearchFacets = __decorate([
    core_1.Pipe({ name: 'searchFacets' })
], SearchFacets);
exports.SearchFacets = SearchFacets;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/facets-search.pipe.js.map

/***/ }),

/***/ 840:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var SelectedFacets = (function () {
    function SelectedFacets() {
    }
    SelectedFacets.prototype.transform = function (allFacets) {
        if (allFacets) {
            return allFacets.filter(function (objFacet) { return objFacet.selected; });
        }
    };
    return SelectedFacets;
}());
SelectedFacets = __decorate([
    core_1.Pipe({
        name: 'selectedFacets',
        pure: false
    })
], SelectedFacets);
exports.SelectedFacets = SelectedFacets;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/facets-selected.pipe.js.map

/***/ }),

/***/ 841:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var HighlightPipe = (function () {
    function HighlightPipe() {
    }
    HighlightPipe.prototype.transform = function (text, search) {
        text = text.toString();
        var pattern = search.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
        pattern = pattern.split(' ').filter(function (t) {
            return t.length > 0;
        }).join('|');
        var regex = new RegExp(pattern, 'gi');
        return search ? text.replace(regex, function (match) { return "<span class=\"highlight\">" + match + "</span>"; }) : text;
    };
    return HighlightPipe;
}());
HighlightPipe = __decorate([
    core_1.Pipe({ name: 'highlight' })
], HighlightPipe);
exports.HighlightPipe = HighlightPipe;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/highlight.pipe.js.map

/***/ }),

/***/ 842:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// import 'rxjs/Rx'; // adds ALL RxJS statics & operators to Observable

Object.defineProperty(exports, "__esModule", { value: true });
// See node_module/rxjs/Rxjs.js
// Import just the rxjs statics and operators needed for THIS app.
// Statics
__webpack_require__(676);
// Operators
__webpack_require__(323);
__webpack_require__(324);
__webpack_require__(677);
__webpack_require__(63);
__webpack_require__(679);
__webpack_require__(681);
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/rxjs.operators.js.map

/***/ }),

/***/ 843:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var Observable_1 = __webpack_require__(1);
var session_service_1 = __webpack_require__(104);
var _ = __webpack_require__(1327);
var global_1 = __webpack_require__(422);
var LoginService = (function () {
    function LoginService(_sessionService) {
        this._sessionService = _sessionService;
    }
    LoginService.prototype.login = function (req) {
        var _this = this;
        return new Observable_1.Observable(function (observer) {
            if (_this._userExists(req)) {
                _this._sessionService.createUserSession(req).subscribe(function (res) {
                    observer.next(res);
                }, function (err) {
                    observer.next(null);
                }, function () {
                    observer.complete();
                });
            }
            else {
                observer.next(null);
                observer.complete();
            }
        });
    };
    LoginService.prototype._userExists = function (req) {
        return _.some(global_1.Global.userList, function (user) { return user.username == req.username && user.password == req.password; });
    };
    LoginService.prototype.logout = function () {
        var _this = this;
        return new Observable_1.Observable(function (observer) {
            _this._sessionService.clearUserSession().subscribe(function (res) {
                observer.next(true);
            }, function (err) {
                observer.next(false);
            }, function () {
                observer.complete();
            });
        });
    };
    return LoginService;
}());
LoginService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof session_service_1.SessionService !== "undefined" && session_service_1.SessionService) === "function" && _a || Object])
], LoginService);
exports.LoginService = LoginService;
var _a;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/login.service.js.map

/***/ }),

/***/ 844:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var common_1 = __webpack_require__(10);
var http_1 = __webpack_require__(37);
var global_1 = __webpack_require__(103);
var http_scout_client_1 = __webpack_require__(447);
__webpack_require__(63);
__webpack_require__(31);
var ScoutService = (function () {
    function ScoutService(httpClient, _jsonp, datePipe, globals) {
        this.httpClient = httpClient;
        this._jsonp = _jsonp;
        this.datePipe = datePipe;
        this.globals = globals;
    }
    ScoutService.prototype.changeCaseStatus = function (scoutID, status) {
        // var url = 'provider_summary_core/select?q=' + encodeURIComponent(searchText) + '&rows=0&wt=json&indent=true&facet=true&facet.sort=index' +
        //     '&facet.field=PARTY_FIRST_NAME&facet.field=PROVIDER_SPECIALTY&facet.field=ZIP_CODE&facet.field=TAX_ID_NUMBER';
        var data = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n                            <soap:Body>\n                                <ChangeStatus xmlns=\"http://dg.scoutcms.com\">\n                                <ClientToken>ve{+5^y!}_Tr1>XBIFo2+u79</ClientToken>\n                                <ScoutID>" + scoutID + "</ScoutID>\n                                <Status>" + status + "</Status>\n                                <EmailAddress>ankur.akvaliya@plutustec.com</EmailAddress>\n                                </ChangeStatus>\n                            </soap:Body>\n                            </soap:Envelope>";
        return this.httpClient.post('ChangeStatus', data).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    ScoutService.prototype.importCase = function (objData) {
        var data = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n        <soap:Body>\n            <ImportCase xmlns=\"http://dg.scoutcms.com\">\n            <ClientToken>ve{+5^y!}_Tr1>XBIFo2+u79</ClientToken>\n            <ReferralDate></ReferralDate>\n            <CaseType>Ineligibility</CaseType>\n            <ReferralMethod>Portal</ReferralMethod>\n            <Department>Healthcare</Department>\n            <Claims>\n                <Claim>\n                    <ClaimNumber></ClaimNumber>\n                    <AdjusterName></AdjusterName>\n                    <AdjusterPhoneNumber></AdjusterPhoneNumber>\n                    <PolicyNumber></PolicyNumber>\n                    <PolicyCountryCode></PolicyCountryCode>\n                    <PolicyState></PolicyState>\n                    <LineOfBusiness></LineOfBusiness>\n                    <DateOfLoss></DateOfLoss>\n                    <DateOfNotice></DateOfNotice>\n                    <CauseOfLoss></CauseOfLoss>\n                    <LossDescription></LossDescription>\n                    <LossLocation>\n                        <Name></Name>\n                        <Line1></Line1>\n                        <Line2></Line2>\n                        <Line3></Line3>\n                        <City></City>\n                        <County></County>\n                        <State></State>\n                        <Country></Country>\n                        <Zip></Zip>\n                    </LossLocation>\n                    <ReferralReasons></ReferralReasons>\n                    <ReferralDate></ReferralDate>\n                    <Comments></Comments>\n                </Claim>                \n            </Claims>\n            <Parties>\n                <Party>\n                    <FirstName>" + objData.providerBasic.PARTY_FIRST_NAME + "</FirstName>\n                    <MiddleName></MiddleName>\n                    <LastName>" + objData.providerBasic.PARTY_LAST_NAME + "</LastName>\n                    <Aliases></Aliases>\n                    <DateOfBirth></DateOfBirth>\n                    <Gender></Gender>\n                    <SSN></SSN>\n                    <Emails></Emails>\n                    <Notes></Notes>\n                    <PhoneNumbers>\n                    </PhoneNumbers>\n                    <Addresses>\n                        <Address>" + objData.providerBasic.STREET_ADDRESS + "</Address>\n                        <Address>" + objData.providerBasic.CITY + "</Address>\n                        <Address>" + objData.providerBasic.STATE + "</Address>\n                        <Address>" + objData.providerBasic.ZIP_CODE + "</Address>\n                    </Addresses>\n                    <Roles>\n                    </Roles>\n                </Party>               \n            </Parties>\n            <Autos>            \n            </Autos>\n            <Exposures>\n            </Exposures>\n            <Assignments>\n                <Assignment>\n                    <AssignedToEmail></AssignedToEmail>\n                    <Role></Role>\n                    <DateCreated></DateCreated>\n                    <IsPrimary>false</IsPrimary>\n                </Assignment>              \n            </Assignments>\n            </ImportCase>\n            </soap:Body>\n        </soap:Envelope>";
        return this.httpClient.post('ImportCase', data).map(function (res) { return res.text(); } /*JSON.parse(xml2json(res.text(),'  '))*/)
            .catch(this.globals.extractError);
    };
    return ScoutService;
}());
ScoutService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_scout_client_1.HttpScoutClient !== "undefined" && http_scout_client_1.HttpScoutClient) === "function" && _a || Object, typeof (_b = typeof http_1.Jsonp !== "undefined" && http_1.Jsonp) === "function" && _b || Object, typeof (_c = typeof common_1.DatePipe !== "undefined" && common_1.DatePipe) === "function" && _c || Object, typeof (_d = typeof global_1.GlobalClass !== "undefined" && global_1.GlobalClass) === "function" && _d || Object])
], ScoutService);
exports.ScoutService = ScoutService;
var _a, _b, _c, _d;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/scout.service.js.map

/***/ }),

/***/ 845:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(37);
var common_1 = __webpack_require__(10);
var global_1 = __webpack_require__(103);
var http_client_1 = __webpack_require__(186);
__webpack_require__(63);
var TwitterService = (function () {
    function TwitterService(httpClient, _jsonp, datePipe, globals) {
        this.httpClient = httpClient;
        this._jsonp = _jsonp;
        this.datePipe = datePipe;
        this.globals = globals;
    }
    TwitterService.prototype.getAccessToken = function () {
        var headers = new http_1.Headers();
        headers.append("Content-Type", "application/x-www-form-urlencoded");
        headers.append("Authorization", 'Basic dUROSHFJWExaTWtsQWhQclplNW5VaVd5MTo2VUpaOTA0N2VrWkc4SmRjZGlkQ1luVWlLOTVNRGhQYlJEQ2VzenRRVGM1RWF5WnhaYQ==');
        var body = new http_1.URLSearchParams();
        body.set('grant_type', 'client_credentials');
        body.set('Username', 'uDNHqIXLZMklAhPrZe5nUiWy1');
        body.set('Password', '6UJZ9047ekZG8JdcdidCYnUiK95MDhPbRDCesztQTc5EayZxZa');
        // body.set('client_secret', '6UJZ9047ekZG8JdcdidCYnUiK95MDhPbRDCesztQTc5EayZxZa');
        // body.set('client_id', 'uDNHqIXLZMklAhPrZe5nUiWy1');
        var url = "https://api.twitter.com/oauth2/token";
        return this.httpClient.postWithHeaders(url, body, headers).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    TwitterService.prototype.getTwitterData = function () {
        var headers = new http_1.Headers();
        headers.append("Authorization", '{"Authorization":"Bearer AAAAAAAAAAAAAAAAAAAAAMxc0QAAAAAApSjDkbhDP15mbeF%2Fh5aHE%2FUc8Mc%3DAHJKTHej8vzN2kbxglvK1lmsQEv32nSWXlBi24Ll42jVBZh1RK"}');
        var url = "https://api.twitter.com/1.1/search/tweets.json?q=%23healthcarefraud&result_type=recent";
        return this.httpClient.getWithHeaders(url, headers).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    TwitterService.prototype.getDialogFeedData = function () {
        var url = "https://app.dialogfeed.com/en/snippet/social-wall-8835.json?api_key=14c61d37d4205650e762c2b219e33c23";
        return this.httpClient.get(url).map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    return TwitterService;
}());
TwitterService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_client_1.HttpClient !== "undefined" && http_client_1.HttpClient) === "function" && _a || Object, typeof (_b = typeof http_1.Jsonp !== "undefined" && http_1.Jsonp) === "function" && _b || Object, typeof (_c = typeof common_1.DatePipe !== "undefined" && common_1.DatePipe) === "function" && _c || Object, typeof (_d = typeof global_1.GlobalClass !== "undefined" && global_1.GlobalClass) === "function" && _d || Object])
], TwitterService);
exports.TwitterService = TwitterService;
var _a, _b, _c, _d;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/twitter.service.js.map

/***/ }),

/***/ 846:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
/**
* Allows the aside to be toggled via click.
*/
var AsideToggleDirective = (function () {
    function AsideToggleDirective() {
    }
    AsideToggleDirective.prototype.toggleOpen = function ($event) {
        $event.preventDefault();
        document.querySelector('body').classList.toggle('aside-menu-open');
    };
    return AsideToggleDirective;
}());
__decorate([
    core_1.HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AsideToggleDirective.prototype, "toggleOpen", null);
AsideToggleDirective = __decorate([
    core_1.Directive({
        selector: '.aside-toggle',
    }),
    __metadata("design:paramtypes", [])
], AsideToggleDirective);
exports.AsideToggleDirective = AsideToggleDirective;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/aside.directive.js.map

/***/ }),

/***/ 847:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(38);
__webpack_require__(206);
var BreadcrumbsComponent = (function () {
    function BreadcrumbsComponent(router, route) {
        this.router = router;
        this.route = route;
    }
    BreadcrumbsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events.filter(function (event) { return event instanceof router_1.NavigationEnd; }).subscribe(function (event) {
            _this.breadcrumbs = [];
            var currentRoute = _this.route.root, url = '';
            do {
                var childrenRoutes = currentRoute.children;
                currentRoute = null;
                childrenRoutes.forEach(function (route) {
                    if (route.outlet === 'primary') {
                        var routeSnapshot = route.snapshot;
                        url += '/' + routeSnapshot.url.map(function (segment) { return segment.path; }).join('/');
                        _this.breadcrumbs.push({
                            label: route.snapshot.data,
                            url: url
                        });
                        currentRoute = route;
                    }
                });
            } while (currentRoute);
        });
    };
    return BreadcrumbsComponent;
}());
BreadcrumbsComponent = __decorate([
    core_1.Component({
        selector: 'breadcrumbs',
        template: "\n    <template ngFor let-breadcrumb [ngForOf]=\"breadcrumbs\" let-last = last>\n        <li class=\"breadcrumb-item\" *ngIf=\"breadcrumb.label.title\" [ngClass]=\"{active: last}\">\n        <a *ngIf=\"!last\" [routerLink]=\"breadcrumb.url\">{{breadcrumb.label.title}}</a>\n        <span *ngIf=\"last\" [routerLink]=\"breadcrumb.url\">{{breadcrumb.label.title}}</span>\n    </template>"
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _a || Object, typeof (_b = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _b || Object])
], BreadcrumbsComponent);
exports.BreadcrumbsComponent = BreadcrumbsComponent;
var _a, _b;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/breadcrumb.component.js.map

/***/ }),

/***/ 848:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var NavDropdownDirective = (function () {
    function NavDropdownDirective() {
        this._open = false;
    }
    /**
    * Checks if the dropdown menu is open or not.
    */
    NavDropdownDirective.prototype.isOpen = function () { return this._open; };
    /**
    * Opens the dropdown menu.
    */
    NavDropdownDirective.prototype.open = function () {
        this._open = true;
    };
    /**
    * Closes the dropdown menu .
    */
    NavDropdownDirective.prototype.close = function () {
        this._open = false;
    };
    /**
    * Toggles the dropdown menu.
    */
    NavDropdownDirective.prototype.toggle = function () {
        if (this.isOpen()) {
            this.close();
        }
        else {
            this.open();
        }
    };
    return NavDropdownDirective;
}());
NavDropdownDirective = __decorate([
    core_1.Directive({
        selector: '.nav-dropdown',
        host: {
            '[class.open]': '_open',
        }
    })
], NavDropdownDirective);
exports.NavDropdownDirective = NavDropdownDirective;
/**
* Allows the dropdown to be toggled via click.
*/
var NavDropdownToggleDirective = (function () {
    function NavDropdownToggleDirective(dropdown) {
        this.dropdown = dropdown;
    }
    NavDropdownToggleDirective.prototype.toggleOpen = function ($event) {
        $event.preventDefault();
        this.dropdown.toggle();
    };
    return NavDropdownToggleDirective;
}());
__decorate([
    core_1.HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], NavDropdownToggleDirective.prototype, "toggleOpen", null);
NavDropdownToggleDirective = __decorate([
    core_1.Directive({
        selector: '.nav-dropdown-toggle',
    }),
    __metadata("design:paramtypes", [NavDropdownDirective])
], NavDropdownToggleDirective);
exports.NavDropdownToggleDirective = NavDropdownToggleDirective;
exports.NAV_DROPDOWN_DIRECTIVES = [NavDropdownDirective, NavDropdownToggleDirective];
// export const NGB_DROPDOWN_DIRECTIVES = [NgbDropdownToggle, NgbDropdown];
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/nav-dropdown.directive.js.map

/***/ }),

/***/ 849:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
/**
* Allows the sidebar to be toggled via click.
*/
var SidebarToggleDirective = (function () {
    function SidebarToggleDirective() {
    }
    //Check if element has class
    SidebarToggleDirective.prototype.hasClass = function (target, elementClassName) {
        return new RegExp('(\\s|^)' + elementClassName + '(\\s|$)').test(target.className);
    };
    //Toggle element class
    SidebarToggleDirective.prototype.toggleClass = function (elem, elementClassName) {
        var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
        if (this.hasClass(elem, elementClassName)) {
            while (newClass.indexOf(' ' + elementClassName + ' ') >= 0) {
                newClass = newClass.replace(' ' + elementClassName + ' ', ' ');
            }
            elem.className = newClass.replace(/^\s+|\s+$/g, '');
        }
        else {
            elem.className += ' ' + elementClassName;
        }
    };
    SidebarToggleDirective.prototype.toggleOpen = function ($event) {
        $event.preventDefault();
        var bodyClass = localStorage.getItem('body-class');
        if (this.hasClass(document.querySelector('body'), 'sidebar-off-canvas')) {
            this.toggleClass(document.querySelector('body'), 'sidebar-opened');
            this.toggleClass(document.querySelector('html'), 'sidebar-opened');
        }
        else if (this.hasClass(document.querySelector('body'), 'sidebar-nav') || bodyClass == 'sidebar-nav') {
            this.toggleClass(document.querySelector('body'), 'sidebar-nav');
            localStorage.setItem('body-class', 'sidebar-nav');
            if (bodyClass == 'sidebar-nav') {
                localStorage.clear();
            }
        }
    };
    return SidebarToggleDirective;
}());
__decorate([
    core_1.HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], SidebarToggleDirective.prototype, "toggleOpen", null);
SidebarToggleDirective = __decorate([
    core_1.Directive({
        selector: '.sidebar-toggle',
    }),
    __metadata("design:paramtypes", [])
], SidebarToggleDirective);
exports.SidebarToggleDirective = SidebarToggleDirective;
var MobileSidebarToggleDirective = (function () {
    function MobileSidebarToggleDirective() {
    }
    //Check if element has class
    MobileSidebarToggleDirective.prototype.hasClass = function (target, elementClassName) {
        return new RegExp('(\\s|^)' + elementClassName + '(\\s|$)').test(target.className);
    };
    //Toggle element class
    MobileSidebarToggleDirective.prototype.toggleClass = function (elem, elementClassName) {
        var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
        if (this.hasClass(elem, elementClassName)) {
            while (newClass.indexOf(' ' + elementClassName + ' ') >= 0) {
                newClass = newClass.replace(' ' + elementClassName + ' ', ' ');
            }
            elem.className = newClass.replace(/^\s+|\s+$/g, '');
        }
        else {
            elem.className += ' ' + elementClassName;
        }
    };
    MobileSidebarToggleDirective.prototype.toggleOpen = function ($event) {
        $event.preventDefault();
        this.toggleClass(document.querySelector('body'), 'mobile-open');
    };
    return MobileSidebarToggleDirective;
}());
__decorate([
    core_1.HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], MobileSidebarToggleDirective.prototype, "toggleOpen", null);
MobileSidebarToggleDirective = __decorate([
    core_1.Directive({
        selector: '[mobile-nav-toggle]',
    }),
    __metadata("design:paramtypes", [])
], MobileSidebarToggleDirective);
exports.MobileSidebarToggleDirective = MobileSidebarToggleDirective;
/**
* Allows the off-canvas sidebar to be closed via click.
*/
var SidebarOffCanvasCloseDirective = (function () {
    function SidebarOffCanvasCloseDirective() {
    }
    //Check if element has class
    SidebarOffCanvasCloseDirective.prototype.hasClass = function (target, elementClassName) {
        return new RegExp('(\\s|^)' + elementClassName + '(\\s|$)').test(target.className);
    };
    //Toggle element class
    SidebarOffCanvasCloseDirective.prototype.toggleClass = function (elem, elementClassName) {
        var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
        if (this.hasClass(elem, elementClassName)) {
            while (newClass.indexOf(' ' + elementClassName + ' ') >= 0) {
                newClass = newClass.replace(' ' + elementClassName + ' ', ' ');
            }
            elem.className = newClass.replace(/^\s+|\s+$/g, '');
        }
        else {
            elem.className += ' ' + elementClassName;
        }
    };
    SidebarOffCanvasCloseDirective.prototype.toggleOpen = function ($event) {
        $event.preventDefault();
        if (this.hasClass(document.querySelector('body'), 'sidebar-off-canvas')) {
            this.toggleClass(document.querySelector('body'), 'sidebar-opened');
        }
    };
    return SidebarOffCanvasCloseDirective;
}());
__decorate([
    core_1.HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], SidebarOffCanvasCloseDirective.prototype, "toggleOpen", null);
SidebarOffCanvasCloseDirective = __decorate([
    core_1.Directive({
        selector: '.sidebar-close',
    }),
    __metadata("design:paramtypes", [])
], SidebarOffCanvasCloseDirective);
exports.SidebarOffCanvasCloseDirective = SidebarOffCanvasCloseDirective;
exports.SIDEBAR_TOGGLE_DIRECTIVES = [SidebarToggleDirective, SidebarOffCanvasCloseDirective, MobileSidebarToggleDirective];
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/sidebar.directive.js.map

/***/ }),

/***/ 850:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/environment.js.map

/***/ }),

/***/ 87:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_client_1 = __webpack_require__(186);
var global_1 = __webpack_require__(103);
__webpack_require__(63);
__webpack_require__(323);
var ProviderOutcomeService = (function () {
    function ProviderOutcomeService(httpClient, globals) {
        this.httpClient = httpClient;
        this.globals = globals;
        //private ApiBaseUrl = 'http://localhost/pocwebexcel/';
        this.ApiBaseUrl = 'http://ai.apatics.net/apiv2/';
    }
    ProviderOutcomeService.prototype.getCityList = function () {
        return this.httpClient.get(this.ApiBaseUrl + 'filter.php?api_code=1')
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    ProviderOutcomeService.prototype.getZipCodeList = function (city) {
        return this.httpClient.get(this.ApiBaseUrl + 'filter.php?api_code=2&VENDOR_CITY=' + city)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    ProviderOutcomeService.prototype.getICDCodeList = function (zipcode, cityName) {
        return this.httpClient.get(this.ApiBaseUrl + 'filter.php?api_code=3&VENDOR_ZIP=' + zipcode + '&VENDOR_CITY=' + cityName)
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    ProviderOutcomeService.prototype.sendSMS = function (sendList, ProviderList) {
        return this.httpClient.post(this.ApiBaseUrl + 'sendSMS.php', { sendList: sendList, ProviderList: ProviderList })
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    ProviderOutcomeService.prototype.getSideFilter = function (filter) {
        return this.httpClient.post(this.ApiBaseUrl + 'filter_fields_api.php', { filter: filter })
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    ProviderOutcomeService.prototype.getAllSideBarFilterForClaimant = function (filter) {
        return this.httpClient.post(this.ApiBaseUrl + 'claimant_filter.php', { filter: filter })
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    ProviderOutcomeService.prototype.getAllSideBarFilterForProviderNetwork = function (filter) {
        return this.httpClient.post(this.ApiBaseUrl + 'network_provrder_filter.php', { filter: filter })
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    ProviderOutcomeService.prototype.getSideBarFilterMap = function (filter) {
        return this.httpClient.post(this.ApiBaseUrl + 'map_filter.php', { filter: filter })
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    ProviderOutcomeService.prototype.getAllProviderDataWithFilter = function (filter) {
        //let Finalfilter = JSON.stringify(filter);
        return this.httpClient.post(this.ApiBaseUrl + 'api.php', { filter: filter })
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    ProviderOutcomeService.prototype.getAllClaimantData = function (filter) {
        return this.httpClient.post(this.ApiBaseUrl + 'claimant.php', { filter: filter })
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    ProviderOutcomeService.prototype.getAllProviderNetworkData = function (filter) {
        return this.httpClient.post(this.ApiBaseUrl + 'nework_provider.php', { filter: filter })
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    ProviderOutcomeService.prototype.getAllMapData = function (filter) {
        return this.httpClient.post(this.ApiBaseUrl + 'map.php', { filter: filter })
            .map(this.globals.extractData)
            .catch(this.globals.extractError);
    };
    return ProviderOutcomeService;
}());
ProviderOutcomeService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_client_1.HttpClient !== "undefined" && http_client_1.HttpClient) === "function" && _a || Object, typeof (_b = typeof global_1.GlobalClass !== "undefined" && global_1.GlobalClass) === "function" && _b || Object])
], ProviderOutcomeService);
exports.ProviderOutcomeService = ProviderOutcomeService;
var _a, _b;
//# sourceMappingURL=D:/WorkSpace/Projects/AI_Apatics/Ai_Apatics_Angular_ServerCopy/src/provider-outcome.service.js.map

/***/ })

},[1329]);
//# sourceMappingURL=main.bundle.js.map